#!/usr/bin/env python3
""" Advent of Code Day 2
"""
from argparse import ArgumentParser, Namespace
from datetime import datetime, timedelta
from pathlib import Path
from typing import Dict, List, Tuple

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm

outcome_points: Dict[str, int] = {
    "win": 6,
    "lose": 0,
    "draw": 3,
}

play_points: Dict[str, int] = {
    "rock": 1,
    "paper": 2,
    "scissors": 3,
}

plays: Dict[str, str] = {
    "A": "rock",
    "B": "paper",
    "C": "scissors",
}

p1_plays: Dict[str, str] = {
    "X": "rock",
    "Y": "paper",
    "Z": "scissors",
}

p2_plays: Dict[str, str] = {
    "X": "lose",
    "Y": "draw",
    "Z": "win",
}

trumps: List[str] = ["rock", "paper", "scissors"]


def parse_args(arglist: List[str] = None) -> Namespace:
    """Parse and validate program arguments.

    Args:
      arglist (List[str], optional):
        Argument list, defaults to None (read from stdin)

    Returns:
      Namespace: Parsed and validated program arguments
    """
    argp: ArgumentParser = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def parse_input(input_path: Path) -> List[Tuple[str, str]]:
    strategy: List[Tuple[str, str]] = []

    with open(input_path, "r") as in_file:
        # Read input
        for line in in_file:
            play, response = line.strip().split()
            strategy.append((play, response))

    return strategy


def match(opponent: str, player: str) -> str:
    if opponent == player:
        return "draw"

    o: int = trumps.index(opponent)
    p: int = trumps.index(player)

    if o == (p + 1) % len(trumps):
        return "lose"

    return "win"


def do_p1(strategy: List[Tuple[str, str]]) -> int:
    # Run simulation
    score: int = 0

    for play, response in strategy:
        score += play_points[p1_plays[response]]
        score += outcome_points[match(plays[play], p1_plays[response])]

    return score


def do_p2(strategy: List[Tuple[str, str]]) -> int:
    # Run simulation
    score: int = 0

    for play, response in strategy:
        outcome: str = p2_plays[response]
        if outcome == "draw":
            response: str = plays[play]
        elif outcome == "win":
            # Respond with the trumping play
            response: str = trumps[(trumps.index(plays[play]) + 1) % len(trumps)]
        else:
            # Respond with the trumped play
            response: str = trumps[(trumps.index(plays[play]) - 1)]

        score += play_points[response]
        score += outcome_points[match(plays[play], response)]

    return score


def main(args: Namespace):
    data: List[Tuple[int, int]] = parse_input(args.input)

    # Part 1
    p1_start: datetime = datetime.now()
    p1: int = do_p1(data)
    p1_end: datetime = datetime.now()
    p1_duration: timedelta = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start: datetime = datetime.now()
    p2: int = do_p2(data)
    p2_end: datetime = datetime.now()
    p2_duration: timedelta = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args: Namespace = parse_args()
    main(args)
