#!/usr/bin/env python3
""" Advent of Code Day 14
"""
import logging
from argparse import ArgumentParser, Namespace
from copy import deepcopy
from datetime import datetime, timedelta
from pathlib import Path
from typing import Final, List, Set, Tuple

import pyray
from pyray import Font, Vector2

pyray.set_trace_log_level(logging.CRITICAL)

FONT_DIR: Final[Path] = Path(__file__).parent.parent / "fonts/Roboto/"
SAND_ORIGIN: Final[Tuple[int, int]] = (500, 0)
ROCK_SIZE: Final[float] = 5.0
ROCK_COLOR: Final[Tuple] = pyray.GRAY
SAND_COLOR: Final[Tuple] = pyray.YELLOW
ORIGIN_COLOR: Final[Tuple] = pyray.SKYBLUE
TEXT_COLOR: Final[Tuple] = pyray.PURPLE
SIM_FONT_SIZE: Final[int] = 32
END_FONT_SIZE: Final[int] = 256
GRAIN_COUNT_TEXT_BUFFER: Final[int] = 300
FONT_SPACING: Final[float] = 2.0


def parse_args(arglist: List[str] = None) -> Namespace:
    """Parse and validate program arguments.

    Args:
      arglist (List[str], optional):
        Argument list, defaults to None (read from stdin)

    Returns:
      Namespace: Parsed and validated program arguments
    """
    argp: ArgumentParser = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    argp.add_argument(
        "-v",
        "--visualize",
        help="visualize solution",
        action="store_true",
        default=False,
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def parse_input(input_path: Path) -> Set[Tuple[int, int]]:
    rocks: List[Tuple[int, int]] = []
    with open(input_path, "r") as in_file:
        # Read input
        for line in in_file:
            points: List[Tuple] = [
                tuple(map(int, p.split(","))) for p in line.strip().split(" -> ")
            ]
            while len(points) > 1:
                x, y = points.pop(0)
                n_x, n_y = points[0]

                if x == n_x:
                    rocks += [(x, yy) for yy in range(min(n_y, y), max(n_y, y) + 1)]
                elif y == n_y:
                    rocks += [(xx, y) for xx in range(min(n_x, x), max(n_x, x) + 1)]

    return set(rocks)


def do_p2_visual(
    rocks: Set[Tuple[int, int]],
    window_height: int = 1000,
    window_width: int = 1000,
    update_threshold: float = 0.00001,
) -> int:
    floor_start: int = max([y for x, y in rocks]) + 2
    grains: int = 0
    sand: Set[Tuple[int, int]] = set()

    sand_x, sand_y = SAND_ORIGIN

    acc: float = 0.0
    complete: bool = False

    pyray.init_window(window_width, window_height, "Advent of Code 2022/14 Part 2")
    pyray.set_target_fps(60)

    center_x: int = window_width // 2
    center_y: int = window_height // 2

    rock_center_x: int = (
        max(x * ROCK_SIZE for x, y in rocks)
        - (max(x * ROCK_SIZE for x, y in rocks) - min(x * ROCK_SIZE for x, y in rocks))
        // 2
    )
    rock_center_y: int = (
        max(y * ROCK_SIZE for x, y in rocks)
        - (max(y * ROCK_SIZE for x, y in rocks) - min(y * ROCK_SIZE for x, y in rocks))
        // 2
    )

    x_offset: int = rock_center_x - center_x

    end_font: Font = pyray.load_font(str(FONT_DIR / "Roboto-Regular.ttf"))
    end_font.baseSize = 128
    sim_font: Font = pyray.load_font(str(FONT_DIR / "Roboto-Regular.ttf"))
    sim_font.baseSize = 12

    while not pyray.window_should_close():
        # Render model
        pyray.begin_drawing()
        pyray.clear_background(pyray.BLACK)
        # Render rocks
        for x, y in rocks:
            x = (x * ROCK_SIZE) - x_offset
            y = y * ROCK_SIZE

            pyray.draw_rectangle(
                int(x - ROCK_SIZE // 2),
                int(y - ROCK_SIZE // 2),
                int(ROCK_SIZE),
                int(ROCK_SIZE),
                ROCK_COLOR,
            )
        # Render origin
        pyray.draw_rectangle(
            int(SAND_ORIGIN[0] * ROCK_SIZE - x_offset - ROCK_SIZE // 2),
            int(SAND_ORIGIN[1] * ROCK_SIZE - ROCK_SIZE // 2),
            int(ROCK_SIZE),
            int(ROCK_SIZE),
            ORIGIN_COLOR,
        )

        # Render sand
        for x, y in sand:
            x = (x * ROCK_SIZE) - x_offset
            y = y * ROCK_SIZE

            pyray.draw_rectangle(
                int(x - ROCK_SIZE // 2),
                int(y - ROCK_SIZE // 2),
                int(ROCK_SIZE),
                int(ROCK_SIZE),
                SAND_COLOR,
            )
        # Render falling grain
        pyray.draw_rectangle(
            int((sand_x * ROCK_SIZE) - x_offset - ROCK_SIZE // 2),
            int(sand_y * ROCK_SIZE - ROCK_SIZE // 2),
            int(ROCK_SIZE),
            int(ROCK_SIZE),
            SAND_COLOR,
        )

        size: Vector2 = pyray.measure_text_ex(
            sim_font, f"Grains: {grains}", SIM_FONT_SIZE, FONT_SPACING
        )
        pyray.draw_text_ex(
            sim_font,
            f"Grains: {grains}",
            pyray.Vector2(
                int(window_width - GRAIN_COUNT_TEXT_BUFFER),
                int(size.y / 2),
            ),
            SIM_FONT_SIZE,
            FONT_SPACING,
            TEXT_COLOR,
        )
        if complete:
            size: Vector2 = pyray.measure_text_ex(
                end_font, "Complete", END_FONT_SIZE, FONT_SPACING
            )
            pyray.draw_text_ex(
                end_font,
                "Complete",
                pyray.Vector2(
                    int(center_x - size.x / 2),
                    int(center_y - size.y / 2),
                ),
                END_FONT_SIZE,
                FONT_SPACING,
                TEXT_COLOR,
            )

        pyray.end_drawing()

        # Update model if we're not done
        if complete:
            continue

        acc += pyray.get_frame_time()
        if acc > update_threshold:
            acc = 0
            if (sand_x, sand_y + 1) not in rocks and sand_y + 1 < floor_start:
                sand_y += 1
            elif (sand_x - 1, sand_y + 1) not in rocks and sand_y + 1 < floor_start:
                sand_x -= 1
                sand_y += 1
            elif (sand_x + 1, sand_y + 1) not in rocks and sand_y + 1 < floor_start:
                sand_x += 1
                sand_y += 1
            else:
                grains += 1
                rocks.add((sand_x, sand_y))
                sand.add((sand_x, sand_y))
                sand_x, sand_y = SAND_ORIGIN
            complete = SAND_ORIGIN in rocks

    return grains


def do_p2(rocks: Set[Tuple[int, int]]) -> int:
    floor_start: int = max([y for x, y in rocks]) + 2
    running: bool = True
    grains: int = 0

    sand_x, sand_y = SAND_ORIGIN

    while running:
        if (sand_x, sand_y + 1) not in rocks and sand_y + 1 < floor_start:
            sand_y += 1
        elif (sand_x - 1, sand_y + 1) not in rocks and sand_y + 1 < floor_start:
            sand_x -= 1
            sand_y += 1
        elif (sand_x + 1, sand_y + 1) not in rocks and sand_y + 1 < floor_start:
            sand_x += 1
            sand_y += 1
        else:
            grains += 1
            rocks.add((sand_x, sand_y))
            sand_x, sand_y = SAND_ORIGIN
        running = SAND_ORIGIN not in rocks

    return grains


def do_p1(rocks: Set[Tuple[int, int]]) -> int:
    void_start: int = max([y for x, y in rocks]) + 1
    running: bool = True
    grains: int = 0

    sand_x, sand_y = SAND_ORIGIN

    while running:
        if (sand_x, sand_y + 1) not in rocks:
            sand_y += 1
        elif (sand_x - 1, sand_y + 1) not in rocks:
            sand_x -= 1
            sand_y += 1
        elif (sand_x + 1, sand_y + 1) not in rocks:
            sand_x += 1
            sand_y += 1
        else:
            grains += 1
            rocks.add((sand_x, sand_y))
            sand_x, sand_y = SAND_ORIGIN
        running = sand_y < void_start

    return grains


def main(args: Namespace):
    data: List[int] = parse_input(args.input)

    # Part 1
    p1_start: datetime = datetime.now()
    p1: int = do_p1(deepcopy(data))
    p1_end: datetime = datetime.now()
    p1_duration: timedelta = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start: datetime = datetime.now()
    p2: int = do_p2(deepcopy(data))
    p2_end: datetime = datetime.now()
    p2_duration: timedelta = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)

    if args.visualize:
        do_p2_visual(deepcopy(data))


if __name__ == "__main__":
    args: Namespace = parse_args()
    main(args)
