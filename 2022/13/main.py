#!/usr/bin/env python3
""" Advent of Code Day 13
"""
import json
from argparse import ArgumentParser, Namespace
from copy import deepcopy
from datetime import datetime, timedelta
from functools import cmp_to_key
from pathlib import Path
from typing import Any, List, Tuple


def parse_args(arglist: List[str] = None) -> Namespace:
    """Parse and validate program arguments.

    Args:
      arglist (List[str], optional):
        Argument list, defaults to None (read from stdin)

    Returns:
      Namespace: Parsed and validated program arguments
    """
    argp: ArgumentParser = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def parse_input(input_path: Path) -> List[Tuple[List[int], List[int]]]:
    packets: List[Tuple[List[int], List[int]]] = []

    with open(input_path, "r") as in_file:
        # Read input
        for pair in in_file.read().strip().split("\n\n"):
            left, right = pair.split("\n")
            packets.append((json.loads(left), json.loads(right)))

    return packets


def cmp(left: Any, right: Any, indent: int = 0) -> int:
    """
    If both values are integers, the lower integer should come first. If the
    left integer is lower than the right integer, the inputs are in the right
    order. If the left integer is higher than the right integer, the inputs
    are not in the right order. Otherwise, the inputs are the same integer;
    continue checking the next part of the input.
    """
    if isinstance(left, int) and isinstance(right, int):
        if left < right:
            return 1
        if right < left:
            return -1
        return 0

    """
    If both values are lists, compare the first value of each list, then the
    second value, and so on. If the left list runs out of items first, the
    inputs are in the right order. If the right list runs out of items first,
    the inputs are not in the right order. If the lists are the same length and
    no comparison makes a decision about the order, continue checking the next
    part of the input.
    """
    if isinstance(left, list) and isinstance(right, list):
        ll = deepcopy(left)
        rr = deepcopy(right)
        while ll and rr:
            li = ll.pop(0)
            ri = rr.pop(0)

            s: int = cmp(li, ri, indent=indent + 2)

            if s != 0:
                return s

        if len(ll) == 0 and len(rr) != 0:
            return 1

        if len(ll) != 0 and len(rr) == 0:
            return -1

    """
    If exactly one value is an integer, convert the integer to a list which
    contains that integer as its only value, then retry the comparison. For
    example, if comparing [0,0,0] and 2, convert the right value to [2] (a
    list containing 2); the result is then found by instead comparing [0,0,0]
    and [2].
    """
    if (
        isinstance(left, int)
        and not isinstance(right, int)
        or not isinstance(left, int)
        and isinstance(right, int)
    ):
        left = [left] if not isinstance(left, list) else left
        right = [right] if not isinstance(right, list) else right
        return cmp(left, right, indent=indent + 2)

    return 0


def do_p1(data: List[Tuple[List[int], List[int]]]) -> int:
    solution: List[bool] = [cmp(l, r) for l, r in data]
    return sum([idx + 1 if s == 1 else 0 for idx, s in enumerate(solution)])


def do_p2(data: List[Tuple[List[int], List[int]]]) -> int:
    flat_data: List[List[int]] = []
    for left, right in data:
        flat_data.append(left)
        flat_data.append(right)
    flat_data.append([[6]])
    flat_data.append([[2]])

    flat_data = sorted(flat_data, key=cmp_to_key(cmp), reverse=True)

    return (flat_data.index([[6]]) + 1) * (flat_data.index([[2]]) + 1)


def main(args: Namespace):
    data: List[Tuple[List[int], List[int]]] = parse_input(args.input)

    # Part 1
    p1_start: datetime = datetime.now()
    p1: int = do_p1(deepcopy(data))
    p1_end: datetime = datetime.now()
    p1_duration: timedelta = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start: datetime = datetime.now()
    p2: int = do_p2(deepcopy(data))
    p2_end: datetime = datetime.now()
    p2_duration: timedelta = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args: Namespace = parse_args()
    main(args)
