#!/usr/bin/env python3
""" Advent of Code Day 9
"""
import os
from argparse import ArgumentParser, Namespace
from datetime import datetime, timedelta
from pathlib import Path
from typing import List, Optional, Tuple

# fmt: off
os.environ["PYGAME_HIDE_SUPPORT_PROMPT"] = "yes"
import pygame
from pygame import Rect, Surface
from pygame.event import Event
from pygame.font import Font
from pygame.time import Clock

# fmt: on

font_dir: Path = Path(__file__).resolve().parent.parent.parent / "fonts"


class Visualizer:
    def __init__(self, width: int, height: int, box_size: int = 18):
        self._width: int = width
        self._height: int = height
        self._surface: Optional[Surface] = None
        self._running: bool = False
        self._states: Optional[List[List[Tuple[int, int]]]] = None
        self._box_size: int = box_size
        self._font: Optional[Font] = None
        self._box_border_radius: float = 10
        self._end_font: Optional[Font] = None
        self._pause: bool = True
        self._acc: float = 0.0
        self._threshold: float = 0.01
        self._clock: Clock = Clock()
        self._cell_size: int = 20
        self._last_state: Optional[List[Tuple[int, int]]] = None

    def _init(self):
        pygame.init()
        self._surface = pygame.display.set_mode(
            (self._width, self._height), pygame.HWSURFACE | pygame.DOUBLEBUF
        )
        pygame.display.set_caption("Advent of Code 2022/09")

        pygame.font.init()
        pygame.font.get_init()
        self._font = pygame.font.Font(font_dir / "Roboto" / "Roboto-Regular.ttf", 14)
        self._end_font = pygame.font.Font(
            font_dir / "Roboto" / "Roboto-Regular.ttf", 64
        )

    def run(
        self, states: List[List[Tuple[int, int]]],
    ):
        self._init()
        self._states = states

        self._running = True

        while self._running:
            for event in pygame.event.get():
                self._handle_event(event)
            self._update()
            self._render()

        self._cleanup()

    def _render(self):
        self._surface.fill((0, 0, 0))

        center_offset_x: float = self._width / 2
        center_offset_y: float = self._height / 2

        done: bool = all(len(x) == 0 for x in self._states)

        if done:
            current_state: List[Tuple[int, int]] = self._last_state
        else:
            current_state: List[Tuple[int, int]] = [s[0] for s in self._states]

        pygame.draw.circle(
            self._surface, (255, 255, 0), (center_offset_x, center_offset_y), 5
        )

        for index, (c_x, c_y) in list(enumerate(current_state))[::-1]:
            x: int = c_x * self._cell_size
            y: int = c_y * self._cell_size
            center_x: int = x - (self._box_size / 2) + center_offset_x
            center_y: int = y - (self._box_size / 2) + center_offset_y
            pygame.draw.rect(
                self._surface,
                (255, 0, 255),
                (center_x, center_y, self._box_size, self._box_size),
                0,
                border_radius=self._box_border_radius,
            )
            label_text: Surface = self._font.render(f"{index}", True, (255, 255, 255))
            label_rect: Rect = label_text.get_rect()
            label_rect.centerx = center_x + label_rect.size[0] / 2
            label_rect.centery = center_y + label_rect.size[1] / 2
            self._surface.blit(label_text, label_rect)

        progress_text: Surface = self._font.render(
            f"{len(self._states[0])} steps left", True, (255, 255, 255)
        )
        progress_rect: Rect = progress_text.get_rect()
        progress_rect.centerx = progress_rect.size[0] // 2
        progress_rect.centery = progress_rect.size[1] // 2
        self._surface.blit(progress_text, progress_rect)

        head_pos_text: Surface = self._font.render(
            f"Head position: {current_state[0]}", True, (255, 255, 255),
        )
        head_pos_rect: Rect = head_pos_text.get_rect()
        head_pos_rect.centerx = head_pos_rect.size[0] // 2
        head_pos_rect.centery = progress_rect.size[1] + head_pos_rect.size[1] // 2
        self._surface.blit(head_pos_text, head_pos_rect)

        tail_pos_text: Surface = self._font.render(
            f"Tail position: {current_state[-1]}", True, (255, 255, 255),
        )
        tail_pos_rect: Rect = tail_pos_text.get_rect()
        tail_pos_rect.centerx = tail_pos_rect.size[0] // 2
        tail_pos_rect.centery = (
            progress_rect.size[1] + head_pos_rect.size[1] + tail_pos_rect.size[1] // 2
        )
        self._surface.blit(tail_pos_text, tail_pos_rect)

        if done:
            finish_text: Surface = self._end_font.render("Complete", True, (255, 0, 0))
            finish_text_rect: Rect = finish_text.get_rect()
            finish_text_rect.centerx = center_offset_x
            finish_text_rect.centery = center_offset_y
            self._surface.blit(finish_text, finish_text_rect)

        pygame.display.update()

    def _update(self):
        self._clock.tick()
        if self._pause:
            return

        if self._acc < self._threshold:
            self._acc += self._clock.get_time() / 1000
            return

        self._acc = 0

        if all(len(x) == 0 for x in self._states):
            return

        self._last_state = []
        for state in self._states:
            self._last_state.append(state.pop(0))

    def _cleanup(self):
        pygame.quit()

    def _handle_event(self, event: Event):
        if event.type == pygame.QUIT:
            self._running = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RETURN:
                self._pause = False
            if event.key == pygame.K_ESCAPE:
                self._running = False
                self._cleanup()


def parse_args(arglist: List[str] = None) -> Namespace:
    """Parse and validate program arguments.

    Args:
      arglist (List[str], optional):
        Argument list, defaults to None (read from stdin)

    Returns:
      Namespace: Parsed and validated program arguments
    """
    argp: ArgumentParser = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    argp.add_argument(
        "-v",
        "--visualize",
        help="run visualization",
        action="store_true",
        default=False,
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def parse_input(input_path: Path) -> List[Tuple[int, int, int]]:
    steps: List[Tuple[int, int, int]] = []

    with open(input_path, "r") as in_file:
        # Read input
        for line in in_file:
            direction, _, magnitude = line.strip().partition(" ")
            dx: int = 1 if direction == "R" else -1 if direction == "L" else 0
            dy: int = 1 if direction == "U" else -1 if direction == "D" else 0

            steps.append((dx, dy, int(magnitude)))

    return steps


def run_snake(steps: List[Tuple[int, int, int]], length: int) -> int:
    segment_steps: List[List[Tuple[int, int]]] = [[] for _ in range(length)]
    segments: List[Tuple[int, int]] = [(0, 0) for _ in range(length)]

    for dx, dy, dist in steps:
        c: int = 0
        while c != dist:
            # Move head
            h_x, h_y = segments[0]
            segments[0] = (h_x + dx, h_y + dy)

            for index, (s_x, s_y) in enumerate(segments[1:]):
                index += 1

                # Check segments and move if needed
                p_x, p_y = segments[index - 1]
                dist_x: int = abs(p_x - s_x)
                dist_y: int = abs(p_y - s_y)

                if dist_x > 1 or dist_y > 1:
                    # Move tail
                    # Three cases
                    if dist_x == 0:  # Case 1: no x difference (should move up or down)
                        s_y += 1 if p_y > s_y else -1
                    elif (
                        dist_y == 0
                    ):  # Case 2: no y difference (should move left or right)
                        s_x += 1 if p_x > s_x else -1
                    else:  # Case 3: both x and y differences
                        s_x += 1 if p_x > s_x else -1
                        s_y += 1 if p_y > s_y else -1

                    segments[index] = (s_x, s_y)

            for index, (x, y) in enumerate(segments):
                segment_steps[index].append((x, y))

            c += 1

    return segment_steps


def main(args: Namespace):
    data: List[Tuple[int, int, int]] = parse_input(args.input)

    # Part 1
    p1_start: datetime = datetime.now()
    p1_steps: List[List[Tuple[int, int]]] = run_snake(data, 2)
    p1: int = len(set(p1_steps[-1]))
    p1_end: datetime = datetime.now()
    p1_duration: timedelta = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start: datetime = datetime.now()
    p2_steps: List[List[Tuple[int, int]]] = run_snake(data, 10)
    p2: int = len(set(p2_steps[-1]))
    p2_end: datetime = datetime.now()
    p2_duration: timedelta = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)

    if args.visualize:
        visualizer: Visualizer = Visualizer(900, 900)
        visualizer.run(p2_steps)


if __name__ == "__main__":
    args: Namespace = parse_args()
    main(args)
