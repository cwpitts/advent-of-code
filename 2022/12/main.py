#!/usr/bin/env python3
""" Advent of Code Day 12
"""
import heapq
import string
from argparse import ArgumentParser, Namespace
from datetime import datetime, timedelta
from pathlib import Path
from typing import Dict, List, Optional, Set, Tuple

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.pyplot import Axes, Figure
from mpl_toolkits.mplot3d import Axes3D
from rich.progress import Progress

from visualizer import Visualizer


def parse_args(arglist: List[str] = None) -> Namespace:
    """Parse and validate program arguments.

    Args:
      arglist (List[str], optional):
        Argument list, defaults to None (read from stdin)

    Returns:
      Namespace: Parsed and validated program arguments
    """
    argp: ArgumentParser = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    argp.add_argument(
        "-v",
        "--visualize",
        help="run visualization",
        action="store_true",
        default=False,
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def visualize(height_map: np.ndarray, start: Tuple[int, int], dest: Tuple[int, int]):
    path: List[Tuple[int, int]] = find_path(height_map, start, dest)

    fig: Figure
    ax1: Axes
    ax2: Axes
    fig, (ax1, ax2) = plt.subplots(ncols=2)
    ax1.imshow(height_map)

    display_map: np.ndarray = np.zeros(height_map.shape)
    for item in path:
        display_map[item] = 1

    ax2.imshow(display_map, cmap="gray")

    ax1.set_title("Height map")
    ax2.set_title(f"Path ({len(path) - 1} steps)")
    fig.suptitle("Advent of Code 2022/12 Part 1 solution")
    fig.set_size_inches(5, 2)

    fig.savefig("p1-solution.png", bbox_inches="tight")

    plt.close("all")

    map_x, map_y = np.meshgrid(
        np.arange(height_map.shape[1]), np.arange(height_map.shape[0])
    )
    path_z: np.ndarray = np.array([height_map[tuple(p)] for p in path]) + 1
    path_y: np.array = np.array([p[0] for p in path])
    path_x: np.array = np.array([p[1] for p in path])

    fig: Figure = plt.figure()
    ax: Axes3D = plt.axes(projection="3d")
    ax.plot_surface(map_x, map_y, height_map, linewidth=1, edgecolor="blue")
    ax.plot3D(path_x, path_y, path_z, "red", linewidth=1)
    ax.set_title("Advent of Code 2022/12 Part 1 solution")

    fig.savefig("p1-solution-3d.png", bbox_inches="tight")

    plt.close("all")

    visualizer: Visualizer = Visualizer(height_map, path)
    visualizer.run()


def get_neighbors(
    point: Tuple[int, int], shape: Tuple[int, int]
) -> List[Tuple[int, int]]:
    neighbors: List[Tuple[int, int]] = []

    y, x = point
    y_bound, x_bound = shape

    for dy, dx in ((0, -1), (0, 1), (-1, 0), (1, 0)):
        n_y: int = y + dy
        if n_y < 0 or n_y == y_bound:
            continue
        n_x: int = x + dx
        if n_x < 0 or n_x == x_bound:
            continue

        neighbors.append((n_y, n_x))

    return neighbors


def find_path(height_map: np.ndarray, start: Tuple[int, int], dest: Tuple[int, int]):
    pq: List[Tuple[int, Tuple[int, int]]] = []

    dist: np.ndarray = np.full(height_map.shape, np.inf)
    dist[start] = 0
    prev: Dict[Tuple[int, int], Tuple[int, int]] = {}
    visited: Set[Tuple[int, int]] = set()

    x, y = np.meshgrid(np.arange(height_map.shape[1]), np.arange(height_map.shape[0]))
    heapq.heappush(pq, (0, start))

    while len(pq) > 0:
        u: Tuple[int, int]
        _, u = heapq.heappop(pq)

        if u in visited:
            continue

        visited.add(u)
        d: int = dist[u]

        for neighbor in filter(
            lambda n: height_map[n] - height_map[u] <= 1,
            get_neighbors(u, height_map.shape),
        ):
            if neighbor in visited:
                continue

            n_dist: int = d + 1
            if n_dist < dist[neighbor]:
                heapq.heappush(pq, (n_dist, neighbor))
                dist[neighbor] = n_dist
                prev[neighbor] = u

    path: List[Tuple[int, int]] = []
    c: Optional[Tuple[int, int]] = dest
    while c is not None:
        path.append(c)
        c = prev.get(c, None)

    return path


def do_p1(height_map: np.ndarray, start: Tuple[int, int], dest: Tuple[int, int]) -> int:
    path: List[Tuple[int, int]] = find_path(height_map, start, dest)
    return len(path) - 1


def do_p2(height_map: np.ndarray, dest: Tuple[int, int]) -> int:
    paths: List[int] = []
    origins: List[Tuple[int, int]] = list(zip(*np.where(height_map == 0)))
    with Progress() as progress:
        path_task = progress.add_task("Searching paths")
        for y, x in origins:
            path: List[Tuple[int, int]] = find_path(height_map, (y, x), dest)
            if (y, x) in path and dest in path:
                paths.append(len(path) - 1)
            progress.advance(path_task, 100 / len(origins))

    return min(paths)


def parse_input(
    input_path: Path,
) -> Tuple[np.ndarray, Tuple[int, int], Tuple[int, int]]:
    with open(input_path, "r") as in_file:
        # Read input
        start: Tuple[int, int]
        dest: Tuple[int, int]
        height_map: List[List[str]] = []
        for row, line in enumerate(in_file.readlines()):
            height_map.append([])
            for col, c in enumerate(line.strip()):
                if c == "S":
                    start = (row, col)
                    height_map[row].append(string.ascii_lowercase.index("a"))
                elif c == "E":
                    dest = (row, col)
                    height_map[row].append(string.ascii_lowercase.index("z"))
                else:
                    height_map[row].append(string.ascii_lowercase.index(c))

    return np.array(height_map), start, dest


def main(args: Namespace):
    height_map, start, end = parse_input(args.input)

    # Part 1
    p1_start: datetime = datetime.now()
    p1: int = do_p1(height_map, start, end)
    p1_end: datetime = datetime.now()
    p1_duration: timedelta = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start: datetime = datetime.now()
    p2: int = do_p2(height_map, end)
    p2_end: datetime = datetime.now()
    p2_duration: timedelta = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)

    if args.visualize:
        visualize(height_map, start, end)


if __name__ == "__main__":
    args: Namespace = parse_args()
    main(args)
