import logging
from typing import Final, List, Tuple

import numpy as np
import pyray
from pyray import Camera3D, Vector3

pyray.set_trace_log_level(logging.CRITICAL)


class Visualizer:
    def __init__(
        self,
        height_map: np.ndarray,
        path: List[Tuple[int, int]],
        threshold: float = 0.02,
        screen_height: int = 800,
        screen_width: int = 600,
    ):
        self._height_map: np.ndarray = height_map.copy()
        self._threshold: float = threshold
        self._screen_height: int = screen_height
        self._screen_width: int = screen_width
        self._path: List[Tuple[int, int]] = path[::-1]
        self._path_index: int = -1
        self._acc: float = 0
        self._position_offset: Final[float] = 1.0
        self._camera_z_position_offset: Final[float] = 30.0
        self._camera_y_position_offset: Final[float] = 10.0
        self._cube_size: Final[float] = 1.0

        pyray.init_window(
            self._screen_height, self._screen_width, "Advent of Code 2022/12"
        )

        # Initialize camera
        self._camera_target: Vector3 = Vector3(0, 0, 0)
        self._camera_pos: Vector3 = Vector3(0, 0, 0)
        self._camera_up: Vector3 = Vector3(0, 1, 0)
        self._camera_fov: float = 45.0
        self._camera: Camera3D = Camera3D(
            self._camera_pos, self._camera_target, self._camera_up, self._camera_fov
        )
        pyray.set_camera_mode(self._camera, pyray.CAMERA_FREE)

        self._update_position()

    def run(self):
        while not pyray.window_should_close():
            self._render()
            self._update()

    def _render(self):
        pyray.begin_drawing()

        pyray.clear_background(pyray.WHITE)

        pyray.begin_mode_3d(self._camera)

        for (y, x), z in np.ndenumerate(self._height_map):
            for zz in np.arange(z + 1):
                pyray.draw_cube(
                    Vector3(x, y, zz),
                    self._cube_size,
                    self._cube_size,
                    self._cube_size,
                    pyray.BLUE,
                )
                pyray.draw_cube_wires(
                    Vector3(x, y, zz),
                    self._cube_size,
                    self._cube_size,
                    self._cube_size,
                    pyray.WHITE,
                )

        cube_color: Tuple = (
            pyray.RED if self._path_index < len(self._path) else pyray.GREEN
        )
        pyray.draw_cube(
            self._position,
            self._cube_size,
            self._cube_size,
            self._cube_size,
            cube_color,
        )
        pyray.draw_cube_wires(
            self._position,
            self._cube_size,
            self._cube_size,
            self._cube_size,
            pyray.WHITE,
        )

        pyray.end_mode_3d()

        pyray.end_drawing()

    def _update(self):
        delta: float = pyray.get_frame_time()

        self._acc += delta

        if self._acc > self._threshold:
            self._acc = 0
            self._update_position()

    def _update_position(self):
        self._path_index += 1
        self._path_index = min(self._path_index, len(self._path))

        if self._path_index < len(self._path):
            y, x = self._path[self._path_index]

            self._position: Vector3 = Vector3(
                x, y, self._height_map[y, x] + self._position_offset
            )
            self._camera.target = Vector3(x, y, self._height_map[y, x])
            self._camera.position = Vector3(
                x,
                y - self._camera_y_position_offset,
                self._height_map[y, x] + self._camera_z_position_offset,
            )
