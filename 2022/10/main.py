#!/usr/bin/env python3
""" Advent of Code Day 10
"""
from abc import ABC, abstractmethod
from argparse import ArgumentParser, Namespace
from copy import deepcopy
from datetime import datetime, timedelta
from pathlib import Path
from typing import Final, List, Tuple

import matplotlib.pyplot as plt
import numpy as np
import pytesseract
import scipy.ndimage as ndi
import skimage.transform as skt
from PIL import Image
from rich.progress import Progress

INDICES: Final[np.ndarray] = np.array([20, 60, 100, 140, 180, 220])
CRT_SHAPE: Final[Tuple[int, int]] = (6, 40)
CRT_UPSCALE: Final[int] = 4
CRT_BORDER: Final[int] = 50


class OpCode(ABC):
    def __init__(self):
        pass

    def clock(self) -> int:
        return self.CLOCK

    @abstractmethod
    def execute(self, register: int) -> int:
        raise NotImplementedError


class Addx(OpCode):
    CLOCK: int = 2

    def __init__(self, operand: int):
        self._operand = operand
        super().__init__()

    def execute(self, register: int) -> int:
        return register + self._operand

    def __repr__(self) -> str:
        return f"Addx({self._operand})"


class Noop(OpCode):
    CLOCK: int = 0

    def __init__(self):
        super().__init__()

    def execute(self, register: int) -> int:
        return register

    def __repr__(self) -> str:
        return "Noop"


class VM:
    def __init__(self, prog: List[OpCode]):
        self._prog: List[OpCode] = deepcopy(prog)
        self._register: int = 1
        self._ip: int = 0
        self._register_list: List[int] = []

    def run(self, limit: int):
        steps: int = 0
        while steps < limit:
            opcode: OpCode = self._prog[self._ip]

            for _ in range(1, opcode.clock()):
                self._register_list.append(self._register)
            self._register_list.append(self._register)

            self._register = opcode.execute(self._register)

            self._ip += 1
            self._ip %= len(self._prog)
            steps += 1

    def register_list(self) -> List[int]:
        return self._register_list


def parse_args(arglist: List[str] = None) -> Namespace:
    """Parse and validate program arguments.

    Args:
      arglist (List[str], optional):
        Argument list, defaults to None (read from stdin)

    Returns:
      Namespace: Parsed and validated program arguments
    """
    argp: ArgumentParser = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def parse_input(input_path: Path) -> List[OpCode]:
    prog: List[OpCode] = []

    with open(input_path, "r") as in_file:
        # Read input
        for line in in_file:
            if line.startswith("noop"):
                prog.append(Noop())
            elif line.startswith("addx"):
                _, _, operand = line.strip().partition(" ")
                prog.append(Addx(int(operand)))

    return prog


def do_p1(prog: List[OpCode]) -> int:
    vm: VM = VM(prog)
    vm.run(200)

    register_list: np.ndarray = np.array(vm.register_list())
    cycles: np.ndarray = np.arange(register_list.shape[0]) + 1
    signal: np.ndarray = register_list * cycles

    return signal[INDICES - 1].sum()


def do_p2(prog: List[OpCode]) -> str:
    crt: np.ndarray = np.zeros(CRT_SHAPE)

    vm: VM = VM(prog)
    vm.run(len(prog))

    register_list: np.ndarray = np.array(vm.register_list())
    cycles: np.ndarray = np.arange(register_list.shape[0]) + 1

    for c, r in zip(
        cycles[: crt.ravel().shape[0]] - 1, register_list[: crt.ravel().shape[0]]
    ):
        if c % CRT_SHAPE[1] in (r - 1, r, r + 1):
            crt.ravel()[c] = 1

    blank_img: np.ndarray = np.ones(
        (crt.shape[0] + CRT_BORDER * 2, crt.shape[1] + CRT_BORDER * 2)
    )
    blank_img[CRT_BORDER:-CRT_BORDER, CRT_BORDER:-CRT_BORDER] = (
        ~crt.astype(bool)
    ).astype(int)
    crt_img: np.ndarray = skt.resize(
        blank_img,
        (blank_img.shape[0] * CRT_UPSCALE, blank_img.shape[1] * CRT_UPSCALE),
        anti_aliasing=True,
    )

    fig, ax = plt.subplots()
    ax.imshow(crt_img, cmap="gray")
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    fig.savefig("p2.png")
    plt.close("all")

    img: np.ndarray = np.array(Image.open("p2.png").convert("RGB"))
    ans: str = pytesseract.image_to_string(img).strip()

    return ans


def main(args: Namespace):
    data: List[OpCode] = parse_input(args.input)

    # Part 1
    p1_start: datetime = datetime.now()
    p1: int = do_p1(data)
    p1_end: datetime = datetime.now()
    p1_duration: timedelta = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start: datetime = datetime.now()
    p2: str = do_p2(data)
    p2_end: datetime = datetime.now()
    p2_duration: timedelta = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args: Namespace = parse_args()
    main(args)
