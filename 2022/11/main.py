#!/usr/bin/env python3
""" Advent of Code Day 11
"""
import math
from argparse import ArgumentParser, Namespace
from copy import deepcopy
from datetime import datetime, timedelta
from pathlib import Path
from typing import Callable, Dict, Final, List

import numpy as np

OPERATOR_TABLE: Final[Dict[str, Callable[[int, int], int]]] = {
    "+": np.add,
    "-": np.subtract,
    "*": np.multiply,
    "/": np.divide,
}


class Monkey:
    def __init__(
        self,
        start_items: List[int],
        operation: Callable[[], None],
        divarg: int,
        tosses: Dict[bool, int],
    ):
        self.items: List[int] = start_items
        self.operation: Callable = operation
        self.divarg: int = divarg
        self.tosses: Dict[bool, int] = tosses
        self.inspections: int = 0

    @classmethod
    def from_lines(cls, lines: List[str]) -> "Monkey":
        item_list: List[int] = list(map(int, lines[1].partition(":")[-1].split(",")))

        equation: str = lines[2].partition("=")[-1].rstrip().strip()
        operator: str = equation.partition(" ")[-1][0]
        operand: str = equation.partition(" ")[-1].partition(" ")[-1]

        _func: Callable[[int, int], int] = OPERATOR_TABLE[operator]

        if operand == "old":

            def operation(p1: int) -> int:
                return _func(p1, p1)

        else:

            def operation(p1: int) -> int:
                return _func(p1, int(operand))

        divarg: int = int(lines[3].partition("divisible by")[-1])
        if_true: int = int(lines[4][-1])
        if_false: int = int(lines[5][-1])

        return cls(
            item_list,
            operation,
            divarg,
            {
                True: if_true,
                False: if_false,
            },
        )


def parse_args(arglist: List[str] = None) -> Namespace:
    """Parse and validate program arguments.

    Args:
      arglist (List[str], optional):
        Argument list, defaults to None (read from stdin)

    Returns:
      Namespace: Parsed and validated program arguments
    """
    argp: ArgumentParser = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def parse_input(input_path: Path) -> List[Monkey]:
    barrel: List[Monkey] = []

    with open(input_path, "r") as in_file:
        # Read input
        inputs: List[str] = in_file.read().strip().split("\n\n")

    for monkey_id, monkey_str in enumerate(inputs):
        lines: List[str] = [x.rstrip().strip() for x in monkey_str.split("\n")]
        barrel.append(Monkey.from_lines(lines))

    return barrel


def do_p1(barrel: List[Monkey], rounds: int) -> int:
    for r in range(rounds):
        for monkey_id, monkey in enumerate(barrel):
            while monkey.items:
                item: int = monkey.items.pop(0)
                item = monkey.operation(item)
                item //= 3
                barrel[monkey.tosses[item % monkey.divarg == 0]].items.append(item)
                monkey.inspections += 1

    return np.multiply(*sorted([m.inspections for m in barrel])[-2:])


def do_p2(barrel: List[Monkey], rounds: int) -> int:
    modulus: int = math.lcm(*[m.divarg for m in barrel])
    for r in range(rounds):
        for monkey_id, monkey in enumerate(barrel):
            while monkey.items:
                item: int = monkey.items.pop(0)
                item = monkey.operation(item)
                item %= modulus
                barrel[monkey.tosses[item % monkey.divarg == 0]].items.append(item)
                monkey.inspections += 1

    return np.multiply(*sorted([m.inspections for m in barrel])[-2:])


def main(args: Namespace):
    data: List[Monkey] = parse_input(args.input)

    # Part 1
    p1_start: datetime = datetime.now()
    p1: int = do_p1(deepcopy(data), 20)
    p1_end: datetime = datetime.now()
    p1_duration: timedelta = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start: datetime = datetime.now()
    p2: int = do_p2(deepcopy(data), 10000)
    p2_end: datetime = datetime.now()
    p2_duration: timedelta = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args: Namespace = parse_args()
    main(args)
