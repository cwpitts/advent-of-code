#!/usr/bin/env python3
""" Advent of Code Day 4
"""
from argparse import ArgumentParser, Namespace
from datetime import datetime, timedelta
from pathlib import Path
from typing import List, Set, Tuple

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm


def parse_args(arglist: List[str] = None) -> Namespace:
    """Parse and validate program arguments.

    Args:
      arglist (List[str], optional):
        Argument list, defaults to None (read from stdin)

    Returns:
      Namespace: Parsed and validated program arguments
    """
    argp: ArgumentParser = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def parse_input(input_path: Path) -> List[Tuple[Tuple[int, int], Tuple[int, int]]]:
    data: List[Tuple[Tuple[int, int], Tuple[int, int]]] = []

    with open(input_path, "r") as in_file:
        # Read input
        for line in in_file.readlines():
            first, second = line.split(",")
            data.append((tuple(map(int, first.split("-"))), tuple(map(int, second.split("-")))))

    return data


def do_p1(data: List[Tuple[Tuple[int, int], Tuple[int, int]]]) -> int:
    count: int = 0

    for (first_start, first_end), (second_start, second_end) in data:
        s1: Set[int] = set(range(first_start, first_end + 1))
        s2: Set[int] = set(range(second_start, second_end + 1))

        if s1.issubset(s2) or s2.issubset(s1):
            count += 1

    return count

def do_p2(data: List[Tuple[Tuple[int, int], Tuple[int, int]]]) -> int:
    count: int = 0

    for (first_start, first_end), (second_start, second_end) in data:
        s1: Set[int] = set(range(first_start, first_end + 1))
        s2: Set[int] = set(range(second_start, second_end + 1))

        if len(s1 & s2) != 0:
            count += 1    

    return count

def main(args: Namespace):
    data: List[Tuple[Tuple[int, int], Tuple[int, int]]] = parse_input(args.input)

    # Part 1
    p1_start: datetime = datetime.now()
    p1: int = do_p1(data)
    p1_end: datetime = datetime.now()
    p1_duration: timedelta = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start: datetime = datetime.now()
    p2: int = do_p2(data)
    p2_end: datetime = datetime.now()
    p2_duration: timedelta = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args: Namespace = parse_args()
    main(args)
