#!/usr/bin/env python3
""" Advent of Code Day 7
"""
from argparse import ArgumentParser, Namespace
from datetime import datetime, timedelta
from pathlib import Path
from typing import Dict, List, Optional

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm


class Node:
    def __init__(
        self,
        parent: Optional["Node"],
        name: str,
        is_dir: bool,
        size: Optional[int] = None,
    ):
        self.parent: Optional["Node"] = parent

        self.name: str = name
        self._size: Optional[int] = size
        self.is_dir: bool = is_dir
        self.children: Dict[str, Node] = {}

    def size(self) -> int:
        if self.is_dir:
            return sum([c.size() for c in self.children.values()])
        else:
            return self._size

    def __str__(self) -> str:
        ret: str = ""
        if self.is_dir:
            ret += "Directory"
        else:
            ret += "File"

        ret += f"{self.name}, size={self.size()}"

        return ret

    def __repr__(self) -> str:
        ret: str = ""
        if self.is_dir:
            ret += "Directory"
        else:
            ret += "File"

        ret += f" {self.name}, size={self.size()}"

        return ret


class FileSystem:
    def __init__(self, root: Node):
        self._root = root
        self._current: Node = root

    def cd(self, name: str):
        if name == "..":
            self._current = (
                self._current.parent
                if self._current.parent is not None
                else self._current
            )
        elif name in self._current.children:
            self._current = self._current.children[name]
        else:
            self._current = self._root
            parts: List[str] = [p for p in name.split("/") if p != ""]
            while parts:
                self.cd(parts.pop(0))

    def add_child(self, child: Node):
        self._current.children[child.name] = child

    def at(self) -> Node:
        return self._current

    @classmethod
    def from_cmds(cls, cmds: List[str]) -> "FileSystem":
        fs: Optional[cls] = None

        for line in cmds:
            cmd_args, _, output = line.partition("\n")
            cmd, _, args = cmd_args.partition(" ")

            if cmd == "cd":
                if args == "/":
                    root: Node = Node(None, "/", True)
                    fs: cls = cls(root)
                else:
                    fs.cd(args)
            elif cmd == "ls":
                for item in output.strip().split("\n"):
                    type_size, _, name = item.partition(" ")
                    is_dir: bool = type_size == "dir"
                    size: Optional[int] = None

                    if not is_dir:
                        size = int(type_size)

                    child: Node = Node(fs.at(), name, is_dir, size=size)
                    fs.add_child(child)

        return fs


def parse_args(arglist: List[str] = None) -> Namespace:
    """Parse and validate program arguments.

    Args:
      arglist (List[str], optional):
        Argument list, defaults to None (read from stdin)

    Returns:
      Namespace: Parsed and validated program arguments
    """
    argp: ArgumentParser = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def parse_input(input_path: Path) -> FileSystem:
    with open(input_path, "r") as in_file:
        # Read input
        arr: List[str] = in_file.read().strip().split("$ ")
        arr = arr[1:]

    fs: FileSystem = FileSystem.from_cmds(arr)
    fs.cd("/")

    return fs


def do_p1(fs: FileSystem) -> int:
    fs.cd("/")

    # Find each directory with size at most 100_000
    def filter_by_size(n: Node, nodes: Optional[List[Node]] = None) -> List[Node]:
        if nodes is None:
            nodes = []

        if n.is_dir and n.size() <= 100_000:
            nodes.append(n)

        for c in n.children.values():
            nodes = filter_by_size(c, nodes)

        return nodes

    nodes: List[Node] = filter_by_size(fs.at())

    return sum([n.size() for n in nodes])


def do_p2(fs: FileSystem) -> int:
    fs.cd("/")

    space_available: int = 70000000 - fs.at().size()
    size_needed: int = 30000000 - space_available

    # Find each directory with size at least the minimum needed
    def filter_by_size(n: Node, nodes: Optional[List[Node]] = None) -> List[Node]:
        if nodes is None:
            nodes = []

        if not n.is_dir:
            return nodes

        if n.size() >= size_needed:
            nodes.append(n)

        for c in n.children.values():
            nodes = filter_by_size(c, nodes)

        return nodes

    nodes: List[Node] = filter_by_size(fs.at())

    min_node: Node = min(nodes, key=lambda x: x.size())

    return min_node.size()


def main(args: Namespace):
    fs: FileSystem = parse_input(args.input)

    # Part 1
    p1_start: datetime = datetime.now()
    p1: int = do_p1(fs)
    p1_end: datetime = datetime.now()
    p1_duration: timedelta = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start: datetime = datetime.now()
    p2: int = do_p2(fs)
    p2_end: datetime = datetime.now()
    p2_duration: timedelta = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args: Namespace = parse_args()
    main(args)
