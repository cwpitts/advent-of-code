#!/usr/bin/env python3
""" Advent of Code Day 16

Based on https://www.reddit.com/r/adventofcode/comments/zn6k1l/comment/j0fti6c/
"""
import functools
import re
from argparse import ArgumentParser, Namespace
from collections import defaultdict as DefaultDict
from datetime import datetime, timedelta
from pathlib import Path
from re import Pattern
from typing import Dict, Final, List, Set, Tuple

import numpy as np

LINE_REGEX: Final[Pattern] = re.compile(
    "^Valve (.*) has flow rate=([0-9]+); tunnel[s]{0,1} lead[s]{0,1} to valve[s]{0,1} (.*)$"
)


def parse_args(arglist: List[str] = None) -> Namespace:
    """Parse and validate program arguments.

    Args:
      arglist (List[str], optional):
        Argument list, defaults to None (read from stdin)

    Returns:
      Namespace: Parsed and validated program arguments
    """
    argp: ArgumentParser = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def parse_input(
    input_path: Path,
) -> Tuple[Set[str], Dict[str, int], Dict[Tuple[str, str], int]]:
    nodes: Set[str] = set()
    flows: Dict[str, int] = dict()
    distances: Dict[Tuple[str, str], int] = DefaultDict(lambda: np.inf)

    with open(input_path, "r") as in_file:
        # Read input
        for line in in_file:
            source, rate, tunnels = re.match(LINE_REGEX, line).groups()

            nodes.add(source)

            rate = int(rate)
            if rate > 0:
                flows[source] = rate
            for dest in tunnels.split(", "):
                distances[source, dest] = 1

    for k in nodes:
        for i in nodes:
            for j in nodes:
                distances[i, j] = min(
                    distances[i, j], distances[i, k] + distances[k, j]
                )

    return nodes, flows, distances


def do_p1(
    nodes: Set[str], flows: Dict[str, int], distances: Dict[Tuple[str, str], int]
) -> int:
    @functools.cache
    def search(time: int, node: str, neighbors=frozenset(flows)):
        paths: List[int] = []
        for neighbor in neighbors:
            # Skip paths that are too long
            if distances[node, neighbor] >= time:
                continue

            length_to_neighbor: int = flows[neighbor] * (
                time - distances[node, neighbor] - 1
            )
            length_from_neighbor: int = search(
                time - distances[node, neighbor] - 1,
                neighbor,
                neighbors - {neighbor},
            )

            paths.append(
                length_to_neighbor
                + length_from_neighbor  # The time taken for each of the neighbors
            )

        if len(paths) == 0:
            return 0
        return max(paths)

    return search(30, "AA")


def do_p2(
    nodes: Set[str], flows: Dict[str, int], distances: Dict[Tuple[str, str], int]
) -> int:
    @functools.cache
    def search(
        time: int, node: str = "AA", neighbors=frozenset(flows), elephant: bool = False
    ):
        paths: List[int] = []

        for neighbor in neighbors:
            if distances[node, neighbor] >= time:
                continue

            flow_to_neighbor: int = flows[neighbor] * (
                time - distances[node, neighbor] - 1
            )
            flow_from_neighbor: int = search(
                time - distances[node, neighbor] - 1,
                neighbor,
                neighbors - {neighbor},
                elephant,
            )
            paths.append(flow_to_neighbor + flow_from_neighbor)

        if elephant:
            paths.append(search(26, neighbors=neighbors))

        if len(paths) == 0:
            return 0

        return max(paths)

    return search(26, "AA", elephant=True)


def main(args: Namespace):
    nodes, flows, distances = parse_input(args.input)

    # Part 1
    p1_start: datetime = datetime.now()
    p1: int = do_p1(nodes, flows, distances)
    p1_end: datetime = datetime.now()
    p1_duration: timedelta = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start: datetime = datetime.now()
    p2: int = do_p2(nodes, flows, distances)
    p2_end: datetime = datetime.now()
    p2_duration: timedelta = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args: Namespace = parse_args()
    main(args)
