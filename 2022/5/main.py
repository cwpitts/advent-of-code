#!/usr/bin/env python3
""" Advent of Code Day 5
"""
import os
import re
from argparse import ArgumentParser, Namespace
from copy import deepcopy
from datetime import datetime, timedelta
from pathlib import Path
from re import Match, Pattern
from typing import Callable, Dict, List, Optional, Tuple

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm

# fmt: off
os.environ["PYGAME_HIDE_SUPPORT_PROMPT"] = "yes"
import pygame
from pygame import Rect, Surface
from pygame.event import Event
from pygame.font import Font
from pygame.time import Clock

# fmt: on

font_dir: Path = Path(__file__).resolve().parent.parent.parent / "fonts"
instruction_regex: Pattern = re.compile("move ([0-9]+) from ([0-9]+) to ([0-9]+)")


class Visualizer:
    def __init__(self, width: int, height: int, box_size: int = 25):
        self._width: int = width
        self._height: int = height
        self._surface: Optional[Surface] = None
        self._running: bool = False
        self._state: Optional[List[List[str]]] = None
        self._instructions: Optional[List[Tuple[int, int, int]]] = None
        self._box_size: int = box_size
        self._font: Optional[Font] = None
        self._box_pos_scale: float = 1.5
        self._box_line_thickness: float = 2
        self._box_border_radius: float = 10
        self._func: Optional[Callable] = None
        self._end_font: Optional[Font] = None
        self._y_bottom_buffer: float = 0.1
        self._end_sleep: float = 5
        self._pause: bool = True
        self._acc: float = 0.0
        self._threshold: float = 0.1
        self._clock: Clock = Clock()

    def _init(self):
        pygame.init()
        self._surface = pygame.display.set_mode(
            (self._width, self._height), pygame.HWSURFACE | pygame.DOUBLEBUF
        )
        pygame.display.set_caption("Advent of Code 2022/05")

        pygame.font.init()
        pygame.font.get_init()
        self._font = pygame.font.Font(font_dir / "Roboto" / "Roboto-Regular.ttf", 12)
        self._end_font = pygame.font.Font(
            font_dir / "Roboto" / "Roboto-Regular.ttf", 36
        )

    def run(
        self,
        state: List[List[str]],
        instructions: List[Tuple[int, int, int]],
        f: Callable,
    ):
        self._init()
        self._state = state
        self._instructions = instructions
        self._func = f

        self._running = True

        while self._running:
            for event in pygame.event.get():
                self._handle_event(event)
            self._update()
            self._render()

        self._cleanup()

    def _render(self):
        self._surface.fill((0, 0, 0))

        # Calculate offset from center of screen
        total_items: int = len(self._state)
        total_x_needed: float = self._box_size * self._box_pos_scale * total_items
        available_x: int = self._width
        x_offset: float = (available_x - total_x_needed) / 2

        for i, _ in enumerate(self._state):
            pygame.draw.rect(
                self._surface,
                (255, 255, 255),
                (
                    i * self._box_size * self._box_pos_scale + x_offset,
                    (self._height * (1 - self._y_bottom_buffer)) - self._box_size,
                    self._box_size,
                    self._box_size,
                ),
                self._box_line_thickness,
            )
            item: Surface = self._font.render(f"{i + 1}", True, (255, 255, 255))
            rect: Rect = item.get_rect()
            rect.centerx = (
                i * self._box_size * self._box_pos_scale + self._box_size / 2 + x_offset
            )
            rect.centery = (
                self._height * (1 - self._y_bottom_buffer)
            ) - self._box_size / 2
            self._surface.blit(item, rect)

        for col, stack in enumerate(self._state):
            for row, c in enumerate(stack):
                pygame.draw.rect(
                    self._surface,
                    (255, 0, 255),
                    (
                        col * self._box_size * self._box_pos_scale + x_offset,
                        (self._height * (1 - self._y_bottom_buffer))
                        - (row + 1) * self._box_size
                        - self._box_size,
                        self._box_size,
                        self._box_size,
                    ),
                    0,  # Thickness 0 for filling
                    border_radius=self._box_border_radius,
                )

                item: Surface = self._font.render(c, True, (255, 255, 255))
                rect: Rect = item.get_rect()
                rect.centerx = (
                    col * self._box_size * self._box_pos_scale
                    + self._box_size / 2
                    + x_offset
                )
                rect.centery = (
                    (self._height * (1 - self._y_bottom_buffer))
                    - (row + 1) * self._box_size
                    - self._box_size / 2
                )
                self._surface.blit(item, rect)

        if not self._instructions:
            msg: str = "".join(c[-1] for c in self._state)
            item: Surface = self._end_font.render(msg, True, (255, 0, 0), (0, 0, 0))
            rect: Rect = item.get_rect(center=(self._width / 2, self._height / 2))
            self._surface.blit(item, rect)

        pygame.display.update()

    def _update(self):
        self._clock.tick()
        if self._pause:
            return

        if not self._instructions:
            self._running = False
            return

        if self._acc < self._threshold:
            self._acc += self._clock.get_time() / 1000
            return

        self._acc = 0
        self._state = self._func(self._state, [self._instructions.pop(0)])

    def _cleanup(self):
        pygame.quit()

    def _handle_event(self, event: Event):
        if event.type == pygame.QUIT:
            self._running = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RETURN:
                self._pause = False
            if event.key == pygame.K_ESCAPE:
                self._cleanup()


def parse_args(arglist: List[str] = None) -> Namespace:
    """Parse and validate program arguments.

    Args:
      arglist (List[str], optional):
        Argument list, defaults to None (read from stdin)

    Returns:
      Namespace: Parsed and validated program arguments
    """
    argp: ArgumentParser = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    argp.add_argument(
        "-v",
        "--visualize",
        help="run visualization",
        action="store_true",
        default=False,
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def parse_input(input_path: Path) -> Tuple[List[List[str]], List[Tuple[int, int, int]]]:
    with open(input_path, "r") as in_file:
        # Read input
        initial_state_str, instructions_str = in_file.read().split("\n\n")
        initial_state_rows: List[str] = initial_state_str.split("\n")

        initial_state: List[List[int]] = []
        state_indices: Dict[int, int] = {}

        for i, v in enumerate(initial_state_rows[-1]):
            if v.isnumeric():
                initial_state.append([])
                state_indices[i] = len(initial_state) - 1

        for row in initial_state_rows[:-1]:
            for i, v in enumerate(row):
                if v.isalpha():
                    initial_state[state_indices[i]].insert(0, v)

        instructions: List[Tuple[int, int, int]] = []
        for item in instructions_str.split("\n"):
            m: Match = re.match(instruction_regex, item)
            if m is None:
                continue
            instructions.append(
                (int(m.group(1)), int(m.group(2)) - 1, int(m.group(3)) - 1)
            )

    return initial_state, instructions


def print_state(state: List[List[str]]):
    s: str = ""
    for i, _ in enumerate(state):
        s += f" {i + 1} "

    s = "-" * len(s) + "\n" + s

    for i in range(max(len(l) for l in state)):
        r: str = ""
        for col in state:
            if len(col) > i:
                r += f" {col[i]} "
            else:
                r += "   "
        s = r + "\n" + s

    s = f"\n{s}\n"

    print(s)


def do_p1(
    state: List[List[str]], instructions: List[Tuple[int, int, int]]
) -> List[List[str]]:
    state = deepcopy(state)
    for count, src, dst in instructions:
        while count > 0:
            c: str = state[src].pop()
            state[dst].append(c)
            count -= 1

    return state


def do_p2(
    state: List[List[str]], instructions: List[Tuple[int, int, int]]
) -> List[List[str]]:
    state = deepcopy(state)
    for count, src, dst in instructions:
        state[dst] += state[src][-count:]
        state[src] = state[src][:-count]

    return state


def visualize(
    state: List[List[str]],
    instructions: List[Tuple[int, int, int]],
    width: int = 800,
    height: int = 1000,
):
    state = deepcopy(state)

    visualizer: Visualizer = Visualizer(width, height)

    visualizer.run(state, instructions, do_p2)


def main(args: Namespace):
    data: Tuple[List[List[str]], List[Tuple[int, int, int]]] = parse_input(args.input)
    state, instructions = data

    # Part 1
    p1_start: datetime = datetime.now()
    p1_state: List[List[str]] = do_p1(state, instructions)
    p1: str = "".join(c[-1] for c in p1_state)
    p1_end: datetime = datetime.now()
    p1_duration: timedelta = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start: datetime = datetime.now()
    p2_state: List[List[str]] = do_p2(state, instructions)
    p2: str = "".join(c[-1] for c in p2_state)
    p2_end: datetime = datetime.now()
    p2_duration: timedelta = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)

    if args.visualize:
        visualize(state, instructions)


if __name__ == "__main__":
    args: Namespace = parse_args()
    main(args)
