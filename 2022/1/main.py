#!/usr/bin/env python3
""" Advent of Code Day 1
"""
from argparse import ArgumentParser, Namespace
from datetime import datetime, timedelta
from pathlib import Path
from typing import List


def parse_args(arglist: List[str] = None) -> Namespace:
    """Parse and validate program arguments.

    Args:
      arglist (List[str], optional):
        Argument list, defaults to None (read from stdin)

    Returns:
      Namespace: Parsed and validated program arguments
    """
    argp: ArgumentParser = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def parse_input(input_path: Path) -> List[List[int]]:
    data: List[List[int]] = []

    with open(input_path, "r") as in_file:
        # Read input
        for elf in in_file.read().split("\n\n"):
            l: List[int] = []
            for calories in elf.strip().split("\n"):
                l.append(int(calories))
            data.append(l)

    return data


def main(args: Namespace):
    data: List[List[int]] = parse_input(args.input)

    # Part 1
    p1_start: datetime = datetime.now()
    p1: int = max(sum(x) for x in data)
    p1_end: datetime = datetime.now()
    p1_duration: timedelta = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start: datetime = datetime.now()
    p2_values: List[int] = sorted((sum(x) for x in data), reverse=True)
    p2: int = sum(p2_values[:3])
    p2_end: datetime = datetime.now()
    p2_duration: timedelta = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args: Namespace = parse_args()
    main(args)
