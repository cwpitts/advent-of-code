#!/usr/bin/env python3
""" Advent of Code Day 15
"""
import re
from argparse import ArgumentParser, Namespace
from datetime import datetime, timedelta
from pathlib import Path
from re import Pattern
from typing import Final, Generator, List, Set, Tuple

from rich.progress import Progress
from scipy.spatial.distance import cityblock

LOCATION_REGEX: Final[Pattern] = re.compile(
    "^Sensor at x=(-{0,1}\d+), y=(-{0,1}\d+): closest beacon is at x=(-{0,1}\d+), y=(-{0,1}\d+)$"
)


def parse_args(arglist: List[str] = None) -> Namespace:
    """Parse and validate program arguments.

    Args:
      arglist (List[str], optional):
        Argument list, defaults to None (read from stdin)

    Returns:
      Namespace: Parsed and validated program arguments
    """
    argp: ArgumentParser = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def parse_input(input_path: Path) -> List[Tuple[Tuple[int, int], Tuple[int, int]]]:
    sensor_locations: List[Tuple[Tuple[int, int], Tuple[int, int]]] = []

    with open(input_path, "r") as in_file:
        # Read input
        for line in in_file:
            coords: List[int] = list(map(int, re.match(LOCATION_REGEX, line).groups()))
            sensor_locations.append((tuple(coords[:2]), tuple(coords[2:])))

    return sensor_locations


def in_range(sensors: Set[Tuple[int, int, int]], x: int, y: int) -> bool:
    for s_x, s_y, d in sensors:
        if abs(s_x - x) + abs(s_y - y) <= d:
            return True

    return False


def do_p1(sensor_locations: List[Tuple[Tuple[int, int], Tuple[int, int]]]) -> int:
    no_beacons: Set[Tuple[int, int]] = set()
    sensors: Set[Tuple[int, int, int]] = set()
    beacons: Set[Tuple[int, int]] = set()
    y: int = 2_000_000

    for (sensor_x, sensor_y), (beacon_x, beacon_y) in sensor_locations:
        dist: int = cityblock((sensor_x, sensor_y), (beacon_x, beacon_y))
        beacons.add((beacon_x, beacon_y))
        sensors.add((sensor_x, sensor_y, dist))

    min_x: int = min([x - d for x, _, d in sensors])
    max_x: int = max([x + d for x, _, d in sensors])

    count: int = 0

    with Progress() as progress:
        x_task = progress.add_task("x")
        for x in range(min_x, max_x):
            if in_range(sensors, x, y) and (x, y) not in beacons:
                count += 1
            progress.advance(x_task, 100 / (max_x - min_x))

    return count


def get_perimeter_points(s_x: int, s_y: int, d: int) -> Generator:
    x: int = s_x
    y: int = s_y - d

    while y != s_y:
        yield x, y
        x += 1
        y += 1
    while y != s_y + d:
        yield x, y
        x -= 1
        y += 1
    while y != s_y:
        yield x, y
        x -= 1
        y -= 1
    while y != s_y - d:
        yield x, y
        x += 1
        y -= 1


def do_p2(sensor_locations: List[Tuple[Tuple[int, int], Tuple[int, int]]]) -> int:
    no_beacons: Set[Tuple[int, int]] = set()
    sensors: Set[Tuple[int, int, int]] = set()
    beacons: Set[Tuple[int, int]] = set()
    max_search: int = 4_000_000

    for (sensor_x, sensor_y), (beacon_x, beacon_y) in sensor_locations:
        dist: int = cityblock((sensor_x, sensor_y), (beacon_x, beacon_y))
        beacons.add((beacon_x, beacon_y))
        sensors.add((sensor_x, sensor_y, dist))

    for sensor_x, sensor_y, dist in sensors:
        for x, y in get_perimeter_points(sensor_x, sensor_y, dist + 1):
            if x < 0 or y < 0 or x > max_search or y > max_search:
                continue
            if not in_range(sensors, x, y) and (x, y) not in beacons:
                return x * 4_000_000 + y


def main(args: Namespace):
    data: List[Tuple[Tuple[int, int], Tuple[int, int]]] = parse_input(args.input)

    # Part 1
    p1_start: datetime = datetime.now()
    p1: int = do_p1(data)
    p1_end: datetime = datetime.now()
    p1_duration: timedelta = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start: datetime = datetime.now()
    p2: int = do_p2(data)
    p2_end: datetime = datetime.now()
    p2_duration: timedelta = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args: Namespace = parse_args()
    main(args)
