#!/usr/bin/env python3
""" Advent of Code Day 3
"""
import string
from argparse import ArgumentParser, Namespace
from datetime import datetime, timedelta
from pathlib import Path
from typing import Dict, List, Set, Tuple

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm

priorities: Dict[str, int] = {
    item: priority + 1 for priority, item in enumerate(string.ascii_letters)
}


def parse_args(arglist: List[str] = None) -> Namespace:
    """Parse and validate program arguments.

    Args:
      arglist (List[str], optional):
        Argument list, defaults to None (read from stdin)

    Returns:
      Namespace: Parsed and validated program arguments
    """
    argp: ArgumentParser = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def parse_input(input_path: Path) -> List[Tuple[Set[str], Set[str]]]:
    compartments: List[Tuple[Set[str], Set[str]]] = []

    with open(input_path, "r") as in_file:
        # Read input
        for line in in_file.readlines():
            compartments.append(
                (set(line[: len(line) // 2]), set(line[len(line) // 2 :]))
            )

    return compartments


def do_p1(data: List[Tuple[Set[str], Set[str]]]) -> int:
    return sum(priorities[list(c1 & c2)[0]] for c1, c2 in data)


def do_p2(data: List[Tuple[Set[str], Set[str]]], group_size: int = 3) -> int:
    p_sum: int = 0

    for i in range(0, len(data), group_size):
        # fmt: off
        sets: List[Tuple[List[str], List[str]]] = data[i:i + group_size]
        # fmt: on
        overlap: Set = set(string.ascii_letters)

        for s1, s2 in sets:
            overlap &= s1 | s2

        p_sum += priorities[list(overlap)[0]]

    return p_sum


def main(args: Namespace):
    data: List[Tuple[Set[str], Set[str]]] = parse_input(args.input)

    # Part 1
    p1_start: datetime = datetime.now()
    p1: int = do_p1(data)
    p1_end: datetime = datetime.now()
    p1_duration: timedelta = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start: datetime = datetime.now()
    p2: int = do_p2(data)
    p2_end: datetime = datetime.now()
    p2_duration: timedelta = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args: Namespace = parse_args()
    main(args)
