#!/usr/bin/env python3
""" Advent of Code Day 8
"""
from argparse import ArgumentParser, Namespace
from datetime import datetime, timedelta
from pathlib import Path
from typing import List

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.pyplot import Axes, Figure
from rich.progress import Progress

plt.rcParams["text.usetex"] = True
plt.style.use("bmh")


def parse_args(arglist: List[str] = None) -> Namespace:
    """Parse and validate program arguments.

    Args:
      arglist (List[str], optional):
        Argument list, defaults to None (read from stdin)

    Returns:
      Namespace: Parsed and validated program arguments
    """
    argp: ArgumentParser = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(sizes: np.ndarray, n_samples: int = 5):
    p2_times: List[float] = []
    p1_times: List[float] = []

    with Progress() as progress:
        size_task: int = progress.add_task("Performance")
        sample_task: int = progress.add_task("Sample")
        for size in sizes:
            progress.reset(sample_task)

            p1_time_samples: List[float] = []
            p2_time_samples: List[float] = []

            for _ in np.arange(n_samples):
                problem: np.ndarray = np.random.randint(1, size // 2, size=(size, size))

                p2_start: datetime = datetime.now()
                do_p2(problem)
                p2_end: datetime = datetime.now()
                p2_time_samples.append((p2_end - p2_start).microseconds)

                p1_start: datetime = datetime.now()
                do_p1(problem)
                p1_end: datetime = datetime.now()
                p1_time_samples.append((p1_end - p1_start).microseconds)
                progress.advance(sample_task, 100 / n_samples)

            p1_times.append(np.mean(p1_time_samples))
            p2_times.append(np.mean(p2_time_samples))
            progress.advance(size_task, 100 / sizes.shape[0])

    fig: Figure
    ax: Axes
    fig, (ax1, ax2) = plt.subplots(ncols=2)
    ax2.plot(sizes, p2_times)
    ax2.set_xlabel("Problem size")
    ax2.set_ylabel(r"P2 time ($\mu$ s)")
    ax2.set_title("P2 runtime")

    ax1.plot(sizes, p1_times)
    ax1.set_xlabel("Problem size")
    ax1.set_ylabel(r"P1 time ($\mu$ s)")
    ax1.set_title("P1 runtime")

    fig.set_size_inches(8, 6)
    fig.suptitle("Runtime growth as problem size grows")

    fig.savefig("perf.png", bbox_inches="tight")


def parse_input(input_path: Path) -> np.ndarray:
    with open(input_path, "r") as in_file:
        # Read input
        data: List[List[int]] = []
        for line in in_file:
            data.append(list(map(int, line.strip())))

    return np.array(data)


def do_p1(data: np.ndarray) -> int:
    visibility_map: np.ndarray = np.full(data.shape, 0)

    visibility_map[:, 0] = 1
    visibility_map[:, -1] = 1
    visibility_map[0, :] = 1
    visibility_map[-1, :] = 1

    cols: np.array = np.arange(1, data.shape[-1] - 1)
    rows: np.array = np.arange(1, data.shape[0] - 1)

    # Propagate from point in all directions
    # If you hit an edge, visible
    # If you hit something taller than you, stop, not visible
    for r in rows:
        for c in cols:
            # Up
            if np.all(data[0:r, c] < data[r, c]):
                visibility_map[r, c] += 1
            # Down
            if np.all(data[r + 1 :, c] < data[r, c]):
                visibility_map[r, c] += 1
            # Left
            if np.all(data[r, :c] < data[r, c]):
                visibility_map[r, c] += 1
            # Right
            if np.all(data[r, c + 1 :] < data[r, c]):
                visibility_map[r, c] += 1

    return np.sum(visibility_map > 0)


def cast(arr: np.ndarray, height: int) -> int:
    dist: int = 0

    for v in arr:
        dist += 1
        if v >= height:
            break

    return dist


def do_p2(data: np.ndarray) -> int:
    scenic_map: np.ndarray = np.full(data.shape, 0)

    cols: np.array = np.arange(1, data.shape[-1] - 1)
    rows: np.array = np.arange(1, data.shape[0] - 1)

    # Cast from point in all directions
    for r in rows:
        for c in cols:
            p: int = 1

            # Up
            p *= cast(data[0:r, c][::-1], data[r, c])

            # Down
            p *= cast(data[r + 1 :, c], data[r, c])

            # Left
            p *= cast(data[r, 0:c][::-1], data[r, c])

            # Right
            p *= cast(data[r, c + 1 :], data[r, c])

            scenic_map[r, c] = p

    return (
        scenic_map[np.unravel_index(scenic_map.argmax(), scenic_map.shape)],
        scenic_map,
    )


def main(args: Namespace):
    data: np.ndarray = parse_input(args.input)

    # Part 1
    p1_start: datetime = datetime.now()
    p1: int = do_p1(data)
    p1_end: datetime = datetime.now()
    p1_duration: timedelta = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start: datetime = datetime.now()
    p2, _ = do_p2(data)
    p2_end: datetime = datetime.now()
    p2_duration: timedelta = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(2 ** np.arange(2, 8))


if __name__ == "__main__":
    args: Namespace = parse_args()
    main(args)
