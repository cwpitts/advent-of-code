#!/usr/bin/env python3
""" Advent of Code Day 12
"""
import itertools
import math
import re
from argparse import ArgumentParser, Namespace
from datetime import datetime, timedelta
from pathlib import Path
from typing import List

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm


class Moon:
    """Moon utility class.
    """

    def __init__(self, x: int, y: int, z: int, dx: int = 0, dy: int = 0, dz: int = 0):
        """Moon constructor.

        Args:
          x (int): Initial x coordinate
          y (int): Initial y coordinate
          z (int): Initial z coordinate
          dx (int, optional): Initial x velocity
          dy (int, optional): Initial y velocity
          dz (int, optional): Initial z velocity

        """
        self.x: int = x
        self.y: int = y
        self.z: int = z
        self.dx: int = dx
        self.dy: int = dy
        self.dz: int = dz

    def move(self):
        """Move moon.

        Moves moon by applying velocity.

        """
        self.x += self.dx
        self.y += self.dy
        self.z += self.dz

    def adjust(self, o: "Moon"):
        """Adjust moon by other moon.

        Args:
          o (Moon): Other moon

        """
        self.dx += np.sign(o.x - self.x) * 1
        self.dy += np.sign(o.y - self.y) * 1
        self.dz += np.sign(o.z - self.z) * 1

    def energy(self) -> int:
        """Compute energy of moon.

        Returns:
          int: Total energy of the ship

        """
        return sum(map(abs, (self.x, self.y, self.z))) * sum(
            map(abs, (self.dx, self.dy, self.dz))
        )

    def __repr__(self) -> str:
        """Construct repr string.

        Returns:
          str: repr string

        """
        return (
            f"pos=<{self.x}, {self.y}, {self.z}>, vel=<{self.dx}, {self.dy}, {self.dz}>"
        )


def parse_args(arglist: List[str] = None) -> Namespace:
    """Parse and validate program arguments.

    Args:
      arglist (List[str], optional):
        Argument list, defaults to None (read from stdin)

    Returns:
      Namespace: Parsed and validated program arguments
    """
    argp: ArgumentParser = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def parse_input(input_path: Path) -> List[Moon]:
    """Parse input.

    Args:
      input_path (Path): Path to input file

    Returns:
      coords (List[Moon]): List of moons

    """
    regex: re.Pattern = re.compile(
        r"<x=(\-{0,1}[0-9]+), y=(\-{0,1}[0-9]+), z=(\-{0,1}[0-9]+)>"
    )
    coords: List[Moon] = []

    with open(input_path, "r") as in_file:
        # Read input
        for line in in_file:
            x, y, z = map(int, re.search(regex, line.strip()).groups())
            coords.append(Moon(x, y, z))

    return coords


def step(moons: List[Moon]):
    """Move all moons one step.

    Args:
      moons (List[Moon]): List of moons

    """
    for moon1, moon2 in itertools.combinations(moons, 2):
        # Adjust velocity of both moons
        moon1.adjust(moon2)
        moon2.adjust(moon1)

    for moon in moons:
        moon.move()


def main(args: Namespace):
    """Main program logic.

    Args:
      args (Namespace): Program arguments

    """
    moons: List[Moon] = parse_input(args.input)

    # Part 1
    p1_start: datetime = datetime.now()

    for iteration in range(1000):
        step(moons)

    p1: int = sum([moon.energy() for moon in moons])

    p1_end: datetime = datetime.now()
    p1_duration: timedelta = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start: datetime = datetime.now()

    moons: List[Moon] = parse_input(args.input)
    x_cycle: int = None
    y_cycle: int = None
    z_cycle: int = None

    step(moons)
    iterations: int = 1

    while x_cycle is None or y_cycle is None or z_cycle is None:
        step(moons)
        iterations += 1

        if x_cycle is None and all(moon.dx == 0 for moon in moons):
            x_cycle = iterations
        if y_cycle is None and all(moon.dy == 0 for moon in moons):
            y_cycle = iterations
        if z_cycle is None and all(moon.dz == 0 for moon in moons):
            z_cycle = iterations

    p2: int = math.lcm(*[x_cycle, y_cycle, z_cycle]) * 2

    p2_end: datetime = datetime.now()
    p2_duration: timedelta = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args: Namespace = parse_args()
    main(args)
