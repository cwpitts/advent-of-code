#!/usr/bin/env python3
""" Advent of Code Day 10
"""
from argparse import ArgumentParser, Namespace
from collections import defaultdict
from datetime import datetime, timedelta
from pathlib import Path
from queue import Empty, PriorityQueue
from typing import Dict, Iterable, List, Tuple

import matplotlib.pyplot as plt
import numpy as np
from PIL import Image, ImageFile
from tqdm import tqdm


def parse_args(arglist: List[str] = None) -> Namespace:
    """Parse and validate program arguments.

    Args:
      arglist (List[str], optional):
        Argument list, defaults to None (read from stdin)

    Returns:
      Namespace: Parsed and validated program arguments
    """
    argp: ArgumentParser = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    argp.add_argument(
        "--viz", help="run visualization", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def plot_all_targets(
    queues: Iterable[PriorityQueue],
    loc: np.ndarray,
    x_min: np.float64,
    x_max: np.float64,
    y_min: np.float64,
    y_max: np.float64,
    color: str = "black",
    loc_color: str = "red",
) -> Tuple[plt.Figure, plt.Axes]:
    """Plot all targets.

    Args:
      queues (Iterable[PriorityQueue]): All target queues
      loc (np.ndarray): (y,x) coordinate pair of location
      color (str, optional): Color of asteroids, defaults to 'black'
      loc_color (str, optional):
        Color of location asteroid, defaults to 'red'

    Returns:
      fig (plt.Figure): MPL figure
      ax (plt.Axes): MPL axes

    """
    fig, ax = plt.subplots()

    ax.scatter(loc[1], loc[0], color=loc_color)

    for Q in queues:
        for _, (y, x) in Q.queue:
            ax.scatter(x, y, color=color)

    ax.set_xlim(x_min, x_max)
    ax.set_ylim(y_min, y_max)

    return fig, ax


def visualize(
    coordinates: np.ndarray,
    loc_color: str = "red",
    color: str = "black",
    laser_color: str = "yellow",
    explosion_color: str = "orange",
):
    """Visualize solution.

    Args:
      coordinates (np.ndarray): Coordinates of targets
      loc_color (str, optional): Color for base location, defaults to 'red'
      color (str, optional): Color for asteroids, defaults to 'black'
      laser_color (str, optional): Color for laser, defaults to 'yellow'
      explosion_color (str, optional):
        Color for exploded asteroid, defaults to 'orange'

    """
    print("Visualizing solution")
    output_dir: Path = Path("output")
    if not output_dir.exists():
        output_dir.mkdir()

    (loc_y, loc_x), targets = find_best_location(coordinates)
    img_idx: int = 0

    target_angles: List[np.float64] = sorted(targets.keys())

    x_min: np.float64 = coordinates[:, 1].min() - 1
    y_min: np.float64 = coordinates[:, 0].min() - 1
    x_max: np.float64 = coordinates[:, 1].max() + 1
    y_max: np.float64 = coordinates[:, 0].max() + 1

    fig, ax = plt.subplots()
    ax.set_xlim(x_min, x_max)
    ax.set_ylim(y_min, y_max)

    ax.scatter(loc_x, loc_y, color=loc_color)

    artists: Dict[plt.Artist] = {}

    for Q in targets.values():
        for _, (y, x) in Q.queue:
            artists[(y, x)] = ax.scatter(x, y, color=color)

    line: plt.Artist = None

    ax.set_title(f"Asteroids destroyed: {img_idx}")
    fig.savefig(output_dir / f"img{img_idx}.png", bbox_inches="tight")
    img_idx += 1

    pbar: tqdm = tqdm(total=sum(len(x.queue) for x in targets.values()))
    while target_angles:
        for angle in target_angles:
            try:
                _, (y, x) = targets[angle].get_nowait()
                artists[(y, x)].remove()
                artists[(y, x)] = ax.scatter(x, y, color="orange")
                line = ax.plot([loc_x, x], [loc_y, y], color=laser_color)[0]
                ax.set_title(f"Asteroids destroyed: {img_idx}")
                fig.savefig(output_dir / f"img{img_idx}.png", bbox_inches="tight")
                artists[(y, x)].remove()
                img_idx += 1
                pbar.update()
            except Empty:
                target_angles.remove(angle)
                continue
            line.remove()

    ax.set_title(f"Asteroids destroyed: {img_idx}")
    fig.savefig(output_dir / f"img{img_idx}.png", bbox_inches="tight")
    plt.close(fig)

    frames: List[Image] = [p for p in output_dir.iterdir() if p.suffix == ".png"]
    frames = [
        Image.open(p)
        for p in sorted(
            frames, key=lambda x: int(x.name.replace("img", "").replace(".png", ""))
        )
    ]

    print("Rendering as GIF")
    frames[0].save(
        output_dir / "animation.gif",
        save_all=True,
        append_images=frames[1:],
        loop=False,
        duration=250,
    )


def compute_target_queues(
    coordinates: np.ndarray, loc: np.ndarray
) -> Dict[float, PriorityQueue]:
    """Compute targeting queues for station.

    Args:
      coordinates (np.ndarray): Coordinates of all asteroids
      loc (np.ndarray): Station location coordinates

    Returns:
      targets (Dict[float, PriorityQueue]):
        Targets sorted into priority queues based on angle

    """
    targets: Dict[float, PriorityQueue] = defaultdict(lambda: PriorityQueue())

    for target in coordinates:
        if np.all(target == loc):
            continue

        target_y, target_x = target - loc

        # Compute angle between target and station location
        angle: np.float64 = np.arctan2(target_y, target_x)

        angle += np.pi / 2

        # Angles must be no less than 0, convert to positive unit
        # circle values
        if angle < 0:
            angle += 2 * np.pi
        # dist: np.float64 = np.sqrt(np.sum(np.square(loc - target)))
        dist: np.float64 = np.sum(np.abs(target - loc))
        targets[angle].put((dist, target))

    return targets


def find_best_location(
    coordinates: np.ndarray,
) -> Tuple[np.ndarray, Dict[np.float64, PriorityQueue]]:
    """Compute best location and associated targeting queues.

    Args:
      coordinates (np.ndarray): Asteroid coordinates

    Returns:
      Tuple[np.ndarray, Dict[np.float64, PriorityQueue]]

    """
    best_loc: np.ndarray = None
    best_targets: Dict[np.float64, PriorityQueue] = None
    for loc in coordinates:
        targets: Dict[np.float64, PriorityQueue] = compute_target_queues(
            coordinates, loc
        )
        if best_targets is None or len(targets) > len(best_targets):
            best_targets = targets
            best_loc = loc

    return (best_loc, best_targets)


def parse_input(input_path: Path) -> np.ndarray:
    """Parse input into y, x coordinate matrix.

    Args:
      input_path (Path): Path to input data

    Returns:
      np.ndarray: y, x coordinate matrix

    """
    with open(input_path, "r") as in_file:
        # Read input
        arr: np.ndarray = np.array(
            [[c for c in row.strip()] for row in in_file.readlines()]
        )

    X, Y = np.meshgrid(np.arange(arr.shape[1]), np.arange(arr.shape[0]))

    x_coords: np.ndarray = X[arr == "#"]
    y_coords: np.ndarray = Y[arr == "#"]

    return np.vstack([y_coords, x_coords]).T


def main(args):
    coordinates: np.ndarray = parse_input(args.input)

    # Part 1
    p1_start: datetime = datetime.now()

    best_loc, best_targets = find_best_location(coordinates)

    p1: int = len(best_targets)

    p1_end: datetime = datetime.now()
    p1_duration: timedelta = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start: datetime = datetime.now()

    n_asteroids_destroyed: int = 0
    target_angles: List[np.float64] = sorted(best_targets.keys())
    while n_asteroids_destroyed < 200:
        for angle in target_angles:
            try:
                dist, current_asteroid = best_targets[angle].get_nowait()
            except Empty:
                continue
            n_asteroids_destroyed += 1

            if n_asteroids_destroyed == 200:
                break

    p2_y, p2_x = current_asteroid
    p2: int = p2_x * 100 + p2_y

    p2_end: datetime = datetime.now()
    p2_duration: timedelta = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)

    if args.viz:
        visualize(coordinates)


if __name__ == "__main__":
    args: Namespace = parse_args()
    main(args)
