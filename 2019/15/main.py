#!/usr/bin/env python3
""" Advent of Code Day 15
"""
import random
from argparse import ArgumentParser, Namespace
from datetime import datetime, timedelta
from enum import Enum
from pathlib import Path
from typing import Dict, List, Tuple, Union

import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
from aoc.intcode import IntcodeVM
from tqdm import tqdm


class Direction(Enum):
    """Enumeration of movement directions.
    """

    NORTH: int = 1
    SOUTH: int = 2
    WEST: int = 3
    EAST: int = 4

    @staticmethod
    def random() -> "Direction":
        """Return random direction.

        Returns:
          Direction: Randomly chosen direction
        """
        return random.choice(
            [Direction.NORTH, Direction.SOUTH, Direction.WEST, Direction.EAST]
        )

    @staticmethod
    def clockwise(d: "Direction") -> "Direction":
        """Return clockwise rotation of direction.

        Args:
          d (Direction): Direction to rotate from

        Returns:
          Direction: Direction that is clockwise to d
        """
        order: List["Direction"] = [
            Direction.NORTH,
            Direction.EAST,
            Direction.SOUTH,
            Direction.WEST,
        ]
        return order[(order.index(d) + 1) % len(order)]


class Cell(Enum):
    """Enumeration of cell types.
    """

    WALL: int = 0
    HALL: int = 1
    GOAL: int = 2

    def __eq__(self, o: Union["Cell", int]) -> bool:
        """Equality operator for Cells.

        Args:
          o (Union[Cell, int]): Other object

        Returns:
          bool:
            True if the cells are equals, or if the int o has a value equal
            to the cell's value

        """
        if isinstance(o, Cell):
            return o.value == self.value

        if isinstance(o, int):
            return o == self.value

        raise ValueError(f"Unsupported comparison with {type(o)} value '{o}'")

    def str(self) -> str:
        """Return string marker.

        Returns:
          str: Marker for the cell type

        """
        if self.value == Cell.WALL:
            return "#"
        elif self.value == Cell.HALL:
            return "."

        # Cell.GOAL
        return "!"

    @classmethod
    def from_int(cls, x: int) -> "Cell":
        """Parse cell from int.

        Args:
          x (int): Integer value

        Returns:
          Cell: Cell type corresponding to integer value

        Raises:
          ValueError:
            When an integer value not corresponding to any cell type is
            passed in

        """
        if x == Cell.WALL.value:
            return Cell.WALL
        if x == Cell.HALL.value:
            return Cell.HALL
        if x == Cell.GOAL.value:
            return Cell.GOAL

        raise ValueError(f"Invalid cell type {x}")


class Robot:
    """Robot to explore maze.

    The robot uses an IntcodeVM to run a maze search program.
    """

    def __init__(self):
        """Robot.

        """
        self._vm: IntcodeVM = IntcodeVM()
        self._map: Dict[Position, str] = {}
        self._shifts: List[Tuple[int, int]] = [(0, -1), (1, 0), (0, 1), (-1, 0)]
        self._oxygen: Tuple[int, int] = None
        self._G: nx.Graph = nx.Graph()

    def _map_to_arr(self, pos: Tuple[int, int]) -> np.ndarray:
        """Convert world map to Numpy array.

        Args:
          pos (Tuple[int, int]): Position of robot

        Returns:
          arr (np.ndarray): NumPy array representing the maze

        """
        min_x: int = 0
        min_y: int = 0
        max_x: int = 0
        max_y: int = 0

        for (x, y) in self._map.keys():
            min_x = min(min_x, x)
            max_x = max(max_x, x)
            min_y = min(min_y, y)
            max_y = max(max_y, y)

        arr: np.ndarray = np.full(
            (max_y - min_y + 1, max_x - min_x + 1), Cell.WALL.value, dtype=np.int64
        )

        for (x, y), v in self._map.items():
            arr[y - min_y, x - min_x] = v.value

        return arr

    def run(self, prog: List[int]) -> List[Tuple[int, int]]:
        """Run program.

        Args:
          prog (List[int]): Intcode program

        Returns:
          List[Tuple[int, int]]: Path from origin to oxygen

        """
        self._vm.load_program(prog)

        self._map_world()
        return self.path_to_oxygen()

    def path_to_oxygen(self) -> List[Tuple[int, int]]:
        """Compute shortest path from origin to oxygen.

        Returns:
          List[Tuple[int, int]]: Path from origin to oxygen
        """
        return nx.algorithms.dijkstra_path(self._G, (0, 0), self._oxygen)

    def _map_world(self):
        """Map world.

        Generates a DFS map of the world.
        """
        # Hug left wall until getting back to the start
        origin: Tuple[int, int] = (0, 0)
        pos: Tuple[int, int] = origin
        new_pos: Tuple[int, int] = pos
        directions: List[int] = [1, 4, 2, 3]
        d_idx: int = 0
        fully_explored: bool = False

        self._map[origin] = Cell.HALL

        while not fully_explored:
            x, y = pos
            dx, dy = self._shifts[d_idx]
            new_pos = (x + dx, y + dy)

            if new_pos == origin:
                fully_explored = True

            result: int = self._vm.run_until_output(str(directions[d_idx]))
            cell: Cell = Cell.from_int(result)
            self._map[new_pos] = cell
            if cell == Cell.WALL:
                d_idx = (d_idx + 1) % len(directions)
            else:
                if cell == Cell.GOAL:
                    self._oxygen = new_pos
                self._found_goal = cell == Cell.GOAL
                d_idx = (d_idx - 1) % len(directions)
                self._G.add_edge(pos, new_pos, weight=1)
                pos = new_pos

    def flood(self) -> int:
        """Compute flood time for entire maze.

        Returns:
          int: Time until all cells are oxygenated
        """
        return max(
            nx.algorithms.single_source_dijkstra_path_length(
                self._G, self._oxygen
            ).values()
        )


def parse_args(arglist: List[str] = None) -> Namespace:
    """Parse and validate program arguments.

    Args:
      arglist (List[str], optional):
        Argument list, defaults to None (read from stdin)

    Returns:
      Namespace: Parsed and validated program arguments
    """
    argp: ArgumentParser = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def parse_input(input_path: Path) -> List[int]:
    with open(input_path, "r") as in_file:
        # Read input
        return list(map(int, in_file.read().split(",")))


def main(args: Namespace):
    prog: List[int] = parse_input(args.input)

    # Part 1
    p1_start: datetime = datetime.now()

    robot: Robot = Robot()
    path: List[Tuple[int, int]] = robot.run(prog)

    p1: int = len(path) - 1

    p1_end: datetime = datetime.now()
    p1_duration: timedelta = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start: datetime = datetime.now()

    p2: int = robot.flood()

    p2_end: datetime = datetime.now()
    p2_duration: timedelta = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args: Namespace = parse_args()
    main(args)
