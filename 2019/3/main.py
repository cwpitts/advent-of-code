#!/usr/bin/env python3
""" Advent of Code Day 3
"""
from argparse import ArgumentParser
from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        data = []
        for row in in_file:
            line = []
            for step in row.strip().split(","):
                direction = step[0]
                magnitude = int(step[1:])
                line.append((direction, magnitude))
            data.append(line)

    # Part 1
    p1_start = datetime.now()
    positions = []
    for wire in data:
        wire_positions = []
        x = 0
        y = 0
        for direction, magnitude in wire:
            if direction == "U":
                target_x = x
                target_y = y + magnitude
            elif direction == "D":
                target_x = x
                target_y = y - magnitude
            elif direction == "R":
                target_x = x + magnitude
                target_y = y
            elif direction == "L":
                target_x = x - magnitude
                target_y = y

            for _ in np.arange(np.abs(target_x - x)):
                x += 1 if target_x > x else -1
                wire_positions.append((x, y))
            for _ in np.arange(np.abs(target_y - y)):
                y += 1 if target_y > y else -1
                wire_positions.append((x, y))
        positions.append(wire_positions)
    intersections = set(positions[0]).intersection(set(positions[1]))
    p1 = None
    for x, y in intersections:
        md = np.abs(x) + np.abs(y)
        if not p1 or md < p1:
            p1 = md
    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()
    p2 = None
    for intersection in intersections:
        dist = positions[0].index(intersection) + positions[1].index(intersection) + 2
        if not p2 or dist < p2:
            p2 = dist
    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args = parse_args()
    main(args)
