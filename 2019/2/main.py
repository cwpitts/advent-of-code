#!/usr/bin/env python3
""" Advent of Code Day 2
"""
from argparse import ArgumentParser
from copy import deepcopy
from datetime import datetime, timedelta
from pathlib import Path
from typing import List

import matplotlib.pyplot as plt
import numpy as np
from aoc.intcode import IntcodeVM
from tqdm import tqdm


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def init_prog(data, noun, verb):
    prog = deepcopy(data)
    prog[1] = noun
    prog[2] = verb

    return prog


def run_prog(prog):
    ip = 0
    while prog[ip] != 99:
        opcode = prog[ip]
        if opcode == 1:
            prog[prog[ip + 3]] = prog[prog[ip + 1]] + prog[prog[ip + 2]]
        elif opcode == 2:
            prog[prog[ip + 3]] = prog[prog[ip + 1]] * prog[prog[ip + 2]]
        else:
            raise RuntimeError(f"Unknown opcode {prog[ip]}")
        ip += 4

    return prog


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        data: List[int] = [int(x.strip()) for x in in_file.read().split(",")]

    # Part 1
    p1_start: datetime = datetime.now()
    vm_p1: IntcodeVM = IntcodeVM()
    prog_p1, ip_p1 = vm_p1.run(data, 12, 2)
    p1: int = prog_p1[0]
    p1_end: datetime = datetime.now()
    p1_duration: timedelta = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start: datetime = datetime.now()
    vm_p2: IntcodeVM = IntcodeVM()
    for noun in np.arange(100):
        for verb in np.arange(100):
            prog_p2, ip_p2 = vm_p2.run(data, noun, verb)
            if prog_p2[0] == 19690720:
                p2 = 100 * noun + verb
                break
    p2_end: datetime = datetime.now()
    p2_duration: timedelta = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args = parse_args()
    main(args)
