#!/usr/bin/env python3
""" Advent of Code Day 13
"""
from argparse import ArgumentParser, Namespace
from collections import Counter
from copy import deepcopy
from datetime import datetime, timedelta
from enum import Enum
from pathlib import Path
from typing import Dict, List, Tuple

import matplotlib.pyplot as plt
import numpy as np
from aoc.intcode import IntcodeVM
from PIL import Image
from tqdm import tqdm


class Tile(Enum):
    EMPTY: int = 0
    WALL: int = 1
    BLOCK: int = 2
    HORIZONTAL_PADDLE: int = 3
    BALL: int = 4
    SCORE: int = -1

    @staticmethod
    def from_int(i: int) -> "Block":
        if i == 0:
            return Tile.EMPTY
        if i == 1:
            return Tile.WALL
        if i == 2:
            return Tile.BLOCK
        if i == 3:
            return Tile.HORIZONTAL_PADDLE
        if i == 4:
            return Tile.BALL

        raise NotImplementedError(f"Tile type not supported: {i}")

class Game:
    def __init__(self, prog: List[int], output_dir: Path, visualize: bool):
        self._vm: IntcodeVM = IntcodeVM()
        self._prog: List[int] = prog
        self._prog[0] = 2
        self._output_dir: Path = output_dir
        self._board: Dict[Tuple[int, int], Tile] = {}
        self._current_frame: int = 0
        self._ball_position: Tuple[int, int] = None
        self._paddle_position: Tuple[int, int] = None
        self._buf: List[int] = []
        self._visualize: bool = visualize
        if visualize:
            self._pbar: tqdm = tqdm()
        else:
            self._pbar: tqdm = None

        self.score: int = 0

    def _output_func(self, x: int):
        self._buf.append(x)

        if len(self._buf) < 3:
            return

        x, y, tile_type = self._buf

        self._buf.clear()

        if (x, y) == (-1, 0):
            self._board[(-1, 0)] = tile_type
            self.score = tile_type
            if self._visualize:
                self._make_outupt_image()
            return

        tile: Tile = Tile.from_int(tile_type)
        self._board[(x, y)] = tile

        if tile == Tile.BALL:
            self._ball_location = (x, y)
            if self._visualize:
                self._make_outupt_image()
        elif tile == Tile.HORIZONTAL_PADDLE:
            self._paddle_location = (x, y)
            if self._visualize:
                self._make_outupt_image()

    def _input_func(self) -> str:
        paddle_x, paddle_y = self._paddle_location
        ball_x, ball_y = self._ball_location

        if ball_x < paddle_x:
            return "-1"
        if ball_x > paddle_x:
            return "1"
        return "0"

    def _make_outupt_image(self):
        arr: np.ndarray = np.zeros((50, 50))
        for (x, y), tile in self._board.items():
            if (x, y) == (-1, 0):
                continue
            arr[y, x] = tile.value

        fig, ax = plt.subplots()
        ax.imshow(arr)
        fig.savefig(self._output_dir / f"{self._current_frame}.png", bbox_inches="tight")
        plt.close("all")

        self._current_frame += 1
        self._pbar.update()

    def play(self):
        if not self._output_dir.exists():
            self._output_dir.mkdir()

        self._vm.run(self._prog, output_func=self._output_func, input_func=self._input_func)

        frame_paths: List[Path] = [p for p in self._output_dir.iterdir() if p.suffix == ".png"]
        frame_paths = sorted(
                    frame_paths, key=lambda x: int(x.name.replace(".png", ""))
            )

        frames: List[Image] = []
        for img_path in frame_paths:
            frames.append(deepcopy(Image.open(img_path)))

        frame[0].save(
            self._output_dir / "animation.gif",
            save_all=True,
            append_images=frames[1:],
            loop=False,
            duration=250,
        )

def parse_args(arglist: List[str] = None) -> Namespace:
    """Parse and validate program arguments.

    Args:
      arglist (List[str], optional):
        Argument list, defaults to None (read from stdin)

    Returns:
      Namespace: Parsed and validated program arguments
    """
    argp: ArgumentParser = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    argp.add_argument("--visualize", help="generate visualization", action="store_true", default=False)

    return argp.parse_args(arglist)


def performance(size):
    pass


def parse_input(input_path: Path) -> List[int]:
    with open(input_path, "r") as in_file:
        # Read input
        return list(map(int, in_file.read().strip().split(",")))


def main(args: Namespace):
    data: List[int] = parse_input(args.input)

    # Part 1
    p1_start: datetime = datetime.now()

    game: Dict[Tuple[int, int], Tile] = {}
    vm_p1: IntcodeVM = IntcodeVM()

    buf_p1: List[int] = []

    def output_func(x: int):
        buf_p1.append(x)
        if len(buf_p1) == 3:
            game[tuple(buf_p1[:-1])] = Tile.from_int(buf_p1[-1])
            buf_p1.clear()

    vm_p1.run(data, output_func=output_func)

    p1: int = Counter(game.values())[Tile.BLOCK]

    p1_end: datetime = datetime.now()
    p1_duration: timedelta = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start: datetime = datetime.now()

    game: Game = Game(data, Path("output"), args.visualize)
    game.play()
    p2: int = game.score

    p2_end: datetime = datetime.now()
    p2_duration: timedelta = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args: Namespace = parse_args()
    main(args)
