#!/usr/bin/env python3
""" Advent of Code Day 11
"""
from argparse import ArgumentParser, Namespace
from collections import defaultdict
from datetime import datetime, timedelta
from pathlib import Path
from typing import Dict, List, Tuple

import matplotlib.pyplot as plt
import numpy as np
from aoc.intcode import IntcodeVM
from tqdm import tqdm


class Heading:
    """Heading.

    """
    def __init__(self, dx: int, dy: int):
        """Heading controls the direction of movement.

        Args:
          dx (int): Change in x coordinate
          dx (int): Change in y coordinate

        """
        self.dx: int = dx
        self.dy: int = dy

    def rotate(self, direction: int):
        """Rotate heading.

        Args:
          direction (int): Direction to turn

        """
        if direction == 0:
            if (self.dx, self.dy) == (0, 1):
                self.dx, self.dy = -1, 0
            elif (self.dx, self.dy) == (-1, 0):
                self.dx, self.dy = 0, -1
            elif (self.dx, self.dy) == (0, -1):
                self.dx, self.dy = 1, 0
            elif (self.dx, self.dy) == (1, 0):
                self.dx, self.dy = 0, 1
        elif direction == 1:
            if (self.dx, self.dy) == (0, 1):
                self.dx, self.dy = 1, 0
            elif (self.dx, self.dy) == (1, 0):
                self.dx, self.dy = 0, -1
            elif (self.dx, self.dy) == (0, -1):
                self.dx, self.dy = -1, 0
            elif (self.dx, self.dy) == (-1, 0):
                self.dx, self.dy = 0, 1


class Position:
    """Position utility class.

    """
    def __init__(self, x: int, y: int):
        """Position.

        Args:
          x (int): x coordinate
          y (int): y coordinate

        """
        self.x: int = x
        self.y: int = y

    def __add__(self, o: Heading):
        """Addition operator.

        Args:
          o (Heading): Heading to adjust position by

        """
        return Position(self.x + o.dx, self.y + o.dy)

    def __hash__(self):
        """Hash operation.

        """
        return hash((self.x, self.y))


def parse_args(arglist: List[str] = None) -> Namespace:
    """Parse and validate program arguments.

    Args:
      arglist (List[str], optional):
        Argument list, defaults to None (read from stdin)

    Returns:
      Namespace: Parsed and validated program arguments
    """
    argp: ArgumentParser = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def parse_input(input_path: Path) -> List[int]:
    """Parse problem input.

    Args:
      input_path (Path): Path to input file

    Returns:
      List[int]: The Intcode program

    """
    with open(input_path, "r") as in_file:
        # Read input
        return list(map(int, in_file.read().strip().split(",")))


def main(args: Namespace):
    """Main program logic.

    Args:
      args: (Namespace): Program arguments

    """
    prog: List[int] = parse_input(args.input)

    # Part 1
    p1_start: datetime = datetime.now()

    pos_p1: Position = Position(0, 0)
    heading_p1: Heading = Heading(0, 1)
    state_p1: Dict[Tuple[int, int], List[int]] = defaultdict(lambda: [0])
    paint_flag_p1: bool = True

    def input_func_p1():
        return str(state_p1[(pos_p1.x, pos_p1.y)][-1])

    def output_func_p1(x: int):
        nonlocal state_p1
        nonlocal pos_p1
        nonlocal heading_p1
        nonlocal paint_flag_p1
        if paint_flag_p1:
            state_p1[(pos_p1.x, pos_p1.y)].append(x)
        else:
            heading_p1.rotate(x)
            pos_p1 += heading_p1
        paint_flag_p1 = not paint_flag_p1

    vm: IntcodeVM = IntcodeVM()
    vm.run(prog, input_func=input_func_p1, output_func=output_func_p1)

    p1: int = len(state_p1)
    # p1: int = sum(sum(cell) for cell in state.values())

    p1_end: datetime = datetime.now()
    p1_duration: timedelta = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start: datetime = datetime.now()

    pos_p2: Position = Position(0, 0)
    heading_p2: Heading = Heading(0, 1)
    state_p2: Dict[Tuple[int, int], List[int]] = defaultdict(lambda: [0])
    state_p2[(0, 0)].append(1)
    paint_flag_p2: bool = True

    def input_func_p2():
        return str(state_p2[(pos_p2.x, pos_p2.y)][-1])

    def output_func_p2(x: int):
        nonlocal state_p2
        nonlocal pos_p2
        nonlocal heading_p2
        nonlocal paint_flag_p2
        if paint_flag_p2:
            state_p2[(pos_p2.x, pos_p2.y)].append(x)
        else:
            heading_p2.rotate(x)
            pos_p2 += heading_p2
        paint_flag_p2 = not paint_flag_p2

    vm: IntcodeVM = IntcodeVM()
    vm.run(prog, input_func=input_func_p2, output_func=output_func_p2)

    output_dir: Path = Path("output")
    if not output_dir.exists():
        output_dir.mkdir()

    fig, ax = plt.subplots()
    min_x: int = min(p[0] for p in state_p2)
    max_x: int = max(p[0] for p in state_p2)
    min_y: int = min(p[1] for p in state_p2)
    max_y: int = max(p[1] for p in state_p2)

    arr: np.ndarray = np.zeros((np.abs(max_y - min_y) + 1, np.abs(max_x - min_x) + 1))
    for (x, y), values in state_p2.items():
        x += np.abs(min_x)
        y += np.abs(min_y)
        arr[y, x] = values[-1]
    fig, ax = plt.subplots()
    arr = arr[::-1]
    ax.imshow(arr)
    fig.savefig(output_dir / "p2.png", bbox_inches="tight")

    p2_end: datetime = datetime.now()
    p2_duration: timedelta = p2_end - p2_start

    print(f"P2 in {p2_duration}, see output directory")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args: Namespace = parse_args()
    main(args)
