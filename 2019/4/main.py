#!/usr/bin/env python3
""" Advent of Code Day 4
"""
from argparse import ArgumentParser
from datetime import datetime
from pathlib import Path
from typing import List

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm


digit_pairs: List[str] = [str(c) * 2 for c in range(10)]


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def is_valid_p2(num: str) -> bool:
    have_exact_double: bool = False
    for pair in digit_pairs:
        idx: int = num.find(pair)

        if idx == -1:
            continue

        if idx == len(num) - 2:
            return True

        if num[idx + 2] != pair[0]:
            return True

    return False


def is_valid_p1(num: str) -> bool:
    double_digits: bool = False

    for idx, digit in enumerate(num):
        if idx == len(num) - 1:
            continue
        if int(digit) > int(num[idx + 1]):
            return False
        if not double_digits and digit == num[idx + 1]:
            double_digits = True

    return double_digits


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        start, stop = map(int, in_file.read().strip().split("-"))

    # Part 1
    p1_start = datetime.now()

    valid_nums_p1: List[str] = []
    for n in range(start, stop + 1):
        n = str(n)
        if is_valid_p1(n):
            valid_nums_p1.append(n)
    p1: int = len(valid_nums_p1)
    
    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()

    p2: int = sum(is_valid_p2(n) for n in valid_nums_p1)
    
    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args = parse_args()
    main(args)
