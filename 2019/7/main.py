#!/usr/bin/env python3
""" Advent of Code Day 7
"""
import itertools
import threading
from argparse import ArgumentParser, Namespace
from datetime import datetime, timedelta
from pathlib import Path
from queue import Queue
from typing import List

import matplotlib.pyplot as plt
import numpy as np
from aoc.intcode import IntcodeVM
from tqdm import tqdm

N_VMS: int = 5


class Worker(threading.Thread):
    """Worker thread.

    Handles executing an IntcodeVM in a thread.
    """

    def __init__(
        self, in_queue: Queue, out_queue: Queue, prog: List[int], init_param: int
    ):
        """Worker thread.

        Args:
          in_queue (Queue): Input queue
          out_queue (Queue): Output queue
          prog (List[int]): Program
          init_param (int): Initial parameter
        """
        self.vm: IntcodeVM = IntcodeVM()
        self.in_queue: Queue = in_queue
        self.out_queue: Queue = out_queue
        self.prog: List[int] = prog
        self.init_param: int = init_param

        super().__init__()

    def run(self):
        """Execute program in IntcodeVM in thread.
        """

        def input_func() -> str:
            if self.init_param is not None:
                v: str = str(self.init_param)
                self.init_param = None
            else:
                v: str = str(self.in_queue.get(timeout=10))
            return v

        def output_func(x: int):
            self.out_queue.put(x)

        self.vm.run(self.prog, input_func=input_func, output_func=output_func)


def parse_args(arglist: List[str] = None) -> Namespace:
    """Parse and validate program arguments.

    Args:
      arglist (List[str], optional):
        Argument list, defaults to None (read from stdin)

    Returns:
      Namespace: Parsed and validated program arguments
    """
    argp: ArgumentParser = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def parse_input(input_path: Path) -> List[int]:
    """Parse input from file.

    Args:
      input_path (Path): Path to input file

    Returns:
      List[int]: Problem input
    """
    with open(input_path, "r") as in_file:
        # Read input
        return list(map(int, in_file.read().strip().split(",")))


def main(args):
    """Execute main program.

    Args:
      args (Namespace): Program arguments
    """
    prog: List[int] = parse_input(args.input)

    vms: List[IntcodeVM] = [IntcodeVM() for _ in range(N_VMS)]

    # Part 1
    p1_start: datetime = datetime.now()

    p1: int = 0

    for params in itertools.permutations(range(N_VMS), N_VMS):
        outputs: List[int] = [0]

        def output_func(x: int):
            outputs.append(x)

        for param, vm in zip(params, vms):

            def input_generator():
                yield param
                yield outputs[-1]

            g = input_generator()

            def input_func():
                return str(next(g))

            vm.run(prog, input_func=input_func, output_func=output_func)

        p1 = max(p1, outputs[-1])

    p1_end: datetime = datetime.now()
    p1_duration: timedelta = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start: datetime = datetime.now()

    p2: int = 0

    for params in itertools.permutations(range(N_VMS, N_VMS * 2), N_VMS):
        outputs: List[int] = [0]

        queues: List[Queue] = [Queue() for _ in range(N_VMS)]
        workers: List[Worker] = [
            Worker(queues[i], queues[(i + 1) % len(queues)], prog, params[i])
            for i in range(N_VMS)
        ]

        for worker in workers:
            worker.start()

        queues[0].put(0)

        for worker in workers:
            worker.join()

        p2 = max(p2, queues[0].queue[-1])

    p2_end: datetime = datetime.now()
    p2_duration: timedelta = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args: Namespace = parse_args()
    main(args)
