#!/usr/bin/env python3
""" Advent of Code Day 6
"""
from argparse import ArgumentParser, Namespace
from datetime import datetime, timedelta
from pathlib import Path
from typing import List

import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
from tqdm import tqdm


def parse_args(arglist: List[str] = None) -> Namespace:
    """Parse and validate program arguments.

    Args:
      arglist (List[str], optional):
        Argument list, defaults to None (read from stdin)

    Returns:
      Namespace: Parsed and validated program arguments
    """
    argp: ArgumentParser = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def parse_input(input_path: Path) -> nx.Graph:
    with open(input_path, "r") as in_file:
        # Read input
        G: nx.Graph = nx.Graph()
        for line in in_file:
            src, dest = line.strip().split(")")
            G.add_edge(src, dest)

    return G


def main(args):
    G: nx.Graph = parse_input(args.input)

    # Part 1
    p1_start: datetime = datetime.now()

    p1: int = 0
    for node in G.nodes:
        if node == "COM":
            continue
        p1 += nx.dijkstra_path_length(G, "COM", node)

    p1_end: datetime = datetime.now()
    p1_duration: timedelta = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start: datetime = datetime.now()

    p2: int = nx.dijkstra_path_length(G, "YOU", "SAN") - 2

    p2_end: datetime = datetime.now()
    p2_duration: timedelta = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args = parse_args()
    main(args)
