#!/usr/bin/env python3
""" Advent of Code Day 8
"""
from argparse import ArgumentParser, Namespace
from datetime import datetime, timedelta
from pathlib import Path
from typing import List

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm


def parse_args(arglist: List[str] = None) -> Namespace:
    """Parse and validate program arguments.

    Args:
      arglist (List[str], optional):
        Argument list, defaults to None (read from stdin)

    Returns:
      Namespace: Parsed and validated program arguments
    """
    argp: ArgumentParser = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    argp.add_argument("-d", "--dimensions", help="dimensions of image", default="6x25")

    args: Namespace = argp.parse_args(arglist)

    args.dimensions = list(map(int, args.dimensions.split("x")))

    return args


def performance(size):
    pass


def parse_input(input_path: Path) -> np.ndarray:
    with open(input_path, "r") as in_file:
        # Read input
        return np.array(list(map(int, in_file.read().strip())))


def main(args):
    data: np.ndarray = parse_input(args.input)

    # Part 1
    p1_start: datetime = datetime.now()

    height, width = args.dimensions

    n_layers: int = data.shape[0] // (height * width)
    data_p1 = data.reshape((n_layers, height, width))

    zero_count: int = 2 ** 64
    p1: int = 0

    for layer in np.arange(n_layers):
        d: np.ndarray = data_p1[layer, :, :]
        new_zero_count: int = np.sum(d == 0)
        if new_zero_count < zero_count:
            zero_count = new_zero_count
            p1 = np.sum(d == 1) * np.sum(d == 2)

    p1_end: datetime = datetime.now()
    p1_duration: timedelta = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start: datetime = datetime.now()

    output_path: Path = Path("output")
    if not output_path.exists():
        output_path.mkdir()

    img: np.ndarray = np.zeros((height, width))
    for row in np.arange(img.shape[0]):
        for col in np.arange(img.shape[1]):
            img[row, col] = data_p1[:, row, col][data_p1[:, row, col] != 2][0]

    fig, ax = plt.subplots()
    ax.imshow(img, cmap="gray")
    fig.savefig(output_path / "p2.png", bbox_inches="tight")

    p2: str = "See output image p2.png"

    p2_end: datetime = datetime.now()
    p2_duration: timedelta = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args: Namespace = parse_args()
    main(args)
