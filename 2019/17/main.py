#!/usr/bin/env python3
""" Advent of Code Day 17
"""
import enum
from argparse import ArgumentParser, Namespace
from datetime import datetime, timedelta
from pathlib import Path
from typing import Dict, List, Tuple

import numpy as np
from aoc.intcode import IntcodeVM


class Turn(enum.Enum):
    LEFT: int = enum.auto()
    RIGHT: int = enum.auto()


turns: Dict[Tuple[int, int], Dict[Turn, Tuple[int, int]]] = {
    (1, 0): {Turn.LEFT: (0, 1), Turn.RIGHT: (0, -1),},
    (-1, 0): {Turn.LEFT: (0, -1), Turn.RIGHT: (0, 1),},
    (0, 1): {Turn.LEFT: (-1, 0), Turn.RIGHT: (1, 0),},
    (0, -1): {Turn.LEFT: (1, 0), Turn.RIGHT: (-1, 0),},
}


def parse_args(arglist: List[str] = None) -> Namespace:
    """Parse and validate program arguments.

    Args:
      arglist (List[str], optional):
        Argument list, defaults to None (read from stdin)

    Returns:
      Namespace: Parsed and validated program arguments
    """
    argp: ArgumentParser = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def parse_input(input_path: Path) -> List[int]:
    with open(input_path, "r") as in_file:
        # Read input
        prog: List[int] = list(map(int, in_file.read().strip().split(",")))

    return prog


def is_intersection(world_map: np.ndarray, row: int, col: int) -> bool:
    """Check if point is intersection.

    Args:
      world_map (np.ndarray): World map
      row (int): Row index
      col (int): Col index

    Returns:
      True if point is an intersection, False otherwise
    """
    for dy in [-1, 1]:
        y: int = row + dy
        if y < 0 or y >= world_map.shape[0]:
            return False
        if world_map[y, col] != "#":
            return False

    for dx in [-1, 1]:
        x: int = col + dx
        if x < 0 or x >= world_map.shape[1]:
            return False
        if world_map[row, x] != "#":
            return False

    return True


def get_neighbors(
    world_map: np.ndarray, node: Tuple[int, int]
) -> List[Tuple[int, int]]:
    """Return neighbors of node.

    Args:
      world_map (np.ndarray): World map
      node (Tuple[int, int]): Point to return neighbors for

    Returns:
      neighbors (List[Tuple[int, int]]): Neighbors of given point
    """
    neighbors: List[Tuple[int, int]] = []
    y, x = node

    for dy in [-1, 1]:
        n_y: int = y + dy
        if n_y < 0 or n_y >= world_map.shape[0]:
            continue
        if world_map[n_y, x] == "#":
            neighbors.append((n_y, x))

    for dx in [-1, 1]:
        n_x: int = x + dx
        if n_x < 0 or n_x >= world_map.shape[1]:
            continue
        if world_map[y, n_x] == "#":
            neighbors.append((y, n_x))

    return neighbors


def in_bounds(world_map: np.ndarray, pos: Tuple[int, int]) -> bool:
    """Determine if point is in bounds.

    Args:
      world_map (np.ndarray): World map
      pos (Tuple[int, int]): Point to check

    Returns:
      True if point is in bounds, False otherwise
    """
    y, x = pos

    if y < 0:
        return False

    if y >= world_map.shape[0]:
        return False

    if x < 0:
        return False

    if x >= world_map.shape[1]:
        return False

    return True


def find_path(world_map: np.ndarray, start_node: Tuple[int, int]) -> List[str]:
    """Find path through scaffolding maze.

    Args:
      world_map (np.ndarray): World map
      start_node (Tuple[int, int]): Starting point of robot

    Returns:
      path (List[str]): List of instructions through maze
    """
    # Start point
    pos: Tuple[int, int] = start_node
    # Start facing north
    direction: Tuple[int, int] = (-1, 0)

    path: List[str] = []
    magnitude: int = 0
    next_step: Tuple[int, int] = None
    while True:
        next_step = (pos[0] + direction[0], pos[1] + direction[1])

        if not in_bounds(world_map, next_step) or world_map[next_step] != "#":
            path.append(str(magnitude))
            magnitude = 0

            left_turn: Tuple[int, int] = turns[direction][Turn.LEFT]
            right_turn: Tuple[int, int] = turns[direction][Turn.RIGHT]

            left_tile: str = (pos[0] + left_turn[0], pos[1] + left_turn[1])
            right_tile: str = (pos[0] + right_turn[0], pos[1] + right_turn[1])

            if in_bounds(world_map, left_tile) and world_map[left_tile] == "#":
                # Turn left
                direction = left_turn
                pos = (pos[0] + direction[0], pos[1] + direction[1])
                magnitude += 1
                path.append("L")
            elif in_bounds(world_map, right_tile) and world_map[right_tile] == "#":
                # Turn right
                direction = right_turn
                pos = (pos[0] + direction[0], pos[1] + direction[1])
                magnitude += 1
                path.append("R")
            else:
                path.append(str(magnitude))
                return path[1:-1]
        else:
            magnitude += 1
            pos = (pos[0] + direction[0], pos[1] + direction[1])


def find_functions(path: List[str]) -> Tuple[List[str], List[str], List[str]]:
    """Find functions in path.

    Args:
      path (List[str]): Path leading through maze

    Returns:
      Tuple[List[str], List[str], List[str]]: A, B, and C functions in path
    """
    for a_length in range(4, 12, 2):
        a_p: List[str] = path
        A: List[str] = a_p[:a_length]
        while a_p[: len(A)] == A:
            a_p: List[str] = a_p[len(A) :]
        for b_length in range(4, 12, 2):
            b_p: List[str] = a_p
            B: List[str] = b_p[:b_length]
            while b_p[: len(B)] == B or b_p[: len(A)] == A:
                if b_p[: len(B)] == B:
                    b_p = b_p[len(B) :]
                if b_p[: len(A)] == A:
                    b_p = b_p[len(A) :]
            for c_length in range(4, 12, 2):
                c_p: List[str] = b_p
                C: List[str] = c_p[:c_length]
                while c_p[: len(C)] == C or c_p[: len(A)] == A or c_p[: len(B)] == B:
                    if c_p[: len(C)] == C:
                        c_p = c_p[len(C) :]
                    if c_p[: len(B)] == B:
                        c_p = c_p[len(B) :]
                    if c_p[: len(A)] == A:
                        c_p = c_p[len(A) :]
                if not c_p:
                    return A, B, C

    return None, None, None


def compress_path(path: List[str]) -> Tuple[List[str], List[str], List[str], List[str]]:
    """Compress path and find program.

    Args:
      path (List[str]): Path to compress

    Returns:
      program (List[str]): Program to run
      A (List[str]): Function A
      B (List[str]): Function B
      C (List[str]): Function C
    """
    A, B, C = find_functions(path)

    program: List[str] = []
    while path:
        if path[: len(A)] == A:
            program.append("A")
            path = path[len(A) :]
        if path[: len(B)] == B:
            program.append("B")
            path = path[len(B) :]
        if path[: len(C)] == C:
            program.append("C")
            path = path[len(C) :]

    return program, A, B, C


def main(args: Namespace):
    data: List[int] = parse_input(args.input)

    m: List[str] = [[]]

    def append_world_map(x: int):
        s: str = chr(x)
        if s == "\n":
            m.append([])
        else:
            m[-1].append(s)

    vm: IntcodeVM = IntcodeVM()
    vm.run(data, output_func=lambda x: append_world_map(x))

    m = [r for r in m if len(r) > 0]

    world_map: np.ndarray = np.array(m)

    # Part 1
    p1_start: datetime = datetime.now()

    p1: int = 0
    for row, col in np.argwhere(world_map == "#"):
        if is_intersection(world_map, row, col):
            p1 += row * col

    start_y, start_x = np.argwhere(world_map == "^")[0]

    p1_end: datetime = datetime.now()
    p1_duration: timedelta = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start: datetime = datetime.now()

    # Depth-first search to produce a list of steps
    start_y, start_x = np.argwhere(world_map == "^")[0]
    full_path: List[Tuple[int, int]] = find_path(world_map, (start_y, start_x))

    # Convert coordinates in path into discrete steps (e.g. up 3, down
    # 8, etc.)
    steps, A, B, C = compress_path(full_path)

    # print(steps)
    # print(A, len(A), len(A) * 2)
    # print(B, len(B), len(B) * 2)
    # print(C, len(C), len(C) * 2)

    data[0] = 2

    # breakpoint()

    def p2_input_generator():
        for item in [steps, A, B, C, "n"]:
            for idx, c in enumerate(item):
                for cc in c:
                    yield str(ord(cc))
                if idx < len(item) - 1:
                    yield str(ord(","))
            yield str(ord("\n"))

    g = p2_input_generator()

    def p2_input_func(*args, **kwargs):
        r = next(g)
        return r

    outputs: List[int] = []

    def p2_output_func(x: int):
        outputs.append(x)

    vm: IntcodeVM = IntcodeVM()
    vm.run(data, input_func=p2_input_func, output_func=p2_output_func)

    p2: int = outputs[-1]

    p2_end: datetime = datetime.now()
    p2_duration: timedelta = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args: Namespace = parse_args()
    main(args)
