#!/usr/bin/env python3
""" Advent of Code Day 9
"""
from argparse import ArgumentParser, Namespace
from datetime import datetime, timedelta
from pathlib import Path
from typing import List

import matplotlib.pyplot as plt
import numpy as np
from aoc.intcode import IntcodeVM
from tqdm import tqdm


def parse_args(arglist: List[str] = None) -> Namespace:
    """Parse and validate program arguments.

    Args:
      arglist (List[str], optional):
        Argument list, defaults to None (read from stdin)

    Returns:
      Namespace: Parsed and validated program arguments
    """
    argp: ArgumentParser = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def parse_input(input_path: Path) -> List[int]:
    with open(input_path, "r") as in_file:
        # Read input
        return list(map(int, in_file.read().strip().split(",")))


def main(args):
    data = parse_input(args.input)

    # Part 1
    p1_start: datetime = datetime.now()

    outputs: List[int] = []

    def output_func(x: int):
        outputs.append(x)

    vm: IntcodeVM = IntcodeVM()
    vm.run(data, input_func=lambda: "1", output_func=output_func)

    p1: int = outputs[-1]

    p1_end: datetime = datetime.now()
    p1_duration: timedelta = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start: datetime = datetime.now()

    vm.run(data, input_func=lambda: "2", output_func=output_func)
    p2: int = outputs[-1]

    p2_end: datetime = datetime.now()
    p2_duration: timedelta = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args: Namespace = parse_args()
    main(args)
