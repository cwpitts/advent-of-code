#!/usr/bin/env python3
""" Advent of Code Day 5
"""
from argparse import ArgumentParser, Namespace
from datetime import datetime, timedelta
from pathlib import Path
from typing import Callable, List

import matplotlib.pyplot as plt
import numpy as np
from aoc.intcode import IntcodeVM
from tqdm import tqdm


def parse_args(arglist: List[str] = None) -> Namespace:
    """Parse and validate program arguments.

    Args:
      arglist (List[str], optional):
        Argument list, defaults to None (read from stdin)

    Returns:
      Namespace: Parsed and validated program arguments
    """
    argp: ArgumentParser = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def parse_input(input_path: Path) -> List[int]:
    with open(input_path, "r") as in_file:
        # Read input
        return [int(x) for x in in_file.read().strip().split(",")]


def make_output_logger(output_list: List[int]) -> Callable:
    def log_output(x):
        output_list.append(x)

    return log_output

def main(args):
    data = parse_input(args.input)

    # Part 1
    p1_start: datetime = datetime.now()

    outputs_p1: List[int] = []

    vm: IntcodeVM = IntcodeVM()
    vm.run(data, input_func=lambda: "1", output_func=make_output_logger(outputs_p1))
    p1: int = outputs_p1[-1]

    p1_end: datetime = datetime.now()
    p1_duration: timedelta = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start: datetime = datetime.now()

    outputs_p2: List[int] = []
    vm.run(data, input_func=lambda: "5", output_func=make_output_logger(outputs_p2))

    p2: int = outputs_p2[-1]

    p2_end: datetime = datetime.now()
    p2_duration: timedelta = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args = parse_args()
    main(args)
