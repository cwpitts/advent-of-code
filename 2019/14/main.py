#!/usr/bin/env python3
""" Advent of Code Day 14
"""
import math
from argparse import ArgumentParser, Namespace
from datetime import datetime, timedelta
from pathlib import Path
from queue import Queue
from typing import Dict, List, NamedTuple, Tuple

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm


class Reaction(NamedTuple):
    """Reaction struct.

    Holds output type, quantity, inputs.

    """

    output_type: str
    output_quantity: int
    inputs: List[Tuple[str, int]]


def parse_args(arglist: List[str] = None) -> Namespace:
    """Parse and validate program arguments.

    Args:
      arglist (List[str], optional):
        Argument list, defaults to None (read from stdin)

    Returns:
      Namespace: Parsed and validated program arguments
    """
    argp: ArgumentParser = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def compute_cost(
    reactions: Dict[str, Reaction], final_type: str, final_count: int
) -> int:
    """Compute cost of target reaction.

    Args:
      reactions (Dict[str, Reaction]): Reaction dictionary
      final_type (str): Target type
      final_count (int): Target count of target type

    Returns
      cost (int): Cost in ORE to produce `final_count` of `final_type`

    Notes:
      Based on https://github.com/jeffjeffjeffrey/advent-of-code/blob/master/2019/day_14.ipynb

    """
    cost: int = 0
    leftovers: Dict[str, int] = {t: 0 for t in reactions}
    Q: Queue = Queue()
    Q.put((final_type, final_count))

    while not Q.empty():
        item_type, item_count = Q.get()
        if item_type == "ORE":
            cost += item_count
        elif item_count <= leftovers[item_type]:
            leftovers[item_type] -= item_count
        else:
            still_need: int = item_count - leftovers[item_type]
            r: Reaction = reactions[item_type]
            n_reactions_needed: int = math.ceil(still_need / r.output_quantity)
            for input_type, input_count in r.inputs:
                Q.put((input_type, input_count * n_reactions_needed))
            leftovers[item_type] = (r.output_quantity * n_reactions_needed) - still_need

    return cost


def parse_input(input_path: Path) -> Dict[str, Reaction]:
    """Parse input file.

    Args:
      input_path (Path): Path to input file

    Returns:
      reactions (Dict[str, Reaction]): Dicitonary of reactions

    """
    reactions: Dict[str, Reaction] = {}

    with open(input_path, "r") as in_file:
        # Read input
        for line in in_file:
            inputs, outputs = line.strip().split(" => ")
            output_count, output_type = outputs.split(" ")
            inputs = [tuple(inp.split(" "))[::-1] for inp in inputs.split(", ")]
            inputs = [(name, int(quantity)) for name, quantity in inputs]
            reactions[output_type] = Reaction(output_type, int(output_count), inputs)

    return reactions


def main(args: Namespace):
    """Main program logic.

    Args:
      args (Namespace): Program arg

    """
    reactions: Dict[str, Reaction] = parse_input(args.input)

    # Part 1
    p1_start: datetime = datetime.now()

    p1: int = compute_cost(reactions, "FUEL", 1)

    p1_end: datetime = datetime.now()
    p1_duration: timedelta = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start: datetime = datetime.now()

    collected_ore: int = 1_000_000_000_000
    start: int = collected_ore // p1
    end: int = start
    while compute_cost(reactions, "FUEL", end) < collected_ore:
        end *= 2

    # Binary search
    check: int = (end + start) // 2
    old_check: int = None

    while check != old_check:
        p: int = compute_cost(reactions, "FUEL", check)
        if p > collected_ore:
            end = check
        elif p < collected_ore:
            start = check

        old_check = check
        check = (end + start) // 2

    p2: int = start

    p2_end: datetime = datetime.now()
    p2_duration: timedelta = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args: Namespace = parse_args()
    main(args)
