"""AOC Game of Life problem framework.

This module provides an abstract framework for running Game of Life
problems in the Avent of Code.

"""
from abc import ABC, abstractmethod
from copy import deepcopy

import numpy as np


class Life(ABC):
    """Life abstract base class.

    This class is a framework for running the game of life problems,
    and allows the user to define their own update rule.

    """

    def __init__(self, initial_state: np.ndarray):
        """Life base class.

        Args:
          initial_state (np.ndarray): Initial state

        """
        self.state: np.ndarray = initial_state.copy()

    @abstractmethod
    def step(self):
        """Iterate the game of life one time.

        """
        raise NotImplementedError

    def run_for_n(self, n: int, verbose: bool = False):
        """Run an arbitraty number of iterations of the game.

        Args:
          n (int): Number of iterations
          verbose (bool, optional): Flag for verbose mode, defaults to False
        """
        for iteration in range(n):
            if verbose:
                print(f"Iteration {iteration}")

            self.step()

    def run_to_convergence(self, verbose: bool = False) -> int:
        """Run until convergence.

        Runs the game until the state converges.

        Args:
          verbose (bool, optional): Flag for verbose mode, defaults to False

        Returns:
          n_iterations (int): Number of iterations until convergence
        """
        old_state: np.ndarray = None

        n_iterations: int = 0

        while np.any(old_state != self.state):
            if verbose:
                print(f"Iteration {n_iterations + 1}")
            old_state = deepcopy(self.state)
            self.step()
            n_iterations += 1

            if verbose:
                print(self)

        return n_iterations

    def __repr__(self) -> str:
        """Build and return repr string.

        Returns:
          str: repr string representing the game
        """
        return repr(self.state)
