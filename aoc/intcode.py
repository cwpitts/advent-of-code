"""AOC IntcodeVM.

This module provides an Intcode VM for running the Intcode programs in
the Advent of Code.

"""
from abc import ABC, abstractmethod
from copy import deepcopy
from enum import Enum
from typing import Callable, List, NamedTuple


class Program:
    def __init__(self, memory: List[int], ip: int, relative_base: int):
        self._memory: List[int] = memory
        self.ip: int = ip
        self.relative_base: int = relative_base

    def halted(self):
        return self._memory[self.ip] == 99

    def __repr__(self) -> str:
        """Build and return repr string

        Returns:
          str: repr string

        """
        if len(self._memory) <= 20:
            mem: str = f"{self._memory}"
        else:
            mem: str = f"[{','.join(map(str, self._memory[:5]))},...,{','.join(map(str, self._memory[-5:]))}]"

        return f"Program:\nIP: {self.ip}\nRB: {self.relative_base}\n{mem}\n"

    def _extend_memory(self, size_needed: int):
        """Extend VM memory if needed.

        Args:
          size_needed (int): Size of memory needed

        """
        if size_needed >= len(self._memory):
            self._memory += [0 for _ in range(size_needed - len(self._memory) + 2)]

    def set_memory_value(self, dest: int, value: int):
        """Set value.

        Args:
          dest (int): Destination in memory
          value: (int): Value to set
        """
        self._extend_memory(dest)
        self._memory[dest] = value

    def get_memory_value(self, i: int) -> int:
        """Get value.

        Args:
          i (int): Destination in memory

        Returns:
          int: Value of memory[i]
        """
        self._extend_memory(i)
        return self._memory[i]

    def get_memory(self) -> List[int]:
        """Get memory of program.

        Returns:
          List[int]: Copy of memory of program

        """
        return deepcopy(self._memory)


class OpcodeType(Enum):
    """Opcode type enumeration.

    Contains all implemented opcode types.

    """

    HALT: int = 99
    ADD: int = 1
    MULT: int = 2
    STORE: int = 3
    OUTPUT: int = 4
    JUMP_IF_TRUE: int = 5
    JUMP_IF_FALSE: int = 6
    LESS_THAN: int = 7
    EQUALS: int = 8
    ADJUST_RELATIVE_BASE: int = 9


class Param(NamedTuple):
    """Parameter class.

    Contains a mode and value for a parameter.
    """

    mode: int
    value: int


class Opcode(ABC):
    """Opcode.

    Opcodes implement the functionality of the Intcode VM.

    """

    def __init__(self, *, left: Param = None, right: Param = None, dest: Param = None):
        """Opcode.

        Args:
          left (int, optional): Left operand, defaults to None
          right (int, optional): Right operand, defaults to None
          dest (int, optional): Destination, defaulst to None
        """
        self._left: Param = left
        self._right: Param = right
        self._dest: Param = dest

    def _resolve_dest_param(self, prog: Program, param: Param) -> int:
        """Resolve destination parameter with mode.

        Args:
          prog (Program): Program
          param (Param): Parameter

        Returns:
          int: Resolved value

        Notes:
        This function does something that seems kind of
        backwards. Since 0 is position mode, you might expect that it
        would return the memory location at the value, but since this
        is going to be used by the opcode to set the value, we want to
        return the actual value itself.

        """
        if param.mode == 0:
            # Position mode
            return param.value

        if param.mode == 2:
            # Relative mode
            return prog.relative_base + param.value

        # Immediate mode
        return prog.get_memory_value(param.value)

    def _resolve_operand_param(self, prog: Program, param: Param) -> int:
        """Resolve operand parameter with mode.

        Args:
          prog (Program): Program
          param (Param): Parameter

        Returns:
          int: Resolved value

        """
        if param.mode == 1:
            # Immediate mode
            return param.value

        if param.mode == 2:
            # Relative mode
            return prog.get_memory_value(prog.relative_base + param.value)

        # Position mode
        return prog.get_memory_value(param.value)

    def _resolve_param(self, prog: Program, param: Param) -> int:
        if param.mode == 0:
            # Position mode
            return prog.get_memory_value(param.value)
        if param.mode == 1:
            # Immediate mode
            return param.value
        if param.mode == 3:
            # Relative mode
            return prog.get_memory_value(param.value + prog.relative_base)

    @classmethod
    def make(
        cls, prog: Program, input_func: Callable, output_func: Callable
    ) -> "Opcode":
        """Read new Opcode subclass from program.

        Args:
          prog (Program): Intcode program
          input_func (Callable): Input function
          output_func (Callable): Output function

        Returns:
          Opcode: Concrete implementation of Opcode
        """
        digits: List[int] = list(
            map(int, str(prog.get_memory_value(prog.ip)).rjust(5, "0"))
        )
        opcode: int = int("".join(map(str, digits[-2:])))
        modes: List[int] = digits[:-2][::-1]

        if opcode == OpcodeType.HALT.value:
            return HaltOpcode()
        if opcode == OpcodeType.ADD.value:
            return AddOpcode(
                left=Param(modes[0], prog.get_memory_value(prog.ip + 1)),
                right=Param(modes[1], prog.get_memory_value(prog.ip + 2)),
                dest=Param(modes[2], prog.get_memory_value(prog.ip + 3)),
            )
        if opcode == OpcodeType.MULT.value:
            return MultOpcode(
                left=Param(modes[0], prog.get_memory_value(prog.ip + 1)),
                right=Param(modes[1], prog.get_memory_value(prog.ip + 2)),
                dest=Param(modes[2], prog.get_memory_value(prog.ip + 3)),
            )
        if opcode == OpcodeType.STORE.value:
            return StoreOpcode(
                dest=Param(modes[0], prog.get_memory_value(prog.ip + 1)),
                input_func=input_func,
            )
        if opcode == OpcodeType.OUTPUT.value:
            return OutputOpcode(
                left=Param(modes[0], prog.get_memory_value(prog.ip + 1)),
                output_func=output_func,
            )
        if opcode == OpcodeType.JUMP_IF_TRUE.value:
            return JITOpcode(
                left=Param(modes[0], prog.get_memory_value(prog.ip + 1)),
                right=Param(modes[1], prog.get_memory_value(prog.ip + 2)),
            )
        if opcode == OpcodeType.JUMP_IF_FALSE.value:
            return JIFOpcode(
                left=Param(modes[0], prog.get_memory_value(prog.ip + 1)),
                right=Param(modes[1], prog.get_memory_value(prog.ip + 2)),
            )
        if opcode == OpcodeType.LESS_THAN.value:
            return LessThanOpcode(
                left=Param(modes[0], prog.get_memory_value(prog.ip + 1)),
                right=Param(modes[1], prog.get_memory_value(prog.ip + 2)),
                dest=Param(modes[2], prog.get_memory_value(prog.ip + 3)),
            )
        if opcode == OpcodeType.EQUALS.value:
            return EqualsOpcode(
                left=Param(modes[0], prog.get_memory_value(prog.ip + 1)),
                right=Param(modes[1], prog.get_memory_value(prog.ip + 2)),
                dest=Param(modes[2], prog.get_memory_value(prog.ip + 3)),
            )
        if opcode == OpcodeType.ADJUST_RELATIVE_BASE.value:
            return ARBOpcode(left=Param(modes[0], prog.get_memory_value(prog.ip + 1)))

        raise RuntimeError(f"Unknown opcode {prog.get_memory_value(prog.ip)}")

    @abstractmethod
    def execute(self, prog: Program):
        """Execute opcode.

        Args:
          prog (Program): Intcode program
        """
        raise NotImplementedError()

    def __repr__(self) -> str:
        """Build and return repr string

        Returns:
          repr_s: repr string

        """
        cls_name: str = self.__class__.__name__
        repr_s: str = f"{cls_name}("

        param_s: List[str] = []

        if self._left is not None:
            param_s.append(f"left={self._left}")

        if self._right is not None:
            param_s.append(f"right={self._right}")

        if self._dest is not None:
            param_s.append(f"dest={self._dest}")

        repr_s += ",".join(param_s)

        repr_s += ")"

        return repr_s


class HaltOpcode(Opcode):
    """The halt opcode does nothing.

    This opcode does nothing, as it signifies program termination.

    """

    def execute(self, prog: Program):
        """Do nothing and do not increment the instruction pointer.

        Args:
          prog (Program): Program

        This opcode halts the program.
        """
        return


class AddOpcode(Opcode):
    """The addition opcode adds the operands together.

    This opcode increments the instruction pointer by four.

    """

    def execute(self, prog: Program):
        """Add the operands and store the result.

        Args:
          prog (Program): Program

        """
        d_op: int = self._resolve_dest_param(prog, self._dest)
        l_op: int = self._resolve_operand_param(prog, self._left)
        r_op: int = self._resolve_operand_param(prog, self._right)

        prog.set_memory_value(d_op, l_op + r_op)

        prog.ip += 4


class MultOpcode(Opcode):
    """The multiplication opcode multiplies the operands together.

    This opcode increments the instruction pointer by four.

    """

    def execute(self, prog: Program):
        """Multiply the operands and store the result.

        Args:
          prog (Program): Program

        """
        d_op: int = self._resolve_dest_param(prog, self._dest)
        l_op: int = self._resolve_operand_param(prog, self._left)
        r_op: int = self._resolve_operand_param(prog, self._right)

        prog.set_memory_value(d_op, l_op * r_op)

        prog.ip += 4


class StoreOpcode(Opcode):
    """The store opcode takes input and stores it.

    This opcode increments the instruction pointer by 2.

    """

    def __init__(
        self, dest: Param, input_func: Callable = input,
    ):
        """Store opcode.

        Args:
          dest (Param): Destination operand
          input_func (Callable, optional): Input function, defaults to input
        """
        self._input_func = input_func
        super().__init__(dest=dest)

    def execute(self, prog: Program):
        """Get input and store it.

        Args:
          prog (Program): Program

        """
        d_op: int = self._resolve_dest_param(prog, self._dest)
        prog.set_memory_value(d_op, int(self._input_func().strip()))

        prog.ip += 2


class OutputOpcode(Opcode):
    """Output the value.

    This opcode increments the instruction pointer by 2.

    """

    def __init__(
        self, left: Param, output_func: Callable = print,
    ):
        """Output opcode.

        Args:
          dest (Param): Destination parameter
          output_func (Callable, optional): Output function, defaults to print
        """
        self._output_func = output_func
        super().__init__(left=left)

    def execute(self, prog: Program):
        """Print output.

        Args:
          prog (Program): Program

        """
        l_op: int = self._resolve_operand_param(prog, self._left)
        self._output_func(l_op)

        prog.ip += 2


class JITOpcode(Opcode):
    """Jump-if-true opcode.

    This opcode moves the instruction pointer based on the truth of
    the first operand.

    """

    def execute(self, prog: Program):
        """Move instruction pointer if true.

        This opcode move the instruction pointer to the right operand
        if first operand results in true.

        Args:
          prog (Program): Program

        """
        l_op: int = self._resolve_operand_param(prog, self._left)
        r_op: int = self._resolve_operand_param(prog, self._right)

        if l_op != 0:
            prog.ip = r_op
        else:
            prog.ip += 3


class JIFOpcode(Opcode):
    """Jump-if-false opcode.

    This opcode moves the instruction pointer if false.
    """

    def execute(self, prog: Program):
        """Jump if false.

        This opcode moves the instruction pointer to the right operand
        if the left operand results in false.

        Args:
          prog (Program): Program

        """
        l_op: int = self._resolve_operand_param(prog, self._left)
        r_op: int = self._resolve_operand_param(prog, self._right)

        if l_op == 0:
            prog.ip = r_op
        else:
            prog.ip += 3


class LessThanOpcode(Opcode):
    """Less-than opcode.

    This opcode moves the instruction pointer by four.
    """

    def execute(self, prog: Program):
        """Less-than opcode.

        This opcode stores 1 in the destination if the left operand is
        less than the right operand, and 0 otherwise.

        Args:
          prog (Program): Program

        """
        d_op: int = self._resolve_dest_param(prog, self._dest)
        l_op: int = self._resolve_operand_param(prog, self._left)
        r_op: int = self._resolve_operand_param(prog, self._right)

        prog.set_memory_value(d_op, int(l_op < r_op))

        prog.ip += 4


class EqualsOpcode(Opcode):
    """Equals opcode.

    This opcode moves the instruction pointer by four.
    """

    def execute(self, prog: Program):
        """Equals opcode.

        This opcode stores 1 in the destination if the left operand is
        equal to the right operand, and 0 otherwise.

        Args:
          prog (Program): Program

        """
        d_op: int = self._resolve_dest_param(prog, self._dest)
        l_op: int = self._resolve_operand_param(prog, self._left)
        r_op: int = self._resolve_operand_param(prog, self._right)

        prog.set_memory_value(d_op, int(l_op == r_op))

        prog.ip += 4


class ARBOpcode(Opcode):
    """Adjust relative base for program.

    This opcode increments the instruction pointer by two.

    """

    def execute(self, prog: Program):
        """Execute opcode.

        Args: prog (Program): Program

        """
        l_op: int = self._resolve_operand_param(prog, self._left)

        prog.relative_base += l_op
        prog.ip += 2


class IntcodeVM:
    """The IntcodeVM runs Intcode programs.

    This implements all of the Intcode opcodes.
    """

    def __init__(self):
        """IntcodeVM.

        Hooray, no parameters!
        """
        self._prog: Program = None

    @property
    def prog(self) -> Program:
        return deepcopy(self._prog)

    def load_program(self, prog: List[int]):
        """Load program into machine.

        Args:
          prog (List[int]): Program

        """
        self._prog = Program(deepcopy(prog), 0, 0)

    def run_until_output(self, inp: str) -> int:
        """Give input, run until next output.

        Args:
          inp (str): Input to machine

        Returns:
          int: Output of machine, or None if halted

        """
        def input_func() -> str:
            return inp

        ret: int = None

        def output_func(x: int):
            nonlocal ret
            ret = x

        while not self._prog.halted():
            opcode: Opcode = Opcode.make(self._prog, input_func, output_func)
            opcode.execute(self._prog)
            if isinstance(opcode, OutputOpcode):
                return ret

        return None

    def run(
        self,
        prog: List[int],
        noun: int = None,
        verb: int = None,
        input_func: Callable = input,
        output_func: Callable = print,
    ) -> List[int]:
        """Execute a program.

        Args:
          prog (List[int]): Initial program memory
          noun (int, optional): Noun for program, defaults to None
          verb (int, optional): Verb for program, defaults to None
          input_func (Callable, optional):
            Function for receiving input, defaults to input
          output_func (Callable, optional)
            Function for sending output, defaults to print

        Returns:
          prog (List[int]): Mutated program
        """
        self.load_program(prog)
        if noun is not None:
            self._prog.set_memory_value(1, noun)
        if verb is not None:
            self._prog.set_memory_value(2, verb)

        while not self._prog.halted():
            opcode: Opcode = Opcode.make(self._prog, input_func, output_func)
            opcode.execute(self._prog)

        return self._prog
