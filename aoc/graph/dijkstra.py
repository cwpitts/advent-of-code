from abc import ABC, abstractmethod
from collections import defaultdict
from queue import PriorityQueue, Queue
from typing import Dict, List, Set, TypeVar, Union


class State(ABC):
    """State is an abstract class for expressing arbitrary graph
    problems.

    """
    T = TypeVar("T", bound="State")

    def __init__(self, cost: int):
        """
        Args:
          cost (int): Cost of state
        """
        self.cost = cost

    @property
    @abstractmethod
    def is_end_state(self) -> bool:
        """Flag for indicating whether or not this is the end state.

        Returns:
          bool: Whether or no this is the end state

        """
        raise NotImplementedError

    @property
    @abstractmethod
    def neighbors(self) -> List["State.T"]:
        """Get neighbors of current state.

        Returns:
          List[State]: List of possible next states
        """
        raise NotImplementedError

    @abstractmethod
    def __hash__(self) -> int:
        """Hash ID.

        Returns:
          int: Hash ID of state
        """
        raise NotImplementedError

    def __lt__(self, o: "State.T") -> bool:
        """Less than operator.

        Returns:
          bool: Whether or not this item is less than o
        """
        return self.cost < o.cost

    def __gt__(self, o: "State.T") -> bool:
        """Greater than operator.

        Returns:
          bool: Whether or not this item is greater than o
        """
        return self.cost > o.cost

    def __eq__(self, o: "State.T") -> bool:
        """Equality operator.

        Returns:
          bool: Whether or not this item is equal to o
        """
        return self.cost == o.cost

    def __ne__(self, o: "State.T") -> bool:
        """Inequality operator.

        Returns:
          bool: Whether or not this item is inequal to o

        Notes:
        This is implemented as the negation of the equality operator
        """
        return not self == o

    def __repr__(self) -> str:
        """Repr method for state

        """
        return f"{self.label}/{self.cost}"


def _path_from_previous(previous: Dict[str, State], node: State) -> List[State]:
    """Compute path from previous pointers

    Args:
      previous (Dict[str, State]): Previous pointers
      node (State): Final node

    Returns:
      path (List[State]): Path
    """
    path: List[State] = []

    while node:
        path.append(node)
        node = previous[node]

    return path[::-1]


def find_shortest_path(start_state: State, verbose=False) -> List[State]:
    """Find shortest path from start node to goal node.

    This method works without a graph, and explores nodes as it comes
    across them. Useful for problems where the graph either cannot be
    known in advance, or where it would be cost-prohibitive to do so.

    Args:
      start_state (State): Start state
      verbose (bool, optional): Run in verbose mode

    Returns:
      List[State]: Shortest path

    Notes:
    Nodes must be generated with knowledge of whether or not they are
    the end state, otherwise this method has no way of completing. Nodes
    must also be able to compute their own neighbors (next viable states).

    """
    visited: Set[State] = set()
    lowest_costs: Dict[State, int] = defaultdict(lambda: 1 << 64)
    previous_states: Dict[State, Union[State, None]] = {start_state: None}

    Q: PriorityQueue[State] = PriorityQueue()
    Q.put((start_state.cost, start_state))
    while Q:
        if verbose:
            print(f"Checked {len(visited)} nodes, Currently have {len(Q.queue)} nodes to check")
        _, u = Q.get()

        print(f"Current best cost for node: {sum([node.cost for node in _path_from_previous(previous_states, u)])}")

        if u.is_end_state:
            if verbose:
                print("Found end state:")
                print(u)
            return _path_from_previous(previous_states, u)

        for v in u.neighbors:
            if v in visited:
                continue

            if v.cost >= lowest_costs[v]:
                continue

            # Shouldn't we know the cost? No, these states are generated on
            # the fly, and you might find a more efficient state path later.
            lowest_costs[v] = v.cost
            Q.put((v.cost, v))
            previous_states[v] = u

        visited.add(u)
