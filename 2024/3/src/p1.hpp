#ifndef AOC_2024_3_P1_HPP
#define AOC_2024_3_P1_HPP

#include <string>
#include <vector>

long long do_p1(const std::vector<std::string>& data);

#endif  // AOC_2024_3_P1_HPP
