#include "p2.hpp"

#include <regex>

const std::regex OP_REGEX("(don\'t|do|mul)(\\(([0-9]+),([0-9]+)\\)|\\(\\))");

long long do_p2(const std::vector<std::string>& data)
{
  long long result = 0;
  bool enabled = true;

  for (const std::string& line : data)
  {
    for (std::sregex_iterator itr(line.begin(), line.end(), OP_REGEX);
         itr != std::sregex_iterator(); itr++)
    {
      std::smatch match = *itr;
      std::string match_str = match.str();

      std::string op = match[1].str();

      if (op == "do")
      {
        enabled = true;
      }
      else if (op == "don't")
      {
        enabled = false;
      }
      else if (enabled and op == "mul")
      {
        long long arg1 = std::atoll(match[3].str().c_str());
        long long arg2 = std::atoll(match[4].str().c_str());
        result += arg1 * arg2;
      }
    }
  }

  return result;
}
