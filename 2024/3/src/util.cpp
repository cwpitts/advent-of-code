#include "util.hpp"

#include <fstream>

std::vector<std::string> readInput(fs::path inputPath)
{
  std::vector<std::string> data;

  std::ifstream in_file(inputPath);

  std::string line;

  while (std::getline(in_file, line))
  {
    data.push_back(line);
  }

  in_file.close();

  return data;
}
