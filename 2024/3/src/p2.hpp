#ifndef AOC_2024_3_P2_HPP
#define AOC_2024_3_P2_HPP

#include <string>
#include <vector>

long long do_p2(const std::vector<std::string>& data);

#endif  // AOC_2024_3_P2_HPP
