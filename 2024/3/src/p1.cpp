#include "p1.hpp"

#include <cstdlib>
#include <regex>

#include "fmt/format.h"

const std::regex MUL_REGEX("mul\\(([0-9]+),([0-9]+)\\)");

long long do_p1(const std::vector<std::string>& data)
{
  long long result = 0;

  for (const std::string& line : data)
  {
    for (std::sregex_iterator itr(line.begin(), line.end(), MUL_REGEX);
         itr != std::sregex_iterator(); itr++)
    {
      std::smatch match = *itr;
      std::string match_str = match.str();
      long long op1 = std::atoll(match[1].str().c_str());
      long long op2 = std::atoll(match[2].str().c_str());
      result += op1 * op2;
    }
  }

  return result;
}
