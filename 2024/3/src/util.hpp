#ifndef AOC_2024_3_UTIL_HPP
#define AOC_2024_3_UTIL_HPP

#include <filesystem>
#include <string>
#include <vector>

namespace fs = std::filesystem;

std::vector<std::string> readInput(fs::path inputPath);

#endif  // AOC_2024_3_UTIL_HPP
