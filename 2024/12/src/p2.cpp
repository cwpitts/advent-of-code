#include "p2.hpp"

#include "fmt/core.h"
#include "xtensor/xpad.hpp"

long long do_p2(const Data& data)
{
  long long p2 = 0;

  region_map regions = find_regions(data.grid);

  // With the perimeters in hand, we can find the corners.
  // clang-format off
	/*
		What is a corner? Some examples (O is outer corner, I is inner
		corner, P is non-corner perimeter, C is the region, . is
		everything else):

		There are four outer corner possibilities:
    ..  ..  C.  .C
		.C  C.  ..  ..

		And four inner corner possibilities:

		CC  CC  .C  C.
		C.  .C  CC  CC

  */
  // clang-format on
  const xt::xarray<char> padded_grid =
      xt::pad(data.grid, {{1, 1}, {1, 1}}, xt::pad_mode::constant, '.');

  for (region_map::iterator itr = regions.begin(); itr != regions.end(); itr++)
  {
    region current_region = itr->second;

    const char c = current_region.type;

    unsigned int n_corners = 0;

    for (coord p : current_region.coords)
    {
      p.row += 1;
      p.col += 1;

      // P.
      // ..
      n_corners += padded_grid.at(p.row + 1, p.col) != c and
                   padded_grid.at(p.row, p.col + 1) != c;

      // .P
      // ..
      n_corners += padded_grid.at(p.row + 1, p.col) != c and
                   padded_grid.at(p.row, p.col - 1) != c;

      // ..
      // .P
      n_corners += padded_grid.at(p.row - 1, p.col) != c and
                   padded_grid.at(p.row, p.col - 1) != c;

      // ..
      // P.
      n_corners += padded_grid.at(p.row - 1, p.col) != c and
                   padded_grid.at(p.row, p.col + 1) != c;

      // PC
      // C.
      n_corners += padded_grid.at(p.row + 1, p.col) == c and
                   padded_grid.at(p.row, p.col + 1) == c and
                   padded_grid.at(p.row + 1, p.col + 1) != c;

      // CP
      // .C
      n_corners += padded_grid.at(p.row + 1, p.col) == c and
                   padded_grid.at(p.row, p.col - 1) == c and
                   padded_grid.at(p.row + 1, p.col - 1) != c;

      // .C
      // CP
      n_corners += padded_grid.at(p.row - 1, p.col) == c and
                   padded_grid.at(p.row, p.col - 1) == c and
                   padded_grid.at(p.row - 1, p.col - 1) != c;

      // C.
      // PC
      n_corners += padded_grid.at(p.row - 1, p.col) == c and
                   padded_grid.at(p.row, p.col + 1) == c and
                   padded_grid.at(p.row - 1, p.col + 1) != c;
    }

    p2 += current_region.coords.size() * n_corners;
  }

  return p2;
}
