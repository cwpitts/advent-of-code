#ifndef AOC_2024_12_UTIL_HPP
#define AOC_2024_12_UTIL_HPP

#include <filesystem>
#include <map>
#include <set>
#include <string>
#include <vector>

#include "xtensor/xarray.hpp"

namespace fs = std::filesystem;

struct Data
{
  xt::xarray<char> grid;
};

struct coord
{
  int row;
  int col;
};

struct region
{
  char type;
  std::set<coord> coords;
};

using region_map = std::map<unsigned int, region>;

Data read_input(fs::path input_path);

coord operator+(const coord first, const coord second);

coord operator-(const coord first, const coord second);

bool operator<(const coord& first, const coord& second);

bool operator==(const coord& first, const coord& second);

bool in_grid(xt::xarray<char> grid, coord c);

region_map find_regions(const xt::xarray<char>& grid);

#endif  // AOC_2024_12_UTIL_HPP
