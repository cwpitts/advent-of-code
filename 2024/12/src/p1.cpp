#include "p1.hpp"

#include <map>

#include "fmt/format.h"
#include "util.hpp"

long long do_p1(const Data& data)
{
  long long p1 = 0;

  region_map regions = find_regions(data.grid);

  for (region_map::iterator itr = regions.begin(); itr != regions.end(); itr++)
  {
    region current_region = itr->second;

    // What is the perimeter? It's the unique set of neighbors that
    // are found by going up/down/left/right from each grid in the
    // region that are not part of the region itself.
    unsigned int perimeter = 0;

    for (const coord& loc : current_region.coords)
    {
      std::vector<coord> neighbors{
          {loc.row - 1, loc.col},
          {loc.row + 1, loc.col},
          {loc.row, loc.col - 1},
          {loc.row, loc.col + 1},
      };

      for (const coord& neighbor : neighbors)
      {
        if (not current_region.coords.contains(neighbor))
        {
          perimeter += 1;
        }
      }
    }

    p1 += perimeter * current_region.coords.size();
  }

  return p1;
}
