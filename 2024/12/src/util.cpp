#include "util.hpp"

#include <fstream>
#include <queue>
#include <string>
#include <vector>

#include "fmt/format.h"
#include "xtensor/xadapt.hpp"
#include "xtensor/xbuilder.hpp"

Data read_input(fs::path input_path)
{
  std::vector<char> data;
  size_t row_count = 0;
  size_t col_count = 0;

  std::ifstream in_file(input_path);

  std::string line;
  while (std::getline(in_file, line))
  {
    if (line == "")
    {
      break;
    }

    col_count = 0;

    for (const char c : line)
    {
      data.push_back(c);
      col_count += 1;
    }

    row_count += 1;
  }

  return Data{xt::adapt(data, {row_count, col_count})};
}

coord operator+(const coord first, const coord second)
{
  return coord{first.row + second.row, first.col + second.col};
}

coord operator-(const coord first, const coord second)
{
  return coord{first.row - second.row, first.col - second.col};
}

bool operator<(const coord& first, const coord& second)
{
  if (first.row < second.row)
  {
    return true;
  }

  return (first.row == second.row) and (first.col < second.col);
}

bool operator==(const coord& first, const coord& second)
{
  return first.row == second.row and first.col == second.col;
}

bool in_grid(xt::xarray<char> grid, coord c)
{
  return (c.row >= 0 and c.row < (int)grid.shape(0) and c.col >= 0 and
          c.col < (int)grid.shape(1));
}

region_map find_regions(const xt::xarray<char>& grid)
{
  region_map regions;
  unsigned int region_idx = 0;

  std::set<coord> visited;

  // For each cell, if it's not already in a region, start a new
  // region set and search until you can't find any more of the same
  // type that aren't in a region.
  for (size_t i = 0; i < grid.shape(0); i += 1)
  {
    for (size_t j = 0; j < grid.shape(1); j += 1)
    {
      coord loc = coord{int(i), int(j)};

      if (visited.contains(loc))
      {
        continue;
      }

      char c = grid.at(loc.row, loc.col);

      std::set<coord> new_region;

      std::queue<coord> queue;
      queue.push(loc);

      while (not queue.empty())
      {
        coord p = queue.front();
        queue.pop();

        if (not in_grid(grid, p))
        {
          continue;
        }

        if (grid.at(p.row, p.col) != c)
        {
          continue;
        }

        if (visited.contains(p))
        {
          continue;
        }

        // Push all neighbors onto queue
        std::vector<coord> neighbors{
            {p.row - 1, p.col},
            {p.row + 1, p.col},
            {p.row, p.col - 1},
            {p.row, p.col + 1},
        };
        for (coord neighbor : neighbors)
        {
          if (visited.contains(neighbor))
          {
            continue;
          }

          if (not in_grid(grid, neighbor))
          {
            continue;
          }

          queue.push(neighbor);
        }

        new_region.insert(p);
        visited.insert(p);
      }

      regions[region_idx] = {c, new_region};
      region_idx += 1;
    }
  }
  return regions;
}
