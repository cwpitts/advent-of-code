#include "p2.hpp"

#include <algorithm>
#include <iostream>
#include <optional>

#include "graaflib/algorithm/topological_sorting/dfs_topological_sorting.h"
#include "graaflib/types.h"
#include "p1.hpp"

long long do_p2(const Data& data)
{
  long long p2 = 0;

  // Can't do a topological sort for this, since the graph may not
  // have a topological sort.

  // Bubble sort may work here.
  // While vector is not sorted:
  //   For each element in vector:
  //     If the element belongs below the object below it, move it

  for (std::vector<int> update : data.updates)
  {
    if (not is_valid(update, data.graph, data.node_label_map))
    {
      while (not is_valid(update, data.graph, data.node_label_map))
      {
        for (size_t idx = 0; idx < update.size() - 1; idx += 1)
        {
          if (data.graph.has_edge(data.node_label_map.at(update[idx + 1]),
                                  data.node_label_map.at(update[idx])))
          {
            int tmp = update[idx + 1];
            update[idx + 1] = update[idx];
            update[idx] = tmp;
          }
        }
      }

      size_t midpoint = update.size() / 2;
      p2 += update[midpoint];
    }
  }

  return p2;
}
