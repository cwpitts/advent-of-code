#ifndef AOC_2024_5_P1_HPP
#define AOC_2024_5_P1_HPP

#include "util.hpp"

bool is_valid(const std::vector<int>& update, const graph_type& graph,
              const std::map<int, int> node_label_map);

long long do_p1(const Data& data);

#endif  // AOC_2024_5_P1_HPP
