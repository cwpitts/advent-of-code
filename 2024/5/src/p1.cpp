#include "p1.hpp"

#include <optional>

#include "fmt/core.h"
#include "graaflib/algorithm/topological_sorting/dfs_topological_sorting.h"
#include "graaflib/types.h"

bool is_valid(const std::vector<int>& update, const graph_type& graph,
              const std::map<int, int> node_label_map)
{
  // There should never be a path from an item to anything before it in the
  // list
  for (size_t idx1 = 0; idx1 < update.size(); idx1 += 1)
  {
    int item1 = update[idx1];
    for (size_t idx2 = idx1 + 1; idx2 < update.size(); idx2 += 1)
    {
      int item2 = update[idx2];
      if (graph.has_edge(node_label_map.at(item2), node_label_map.at(item1)))
      {
        return false;
      }
    }
  }

  return true;
}

long long do_p1(const Data& data)
{
  long long p1 = 0;

  for (const std::vector<int>& update : data.updates)
  {
    if (is_valid(update, data.graph, data.node_label_map))
    {
      size_t midpoint = update.size() / 2;
      p1 += update[midpoint];
    }
  }

  return p1;
}
