#ifndef AOC_2024_5_UTIL_HPP
#define AOC_2024_5_UTIL_HPP

#include <filesystem>
#include <map>
#include <ostream>
#include <set>
#include <string>
#include <vector>

#include "graaflib/graph.h"

namespace fs = std::filesystem;

using graph_type = graaf::graph<int, int, graaf::graph_type::DIRECTED>;

struct Data
{
  graph_type graph;
  std::vector<std::vector<int>> updates;
  std::map<int, int> node_label_map;
};

Data read_input(fs::path input_path);

std::ostream& operator<<(std::ostream& o, const std::vector<int>& v);

#endif  // AOC_2024_5_UTIL_HPP
