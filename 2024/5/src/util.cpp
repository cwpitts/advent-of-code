#include "util.hpp"

#include <fstream>
#include <sstream>
#include <string>

#include "fmt/format.h"

Data read_input(fs::path input_path)
{
  Data data;

  std::ifstream in_file(input_path);

  std::string line;

  // Parse edges
  while (std::getline(in_file, line))
  {
    if (line == "")
    {
      break;
    }

    unsigned int split_idx = line.find("|");

    unsigned int dst = std::stoul(line.substr(0, split_idx));
    unsigned int src = std::stoul(line.substr(split_idx + 1));

    if (not data.node_label_map.contains(src))
    {
      data.node_label_map.insert({src, data.graph.add_vertex(src)});
    }
    if (not data.node_label_map.contains(dst))
    {
      data.node_label_map.insert({dst, data.graph.add_vertex(dst)});
    }

    data.graph.add_edge(data.node_label_map[dst], data.node_label_map[src], 1);
  }

  // Parse updates
  while (std::getline(in_file, line))
  {
    if (line == "")
    {
      break;
    }

    std::vector<int> updates;
    std::istringstream iss(line);
    std::string item;
    while (std::getline(iss, item, ','))
    {
      updates.push_back(std::stoul(item));
    }
    data.updates.push_back(updates);
  }

  in_file.close();

  return data;
}

std::ostream& operator<<(std::ostream& o, const std::vector<int>& v)
{
  for (size_t i = 0; i < v.size(); i += 1)
  {
    o << v[i];

    if (i < v.size() - 1)
    {
      o << " ";
    }
  }

  return o;
}
