#ifndef AOC_2024_8_P2_HPP
#define AOC_2024_8_P2_HPP

#include "util.hpp"

long long do_p2(const Data& data);

#endif  // AOC_2024_8_P2_HPP
