#include "util.hpp"

#include <fstream>

#include "xtensor/xadapt.hpp"

Data read_input(fs::path input_path)
{
  std::vector<char> data;
  std::set<char> frequencies;

  std::ifstream in_file(input_path);
  std::string line;
  size_t row_count = 0;
  size_t col_count = 0;
  while (std::getline(in_file, line))
  {
    if (line == "")
    {
      break;
    }
    col_count = 0;

    for (const char c : line)
    {
      data.push_back(c);
      col_count += 1;

      if (c != '.')
      {
        frequencies.insert(c);
      }
    }

    row_count += 1;
  }
  in_file.close();

  return Data{xt::adapt(data, {row_count, col_count}), frequencies};
}

std::ostream& operator<<(std::ofstream& o, const Data& d)
{
  o << d.grid << std::endl;
  for (const char c : d.frequencies)
  {
    o << c << " ";
  }

  return o;
}

std::ostream& operator<<(std::ofstream& o, const std::set<char>& s)
{
  for (const char c : s)
  {
    o << c << " ";
  }

  o << std::endl;

  return o;
}

std::pair<int, int> operator-(const std::pair<int, int> first,
                              const std::pair<int, int> second)
{
  return std::pair<int, int>{first.first - second.first,
                             first.second - second.second};
}

std::pair<int, int> operator+(const std::pair<int, int> first,
                              const std::pair<int, int> second)
{
  return std::pair<int, int>{first.first + second.first,
                             first.second + second.second};
}

bool in_grid(const xt::xarray<char>& grid, std::pair<int, int> point)
{
  return point.first >= 0 and point.first < (int)(grid.shape(1)) and
         point.second >= 0 and point.second < (int)(grid.shape(0));
}
