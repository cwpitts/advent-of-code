#ifndef AOC_2024_8_UTIL_HPP
#define AOC_2024_8_UTIL_HPP

#include <filesystem>
#include <fstream>
#include <ostream>
#include <set>
#include <string>
#include <utility>
#include <vector>

#include "xtensor/xarray.hpp"
#include "xtensor/xio.hpp"

namespace fs = std::filesystem;

struct Data
{
  xt::xarray<char> grid;
  std::set<char> frequencies;
};

Data read_input(fs::path input_path);

std::ostream& operator<<(std::ofstream& o, const Data& d);

std::ostream& operator<<(std::ofstream& o, const std::set<char>& s);

std::pair<int, int> operator-(const std::pair<int, int> first,
                              const std::pair<int, int> second);

std::pair<int, int> operator+(const std::pair<int, int> first,
                              const std::pair<int, int> second);

bool in_grid(const xt::xarray<char>& grid, std::pair<int, int> point);

#endif  // AOC_2024_8_UTIL_HPP
