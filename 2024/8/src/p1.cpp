#include "p1.hpp"

#include <map>

#include "fmt/format.h"
#include "xtensor/xiterator.hpp"
#include "xtensor/xoperation.hpp"
#include "xtensor/xstorage.hpp"
#include "xtensor/xtensor_forward.hpp"

long long do_p1(const Data& data)
{
  std::set<std::pair<int, int>> points;

  // For each frequency, iterate over all pairs of locations, compute
  // slope, and project
  for (const char c : data.frequencies)
  {
    xt::xarray<bool> mask = xt::equal(data.grid, c);
    std::vector<xt::svector<std::size_t, 4>> indices = xt::argwhere(mask);

    // const xt::svector<std::size_t, 4>& idx : indices
    for (unsigned int i = 0; i < indices.size() - 1; i += 1)
    {
      std::pair<int, int> first_point{indices[i][0], indices[i][1]};
      for (unsigned int j = i + 1; j < indices.size(); j += 1)
      {
        std::pair<int, int> second_point{indices[j][0], indices[j][1]};

        // Compute slope
        std::pair<int, int> slope = first_point - second_point;

        // If point is in grid, add to locations
        std::pair<int, int> up_point = first_point + slope;
        std::pair<int, int> down_point = second_point - slope;

        if (in_grid(data.grid, up_point))
        {
          points.insert(up_point);
        }
        if (in_grid(data.grid, down_point))
        {
          points.insert(down_point);
        }
      }
    }
  }

  return points.size();
}
