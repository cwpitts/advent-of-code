#include "p1.hpp"

#include "util.hpp"

unsigned int advance_block_index(const std::vector<int>& data, int index)
{
  while (data[index] == -1)
  {
    index -= 1;
  }

  return index;
}

unsigned int advance_free_index(const std::vector<int>& data, int index)
{
  while (data[index] != -1)
  {
    index += 1;
  }

  return index;
}

long long do_p1(const std::vector<int>& data)
{
  std::vector<int> data_mutable = data;

  unsigned int block_index =
      advance_block_index(data_mutable, data_mutable.size() - 1);
  unsigned int free_index = advance_free_index(data_mutable, 0);

  while (free_index < block_index)
  {
    // Move to next slot that's non-empty
    block_index = advance_block_index(data_mutable, block_index);
    free_index = advance_free_index(data_mutable, free_index);

    if (not(free_index < block_index))
    {
      break;
    }

    data_mutable[free_index] = data_mutable[block_index];
    data_mutable[block_index] = -1;
  }

  long long p1 = 0;

  for (unsigned int i = 0; i < free_index; i += 1)
  {
    p1 += data_mutable[i] * i;
  }

  return p1;
}
