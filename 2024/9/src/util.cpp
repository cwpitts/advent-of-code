#include "util.hpp"

#include <fstream>
#include <iostream>
#include <string>

std::vector<int> read_input(fs::path input_path)
{
  std::vector<int> data;

  std::ifstream in_file(input_path);

  std::string line;
  std::getline(in_file, line);
  bool is_file = true;
  int id = 0;
  for (const char c : line)
  {
    for (unsigned int i = 0; i < c - '0'; i += 1)
    {
      if (is_file)
      {
        data.push_back(id);
      }
      else
      {
        data.push_back(-1);
      }
    }

    if (is_file)
    {
      id += 1;
    }

    is_file = not is_file;
  }

  in_file.close();

  return data;
}

void print_filesystem(const std::vector<int> data)
{
  for (int d : data)
  {
    if (d == -1)
    {
      std::cout << '.';
    }
    else
    {
      std::cout << d;
    }
  }
  std::cout << std::endl;
}
