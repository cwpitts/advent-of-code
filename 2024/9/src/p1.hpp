#ifndef AOC_2024_9_P1_HPP
#define AOC_2024_9_P1_HPP

#include <vector>

long long do_p1(const std::vector<int>& data);

#endif  // AOC_2024_9_P1_HPP
