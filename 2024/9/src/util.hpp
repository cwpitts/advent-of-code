#ifndef AOC_2024_9_UTIL_HPP
#define AOC_2024_9_UTIL_HPP

#include <filesystem>
#include <string>
#include <vector>

namespace fs = std::filesystem;

std::vector<int> read_input(fs::path input_path);

void print_filesystem(const std::vector<int> data);

#endif  // AOC_2024_9_UTIL_HPP
