#include "p2.hpp"

#include <vector>

#include "fmt/format.h"
#include "util.hpp"

int find_next_free_block(const std::vector<int>& data, unsigned int size)
{
  int index = 0;

  while (index < data.size())
  {
    // Find next free index
    while (data[index] != -1)
    {
      index += 1;
    }

    // Go until either the end or until we don't have a free block
    int index2 = index;
    while (data[index2] == -1 and index2 < data.size())
    {
      index2 += 1;
    }

    // Check size
    if (index2 - index >= size)
    {
      return index;
    }

    index += 1;
  }

  return -1;
}

long long do_p2(const std::vector<int>& data)
{
  std::vector<int> data_mutable = data;

  // Start from back, move until the index is at the start of the file
  int file_index = data_mutable.size() - 1;

  while (file_index > 0)
  {
    while (data_mutable[file_index] == -1)
    {
      file_index -= 1;
    }

    int file_id = data_mutable[file_index];
    int file_size = 1;

    while (data_mutable[file_index - 1] == file_id and file_index > 0)
    {
      file_index -= 1;
      file_size += 1;
    }

    int free_index = find_next_free_block(data_mutable, file_size);

    if (free_index == -1 or free_index > file_index)
    {
      file_index -= 1;
      while (data_mutable[file_index] == -1)
      {
        file_index -= 1;
      }

      continue;
    }

    for (unsigned int i = free_index; i < free_index + file_size; i += 1)
    {
      data_mutable[i] = file_id;
    }

    for (unsigned int i = file_index; i < file_index + file_size; i += 1)
    {
      data_mutable[i] = -1;
    }
  }

  long long p2 = 0;

  for (unsigned int i = 0; i < data_mutable.size(); i += 1)
  {
    if (data_mutable[i] == -1)
    {
      continue;
    }

    p2 += data_mutable[i] * i;
  }

  return p2;
}
