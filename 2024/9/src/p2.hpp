#ifndef AOC_2024_9_P2_HPP
#define AOC_2024_9_P2_HPP

#include <vector>

long long do_p2(const std::vector<int>& data);

#endif  // AOC_2024_9_P2_HPP
