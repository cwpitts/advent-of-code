#ifndef AOC_2024_10_P1_HPP
#define AOC_2024_10_P1_HPP

#include <vector>

#include "util.hpp"
#include "xtensor/xarray.hpp"

std::vector<std::vector<std::pair<int, int>>> find_trails(
    const xt::xarray<int> grid, std::vector<std::pair<int, int>> current_trail,
    std::pair<int, int> loc);

long long do_p1(const Data& data);

#endif  // AOC_2024_10_P1_HPP
