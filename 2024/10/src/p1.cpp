#include "p1.hpp"

#include <cstdlib>
#include <iostream>
#include <set>

#include "fmt/format.h"
#include "util.hpp"

std::vector<std::vector<std::pair<int, int>>> find_trails(
    const xt::xarray<int> grid, std::vector<std::pair<int, int>> current_trail,
    std::pair<int, int> loc)
{
  const std::vector<std::pair<int, int>> directions{
      {-1, 0},
      {1, 0},
      {0, -1},
      {0, 1},
  };

  current_trail.push_back(loc);

  if (grid.at(loc.first, loc.second) == 9)
  {
    return {current_trail};
  }

  std::vector<std::vector<std::pair<int, int>>> trails;

  for (const std::pair<int, int>& direction : directions)
  {
    std::pair<int, int> new_loc = loc + direction;
    if (not in_grid(grid, new_loc))
    {
      continue;
    }

    if (grid.at(new_loc.first, new_loc.second) -
            grid.at(loc.first, loc.second) !=
        1)
    {
      continue;
    }

    // Explore
    std::vector<std::vector<std::pair<int, int>>> paths =
        find_trails(grid, current_trail, new_loc);

    trails.insert(trails.end(), paths.begin(), paths.end());
  }

  return trails;
}

long long do_p1(const Data& data)
{
  long long p1 = 0;

  xt::xarray<bool> mask = xt::equal(data.grid, 0);
  std::vector<xt::svector<std::size_t, 4>> trailheads = xt::argwhere(mask);

  for (const xt::svector<std::size_t, 4>& idx : trailheads)
  {
    int row = idx[0];
    int col = idx[1];

    std::vector<std::vector<std::pair<int, int>>> trails =
        find_trails(data.grid, {}, std::pair<int, int>{row, col});

    std::set<std::pair<int, int>> unique_peaks;
    for (const std::vector<std::pair<int, int>> trail : trails)
    {
      unique_peaks.insert(trail[trail.size() - 1]);
    }
    p1 += unique_peaks.size();
  }

  return p1;
}
