#include "p2.hpp"

#include "p1.hpp"

long long do_p2(const Data& data)
{
  long long p2 = 0;

  xt::xarray<bool> mask = xt::equal(data.grid, 0);
  std::vector<xt::svector<std::size_t, 4>> trailheads = xt::argwhere(mask);

  for (const xt::svector<std::size_t, 4>& idx : trailheads)
  {
    int row = idx[0];
    int col = idx[1];

    std::vector<std::vector<std::pair<int, int>>> trails =
        find_trails(data.grid, {}, std::pair<int, int>{row, col});

    p2 += trails.size();
  }

  return p2;
}
