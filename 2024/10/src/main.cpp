/* Advent of Code 2024 problem 10
 */
#include <algorithm>
#include <chrono>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>

#include "CLI/CLI11.hpp"
#include "p1.hpp"
#include "p2.hpp"
#include "rang/rang.hpp"
#include "util.hpp"
#include "xtensor/xio.hpp"

using time_point = std::chrono::high_resolution_clock::time_point;
using duration = std::chrono::duration<double>;
using DoubleMilliseconds =
    std::chrono::duration<double, std::chrono::milliseconds::period>;

namespace fs = std::filesystem;

time_point clock_time() { return std::chrono::high_resolution_clock::now(); }

int main(int argc, char* argv[])
{
  CLI::App app;

  fs::path input_path;
  app.add_option("input", input_path, "Path to input data")
      ->required(true)
      ->check(CLI::ExistingFile);

  CLI11_PARSE(app, argc, argv);

  const Data data = read_input(input_path);

  std::cout << data.grid << std::endl;

  time_point p1_start = clock_time();
  long long p1 = do_p1(data);
  time_point p1_end = clock_time();
  duration p1_time = p1_end - p1_start;
  std::cout << rang::style::bold << p1 << rang::style::reset << " in "
            << rang::fg::green << DoubleMilliseconds(p1_end - p1_start)
            << rang::fg::reset << std::endl;

  time_point p2_start = clock_time();
  long long p2 = do_p2(data);
  time_point p2_end = clock_time();
  duration p2_time = p2_end - p2_start;
  std::cout << rang::style::bold << p2 << rang::style::reset << " in "
            << rang::fg::green << DoubleMilliseconds(p2_end - p2_start)
            << rang::fg::reset << std::endl;

  return 0;
}
