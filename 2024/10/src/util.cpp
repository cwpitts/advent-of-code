#include "util.hpp"

#include <fstream>
#include <iostream>
#include <string>

#include "xtensor/xadapt.hpp"

Data read_input(fs::path input_path)
{
  std::vector<int> grid;
  size_t row_count = 0;
  size_t col_count = 0;

  std::ifstream in_file(input_path);
  std::string line;

  while (std::getline(in_file, line))
  {
    if (line == "")
    {
      break;
    }

    col_count = 0;
    for (const char c : line)
    {
      grid.push_back(c - '0');
      col_count += 1;
    }

    row_count += 1;
  }

  in_file.close();

  return Data{xt::adapt(grid, {row_count, col_count})};
}

std::pair<int, int> operator-(const std::pair<int, int> first,
                              const std::pair<int, int> second)
{
  return std::pair<int, int>{first.first - second.first,
                             first.second - second.second};
}

std::pair<int, int> operator+(const std::pair<int, int> first,
                              const std::pair<int, int> second)
{
  return std::pair<int, int>{first.first + second.first,
                             first.second + second.second};
}

bool in_grid(const xt::xarray<int>& grid, std::pair<int, int> point)
{
  return point.first >= 0 and point.first < (int)(grid.shape(1)) and
         point.second >= 0 and point.second < (int)(grid.shape(0));
}
