#ifndef AOC_2024_10_UTIL_HPP
#define AOC_2024_10_UTIL_HPP

#include <filesystem>
#include <string>
#include <vector>

#include "xtensor/xarray.hpp"

namespace fs = std::filesystem;

struct Data
{
  xt::xarray<int> grid;
};

Data read_input(fs::path input_path);

std::pair<int, int> operator-(const std::pair<int, int> first,
                              const std::pair<int, int> second);

std::pair<int, int> operator+(const std::pair<int, int> first,
                              const std::pair<int, int> second);

bool in_grid(const xt::xarray<int>& grid, std::pair<int, int> point);

#endif  // AOC_2024_10_UTIL_HPP
