#include <gtest/gtest.h>

#include <cmath>
#include <cstdint>
#include <cstdio>
#include <filesystem>
#include <iterator>
#include <limits>
#include <memory>
#include <optional>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <typeinfo>
#include <vector>

#include "p1.hpp"
#include "p2.hpp"
#include "util.hpp"

namespace fs = std::filesystem;

const fs::path TEST_DIR = fs::current_path().parent_path() / "test";

class AOC2024Day11 : public ::testing::TestWithParam<
                         std::tuple<std::string, long long, long long>>
{
 protected:
};

TEST_P(AOC2024Day11, Part1)
{
  const std::string input = std::get<0>(GetParam());
  const long long expected_output = std::get<1>(GetParam());

  const std::vector<unsigned long long> data =
      read_input(TEST_DIR / "data" / input);

  const long long actual_output = do_p1(data);

  ASSERT_EQ(expected_output, actual_output);
}

TEST_P(AOC2024Day11, Part2)
{
  const std::string input = std::get<0>(GetParam());
  const long long expected_output = std::get<2>(GetParam());

  const std::vector<unsigned long long> data =
      read_input(TEST_DIR / "data" / input);

  const long long actual_output = do_p2(data);

  ASSERT_EQ(expected_output, actual_output);
}

INSTANTIATE_TEST_CASE_P(
    RunProgramTests, AOC2024Day11,
    ::testing::Values(std::make_tuple("test1", 55312, 65601038650482),
                      std::make_tuple("input", 228668, 270673834779359)));
