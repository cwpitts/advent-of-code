#include "p2.hpp"

#include <map>
#include <string>

const unsigned int NUM_ITERATIONS = 75;

long long do_p2(const std::vector<unsigned long long>& data)
{
  std::map<unsigned long long, unsigned long long> stones;

  for (const unsigned long long v : data)
  {
    stones[v] = 1;
  }

  for (unsigned int i = 0; i < NUM_ITERATIONS; i += 1)
  {
    std::map<unsigned long long, unsigned long long> new_stones;

    // Every transformation rule can be applied at once, because
    // position doesn't matter, only count, and the transformation is
    // the same for every stone where the rule applies.
    for (std::map<unsigned long long, unsigned long long>::iterator itr =
             stones.begin();
         itr != stones.end(); itr++)
    {
      unsigned long long stone = itr->first;
      unsigned long long count = itr->second;

      if (stone == 0)
      {
        new_stones[1] += count;
      }
      else if (std::to_string(stone).size() % 2 == 0)
      {
        std::string str = std::to_string(stone);
        unsigned int str_len = str.length();
        std::string left = str.substr(0, str_len / 2);
        std::string right = str.substr(str_len / 2, str_len / 2);

        while (right[0] == '0' and right.size() > 1)
        {
          right.erase(right.begin());
        }

        new_stones[std::stoull(left)] += count;
        new_stones[std::stoull(right)] += count;
      }
      else
      {
        new_stones[stone * 2024] += count;
      }
    }

    stones = new_stones;
  }

  long long p2 = 0;
  for (std::map<unsigned long long, unsigned long long>::iterator itr =
           stones.begin();
       itr != stones.end(); itr++)
  {
    p2 += itr->second;
  }
  return p2;
}
