#ifndef AOC_2024_11_UTIL_HPP
#define AOC_2024_11_UTIL_HPP

#include <filesystem>
#include <string>
#include <vector>

namespace fs = std::filesystem;

std::vector<unsigned long long> read_input(fs::path input_path);

#endif  // AOC_2024_11_UTIL_HPP
