#ifndef AOC_2024_11_P2_HPP
#define AOC_2024_11_P2_HPP

#include <vector>

long long do_p2(const std::vector<unsigned long long>& data);

#endif  // AOC_2024_11_P2_HPP
