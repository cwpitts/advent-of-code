#include "util.hpp"

#include <fstream>
#include <sstream>

std::vector<unsigned long long> read_input(fs::path input_path)
{
  std::vector<unsigned long long> data;

  std::ifstream in_file(input_path);

  std::string line;
  std::getline(in_file, line);
  std::istringstream iss(line);

  unsigned long long value;

  while (iss >> value)
  {
    data.push_back(value);
  }

  in_file.close();

  return data;
}
