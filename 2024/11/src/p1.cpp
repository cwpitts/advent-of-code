#include "p1.hpp"

#include <iostream>

const unsigned int NUM_ITERATIONS = 25;

std::vector<unsigned long long> sim(std::vector<unsigned long long> stones,
                                    unsigned int n_iterations)
{
  for (unsigned int i = 0; i < n_iterations; i += 1)
  {
    unsigned long long n_stones = stones.size();

    for (unsigned int j = 0; j < n_stones; j += 1)
    {
      if (stones[j] == 0)
      {
        stones[j] = 1;
      }
      else if (std::to_string(stones[j]).size() % 2 == 0)
      {
        std::string str = std::to_string(stones[j]);
        unsigned int str_len = str.length();
        std::string left = str.substr(0, str_len / 2);
        std::string right = str.substr(str_len / 2, str_len / 2);

        while (right[0] == '0' and right.size() > 1)
        {
          right.erase(right.begin());
        }

        stones[j] = std::stoull(left);
        stones.insert(stones.begin() + j + 1, std::stoull(right));

        j += 1;
        n_stones += 1;
      }
      else
      {
        stones[j] *= 2024;
      }
    }
  }

  return stones;
}

long long do_p1(const std::vector<unsigned long long>& data)
{
  std::vector<unsigned long long> result = sim(data, NUM_ITERATIONS);
  return result.size();
}
