#ifndef AOC_2024_11_P1_HPP
#define AOC_2024_11_P1_HPP

#include <vector>

std::vector<unsigned long long> sim(std::vector<unsigned long long> stones,
                                    unsigned int n_iterations);

long long do_p1(const std::vector<unsigned long long>& data);

#endif  // AOC_2024_11_P1_HPP
