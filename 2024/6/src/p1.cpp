#include "p1.hpp"

#include <set>
#include <utility>

long long do_p1(const Data& data)
{
  std::set<std::pair<unsigned int, unsigned int>> locations;

  Direction direction{data.init_dy, data.init_dx};

  int x = data.init_x;
  int y = data.init_y;

  while (y >= 0 and y < data.floor_map.shape(0) and x >= 0 and
         x < data.floor_map.shape(1))
  {
    // Add current location to set
    locations.insert({y, x});

    // Valid moves are proceed or turn if there's an obstacle
    int new_y = y + direction.dy;
    int new_x = x + direction.dx;
    if (new_y >= 0 and new_y < data.floor_map.shape(0) and new_x >= 0 and
        new_x < data.floor_map.shape(1))
    {
      while (data.floor_map.at(new_y, new_x) == '#')
      {
        direction = turn(direction);
        new_y = y + direction.dy;
        new_x = x + direction.dx;
      }
    }
    y += direction.dy;
    x += direction.dx;
  }

  return locations.size();
}
