#include "util.hpp"

#include <fstream>

#include "xtensor/xadapt.hpp"

Data read_input(fs::path input_path)
{
  Data data;

  std::vector<char> input_data;

  std::ifstream in_file(input_path);

  std::string line;
  unsigned int row_idx = 0;
  unsigned int col_idx = 0;

  while (std::getline(in_file, line))
  {
    if (line == "")
    {
      break;
    }

    for (col_idx = 0; col_idx < line.size(); col_idx += 1)
    {
      char c = line[col_idx];

      if (c == '^')
      {
        data.init_x = col_idx;
        data.init_y = row_idx;
        c = '.';
      }

      input_data.push_back(c);
    }

    row_idx += 1;
  }

  in_file.close();

  data.floor_map = xt::adapt(input_data, {row_idx, col_idx});
  data.init_dy = -1;
  data.init_dx = 0;

  return data;
}

std::ostream& operator<<(std::ostream& o, const Data& data)
{
  o << "Map size: " << data.floor_map.shape(0) << "x" << data.floor_map.shape(1)
    << std::endl;

  for (int i = 0; i < data.floor_map.shape(0); i += 1)
  {
    for (int j = 0; j < data.floor_map.shape(1); j += 1)
    {
      if (i == data.init_y and j == data.init_x)
      {
        o << 'x';
      }
      else
      {
        o << data.floor_map.at(i, j);
      }
    }
    o << std::endl;
  }

  return o;
}

Direction turn(const Direction direction)
{
  if (direction.dy == -1 and direction.dx == 0)
  {
    return Direction(0, 1);
  }

  if (direction.dy == 0 and direction.dx == 1)
  {
    return Direction(1, 0);
  }

  if (direction.dy == 1 and direction.dx == 0)
  {
    return Direction(0, -1);
  }

  // Final case is direction.dy == 0 and direction.dx == -1
  return Direction(-1, 0);
}
