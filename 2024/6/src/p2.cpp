#include "p2.hpp"

#include <cstddef>
#include <iostream>
#include <map>
#include <set>
#include <typeinfo>

#include "fmt/format.h"
#include "xtensor/xarray.hpp"
#include "xtensor/xsort.hpp"
#include "xtensor/xstrides.hpp"
#include "xtensor/xtensor.hpp"
#include "xtensor/xview.hpp"

long long do_p2(const Data& data)
{
  long long p2 = 0;

  // For each i,j index place an obstacle in a copy of the map, then
  // run and see if we have a loop.  For brute-force loop detection,
  // say a loop occurred if a position gets visited 4+ times.

  for (size_t i = 0; i < data.floor_map.shape(0); i += 1)
  {
    for (size_t j = 0; j < data.floor_map.shape(1); j += 1)
    {
      if (data.floor_map.at(i, j) != '.')
      {
        continue;
      }

      if (i == data.init_y and j == data.init_x)
      {
        continue;
      }

      // Copy data
      Data data_sim = data;
      data_sim.floor_map.at(i, j) = '#';

      std::set<std::tuple<int, int, int, int>> history;

      Direction direction{data_sim.init_dy, data_sim.init_dx};

      int x = data_sim.init_x;
      int y = data_sim.init_y;

      while (y >= 0 and y < data_sim.floor_map.shape(0) and x >= 0 and
             x < data_sim.floor_map.shape(1))
      {
        if (history.contains({y, x, direction.dy, direction.dx}))
        {
          p2 += 1;
          break;
        }
        history.insert({y, x, direction.dy, direction.dx});

        // Valid moves are proceed or turn if there's an obstacle
        int new_y = y + direction.dy;
        int new_x = x + direction.dx;
        if (new_y >= 0 and new_y < data_sim.floor_map.shape(0) and
            new_x >= 0 and new_x < data_sim.floor_map.shape(1))
        {
          while (data_sim.floor_map.at(new_y, new_x) == '#')
          {
            direction = turn(direction);
            new_y = y + direction.dy;
            new_x = x + direction.dx;
          }
        }

        y += direction.dy;
        x += direction.dx;
      }
    }
  }

  return p2;
}
