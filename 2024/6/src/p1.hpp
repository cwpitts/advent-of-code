#ifndef AOC_2024_6_P1_HPP
#define AOC_2024_6_P1_HPP

#include "util.hpp"

long long do_p1(const Data& data);

#endif  // AOC_2024_6_P1_HPP
