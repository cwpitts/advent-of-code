#ifndef AOC_2024_6_UTIL_HPP
#define AOC_2024_6_UTIL_HPP

#include <filesystem>
#include <ostream>
#include <string>
#include <vector>

#include "xtensor/xio.hpp"
#include "xtensor/xslice.hpp"
#include "xtensor/xtensor.hpp"
#include "xtensor/xtensor_forward.hpp"
#include "xtensor/xview.hpp"

namespace fs = std::filesystem;

struct Direction
{
  int dy;
  int dx;
};

struct Data
{
  xt::xarray<char> floor_map;
  int init_dy;
  int init_dx;
  int init_x;
  int init_y;
};

Direction turn(const Direction direction);

Data read_input(fs::path input_path);

std::ostream& operator<<(std::ostream& o, const Data& data);

#endif  // AOC_2024_6_UTIL_HPP
