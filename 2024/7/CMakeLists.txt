cmake_minimum_required(VERSION 3.22)
set(PROJECT_NAME aoc-2024-7)
project(
	${PROJECT_NAME}
	LANGUAGES CXX
	DESCRIPTION "Advent of Code Day 7"
)
if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release)
endif()
set(CMAKE_CXX_FLAGS "-Wall -Wextra")
set(CMAKE_CXX_FLAGS_DEBUG "-g")
set(CMAKE_CXX_FLAGS_RELEASE "-O3")
set(CMAKE_CXX_STANDARD 20)

include(FetchContent)

FetchContent_Declare(fmt
  GIT_REPOSITORY https://github.com/fmtlib/fmt.git
  GIT_TAG 10.1.1
)
FetchContent_MakeAvailable(fmt)

FetchContent_Declare(
  spdlog
  GIT_REPOSITORY https://github.com/gabime/spdlog.git
  GIT_TAG v1.11.0
)
FetchContent_MakeAvailable(spdlog)

# https://github.com/xtensor-stack/xtensor/issues/2802
find_package(xtl QUIET)
if(NOT xtensor_FOUND)
    message(STATUS "xtl not found, attempting to access it via FetchContent")
    FetchContent_Declare(xtl GIT_REPOSITORY https://github.com/xtensor-stack/xtl.git GIT_TAG master)
    FetchContent_MakeAvailable(xtl)
else()
    message(STATUS "Found xtl library.")
endif()

# https://github.com/xtensor-stack/xtensor/issues/2802
find_package(xtensor QUIET)
if(NOT xtensor_FOUND)
    message(STATUS "xtensor not found, attempting to access it via FetchContent")
    FetchContent_Declare(xtensor GIT_REPOSITORY https://github.com/xtensor-stack/xtensor.git GIT_TAG master)
    FetchContent_MakeAvailable(xtensor)
else()
    message(STATUS "Found xtensor library.")
endif()

set(SRC
  ${CMAKE_CURRENT_LIST_DIR}/src/p1.cpp
  ${CMAKE_CURRENT_LIST_DIR}/src/p2.cpp
  ${CMAKE_CURRENT_LIST_DIR}/src/util.cpp
	${CMAKE_CURRENT_LIST_DIR}/src/main.cpp)
set(THIRDPARTY
  ${CMAKE_CURRENT_LIST_DIR}/thirdparty)

add_executable(aoc-2024-7
	${SRC})
target_link_libraries(aoc-2024-7
  fmt::fmt
  spdlog::spdlog
  xtensor)
target_include_directories(aoc-2024-7
  PRIVATE
  ${THIRDPARTY}/include
  ${xtensor_INCLUDE_DIRS})



FetchContent_Declare(
  googletest
  URL https://github.com/google/googletest/archive/03597a01ee50ed33e9dfd640b249b4be3799d395.zip)
FetchContent_MakeAvailable(googletest)

include(CTest)
set(TEST_DIR ${CMAKE_CURRENT_LIST_DIR}/test)

add_executable(test2024-7
  ${CMAKE_CURRENT_LIST_DIR}/src/p1.cpp
  ${CMAKE_CURRENT_LIST_DIR}/src/p2.cpp
  ${CMAKE_CURRENT_LIST_DIR}/src/util.cpp
  ${TEST_DIR}/test2024-7.cpp)
target_link_libraries(test2024-7
  GTest::gtest_main
	GTest::gmock
  fmt::fmt
  spdlog::spdlog
  xtensor)
target_include_directories(test2024-7
  PRIVATE
  ${CMAKE_CURRENT_LIST_DIR}/src
  ${xtensor_INCLUDE_DIRS})

include(GoogleTest)
gtest_discover_tests(test2024-7)


