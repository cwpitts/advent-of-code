#ifndef AOC_2024_7_UTIL_HPP
#define AOC_2024_7_UTIL_HPP

#include <filesystem>
#include <ostream>
#include <string>
#include <vector>

namespace fs = std::filesystem;

struct Equation
{
  long long answer;
  std::vector<long long> inputs;
};

enum Operator
{
  ADD,
  MUL,
  CAT,
};

std::vector<Equation> read_input(fs::path input_path);

std::ostream& operator<<(std::ostream& o, const Equation& e);

#endif  // AOC_2024_7_UTIL_HPP
