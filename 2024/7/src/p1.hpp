#ifndef AOC_2024_7_P1_HPP
#define AOC_2024_7_P1_HPP

#include <optional>
#include <vector>

#include "util.hpp"

std::optional<std::vector<std::vector<char>>> eval(
    Equation e, long long running_total, std::vector<char> operators,
    std::vector<Operator> options);

long long do_p1(const std::vector<Equation> data);

#endif  // AOC_2024_7_P1_HPP
