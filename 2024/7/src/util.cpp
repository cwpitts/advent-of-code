#include "util.hpp"

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <sstream>

std::vector<Equation> read_input(fs::path input_path)
{
  std::vector<Equation> equations;

  std::ifstream in_file(input_path);

  std::string line;

  while (std::getline(in_file, line))
  {
    if (line == "")
    {
      break;
    }

    long long answer = std::atoll(line.substr(0, line.find(":")).c_str());
    std::vector<long long> inputs;

    std::istringstream iss(line.substr(line.find(":") + 2));

    std::string item;

    while (std::getline(iss, item, ' '))
    {
      inputs.push_back(std::atoll(item.c_str()));
    }

    equations.push_back(Equation{answer, inputs});
  }

  in_file.close();

  return equations;
}

std::ostream& operator<<(std::ostream& o, const Equation& e)
{
  o << e.answer << ": ";
  for (size_t i = 0; i < e.inputs.size(); i += 1)
  {
    o << e.inputs[i];
    if (i < e.inputs.size() - 1)
    {
      o << " ";
    }
  }
  return o;
}
