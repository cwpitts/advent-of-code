#include "p2.hpp"

#include <optional>
#include <vector>

#include "p1.hpp"

long long do_p2(const std::vector<Equation> data)
{
  long long p2 = 0;

  for (Equation e : data)
  {
    std::optional<std::vector<std::vector<char>>> operators =
        eval(e, 0, {}, {Operator::ADD, Operator::MUL, Operator::CAT});

    if (operators.has_value())
    {
      p2 += e.answer;
    }
  }

  return p2;
}
