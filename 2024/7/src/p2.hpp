#ifndef AOC_2024_7_P2_HPP
#define AOC_2024_7_P2_HPP

#include "util.hpp"

long long do_p2(const std::vector<Equation> data);

#endif  // AOC_2024_7_P2_HPP
