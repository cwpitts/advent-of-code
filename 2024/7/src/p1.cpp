#include "p1.hpp"

#include <algorithm>
#include <optional>

#include "util.hpp"

std::optional<std::vector<std::vector<char>>> eval(
    Equation e, long long running_total, std::vector<char> operators,
    std::vector<Operator> options)
{
  // No more operators
  if (e.inputs.size() == 0)
  {
    if (running_total == e.answer)
    {
      return std::vector<std::vector<char>>{operators};
    }
    return std::nullopt;
  }

  // We can also only go up with '+' and '*', so if we've gone over
  // and those are the options we're also done.
  if (std::find(options.begin(), options.end(), Operator::CAT) ==
          options.end() and
      running_total > e.answer)
  {
    return std::nullopt;
  }

  std::vector<std::vector<char>> results;

  for (Operator op : options)
  {
    Equation branch = e;
    long long new_running_total = running_total;

    switch (op)
    {
      case Operator::ADD: {
        new_running_total += branch.inputs[0];
        break;
      }
      case Operator::MUL: {
        new_running_total *= branch.inputs[0];
        break;
      }
      case Operator::CAT: {
        new_running_total = std::stoll(std::to_string(new_running_total) +
                                       std::to_string(branch.inputs[0]));
      }
    }

    branch.inputs.erase(branch.inputs.begin());

    std::optional<std::vector<std::vector<char>>> branch_result =
        eval(branch, new_running_total, operators, options);

    if (branch_result.has_value())
    {
      results.insert(results.end(), branch_result.value().begin(),
                     branch_result.value().end());
    }
  }

  if (results.size() > 0)
  {
    return results;
  }

  return std::nullopt;
}

long long do_p1(const std::vector<Equation> data)
{
  long long p1 = 0;

  for (Equation e : data)
  {
    std::optional<std::vector<std::vector<char>>> operators =
        eval(e, 0, {}, {Operator::ADD, Operator::MUL});

    if (operators.has_value())
    {
      p1 += e.answer;
    }
  }

  return p1;
}
