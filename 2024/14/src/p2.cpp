#include "p2.hpp"

#include <fstream>
#include <limits>
#include <set>
#include <utility>

#include "fmt/core.h"
#include "util.hpp"
#include "xtensor/xmanipulation.hpp"
#include "xtensor/xmath.hpp"
#include "xtensor/xview.hpp"

long long do_p2(const Data& data)
{
  // How to find tree? The frame with the tree has the lowest safety
  // score, so step for N_STEPS iterations and find the lowest score
  // step.
  long long min_score_index = 0;
  long long min_score = std::numeric_limits<long long>::max();

  xt::xarray<long long> positions = data.positions;

  // write_grid(positions, data.dimensions,
  //            OUTPUT_DIR / fmt::format("frame{}", 0));

  for (long long step = 0; step < 10000; step += 1)
  {
    step_sim(positions, data);

    // std::set<std::pair<long long, long long>> unique_positions;

    // for (size_t i = 0; i < positions.shape(0); i += 1)
    // {
    //   xt::xarray<long long> row = xt::view(positions, i, xt::all());
    //   unique_positions.insert({row.at(0), row.at(1)});
    // }

    long long score = safety_factor(positions, data.dimensions);

    // if (unique_positions.size() == positions.shape(0))
    // {
    //   fmt::println("All unique positions at step {}, safety score is {}",
    //                step + 1, score);
    // }

    if (score < min_score)
    {
      min_score = score;
      min_score_index = step;
    }

    // write_grid(positions, data.dimensions,
    //            OUTPUT_DIR / fmt::format("frame{}", step + 1));
  }

  return min_score_index + 1;
}
