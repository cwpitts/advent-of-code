#ifndef AOC_2024_14_P1_HPP
#define AOC_2024_14_P1_HPP

#include "util.hpp"

long long do_p1(const Data& data);

#endif  // AOC_2024_14_P1_HPP
