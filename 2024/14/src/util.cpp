#include "util.hpp"

#include <cstddef>
#include <filesystem>
#include <fstream>
#include <regex>

#include "fmt/core.h"
#include "fmt/format.h"
#include "xtensor/xadapt.hpp"
#include "xtensor/xbuilder.hpp"
#include "xtensor/xslice.hpp"
#include "xtensor/xvectorize.hpp"
#include "xtensor/xview.hpp"

const std::regex DATA_REGEX(
    "p=([0-9]+),([0-9]+) v=([-]{0,1}[0-9]+),([-]{0,1}[0-9]+)");

Data read_input(fs::path input_path, bool small_grid)
{
  std::vector<int> positions;
  std::vector<int> velocities;

  std::ifstream in_file(input_path);

  size_t line_count = 0;

  std::string line;

  while (std::getline(in_file, line))
  {
    if (line == "")
    {
      break;
    }

    std::smatch match;

    std::regex_match(line, match, DATA_REGEX);

    positions.push_back(std::stoll(match[1].str()));
    positions.push_back(std::stoll(match[2].str()));
    velocities.push_back(std::stoll(match[3].str()));
    velocities.push_back(std::stoll(match[4].str()));

    line_count += 1;
  }

  in_file.close();

  std::vector<size_t> shape{line_count, 2};

  if (small_grid)
  {
    return Data{xt::adapt(positions, shape), xt::adapt(velocities, shape),
                GRID_SIZE_SMALL};
  }

  return Data{xt::adapt(positions, shape), xt::adapt(velocities, shape),
              GRID_SIZE_LARGE};
}

void print_grid(const xt::xarray<long long>& positions,
                const xt::xarray<long long>& dimensions)
{
  xt::xarray<long long> grid =
      xt::zeros<long long>({dimensions.at(1), dimensions.at(0)});

  for (size_t i = 0; i < positions.shape(0); i += 1)
  {
    const xt::xarray<long long> p = xt::view(positions, i, xt::all());
    grid.at(p.at(1), p.at(0)) += 1;
  }

  std::cout << dimensions.at(1) << " rows, " << dimensions.at(0) << " columns"
            << std::endl;

  for (size_t i = 0; i < grid.shape(0); i += 1)
  {
    for (size_t j = 0; j < grid.shape(1); j += 1)
    {
      if (grid.at(i, j) == 0)
      {
        std::cout << ".";
      }
      else
      {
        std::cout << grid.at(i, j);
      }
    }
    std::cout << std::endl;
  }
}

void write_grid(const xt::xarray<long long>& positions,
                const xt::xarray<long long>& dimensions, fs::path output_path)
{
  xt::xarray<long long> grid =
      xt::zeros<long long>({dimensions.at(1), dimensions.at(0)});

  for (size_t i = 0; i < positions.shape(0); i += 1)
  {
    const xt::xarray<long long> p = xt::view(positions, i, xt::all());
    grid.at(p.at(1), p.at(0)) += 1;
  }

  std::ofstream out_file(output_path);

  for (size_t i = 0; i < grid.shape(0); i += 1)
  {
    for (size_t j = 0; j < grid.shape(1); j += 1)
    {
      if (grid.at(i, j) == 0)
      {
        out_file << ".";
      }
      else
      {
        out_file << grid.at(i, j);
      }
    }
    out_file << std::endl;
  }

  out_file.close();
}

long long safety_factor(const xt::xarray<long long> positions,
                        const xt::xarray<long long> dimensions)
{
  // Zero out middle row and column
  xt::xarray<long long> mid_dims = dimensions / 2;

  xt::xarray<bool> upper = xt::view(positions, xt::all(), 1) < mid_dims.at(1);
  xt::xarray<bool> lower = xt::view(positions, xt::all(), 1) > mid_dims.at(1);
  xt::xarray<bool> left = xt::view(positions, xt::all(), 0) < mid_dims.at(0);
  xt::xarray<bool> right = xt::view(positions, xt::all(), 0) > mid_dims.at(0);

  // Quadrant sums
  xt::xarray<bool> Q1 = upper and left;
  xt::xarray<bool> Q2 = upper and right;
  xt::xarray<bool> Q3 = lower and right;
  xt::xarray<bool> Q4 = lower and left;

  int q1_count = xt::sum(Q1)();
  int q2_count = xt::sum(Q2)();
  int q3_count = xt::sum(Q3)();
  int q4_count = xt::sum(Q4)();

  return q1_count * q2_count * q3_count * q4_count;
}

void step_sim(xt::xarray<long long>& positions, const Data& data)
{
  positions += data.velocities;

  // Update x column with wraparound
  xt::xarray<long long> x_col = xt::view(positions, xt::all(), 0);
  xt::view(positions, xt::all(), 0) =
      xt::vectorize(wrap)(x_col, data.dimensions.at(0));

  // Update y column with wraparound
  xt::xarray<long long> y_col = xt::view(positions, xt::all(), 1);
  xt::view(positions, xt::all(), 1) =
      xt::vectorize(wrap)(y_col, data.dimensions.at(1));
}

long long wrap(long long value, long long modulus)
{
  long long return_val = (value % modulus + modulus) % modulus;
  return return_val;
}
