#ifndef AOC_2024_14_UTIL_HPP
#define AOC_2024_14_UTIL_HPP

#include <filesystem>
#include <string>
#include <vector>

#include "xtensor/xarray.hpp"
#include "xtensor/xio.hpp"
#include "xtensor/xmanipulation.hpp"

namespace fs = std::filesystem;

const xt::xarray<long long> GRID_SIZE_SMALL{11, 7};
const xt::xarray<long long> GRID_SIZE_LARGE{101, 103};
const long long N_STEPS = 100;
const fs::path OUTPUT_DIR("output");

struct Data
{
  xt::xarray<long long> positions;
  xt::xarray<long long> velocities;
  xt::xarray<long long> dimensions;
};

Data read_input(fs::path input_path, bool small_grid = false);

void print_grid(const xt::xarray<long long>& positions,
                const xt::xarray<long long>& dimensions);

void write_grid(const xt::xarray<long long>& positions,
                const xt::xarray<long long>& dimensions, fs::path output_path);

long long safety_factor(const xt::xarray<long long> positions,
                        const xt::xarray<long long> dimensions);

void step_sim(xt::xarray<long long>& positions, const Data& data);

long long wrap(long long value, long long modulus);

#endif  // AOC_2024_14_UTIL_HPP
