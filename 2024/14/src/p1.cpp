#include "p1.hpp"

#include <functional>

#include "fmt/format.h"
#include "util.hpp"
#include "xtensor/xarray.hpp"
#include "xtensor/xfunction.hpp"
#include "xtensor/xio.hpp"
#include "xtensor/xvectorize.hpp"
#include "xtensor/xview.hpp"

long long do_p1(const Data& data)
{
  xt::xarray<long long> positions = data.positions;

  positions += data.velocities * N_STEPS;
  // Update x column with wraparound
  xt::xarray<long long> x_col = xt::view(positions, xt::all(), 0);
  xt::view(positions, xt::all(), 0) =
      xt::vectorize(wrap)(x_col, data.dimensions.at(0));

  // Update y column with wraparound
  xt::xarray<long long> y_col = xt::view(positions, xt::all(), 1);
  xt::view(positions, xt::all(), 1) =
      xt::vectorize(wrap)(y_col, data.dimensions.at(1));

  return safety_factor(positions, data.dimensions);
}
