#include "p2.hpp"

#include "xtensor/xarray.hpp"
#include "xtensor/xsort.hpp"

long long do_p2(const Data& data)
{
  xt::xarray<long long> arr1 = xt::adapt(data.arr1);
  xt::xarray<long long> arr2 = xt::adapt(data.arr2);

  long long score = 0;

  std::map<long long, long long> counts;

  for (const long long& v1 : data.arr1)
  {
    if (!counts.contains(v1))
    {
      counts[v1] = xt::sum(xt::equal(arr2, v1))();
    }
    score += v1 * counts[v1];
  }

  return score;
}

long long do_p2_std_only(const Data& data)
{
  std::map<long long, long long> counts;

  for (const long long& v2 : data.arr2)
  {
    if (!counts.contains(v2))
    {
      counts[v2] = 0;
    }

    counts[v2] += 1;
  }

  long long score = 0;

  for (const long long& v1 : data.arr1)
  {
    score += v1 * counts[v1];
  }

  return score;
}
