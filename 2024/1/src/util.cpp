#include "util.hpp"

#include <fstream>
#include <sstream>
#include <string>

#include "fmt/format.h"

Data readInput(fs::path inputPath)
{
  std::vector<long long> arr1;
  std::vector<long long> arr2;

  std::ifstream inFile(inputPath);

  std::string line;

  while (std::getline(inFile, line))
  {
    std::istringstream iss(line);

    long long v1;
    long long v2;

    iss >> v1 >> v2;

    arr1.push_back(v1);
    arr2.push_back(v2);
  }

  inFile.close();

  return Data{arr1, arr2};
}

std::ostream& operator<<(std::ostream& o, const std::vector<long long> v)
{
  for (unsigned int i = 0; i < v.size(); i += 1)
  {
    o << v[i];
    if (i < v.size() - 1)
    {
      o << " ";
    }
  }

  return o;
}
