#include "p1.hpp"

#include <algorithm>

#include "fmt/format.h"
#include "xtensor/xarray.hpp"
#include "xtensor/xsort.hpp"

long long do_p1(const Data& data)
{
  xt::xarray<long long> arr1 = xt::adapt(data.arr1);
  xt::xarray<long long> arr2 = xt::adapt(data.arr2);

  arr1 = xt::sort(arr1);
  arr2 = xt::sort(arr2);
  xt::xarray<long long> arr3 = xt::abs(arr1 - arr2);

  return xt::sum(arr3)();
}

long long do_p1_std_only(const Data& data)
{
  Data data2 = data;
  std::sort(data2.arr1.begin(), data2.arr1.end());
  std::sort(data2.arr2.begin(), data2.arr2.end());

  long long sum = 0;

  for (unsigned int i = 0; i < data2.arr1.size(); i += 1)
  {
    sum += std::abs(data2.arr1[i] - data2.arr2[i]);
  }

  return sum;
}
