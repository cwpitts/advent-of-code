#ifndef AOC_2024_1_P2_HPP
#define AOC_2024_1_P2_HPP

#include <map>

#include "util.hpp"

long long do_p2(const Data& data);
long long do_p2_std_only(const Data& data);

#endif  // AOC_2024_1_P2_HPP
