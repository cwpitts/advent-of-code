#ifndef AOC_2024_1_UTIL_HPP
#define AOC_2024_1_UTIL_HPP

#include <filesystem>
#include <ostream>
#include <string>
#include <vector>

namespace fs = std::filesystem;

struct Data
{
  std::vector<long long> arr1;
  std::vector<long long> arr2;
};

Data readInput(fs::path inputPath);

std::ostream& operator<<(std::ostream& o, const std::vector<long long> v);

#endif  // AOC_2024_1_UTIL_HPP
