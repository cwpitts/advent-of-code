/* Advent of Code 2024 problem 1
 */
#include <algorithm>
#include <chrono>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>

#include "CLI/CLI11.hpp"
#include "fmt/core.h"
#include "p1.hpp"
#include "p2.hpp"
#include "rang/rang.hpp"
#include "util.hpp"
#include "xtensor/xarray.hpp"
#include "xtensor/xrandom.hpp"

using time_point = std::chrono::high_resolution_clock::time_point;
using duration = std::chrono::duration<double>;
using DoubleMilliseconds =
    std::chrono::duration<double, std::chrono::milliseconds::period>;

namespace fs = std::filesystem;

time_point clockTime() { return std::chrono::high_resolution_clock::now(); }

void do_speed_test(const fs::path output_dir)
{
  std::vector<double> p1_xtensor_times;
  std::vector<double> p1_std_only_times;

  std::vector<double> p2_xtensor_times;
  std::vector<double> p2_std_only_times;

  xt::xarray<long long> problem_sizes = {1,   5,    10,   15,    25,   50,  100,
                                         200, 300,  400,  500,   600,  700, 800,
                                         900, 1000, 5000, 10000, 50000};

  for (long long size : problem_sizes)
  {
    fmt::println("Size: {}", size);

    // Initialize the problem
    xt::xarray<long long> arr1 = xt::random::randint<long long>({size});
    xt::xarray<long long> arr2 = xt::random::randint<long long>({size});

    Data data{std::vector<long long>(arr1.data(), arr1.data() + arr1.size()),
              std::vector<long long>(arr2.data(), arr2.data() + arr2.size())};

    // Part 1
    time_point p1_start_xtensor = clockTime();
    do_p1(data);
    time_point p1_end_xtensor = clockTime();
    duration p1_time_xtensor = p1_end_xtensor - p1_start_xtensor;
    p1_xtensor_times.push_back(DoubleMilliseconds(p1_time_xtensor).count());

    time_point p1_start_std_only = clockTime();
    do_p1(data);
    time_point p1_end_std_only = clockTime();
    duration p1_time_std_only = p1_end_std_only - p1_start_std_only;
    p1_std_only_times.push_back(DoubleMilliseconds(p1_time_std_only).count());

    // Part 2
    time_point p2_start_xtensor = clockTime();
    do_p2(data);
    time_point p2_end_xtensor = clockTime();
    duration p2_time_xtensor = p2_end_xtensor - p2_start_xtensor;
    p2_xtensor_times.push_back(DoubleMilliseconds(p2_time_xtensor).count());

    time_point p2_start_std_only = clockTime();
    do_p2(data);
    time_point p2_end_std_only = clockTime();
    duration p2_time_std_only = p2_end_std_only - p2_start_std_only;
    p2_std_only_times.push_back(DoubleMilliseconds(p2_time_std_only).count());
  }

  std::ofstream out_file(output_dir / "stats.csv");
  out_file << "size,p1_xtensor,p1_std_only,p2_xtensor,p2_std_only" << std::endl;
  for (unsigned int i = 0; i < problem_sizes.size(); i += 1)
  {
    out_file << problem_sizes[i] << "," << p1_xtensor_times[i] << ","
             << p1_std_only_times[i] << "," << p2_xtensor_times[i] << ","
             << p2_std_only_times[i] << std::endl;
  }
  out_file.close();
}

int main(int argc, char* argv[])
{
  CLI::App app;

  fs::path input_path;
  app.add_option("input", input_path, "Path to input data")->required(true);

  bool speed_test = false;
  app.add_flag("--speed-test", speed_test,
               "Compare xtensor/std-only implementations");

  fs::path output_dir;
  app.add_option("-O", output_dir, "Output directory")
      ->default_val(fs::path("output"));

  CLI11_PARSE(app, argc, argv);

  Data data = readInput(input_path);

  time_point p1_start = clockTime();
  long long p1 = do_p1_std_only(data);
  time_point p1_end = clockTime();
  duration p1_time = p1_end - p1_start;
  std::cout << rang::style::bold << p1 << rang::style::reset << " in "
            << rang::fg::green << DoubleMilliseconds(p1_end - p1_start)
            << rang::fg::reset << std::endl;

  time_point p2_start = clockTime();
  long long p2 = do_p2_std_only(data);
  time_point p2_end = clockTime();
  duration p2_time = p2_end - p2_start;
  std::cout << rang::style::bold << p2 << rang::style::reset << " in "
            << rang::fg::green << DoubleMilliseconds(p2_end - p2_start)
            << rang::fg::reset << std::endl;

  if (speed_test)
  {
    fs::create_directories(output_dir);
    do_speed_test(output_dir);
  }

  return 0;
}
