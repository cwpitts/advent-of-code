#include "util.hpp"

#include <cmath>
#include <fstream>
#include <regex>

#include "fmt/core.h"
#include "fmt/format.h"

std::ostream& operator<<(std::ostream& o, const machine& machine)
{
  o << "A: X+" << machine.claw.A.dx << ", Y+" << machine.claw.A.dy << std::endl;
  o << "B: X+" << machine.claw.B.dx << ", Y+" << machine.claw.B.dy << std::endl;
  o << "Prize: " << machine.prize.x << "," << machine.prize.y;

  return o;
}

std::ifstream& operator>>(std::ifstream& ifs, machine& machine)
{
  std::string a_line;
  std::string b_line;
  std::string prize_line;

  if (not std::getline(ifs, a_line) or a_line == "")
  {
    fmt::println("Failed to read A line, was {}", a_line);
    ifs.setstate(std::ios::failbit);
    return ifs;
  }

  if (not std::getline(ifs, b_line) or b_line == "")
  {
    fmt::println("Failed to read B line");
    ifs.setstate(std::ios::failbit);
    return ifs;
  }

  if (not std::getline(ifs, prize_line) or prize_line == "")
  {
    fmt::println("Failed to read prize line");
    ifs.setstate(std::ios::failbit);
    return ifs;
  }

  std::string junk;
  std::getline(ifs, junk);
  ifs.clear();

  std::regex A_REGEX("Button A: X\\+([0-9]+), Y\\+([0-9]+)");
  std::regex B_REGEX("Button B: X\\+([0-9]+), Y\\+([0-9]+)");
  std::regex PRIZE_REGEX("Prize: X=([0-9]+), Y=([0-9]+)");

  std::smatch match;

  std::regex_search(a_line, match, A_REGEX);
  machine.claw.A.dx = std::stoul(match[1].str());
  machine.claw.A.dy = std::stoul(match[2].str());

  std::regex_search(b_line, match, B_REGEX);
  machine.claw.B.dx = std::stoul(match[1].str());
  machine.claw.B.dy = std::stoul(match[2].str());

  std::regex_search(prize_line, match, PRIZE_REGEX);
  machine.prize.x = std::stoul(match[1].str());
  machine.prize.y = std::stoul(match[2].str());

  return ifs;
}

std::vector<machine> read_input(fs::path input_path)
{
  std::vector<machine> machines;

  std::ifstream in_file(input_path);

  std::string line;

  machine new_machine;

  while (in_file >> new_machine)
  {
    machines.push_back(new_machine);
  }

  in_file.close();

  return machines;
}

bool all_long_long(xt::xarray<double> arr, double tolerance)
{
  for (const double value : arr)
  {
    if (std::abs(value - std::round(value)) > tolerance)
    {
      return false;
    }
  }

  return true;
}
