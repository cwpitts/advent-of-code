#ifndef AOC_2024_13_P2_HPP
#define AOC_2024_13_P2_HPP

#include <vector>

#include "util.hpp"

long long do_p2(const std::vector<machine>& machines);

#endif  // AOC_2024_13_P2_HPP
