#ifndef AOC_2024_13_P1_HPP
#define AOC_2024_13_P1_HPP

#include "util.hpp"

long long do_p1(const std::vector<machine> machines);

#endif  // AOC_2024_13_P1_HPP
