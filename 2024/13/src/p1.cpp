#include "p1.hpp"

#include <iostream>

#include "xtensor-blas/xlinalg.hpp"
#include "xtensor/xadapt.hpp"
#include "xtensor/xarray.hpp"
#include "xtensor/xio.hpp"
#include "xtensor/xmanipulation.hpp"
#include "xtensor/xoperation.hpp"

long long do_p1(const std::vector<machine> machines)
{
  long long p1 = 0;

  for (const machine& machine : machines)
  {
    xt::xarray<double> A{
        {double(machine.claw.A.dx), double(machine.claw.B.dx)},
        {double(machine.claw.A.dy), double(machine.claw.B.dy)}};
    xt::xarray<double> b{{double(machine.prize.x), double(machine.prize.y)}};
    b = xt::transpose(b);

    xt::xarray<double> x = xt::linalg::solve(A, b);

    if (xt::all(x >= 0) and all_long_long(x))
    {
      p1 += 3 * x.at(0, 0);
      p1 += x.at(1, 0);
    }
  }

  return p1;
}
