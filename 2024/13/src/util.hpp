#ifndef AOC_2024_13_UTIL_HPP
#define AOC_2024_13_UTIL_HPP

#include <filesystem>
#include <ostream>
#include <string>
#include <vector>

#include "xtensor/xarray.hpp"

namespace fs = std::filesystem;

const unsigned int COST_A = 3;
const unsigned int COST_B = 1;

struct button
{
  unsigned int dx;
  unsigned int dy;
};

struct claw
{
  button A;
  button B;
};

struct point
{
  int x;
  int y;
};

struct machine
{
  struct claw claw;
  point prize;
};

std::vector<machine> read_input(fs::path input_path);

std::ifstream& operator>>(std::ifstream& ifs, machine& machine);

std::ostream& operator<<(std::ostream& o, const machine& machine);

bool all_long_long(xt::xarray<double> arr, double tolerance = 1e-5);

#endif  // AOC_2024_13_UTIL_HPP
