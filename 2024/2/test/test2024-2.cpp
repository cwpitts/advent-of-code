#include <gtest/gtest.h>

#include <cmath>
#include <cstdint>
#include <cstdio>
#include <filesystem>
#include <iterator>
#include <limits>
#include <memory>
#include <optional>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <typeinfo>
#include <vector>

#include "p1.hpp"
#include "p2.hpp"
#include "util.hpp"

namespace fs = std::filesystem;

const fs::path TEST_DIR = fs::current_path().parent_path() / "test";

class AOC2024Day2 : public ::testing::TestWithParam<
                        std::tuple<std::string, long long, long long>>
{
 protected:
};

TEST_P(AOC2024Day2, Part1)
{
  const std::string input = std::get<0>(GetParam());
  const long long expectedOutput = std::get<1>(GetParam());

  const Data data = readInput(TEST_DIR / "data" / input);

  const long long actualOutput = do_p1(data);

  ASSERT_EQ(expectedOutput, actualOutput);
}

TEST_P(AOC2024Day2, Part2)
{
  const std::string input = std::get<0>(GetParam());
  const long long expectedOutput = std::get<2>(GetParam());

  const Data data = readInput(TEST_DIR / "data" / input);

  const long long actualOutput = do_p2(data);

  ASSERT_EQ(expectedOutput, actualOutput);
}

INSTANTIATE_TEST_CASE_P(RunProgramTests, AOC2024Day2,
                        ::testing::Values(std::make_tuple("input", 663, 692),
                                          std::make_tuple("test1", 2, 4)));
