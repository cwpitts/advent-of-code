#include "p1.hpp"

#include "xtensor/xio.hpp"
#include "xtensor/xslice.hpp"
#include "xtensor/xtensor_forward.hpp"
#include "xtensor/xview.hpp"

const xt::placeholders::xtuph _ = xt::placeholders::_;

long long do_p1(const Data& data)
{
  long long safe_reports = 0;

  for (const std::vector<long long>& report : data.reports)
  {
    xt::xarray<long long> row = xt::adapt(report);
    xt::xarray<long long> row_shifted = xt::roll(row, 1);
    row_shifted[0] = row[0];
    xt::xarray<long long> diff = xt::view(row - row_shifted, xt::range(1, _));

    // All increasing or all decreasing
    if (not xt::all(diff > 0) and not xt::all(diff < 0))
    {
      continue;
    }

    // At most 3
    if (not xt::all(xt::abs(diff) < 4))
    {
      continue;
    }

    safe_reports += 1;
  }

  return safe_reports;
}
