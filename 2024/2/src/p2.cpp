#include "p2.hpp"

#include "xtensor/xarray.hpp"
#include "xtensor/xbuilder.hpp"
#include "xtensor/xio.hpp"
#include "xtensor/xslice.hpp"
#include "xtensor/xsort.hpp"
#include "xtensor/xtensor.hpp"
#include "xtensor/xtensor_forward.hpp"
#include "xtensor/xview.hpp"

const xt::placeholders::xtuph _ = xt::placeholders::_;

long long do_p2(const Data& data)
{
  long long safe_reports = 0;

  unsigned int rowNum = 0;
  for (const std::vector<long long>& report : data.reports)
  {
    xt::xarray<long long> row = xt::adapt(report);

    // Try removing each value
    for (unsigned int i = 0; i < row.shape(0); i += 1)
    {
      xt::xarray<bool> mask = xt::full_like(row, true);
      mask[i] = false;

      xt::xarray<long long> new_row = xt::filter(row, mask);

      xt::xarray<long long> row_shifted = xt::roll(new_row, 1);
      row_shifted[0] = new_row[0];
      xt::xarray<long long> diff =
          xt::view(new_row - row_shifted, xt::range(1, _));

      // All increasing or all decreasing
      if (not xt::all(diff > 0) and not xt::all(diff < 0))
      {
        continue;
      }

      // At most 3
      if (not xt::all(xt::abs(diff) < 4))
      {
        continue;
      }

      safe_reports += 1;
      break;
    }

    rowNum += 1;
  }

  return safe_reports;
}
