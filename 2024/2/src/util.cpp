#include "util.hpp"

#include <fstream>
#include <iostream>
#include <sstream>

Data readInput(fs::path inputPath)
{
  std::ifstream inFile(inputPath);

  std::vector<std::vector<long long>> reports;

  std::string line;

  while (std::getline(inFile, line))
  {
    if (line == "")
    {
      break;
    }

    std::vector<long long> report;

    std::istringstream iss(line);

    long long value;

    while (iss >> value)
    {
      report.push_back(value);
    }

    reports.push_back(report);
  }

  inFile.close();

  return Data{reports};
}
