#ifndef AOC_2024_2_UTIL_HPP
#define AOC_2024_2_UTIL_HPP

#include <filesystem>
#include <string>
#include <vector>

namespace fs = std::filesystem;

struct Data
{
  std::vector<std::vector<long long>> reports;
};

Data readInput(fs::path inputPath);

#endif  // AOC_2024_2_UTIL_HPP
