#ifndef AOC_2024_2_P1_HPP
#define AOC_2024_2_P1_HPP

#include "util.hpp"
#include "xtensor/xarray.hpp"

long long do_p1(const xt::xarray<char>& data);

#endif  // AOC_2024_2_P1_HPP
