from typing import Callable, List

import pytest
from aoc import intcode


@pytest.mark.parametrize(
    "prog_in, expected_prog_out, inputs, expected_outputs",
    [
        ([1, 0, 0, 0, 99], [2, 0, 0, 0, 99], None, None),
        ([2, 3, 0, 3, 99], [2, 3, 0, 6, 99], None, None),
        ([2, 4, 4, 5, 99, 0], [2, 4, 4, 5, 99, 9801], None, None),
        ([1, 1, 1, 4, 99, 5, 6, 0, 99], [30, 1, 1, 4, 2, 5, 6, 0, 99], None, None),
        ([1002, 4, 3, 4, 33], [1002, 4, 3, 4, 99], None, None),
        ([1101, 100, -1, 4, 0], [1101, 100, -1, 4, 99], None, None),
        (
            [3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8],
            [3, 9, 8, 9, 10, 9, 4, 9, 99, 0, 8],
            ["1"],
            [0],
        ),
        (
            [3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8],
            [3, 9, 8, 9, 10, 9, 4, 9, 99, 1, 8],
            ["8"],
            [1],
        ),
        (
            [3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8],
            [3, 9, 8, 9, 10, 9, 4, 9, 99, 0, 8],
            ["9"],
            [0],
        ),
        (
            [3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1],
            [3, 3, 1105, 0, 9, 1101, 0, 0, 12, 4, 12, 99, 0],
            ["0"],
            [0],
        ),
        (
            [3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1],
            [3, 3, 1105, 100, 9, 1101, 0, 0, 12, 4, 12, 99, 1],
            ["100"],
            [1],
        ),
        (
            [109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99],
            [109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99]
            + [0] * 84
            + [16, 1],
            None,
            [109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99],
        ),
    ],
)
def test_incode_programs(
    prog_in: List[int],
    expected_prog_out: List[int],
    inputs: List[str],
    expected_outputs: List[int],
):
    if inputs is not None:

        def input_func() -> str:
            return inputs.pop(0)

    else:
        input_func: Callable = None

    if expected_outputs is not None:
        actual_outputs: List[int] = []

        def output_func(x: int):
            actual_outputs.append(x)

    else:
        output_func: Callable = None
        actual_outputs: List[int] = None

    vm: intcode.IntcodeVM = intcode.IntcodeVM()
    actual_prog_out = vm.run(prog_in, input_func=input_func, output_func=output_func)

    assert actual_prog_out.get_memory() == expected_prog_out
    assert actual_outputs == expected_outputs


@pytest.mark.parametrize(
    "prog_in, noun, verb, expected_prog_out",
    [
        ([1, 0, 0, 0, 99], 0, 0, [2, 0, 0, 0, 99]),
        ([2, 0, 1, 3, 99], 3, 0, [2, 3, 0, 6, 99]),
    ],
)
def test_intcode_noun_verb(
    prog_in: List[int], noun: int, verb: int, expected_prog_out: List[int]
):
    vm: intcode.IntcodeVM = intcode.IntcodeVM()
    actual_prog_out = vm.run(prog_in, noun, verb)

    assert actual_prog_out.get_memory() == expected_prog_out
