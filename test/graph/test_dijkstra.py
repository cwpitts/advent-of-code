from typing import List

import pytest
from aoc.graph import dijkstra


def test__path_from_previous():
    p = {"A": None, "B": "A", "C": "B"}

    assert dijkstra._path_from_previous(p, "C") == ["A", "B", "C"]


def test_shortest_path_short():
    class GraphNode(dijkstra.State):
        def __init__(self, cost, label):
            super().__init__(cost)
            self.label = label

        @property
        def is_end_state(self) -> bool:
            return self.label == "D"

        @property
        def neighbors(self) -> List["GraphNode"]:
            if self.label == "A":
                return [GraphNode(1, "B"), GraphNode(10, "C")]
            elif self.label == "B":
                return [GraphNode(1000, "D")]
            elif self.label == "C":
                return [GraphNode(1, "D")]

            return []

        def __hash__(self) -> int:
            return sum([ord(c) for c in self.label])

    path: List[GraphNode] = dijkstra.find_shortest_path(GraphNode(0, "A"))
    assert [s.label for s in path] == ["A", "C", "D"]
    assert sum([s.cost for s in path]) == 11
