#! usr/bin/env python3
import json
from pathlib import Path

import requests

token_path = Path("~/tokens/aoc.json").expanduser()
with open(token_path, "r") as in_file:
    cookies = json.load(in_file)

day = "{{ cookiecutter.problem }}".lstrip("0")
year = "{{ cookiecutter.year }}"
resp = requests.get(
    f"https://adventofcode.com/{year}/day/{day}/input",
    cookies=cookies
)
test_dir = Path("test")
data_dir = test_dir / "data"

if not data_dir.exists():
    data_dir.mkdir(parents=True)

with open(data_dir / "input", "w") as input_file:
    input_file.write(resp.text)
