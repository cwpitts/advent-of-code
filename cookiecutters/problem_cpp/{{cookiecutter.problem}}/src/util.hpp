#ifndef AOC_{{cookiecutter.year|upper}}_{{cookiecutter.problem|upper}}_UTIL_HPP
#define AOC_{{cookiecutter.year|upper}}_{{cookiecutter.problem|upper}}_UTIL_HPP

#include <filesystem>
#include <string>
#include <vector>

namespace fs = std::filesystem;

std::vector<std::string> read_input(fs::path input_path);

#endif  // AOC_{{cookiecutter.year|upper}}_{{cookiecutter.problem|upper}}_UTIL_HPP