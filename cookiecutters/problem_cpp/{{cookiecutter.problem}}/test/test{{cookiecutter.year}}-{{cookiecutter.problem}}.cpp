#include <gtest/gtest.h>

#include <cmath>
#include <cstdint>
#include <cstdio>
#include <filesystem>
#include <iterator>
#include <limits>
#include <memory>
#include <optional>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <typeinfo>
#include <vector>

#include "p1.hpp"
#include "p2.hpp"

namespace fs = std::filesystem;

const fs::path TEST_DIR = fs::current_path().parent_path() / "test";

class AOC{{ cookiecutter.year }}Day{{ cookiecutter.problem }}
    : public ::testing::TestWithParam<
          std::tuple<std::string, std::string, std::string>>
{
   protected:
};

TEST_P(AOC{{ cookiecutter.year }}Day{{ cookiecutter.problem }}, Part1)
{
  const std::string input = std::get<0>(GetParam());
  const std::string expected_output = std::get<1>(GetParam());

	FAIL();
}

TEST_P(AOC{{ cookiecutter.year }}Day{{ cookiecutter.problem }}, Part2)
{
  const std::string input = std::get<0>(GetParam());
  const std::string expected_output = std::get<2>(GetParam());
  
	FAIL();
}

INSTANTIATE_TEST_CASE_P(
    RunProgramTests, AOC{{ cookiecutter.year }}Day{{ cookiecutter.problem }},
    ::testing::Values(
        std::make_tuple("input", "", "")));