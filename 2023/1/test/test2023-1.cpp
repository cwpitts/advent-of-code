#include <gtest/gtest.h>

#include <cmath>
#include <cstdint>
#include <cstdio>
#include <filesystem>
#include <iterator>
#include <limits>
#include <memory>
#include <optional>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <typeinfo>
#include <vector>

#include "fmt/format.h"
#include "p1.hpp"
#include "p2.hpp"
#include "util.hpp"

namespace fs = std::filesystem;

class AOC2023Day1 : public ::testing::TestWithParam<
                        std::tuple<std::string, long long, long long>>
{
 protected:
};

TEST_P(AOC2023Day1, Part1)
{
  const std::string inputName = std::get<0>(GetParam());
  const long long expectedOutput = std::get<1>(GetParam());

  fs::path testFile(fmt::format("../test/{}", inputName));

  std::vector<std::string> input = readInput(testFile);

  long long output = doP1(input);

  ASSERT_EQ(expectedOutput, output);
}

TEST_P(AOC2023Day1, Part2)
{
  const std::string inputName = std::get<0>(GetParam());
  const long long expectedOutput = std::get<2>(GetParam());

  fs::path testFile(fmt::format("../test/{}", inputName));

  std::vector<std::string> input = readInput(testFile);

  long long output = doP2(input);

  ASSERT_EQ(expectedOutput, output);
}

INSTANTIATE_TEST_CASE_P(RunProgramTests, AOC2023Day1,
                        ::testing::Values(std::make_tuple("test1", 142, 142),
                                          std::make_tuple("test2", 209, 281),
                                          std::make_tuple("input", 54601,
                                                          54078)));
