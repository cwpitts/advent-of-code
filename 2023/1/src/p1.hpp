#ifndef AOC_2023_1_P1_HPP
#define AOC_2023_1_P1_HPP

#include <string>
#include <vector>

long long doP1(const std::vector<std::string>& data);

#endif  // AOC_2023_1_P1_HPP
