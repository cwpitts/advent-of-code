#include "p1.hpp"

#include <cctype>
#include <iostream>

#include "fmt/format.h"
#include "util.hpp"

long long doP1(const std::vector<std::string>& data)
{
  long long answer = 0;

  for (const std::string& s : data)
  {
    std::vector<char> digits;
    for (const char& c : s)
    {
      if (std::isdigit(c))
      {
        digits.push_back(c);
      }
    }

    std::cout << "Line is: " << s << ", digits are " << digits << std::endl;

    if (digits.size() == 1)
    {
      answer += std::atoll(fmt::format("{}{}", digits[0], digits[0]).c_str());
    }
    else if (digits.size() > 0)
    {
      answer += std::atoll(
          fmt::format("{}{}", digits[0], digits[digits.size() - 1]).c_str());
    }
  }

  return answer;
}
