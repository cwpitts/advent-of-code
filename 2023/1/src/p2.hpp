#ifndef AOC_2023_1_P2_HPP
#define AOC_2023_1_P2_HPP

#include <string>
#include <vector>

long long doP2(const std::vector<std::string>& data);

#endif  // AOC_2023_1_P2_HPP
