/* Advent of Code 2023 problem 1
 */
#include <bits/chrono.h>

#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>

#include "fmt/format.h"
#include "p1.hpp"
#include "p2.hpp"
#include "util.hpp"

using time_point = std::chrono::high_resolution_clock::time_point;
using duration = std::chrono::duration<double>;

time_point clockTime() { return std::chrono::high_resolution_clock::now(); }

int main(int argc, char* argv[])
{
  // Read input
  std::vector<std::string> data = readInput(fs::path(argv[1]));

  time_point p1_start = clockTime();
  long long p1 = doP1(data);
  time_point p1_end = clockTime();
  duration p1_time = p1_end - p1_start;
  fmt::print("{} in {}\n", p1,
             std::chrono::duration_cast<std::chrono::seconds>(p1_time).count());

  time_point p2_start = clockTime();
  long long p2 = doP2(data);
  time_point p2_end = clockTime();
  duration p2_time = p2_end - p2_start;
  fmt::print("{} in {}\n", p2,
             std::chrono::duration_cast<std::chrono::seconds>(p2_time).count());

  // Write output
  std::ofstream outFile(argv[2]);
  outFile.close();
  return 0;
}
