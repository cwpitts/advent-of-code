#include "p2.hpp"

#include <iostream>
#include <map>
#include <regex>

#include "fmt/format.h"
#include "util.hpp"

const std::regex DIGIT_PATTERN(
    "(1|2|3|4|5|6|7|8|9|one|two|three|four|five|six|seven|eight|nine)");

const std::map<std::string, char> STR_TO_DIGIT{
    {"one", '1'}, {"two", '2'},   {"three", '3'}, {"four", '4'}, {"five", '5'},
    {"six", '6'}, {"seven", '7'}, {"eight", '8'}, {"nine", '9'}};

long long doP2(const std::vector<std::string>& data)
{
  long long answer = 0;

  for (const std::string& s : data)
  {
    std::vector<char> digits;

    std::sregex_iterator words_begin =
        std::sregex_iterator(s.begin(), s.end(), DIGIT_PATTERN);
    std::sregex_iterator words_end = std::sregex_iterator();

    unsigned int index = 0;
    while (index < s.size())
    {
      std::smatch match;
      std::string ss = s.substr(index);
      if (std::regex_search(ss, match, DIGIT_PATTERN))
      {
        if (match.length() == 1)
        {
          digits.push_back(match.str()[0]);
        }
        else
        {
          digits.push_back(STR_TO_DIGIT.at(match.str()));
        }

        if (match.position() == 0)
        {
          if (match.length() == 1)
          {
            index += 1;
          }
          else
          {
            index += match.length();
          }
        }
        else
        {
          index += match.position() + 1;
        }
      }
      else
      {
        index += 1;
      }
    }

    if (digits.size() == 1)
    {
      std::cout << std::atoll(fmt::format("{}{}", digits[0], digits[0]).c_str())
                << std::endl;
      answer += std::atoll(fmt::format("{}{}", digits[0], digits[0]).c_str());
    }
    else if (digits.size() > 0)
    {
      std::cout
          << fmt::format("{}{}", digits[0], digits[digits.size() - 1]).c_str()
          << std::endl;
      answer += std::atoll(
          fmt::format("{}{}", digits[0], digits[digits.size() - 1]).c_str());
    }
  }

  return answer;
}
