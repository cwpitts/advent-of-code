#include "util.hpp"

#include <fstream>

std::ostream& operator<<(std::ostream& out, const std::vector<char>& v)
{
  out << "[";

  for (size_t i = 0; i < v.size(); i += 1)
  {
    out << std::string(1, v[i]);

    if (i < v.size() - 1)
    {
      out << ", ";
    }
  }
  out << "]";

  return out;
}

std::vector<std::string> readInput(fs::path inputPath)
{
  std::vector<std::string> input;
  std::ifstream inFile(inputPath);

  std::string line;

  while (std::getline(inFile, line))
  {
    input.push_back(line);
  }

  inFile.close();
  return input;
}
