#ifndef AOC_2023_1_UTIL_HPP
#define AOC_2023_1_UTIL_HPP

#include <filesystem>
#include <ostream>
#include <string>
#include <vector>

namespace fs = std::filesystem;

// template <typename T>
// std::ostream& operator<<(std::ostream& out, const std::vector<T>& v)
// {
//   out << "[";

//   for (size_t i = 0; i < v.size(); i += 1)
//   {
//     const T& t = v[i];
//     out << std::to_string(t);

//     if (i < v.size() - 1)
//     {
//       out << ", ";
//     }
//   }
//   out << "]";

//   return out;
// }

std::ostream& operator<<(std::ostream& out, const std::vector<char>& v);

std::vector<std::string> readInput(fs::path inputPath);

#endif  // AOC_2023_1_UTIL_HPP
