#ifndef AOC_2023_8_UTIL_HPP
#define AOC_2023_8_UTIL_HPP

#include <filesystem>
#include <map>
#include <memory>
#include <ostream>
#include <set>
#include <string>
#include <vector>

namespace fs = std::filesystem;

std::ostream& operator<<(std::ostream& o, const std::vector<std::string> v);

struct Node
{
  std::vector<std::unique_ptr<Node>> children;
  std::string label;
};

struct Data
{
  std::map<std::string, std::vector<std::string>> graph;
  std::set<std::string> startNodes;
  std::set<std::string> endNodes;
  std::vector<unsigned int> instructions;
};

Data readInput(fs::path inputPath);

#endif  // AOC_2023_8_UTIL_HPP
