#include "util.hpp"

#include <algorithm>
#include <fstream>
#include <regex>

#include "fmt/format.h"

const std::regex LABEL_PATTERN("([0-9A-Z]+) = ");
const std::regex CHILD_PATTERN("\\(([0-9A-Z]+), ([0-9A-Z]+)\\)");

Data readInput(fs::path inputPath)
{
  std::ifstream inFile(inputPath);

  Data data;

  std::string line;

  while (std::getline(inFile, line))
  {
    if (data.instructions.size() == 0)
    {
      for (const char c : line)
      {
        data.instructions.push_back((unsigned int)(c == 'R'));
      }
      continue;
    }

    if (line == "")
    {
      continue;
    }

    std::smatch labelMatch;
    std::regex_search(line, labelMatch, LABEL_PATTERN);

    const std::string label = labelMatch[1].str();

    if (label.ends_with("A"))
    {
      data.startNodes.insert(label);
    }
    else if (label.ends_with("Z"))
    {
      data.endNodes.insert(label);
    }

    std::smatch childMatch;
    std::regex_search(line, childMatch, CHILD_PATTERN);

    const std::string left = childMatch[1].str();
    const std::string right = childMatch[2].str();

    data.graph[label] = std::vector<std::string>{left, right};
  }

  inFile.close();

  if (data.startNodes.size() != data.endNodes.size())
  {
    throw new std::runtime_error("Start/end node sets not the same size");
  }

  return data;
}

std::ostream& operator<<(std::ostream& o, const std::vector<std::string> v)
{
  o << "{";

  for (size_t i = 0; i < v.size(); i += 1)
  {
    o << v.at(i);

    if (i < v.size() - 1)
    {
      o << ", ";
    }
  }

  o << "}";

  return o;
}
