#ifndef AOC_2023_8_P2_HPP
#define AOC_2023_8_P2_HPP

#include <vector>

#include "util.hpp"

long long gcd(long long a, long long b);

long long lcm(const std::vector<long long>& values);

long long doP2(const Data& data);

#endif  // AOC_2023_8_P2_HPP
