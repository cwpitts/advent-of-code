#include "p2.hpp"

#include <iostream>
#include <map>
#include <numeric>
#include <vector>

#include "fmt/core.h"

bool done(const std::vector<long long>& lengths)
{
  for (const long long& l : lengths)
  {
    if (l == -1)
    {
      return false;
    }
  }

  return true;
}

long long doP2(const Data& data)
{
  long long steps = 0;

  std::vector<long long> lengths;
  std::vector<std::string> currentNodes;
  for (std::string s : data.startNodes)
  {
    currentNodes.push_back(s);
    lengths.push_back(-1);
  }

  int instruction = 0;

  while (not done(lengths))
  {
    for (size_t i = 0; i < currentNodes.size(); i += 1)
    {
      currentNodes.at(i) = data.graph.at(currentNodes.at(i))
                               .at(data.instructions.at(instruction));
    }

    instruction = (instruction + 1) % data.instructions.size();
    steps += 1;

    for (size_t i = 0; i < currentNodes.size(); i += 1)
    {
      if (data.endNodes.contains(currentNodes.at(i)) and lengths.at(i) == -1)
      {
        lengths.at(i) = steps;
      }
    }
  }

  // At this point, we know how long it will take each of them to make
  // it to the end, so we find the least common multiple of the
  // values.
  return lcm(lengths);
}

long long gcd(long long a, long long b)
{
  while (b != 0)
  {
    long long t = b;
    b = a % b;
    a = t;
  }

  return a;
}

long long lcm(const std::vector<long long>& values)
{
  long long result = values[0];

  for (size_t i = 1; i < values.size(); i += 1)
  {
    result = (((values[i] * result)) / (gcd(values[i], result)));
  }

  return result;
}
