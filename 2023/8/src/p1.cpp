#include "p1.hpp"

#include <iostream>

#include "fmt/format.h"

long long doP1(const Data& data)
{
  long long result = 0;

  std::string currentNode = "AAA";
  int instruction = 0;

  while (currentNode != "ZZZ")
  {
    currentNode =
        data.graph.at(currentNode).at(data.instructions.at(instruction));
    instruction = (instruction + 1) % data.instructions.size();
    result += 1;
  }

  return result;
}
