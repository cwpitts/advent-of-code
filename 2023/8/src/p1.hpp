#ifndef AOC_2023_8_P1_HPP
#define AOC_2023_8_P1_HPP

#include "util.hpp"

long long doP1(const Data& data);

#endif  // AOC_2023_8_P1_HPP
