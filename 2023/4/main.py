#!/usr/bin/env python3
""" Advent of Code Day 4
"""
import math
import timeit
from argparse import ArgumentParser, Namespace
from datetime import datetime, timedelta
from heapq import heapify, heappop, heappush
from pathlib import Path
from queue import Queue
from typing import Dict, List, Set, Tuple

import matplotlib.pyplot as plt
import numpy as np
from rich.progress import Progress
from sortedcontainers import SortedList


def parse_args(arglist: List[str] = None) -> Namespace:
    """Parse and validate program arguments.

    Args:
      arglist (List[str], optional):
        Argument list, defaults to None (read from stdin)

    Returns:
      Namespace: Parsed and validated program arguments
    """
    argp: ArgumentParser = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def do_p2(data: List[Tuple[Set[int], Set[int]]]) -> int:
    card_counts: Dict[int, int] = {i: 0 for i, _ in enumerate(data)}

    for index, (winning, card) in enumerate(data):
        card_counts[index] += 1
        matches: Set[int] = winning & card

        if len(matches) == 0:
            continue

        for i in range(index + 1, index + 1 + len(matches)):
            card_counts[i] += card_counts[index]

    return sum(card_counts.values())


def do_p1(data: List[Tuple[Set[int], Set[int]]]) -> int:
    score: int = 0
    for winning, card in data:
        matches: Set[int] = winning & card
        if len(matches) == 0:
            continue
        count: int = 2 ** (len(matches) - 1) if len(matches) > 1 else 1
        score += count

    return score


def parse_input(input_path: Path) -> List[Tuple[Set[int], Set[int]]]:
    cards: List[Tuple[Set[int], Set[int]]] = []

    with open(input_path, "r") as in_file:
        # Read input
        # Example input:
        # Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
        for line in in_file:
            winning, _, card = line.strip().partition(" | ")
            _, _, winning = winning.partition(": ")
            cards.append(
                (
                    set(list(map(int, winning.split()))),
                    set(list(map(int, card.split()))),
                )
            )

    return cards


def main(args: Namespace):
    data: List[Tuple[Set[int], Set[int]]] = parse_input(args.input)

    # Part 1
    p1_start: datetime = datetime.now()
    p1: int = do_p1(data)
    p1_end: datetime = datetime.now()
    p1_duration: timedelta = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start: datetime = datetime.now()
    p2: int = do_p2(data)
    p2_end: datetime = datetime.now()
    p2_duration: timedelta = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args: Namespace = parse_args()
    main(args)
