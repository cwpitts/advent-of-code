#include "p2.hpp"

#include <algorithm>

#include "p1.hpp"

long long doP2(const Data& data)
{
  Data revData;

  for (const std::vector<long long>& sequence : data.values)
  {
    std::vector<long long> rSequence(sequence.size());

    std::reverse_copy(std::begin(sequence), std::end(sequence),
                      std::begin(rSequence));

    revData.values.push_back(rSequence);
  }

  return doP1(revData);
}
