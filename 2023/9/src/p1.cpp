#include "p1.hpp"

#include <iostream>

#define LAST_OF(x) x[x.size() - 1]

bool allZeros(const std::vector<long long>& v)
{
  for (const long long i : v)
  {
    if (i != 0)
    {
      return false;
    }
  }

  return true;
}

long long doP1(const Data& data)
{
  long long result = 0;

  for (const std::vector<long long>& sequence : data.values)
  {
    std::vector<std::vector<long long>> differences{sequence};

    while (not allZeros(LAST_OF(differences)))
    {
      std::vector<long long> diff;

      for (size_t i = 0; i < LAST_OF(differences).size() - 1; i += 1)
      {
        long long newValue =
            LAST_OF(differences)[i + 1] - LAST_OF(differences)[i];
        diff.push_back(newValue);
      }

      differences.push_back(diff);
    }

    // Extrapolate
    for (size_t i = 0; i < differences.size(); i += 1)
    {
      if (i == 0)
      {
        LAST_OF(differences).push_back(0);
        continue;
      }

      size_t idx = differences.size() - 1 - i;

      long long prevLast = LAST_OF(differences[idx + 1]);
      long long last = LAST_OF(differences[idx]);

      differences[idx].push_back(last + prevLast);
    }

    result += LAST_OF(differences[0]);
  }

  return result;
}
