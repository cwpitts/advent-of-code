#ifndef AOC_2023_9_P2_HPP
#define AOC_2023_9_P2_HPP

#include "util.hpp"

long long doP2(const Data& data);

#endif  // AOC_2023_9_P2_HPP
