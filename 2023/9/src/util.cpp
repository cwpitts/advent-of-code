#include "util.hpp"

#include <fstream>
#include <sstream>
#include <string>

/*
	clang-format off

	0 3 6 9 12 15
	1 3 6 10 15 21
	10 13 16 21 30 45

	clang-format on
 */
Data readInput(fs::path inputPath)
{
  Data data;

  std::ifstream inFile(inputPath);

  std::string line;

  while (std::getline(inFile, line))
  {
    data.values.push_back(std::vector<long long>{});

    std::istringstream iss(line);

    long long value;

    while (iss >> value)
    {
      data.values[data.values.size() - 1].push_back(value);
    }
  }

  inFile.close();

  return data;
}

std::ostream& operator<<(std::ostream& o, const std::vector<long long>& v)
{
  o << "[";

  for (size_t i = 0; i < v.size(); i += 1)
  {
    o << v[i];

    if (i < v.size() - 1)
    {
      o << ", ";
    }
  }

  o << "]";

  return o;
}
