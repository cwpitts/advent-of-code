#ifndef AOC_2023_9_UTIL_HPP
#define AOC_2023_9_UTIL_HPP

#include <filesystem>
#include <ostream>
#include <string>
#include <vector>

namespace fs = std::filesystem;

struct Data
{
  std::vector<std::vector<long long>> values;
};

Data readInput(fs::path inputPath);

std::ostream& operator<<(std::ostream& o, const std::vector<long long>& v);

#endif  // AOC_2023_9_UTIL_HPP
