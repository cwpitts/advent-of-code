#ifndef AOC_2023_9_P1_HPP
#define AOC_2023_9_P1_HPP

#include "util.hpp"

bool allZeros(const std::vector<long long>& v);

long long doP1(const Data& data);

#endif  // AOC_2023_9_P1_HPP
