#include <gtest/gtest.h>

#include <cmath>
#include <cstdint>
#include <cstdio>
#include <filesystem>
#include <iterator>
#include <limits>
#include <memory>
#include <optional>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <typeinfo>
#include <vector>

#include "p1.hpp"
#include "p2.hpp"
#include "util.hpp"

namespace fs = std::filesystem;

const fs::path TEST_DIR = fs::current_path().parent_path() / "test";

class AOC2023Day2 : public ::testing::TestWithParam<
                        std::tuple<std::string, unsigned int, unsigned int>>
{
 protected:
};

TEST_P(AOC2023Day2, Part1)
{
  const std::string input = std::get<0>(GetParam());
  const unsigned long expectedOutput = std::get<1>(GetParam());

  const std::map<unsigned int, Game> data = readInput(TEST_DIR / input);

  unsigned long p1 = doP1(data);

  ASSERT_EQ(p1, expectedOutput);
}

TEST_P(AOC2023Day2, Part2)
{
  const std::string input = std::get<0>(GetParam());
  const unsigned long expectedOutput = std::get<2>(GetParam());

  const std::map<unsigned int, Game> data = readInput(TEST_DIR / input);

  unsigned long p2 = doP2(data);

  ASSERT_EQ(p2, expectedOutput);
}

INSTANTIATE_TEST_CASE_P(RunProgramTests, AOC2023Day2,
                        ::testing::Values(std::make_tuple("input", 2377, 71220),
                                          std::make_tuple("test1", 8, 2286)));
