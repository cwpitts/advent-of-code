#ifndef AOC_2023_2_UTIL_HPP
#define AOC_2023_2_UTIL_HPP

#include <filesystem>
#include <map>
#include <string>
#include <vector>

namespace fs = std::filesystem;

/**
 * The Elf would first like to know which games would have been
 * possible if the bag contained only 12 red cubes, 13 green cubes,
 * and 14 blue cubes?
 */
const std::map<std::string, unsigned int> MAX_COLOR_COUNTS{
    {"red", 12}, {"green", 13}, {"blue", 14}};

typedef std::map<std::string, unsigned int> Round;
typedef std::vector<Round> Game;

std::map<unsigned int, Game> readInput(fs::path inputPath);

#endif  // AOC_2023_2_UTIL_HPP
