#include "p1.hpp"

#include <algorithm>

#include "spdlog/spdlog.h"
#include "util.hpp"

bool isValidGame(const Game& game)
{
  for (const Round& round : game)
  {
    for (const std::pair<std::string, unsigned int> draw : round)
    {
      if (draw.second > MAX_COLOR_COUNTS.at(draw.first))
      {
        return false;
      }
    }
  }

  return true;
}

unsigned long doP1(const std::map<unsigned int, Game>& data)
{
  unsigned long result = 0;

  for (const std::pair<unsigned int, Game> game : data)
  {
    if (not isValidGame(game.second))
    {
      continue;
    }
    result += game.first;
  }

  return result;
}
