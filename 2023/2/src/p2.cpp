#include "p2.hpp"

#include "util.hpp"

unsigned long doP2(const std::map<unsigned int, Game>& data)
{
  unsigned long result = 0;

  for (const std::pair<unsigned int, Game> game : data)
  {
    std::map<std::string, unsigned int> minColors;
    for (const Round& round : game.second)
    {
      for (const std::pair<std::string, unsigned int> draw : round)
      {
        if (not minColors.contains(draw.first))
        {
          minColors[draw.first] = draw.second;
        }
        else
        {
          minColors[draw.first] = std::max(minColors[draw.first], draw.second);
        }
      }
    }

    unsigned long power = 1;
    for (std::pair<std::string, unsigned int> item : minColors)
    {
      power *= item.second;
    }

    result += power;
  }

  return result;
}
