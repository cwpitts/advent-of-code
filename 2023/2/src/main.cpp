/* Advent of Code 2023 problem 2
 */
#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <map>
#include <ratio>
#include <set>
#include <sstream>
#include <string>
#include <vector>

#include "fmt/format.h"
#include "p1.hpp"
#include "p2.hpp"
#include "util.hpp"

using time_point = std::chrono::high_resolution_clock::time_point;
using duration = std::chrono::duration<double>;

time_point clockTime() { return std::chrono::high_resolution_clock::now(); }

int main(int argc, char* argv[])
{
  const std::map<unsigned int, Game> games = readInput(argv[1]);

  time_point p1_start = clockTime();
  unsigned long p1 = doP1(games);
  time_point p1_end = clockTime();
  duration p1_time = p1_end - p1_start;
  std::cout << p1 << " in "
            << std::chrono::duration_cast<std::chrono::milliseconds>(p1_time)
            << std::endl;

  time_point p2_start = clockTime();
  unsigned long p2 = doP2(games);
  time_point p2_end = clockTime();
  duration p2_time = p2_end - p2_start;
  std::cout << p2 << " in "
            << std::chrono::duration_cast<std::chrono::milliseconds>(p2_time)
            << std::endl;

  // Write output
  std::ofstream outFile(argv[2]);
  outFile.close();
  return 0;
}
