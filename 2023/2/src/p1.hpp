#ifndef AOC_2023_2_P1_HPP
#define AOC_2023_2_P1_HPP

#include "util.hpp"

unsigned long doP1(const std::map<unsigned int, Game>& data);

#endif  // AOC_2023_2_P1_HPP
