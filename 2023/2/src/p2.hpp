#ifndef AOC_2023_2_P2_HPP
#define AOC_2023_2_P2_HPP

#include "util.hpp"

unsigned long doP2(const std::map<unsigned int, Game>& data);

#endif  // AOC_2023_2_P2_HPP
