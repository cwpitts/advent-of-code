#include "util.hpp"

#include <fstream>
#include <regex>
#include <string>

#include "spdlog/spdlog.h"

const std::regex GAME_REGEX("Game ([0-9]+):");
const std::regex DRAW_REGEX("([0-9]+) (red|green|blue)");

std::map<unsigned int, Game> readInput(fs::path inputPath)
{
  std::map<unsigned int, Game> games;

  std::ifstream inFile(inputPath);

  std::string line;

  while (std::getline(inFile, line))
  {
    Game game;
    std::smatch gameMatch;
    std::regex_search(line, gameMatch, GAME_REGEX);

    unsigned int gameID = std::stoul(gameMatch[1].str());

    line = line.substr(line.find(": ") + 2);

    unsigned long startIndex = 0;
    while (line.size() != 0)
    {
      std::string round;
      if ((startIndex = line.find(";")) != std::string::npos)
      {
        round = line.substr(0, startIndex);
        line = line.substr(startIndex + 2);
      }
      else
      {
        round = line;
        line = "";
      }

      Round newRound;

      std::smatch drawMatch;
      for (std::regex_iterator i =
               std::sregex_iterator(round.begin(), round.end(), DRAW_REGEX);
           i != std::sregex_iterator(); i++)
      {
        std::smatch match = *i;
        unsigned int count = std::stoul(match[1].str());
        std::string color = match[2].str();
        newRound[color] = count;
      }
      game.push_back(newRound);
    }
    games[gameID] = game;
  }

  inFile.close();

  return games;
}
