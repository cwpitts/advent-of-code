#include "p1.hpp"

#include <algorithm>
#include <iostream>

#include "util.hpp"

long long doP1(std::vector<Entry> data)
{
  long long result = 0;

  std::sort(data.begin(), data.end(), sortEntries);

  for (size_t i = 0; i < data.size(); i += 1)
  {
    result += data[i].second * (i + 1);
  }

  return result;
}
