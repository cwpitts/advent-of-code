#ifndef AOC_2023_7_P1_HPP
#define AOC_2023_7_P1_HPP

#include "util.hpp"

long long doP1(const std::vector<Entry> data);

#endif  // AOC_2023_7_P1_HPP
