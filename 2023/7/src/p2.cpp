#include "p2.hpp"

#include <algorithm>
#include <iostream>

long long doP2(std::vector<Entry> data)
{
  for (Entry& entry : data)
  {
    for (long long& i : entry.first)
    {
      if (i == JACK_VAL)
      {
        i = JOKER_VAL;
      }
    }
  }

  long long result = 0;

  std::sort(data.begin(), data.end(), sortEntries);

  for (size_t i = 0; i < data.size(); i += 1)
  {
    result += data[i].second * (i + 1);
  }

  return result;
}
