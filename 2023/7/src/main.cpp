/* Advent of Code 2023 problem 7
 */
#include <algorithm>
#include <chrono>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>

#include "CLI/CLI11.hpp"
#include "p1.hpp"
#include "p2.hpp"
#include "rang/rang.hpp"
#include "util.hpp"

using time_point = std::chrono::high_resolution_clock::time_point;
using duration = std::chrono::duration<double>;
using DoubleMilliseconds =
    std::chrono::duration<double, std::chrono::milliseconds::period>;

namespace fs = std::filesystem;

time_point clockTime() { return std::chrono::high_resolution_clock::now(); }

int main(int argc, char* argv[])
{
  CLI::App app;

  fs::path inputPath;
  app.add_option("input", inputPath, "Path to input data")->required(true);

  CLI11_PARSE(app, argc, argv);

  const std::vector<Entry> data = readInput(inputPath);

  time_point p1_start = clockTime();
  long long p1 = doP1(data);
  time_point p1_end = clockTime();
  duration p1_time = p1_end - p1_start;
  std::cout
      << rang::style::bold << p1 << rang::style::reset << " in "
      << rang::fg::green
      << DoubleMilliseconds(
             p1_end -
             p1_start)  // std::chrono::duration_cast<std::chrono::milliseconds>(p1_time)
      << rang::fg::reset << std::endl;

  time_point p2_start = clockTime();
  long long p2 = doP2(data);
  time_point p2_end = clockTime();
  duration p2_time = p2_end - p2_start;
  std::cout
      << rang::style::bold << p2 << rang::style::reset << " in "
      << rang::fg::green
      << DoubleMilliseconds(
             p2_end -
             p2_start)  // std::chrono::duration_cast<std::chrono::milliseconds>(p2_time)
      << rang::fg::reset << std::endl;

  return 0;
}
