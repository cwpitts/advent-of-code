#ifndef AOC_2023_7_P2_HPP
#define AOC_2023_7_P2_HPP

#include "util.hpp"

long long doP2(std::vector<Entry> data);

#endif  // AOC_2023_7_P2_HPP
