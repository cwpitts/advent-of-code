#ifndef AOC_2023_7_UTIL_HPP
#define AOC_2023_7_UTIL_HPP

#include <filesystem>
#include <ostream>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

namespace fs = std::filesystem;

enum HandType
{
  HIGH_CARD = 1,
  ONE_PAIR,
  TWO_PAIR,
  THREE_KIND,
  FULL_HOUSE,
  FOUR_KIND,
  FIVE_KIND,
};

constexpr long long JOKER_VAL = 1;
constexpr long long JACK_VAL = 11;

typedef std::vector<long long> Hand;

typedef std::pair<Hand, long long> Entry;

std::vector<Entry> readInput(fs::path inputPath);

unsigned int getValue(char c);

HandType getHandType(Hand hand);

std::ostream& operator<<(std::ostream& o, const std::vector<Entry> e);

std::ostream& operator<<(std::ostream& o, const HandType t);

bool sortEntries(Entry e1, Entry e2);

#endif  // AOC_2023_7_UTIL_HPP
