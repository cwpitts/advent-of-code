#include "util.hpp"

#include <fstream>
#include <map>
#include <set>
#include <sstream>
#include <stdexcept>

#include "fmt/format.h"

unsigned int getValue(char c)
{
  switch (c)
  {
    case 'A': {
      return 14;
    }
    case 'K': {
      return 13;
    }
    case 'Q': {
      return 12;
    }
    case 'J': {
      return 11;
    }
    case 'T': {
      return 10;
    };
    default: {
      return c - '0';
    }
  }

  throw new std::runtime_error("Invalid value " + std::to_string(c));
}

char getFace(unsigned int v)
{
  switch (v)
  {
    case 14: {
      return 'A';
    }
    case 13: {
      return 'K';
    }
    case 12: {
      return 'Q';
    }
    case 11: {
      return 'J';
    }
    case 10: {
      return 'T';
    }
    case 1: {
      return 'J';
    }
    default: {
      return v + '0';
    }
  }
}

std::vector<Entry> readInput(fs::path inputPath)
{
  std::ifstream inFile(inputPath);

  std::vector<Entry> data;

  std::string line;

  while (std::getline(inFile, line))
  {
    std::string hand;
    unsigned int bid;

    std::istringstream iss(line);

    iss >> hand;
    iss >> bid;

    Entry entry{Hand{
                    getValue(hand[0]),
                    getValue(hand[1]),
                    getValue(hand[2]),
                    getValue(hand[3]),
                    getValue(hand[4]),
                },
                bid};

    data.push_back(entry);
  }

  inFile.close();

  return data;
}

HandType getHandType(Hand hand)
{
  std::map<unsigned int, unsigned int> counts;
  std::set<unsigned int> seenValues;

  for (size_t i = 0; i < hand.size(); i += 1)
  {
    seenValues.insert(hand[i]);
    if (not counts.contains(hand[i]))
    {
      counts[hand[i]] = 0;
    }
    counts[hand[i]] += 1;
  }

  // Handle jokers, which are coded as ones
  if (counts[JOKER_VAL] == hand.size())
  {
    return HandType::FIVE_KIND;
  }
  if (counts.contains(JOKER_VAL))
  {
    // Switch to most common, highest card
    unsigned int highestCount = 0;
    unsigned int highestValue = 0;

    for (std::pair<const unsigned int, unsigned int> item : counts)
    {
      unsigned int value;
      unsigned int count;

      std::tie(value, count) = item;

      if (value == 1)
      {
        continue;
      }

      if (highestCount == 0)
      {
        // Base case: entered once
        highestValue = value;
        highestCount = count;
      }
      else if (count > highestCount)
      {
        // Found a value with a higher count
        highestValue = value;
        highestCount = count;
      }
      else if (count == highestCount && value > highestValue)
      {
        // Found a value with the *same* count, but a higher value
        highestValue = value;
        highestCount = count;
      }
    }
    counts[highestValue] += counts[JOKER_VAL];
    counts.erase(JOKER_VAL);
    seenValues.erase(JOKER_VAL);
  }

  // Five of a kind is fairly obvious
  if (seenValues.size() == 1)
  {
    return HandType::FIVE_KIND;
  }

  if (seenValues.size() == 2)
  {
    // Could have four of a kind:
    // AAAAB
    // ABBBB
    //
    // Or full house:
    // AAABB
    // AABBB
    for (std::pair<unsigned int, unsigned int> item : counts)
    {
      if (item.second == 4)
      {
        return HandType::FOUR_KIND;
      }
    }

    return HandType::FULL_HOUSE;
  }

  if (seenValues.size() == 3)
  {
    // Could have three of a kind or two pair:
    // AAABC
    // AABBC
    for (std::pair<unsigned int, unsigned int> item : counts)
    {
      if (item.second == 3)
      {
        return HandType::THREE_KIND;
      }
    }

    return HandType::TWO_PAIR;
  }

  if (seenValues.size() == 4)
  {
    return HandType::ONE_PAIR;
  }

  return HIGH_CARD;
}

std::ostream& operator<<(std::ostream& o, const std::vector<Entry> e)
{
  for (size_t i = 0; i < e.size(); i += 1)
  {
    o << "(Rank " << i + 1 << ") ";

    for (size_t j = 0; j < e[i].first.size(); j += 1)
    {
      o << getFace(e[i].first[j]);
      if (j < e[i].first.size() - 1)
      {
        o << "|";
      }
    }

    o << " " << e[i].second << " " << getHandType(e[i].first);

    if (i < e.size() - 1)
    {
      o << std::endl;
    }
  }

  return o;
}

std::ostream& operator<<(std::ostream& o, const HandType t)
{
  switch (t)
  {
    case HandType::FIVE_KIND: {
      o << "FIVE_KIND";
      break;
    }
    case HandType::FOUR_KIND: {
      o << "FOUR_KIND";
      break;
    }
    case HandType::FULL_HOUSE: {
      o << "FULL_HOUSE";
      break;
    }
    case HandType::THREE_KIND: {
      o << "THREE_KIND";
      break;
    }
    case HandType::TWO_PAIR: {
      o << "TWO_PAIR";
      break;
    }
    case HandType::ONE_PAIR: {
      o << "ONE_PAIR";
      break;
    }
    case HandType::HIGH_CARD: {
      o << "HIGH_CARD";
      break;
    }
  }

  return o;
}

bool sortEntries(Entry e1, Entry e2)
{
  Hand h1 = e1.first;
  Hand h2 = e2.first;

  HandType t1 = getHandType(h1);
  HandType t2 = getHandType(h2);

  if (t1 != t2)
  {
    return t1 < t2;
  }

  // For HIGH_CARD or equal types, compare values
  for (size_t i = 0; i < h1.size(); i += 1)
  {
    if (h1[i] != h2[i])
    {
      return h1[i] < h2[i];
    }
  }

  return false;
}
