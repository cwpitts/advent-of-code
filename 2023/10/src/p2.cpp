#include "p2.hpp"

#include "p1.hpp"
#include "util.hpp"

long long count_inner_polygon(const Data& data, DistanceMap distMap)
{
  long long count = 0;
  bool currentlyIn = false;

  // Track the number of vertical pipe crossings: even number means out, odd
  // number means in.

  for (unsigned int i = 0; i < data.map.size(); i += 1)
  {
    currentlyIn = false;

    for (unsigned int j = 0; j < data.map[i].size(); j += 1)
    {
      char c = data.map[i][j].c;

      // Switch parity
      if (distMap.contains(Coordinate{i, j}))
      {
        if (c == '|' or c == 'J' or c == 'L')
        {
          currentlyIn = not currentlyIn;
        }
      }
      else if (currentlyIn)
      {
        count += 1;
      }
    }
  }

  return count;
}

long long do_p2(const Data& data)
{
  DistanceMap distMap = compute_distance_map(data);
  return count_inner_polygon(data, distMap);
}
