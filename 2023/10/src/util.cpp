#include "util.hpp"

#include <fstream>
#include <iostream>
#include <stdexcept>
#include <string>

#include "fmt/format.h"

unsigned long long compute_adj_index(unsigned long long i, const Data& d,
                                     unsigned long long j)
{
  return i * d.map[0].size() + j;
}

struct Pipe parsePipe(char c)
{
  /*
		clang-format off

		| is a vertical pipe connecting north and south.
		- is a horizontal pipe connecting east and west.
		L is a 90-degree bend connecting north and east.
		J is a 90-degree bend connecting north and west.
		7 is a 90-degree bend connecting south and west.
		F is a 90-degree bend connecting south and east.
		. is ground; there is no pipe in this tile.
		S is the starting position of the animal; there is a
		pipe on this tile, but your sketch doesn't show
		what shape the pipe has.
		
		clang-format on
	*/

  Pipe pipe;
  pipe.c = c;

  switch (c)
  {
    case '|': {
      pipe.connections = std::vector<Connection>{NORTH, SOUTH};
      break;
    }
    case '-': {
      pipe.connections = std::vector<Connection>{EAST, WEST};
      break;
    }
    case 'L': {
      pipe.connections = std::vector<Connection>{NORTH, EAST};
      break;
    }
    case 'J': {
      pipe.connections = std::vector<Connection>{NORTH, WEST};
      break;
    }
    case '7': {
      pipe.connections = std::vector<Connection>{SOUTH, WEST};
      break;
    }
    case 'F': {
      pipe.connections = std::vector<Connection>{SOUTH, EAST};
      break;
    }
    case '.': {
      pipe.connections = std::vector<Connection>{};
      break;
    }
    case 'S': {
      pipe.connections = std::vector<Connection>{};
      break;
    }
    default: {
      throw std::runtime_error{"Bad pipe character " + std::string(1, c)};
    }
  }

  return pipe;
}

Data readInput(fs::path inputPath)
{
  Data data;

  std::ifstream inFile(inputPath);

  std::vector<std::string> lines;
  std::string line;

  while (std::getline(inFile, line))
  {
    lines.push_back(line);
  }

  // fmt::print("Lines: {}\n", lines.size());

  data.graph = std::vector<std::vector<bool>>(
      lines.size() * lines.size(),
      std::vector<bool>(lines.size() * lines.size(), false));
  data.map = std::vector<std::vector<Pipe>>(
      lines.size(),
      std::vector<Pipe>(line.size(), Pipe{'.', std::vector<Connection>{}}));

  for (unsigned long long i = 0; i < lines.size(); i += 1)
  {
    for (unsigned long long j = 0; j < lines[i].size(); j += 1)
    {
      char value = lines[i][j];

      if (value == 'S')
      {
        data.S = Coordinate{(long long)(i), (long long)(j)};
      }
      Pipe pipe = parsePipe(value);
      data.map[i][j] = pipe;
    }
  }

  inFile.close();

  return data;
}

// Data readInput(fs::path inputPath)
// {
//   Data data;

//   std::ifstream inFile(inputPath);

//   std::vector<std::string> lines;
//   std::string line;

//   while (std::getline(inFile, line))
//   {
//     lines.push_back(line);
//   }

//   fmt::print("Lines: {}\n", lines.size());

//   data.graph = std::vector<std::vector<bool>>(
//       lines.size() * lines.size(),
//       std::vector<bool>(lines.size() * lines.size(), false));
//   data.map = std::vector<std::vector<char>>(
//       lines.size(), std::vector<char>(line.size(), '.'));

//   fmt::print("Graph size: {}x{}\n", data.graph.size(), data.graph[0].size());
//   fmt::print("Map size: {}x{}\n", data.map.size(), data.map[0].size());

//   for (unsigned long long i = 0; i < lines.size(); i += 1)
//   {
//     for (unsigned long long j = 0; j < lines[i].size(); j += 1)
//     {
//       char value = lines[i][j];
//       data.map[i][j] = value;

//       /*
//   			clang-format off

//   			| is a vertical pipe connecting north and south.
//   			- is a horizontal pipe connecting east and west.
//   			L is a 90-degree bend connecting north and east.
//   			J is a 90-degree bend connecting north and west.
//   			7 is a 90-degree bend connecting south and west.
//   			F is a 90-degree bend connecting south and east.
//   			. is ground; there is no pipe in this tile.
//   			S is the starting position of the animal; there is a
// 				pipe on this tile, but your sketch doesn't show
// 				what shape the pipe has.

//   			clang-format on
//        */

//       // Adjacency matrix row index is i * mapRowSize + j
//       unsigned long long adjIndex = compute_adj_index(i, data, j);
//       data.graph[adjIndex][adjIndex] = true;

//       switch (value)
//       {
//         case 'S': {
//           data.S = std::make_pair(i, j);
//           break;
//         }
//         case '.': {
//           // . is ground; there is no pipe in this tile.
//           break;
//         }
//         case '|': {
//           // | is a vertical pipe connecting north and south.
//           if (i > 0)
//           {
//             data.graph[adjIndex][compute_adj_index(i - 1, data, j)] = true;
//           }
//           if (i < data.map.size() - 1)
//           {
//             data.graph[adjIndex][compute_adj_index(i + 1, data, j)] = true;
//           }
//           break;
//         }
//         case '-': {
//           // - is a horizontal pipe connecting east and west.
//           if (j > 0)
//           {
//             data.graph[adjIndex][compute_adj_index(i, data, j - 1)] = true;
//           }
//           if (j < data.map[0].size() - 1)
//           {
//             data.graph[adjIndex][compute_adj_index(i, data, j + 1)] = true;
//           }
//           break;
//         }
//         case 'L': {
//           // L is a 90-degree bend connecting north and east.
//           if (i > 0)
//           {
//             data.graph[adjIndex][compute_adj_index(i - 1, data, j)] = true;
//           }
//           if (j < data.map[0].size() - 1)
//           {
//             data.graph[adjIndex][compute_adj_index(i, data, j + 1)] = true;
//           }
//           break;
//         }
//         case 'J': {
//           // J is a 90-degree bend connecting north and west.
//           if (i > 0)
//           {
//             data.graph[adjIndex][compute_adj_index(i - 1, data, j)] = true;
//           }
//           if (j > 0)
//           {
//             data.graph[adjIndex][compute_adj_index(i, data, j - 1)] = true;
//           }
//           break;
//         }
//         case '7': {
//           // 7 is a 90-degree bend connecting south and west.
//           if (i < data.map.size() - 1)
//           {
//             data.graph[adjIndex][compute_adj_index(i + 1, data, j)] = true;
//           }
//           if (j > 0)
//           {
//             data.graph[adjIndex][compute_adj_index(i, data, j - 1)] = true;
//           }
//           break;
//         }
//         case 'F': {
//           // F is a 90-degree bend connecting south and east.
//           if (i < data.map.size() - 1)
//           {
//             data.graph[adjIndex][compute_adj_index(i + 1, data, j)] = true;
//           }
//           if (j < data.map[0].size() - 1)
//           {
//             data.graph[adjIndex][compute_adj_index(i, data, j + 1)] = true;
//           }
//           break;
//         }
//       }
//     }
//   }

//   inFile.close();

//   return data;
// }

std::ostream& operator<<(std::ostream& o, const Data& data)
{
  for (const std::vector<bool>& row : data.graph)
  {
    for (size_t i = 0; i < row.size(); i += 1)
    {
      o << int(row[i]);
      if (i < row.size() - 1)
      {
        o << " ";
      }
    }
    o << std::endl;
  }

  for (const std::vector<Pipe>& row : data.map)
  {
    for (size_t i = 0; i < row.size(); i += 1)
    {
      o << row[i].c;
      if (i < row.size() - 1)
      {
        o << " ";
      }
    }
    o << std::endl;
  }

  o << "S: (" << data.S.row << ", " << data.S.col << ")";

  return o;
}

bool operator<(const Coordinate& lhs, const Coordinate& rhs)
{
  return (lhs.col < rhs.col) or (lhs.col == rhs.col && lhs.row < rhs.row);
}

bool operator==(const Coordinate& lhs, const Coordinate& rhs)
{
  return (lhs.row == rhs.row) and (lhs.col == rhs.col);
}

bool operator==(const Connection& lhs, const Connection& rhs)
{
  return (lhs.dx == rhs.dx) and (lhs.dy == rhs.dy);
}

std::ostream& operator<<(std::ostream& o, const std::vector<Connection>& v)
{
  for (const Connection& c : v)
  {
    if (c == NORTH)
    {
      o << "NORTH" << " ";
      continue;
    }

    if (c == SOUTH)
    {
      o << "SOUTH" << " ";
      continue;
    }

    if (c == EAST)
    {
      o << "EAST" << " ";
      continue;
    }

    if (c == WEST)
    {
      o << "WEST" << " ";
      continue;
    }
  }

  return o;
}
