/* Advent of Code 2023 problem 10
 */
#include <chrono>
#include <cmath>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <string>

#include "CLI/CLI11.hpp"
#include "p1.hpp"
#include "p2.hpp"
#include "rang/rang.hpp"
#include "util.hpp"

using time_point = std::chrono::high_resolution_clock::time_point;
using duration = std::chrono::duration<double>;
using DoubleMilliseconds =
    std::chrono::duration<double, std::chrono::milliseconds::period>;

namespace fs = std::filesystem;

time_point clockTime() { return std::chrono::high_resolution_clock::now(); }

int main(int argc, char* argv[])
{
  CLI::App app;

  fs::path inputPath;
  app.add_option("input", inputPath, "Path to input data")->required(true);

  bool visualize = false;
  app.add_flag("--visualize", visualize, "Save visualization artifacts");

  CLI11_PARSE(app, argc, argv);

  const Data data = readInput(inputPath);

  time_point p1_start = clockTime();
  long long p1 = do_p1(data);
  time_point p1_end = clockTime();
  duration p1_time = p1_end - p1_start;
  std::cout << rang::style::bold << p1 << rang::style::reset << " in "
            << rang::fg::green << DoubleMilliseconds(p1_time) << rang::fg::reset
            << std::endl;

  time_point p2_start = clockTime();
  long long p2 = do_p2(data);
  time_point p2_end = clockTime();
  duration p2_time = p2_end - p2_start;
  std::cout << rang::style::bold << p2 << rang::style::reset << " in "
            << rang::fg::green << DoubleMilliseconds(p2_end - p2_start)
            << rang::fg::reset << std::endl;

  if (visualize)
  {
    fs::path outputDir("output");
    fs::create_directory(outputDir);

    std::ofstream outFile(outputDir / "distmap");

    DistanceMap distMap = compute_distance_map(data);

    for (unsigned int i = 0; i < data.map.size(); i += 1)
    {
      for (unsigned int j = 0; j < data.map[i].size(); j += 1)
      {
        Coordinate coord{i, j};

        if (distMap.contains(coord))
        {
          outFile << distMap[coord];
        }
        else
        {
          outFile << -1;
        }

        if (j < data.map[i].size() - 1)
        {
          outFile << ',';
        }
      }
      outFile << std::endl;
    }

    outFile.close();
  }

  return 0;
}
