#ifndef AOC_2023_10_UTIL_HPP
#define AOC_2023_10_UTIL_HPP

#include <filesystem>
#include <iostream>
#include <ostream>
#include <string>
#include <vector>

namespace fs = std::filesystem;

struct Connection
{
  int dy;
  int dx;
};

const Connection NORTH{-1, 0};
const Connection SOUTH{1, 0};
const Connection EAST{0, 1};
const Connection WEST{0, -1};

struct Pipe
{
  char c;
  std::vector<Connection> connections;
};

struct Coordinate
{
  long long row;
  long long col;
};

struct Data
{
  std::vector<std::vector<bool>> graph;
  std::vector<std::vector<Pipe>> map;
  Coordinate S;
};

Data readInput(fs::path inputPath);

std::ostream& operator<<(std::ostream& o, const Data& data);

unsigned long long compute_adj_index(unsigned long long i, const Data& d,
                                     unsigned long long j);

struct Pipe parsePipe(char c);

bool operator<(const Coordinate& lhs, const Coordinate& rhs);

bool operator==(const Coordinate& lhs, const Coordinate& rhs);

bool operator==(const Connection& lhs, const Connection& rhs);

std::ostream& operator<<(std::ostream& o, const std::vector<Connection>& v);

#endif  // AOC_2023_10_UTIL_HPP
