#ifndef AOC_2023_10_P2_HPP
#define AOC_2023_10_P2_HPP

#include "util.hpp"

long long do_p2(const Data& data);

#endif  // AOC_2023_10_P2_HPP
