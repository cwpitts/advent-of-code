#ifndef AOC_2023_10_P1_HPP
#define AOC_2023_10_P1_HPP

#include <algorithm>
#include <map>

#include "util.hpp"

using DistanceMap = std::map<Coordinate, long long>;

std::map<Coordinate, long long> compute_distance_map(const Data& data);
long long do_p1(const Data& data);

template <typename T>
bool vector_contains(const std::vector<T>& v, const T& item)
{
  return std::find(v.begin(), v.end(), item) != v.end();
}

#endif  // AOC_2023_10_P1_HPP
