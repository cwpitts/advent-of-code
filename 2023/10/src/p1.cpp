#include "p1.hpp"

#include <queue>

std::map<Coordinate, long long> compute_distance_map(const Data& data)
{
  // Compute flood-fill minimum distance
  std::map<Coordinate, long long> distances;
  distances.insert({data.S, 0});

  std::queue<Coordinate> queue;
  queue.push(data.S);

  while (not queue.empty())
  {
    Coordinate currentLoc = queue.front();
    queue.pop();

    // S is a special case, no way to know what pipe it is, so you
    // can't check its connections. In that case, you need to check
    // for neighbors that pipe *into* S.
    if (currentLoc == data.S)
    {
      /*
				clang-format off
				
				Idea: only need four checks:

				- Does the NORTH neighbor have a SOUTH
				connection?
				- Does the SOUTH neighbor have a NORTH
				connection?
				- Does the EAST neighbor have a WEST
				connection?
				- Does the WEST neighbor have an EAST
				connection?
				
				For each of the above which is true, enqueue,
				since they connect to S.

				clang-format on
       */

      Coordinate northNeighbor{currentLoc.row + NORTH.dy,
                               currentLoc.col + NORTH.dx};
      Coordinate southNeighbor{currentLoc.row + SOUTH.dy,
                               currentLoc.col + SOUTH.dx};
      Coordinate eastNeighbor{currentLoc.row + EAST.dy,
                              currentLoc.col + EAST.dx};
      Coordinate westNeighbor{currentLoc.row + WEST.dy,
                              currentLoc.col + WEST.dx};

      // North check
      if (currentLoc.row > 0 and
          vector_contains(
              data.map[northNeighbor.row][northNeighbor.col].connections,
              SOUTH))
      {
        // fmt::print("Will check north from S\n");
        distances[northNeighbor] = 1;
        queue.push(northNeighbor);
      }
      // South check
      // fmt::print("data.map[SOUTH][SOUTH].connections contains NORTH? {}\n",
      //            vector_contains(
      //                data.map[southNeighbor.row][southNeighbor.col].connections,
      //                NORTH));
      // std::cout << data.map[southNeighbor.row][southNeighbor.col].connections
      //           << std::endl;
      if (currentLoc.row < data.map.size() - 1 and
          vector_contains(
              data.map[southNeighbor.row][southNeighbor.col].connections,
              NORTH))
      {
        // fmt::print("Will check south from S (row {}, col {})\n",
        //            southNeighbor.row, southNeighbor.col);
        distances[southNeighbor] = 1;
        queue.push(southNeighbor);
      }
      // East check
      if (currentLoc.col < data.map[0].size() - 1 and
          vector_contains(
              data.map[eastNeighbor.row][eastNeighbor.col].connections, WEST))
      {
        // fmt::print("Will check east from S (row {}, col {})\n",
        //            eastNeighbor.row, eastNeighbor.col);
        distances[eastNeighbor] = 1;
        queue.push(eastNeighbor);
      }
      // West check
      if (currentLoc.col > 0 and
          vector_contains(
              data.map[westNeighbor.row][westNeighbor.col].connections, EAST))
      {
        // fmt::print("Will check west from S\n");
        distances[westNeighbor] = 1;
        queue.push(westNeighbor);
      }
      continue;
    }

    // Check adjacent pixels using map
    Pipe currentPipe = data.map[currentLoc.row][currentLoc.col];

    for (const Connection& connection : currentPipe.connections)
    {
      // Each pipe has a dx/dy, those can be checked to make sure they're not
      // going out of bounds and they're not invalid (just a '.')
      Coordinate newLoc{currentLoc.row + connection.dy,
                        currentLoc.col + connection.dx};

      // Out-of-bounds check
      if (newLoc.row < 0 or newLoc.col < 0 or
          newLoc.row > data.map.size() - 1 or
          newLoc.col > data.map[0].size() - 1)
      {
        continue;
      }

      // '.' check
      if (data.map[newLoc.row][newLoc.col].c == '.')
      {
        continue;
      }

      // If the pipe isn't taking you out of bounds, check to see if
      // the neighbor has already been colored
      if (distances.contains(newLoc))
      {
        continue;
      }

      // If the neighbor hasn't been colored, color it (add it to the
      // distance map) and enqueue it for processing
      distances.insert({newLoc, distances[currentLoc] + 1});
      queue.push(newLoc);
    }
  }

  return distances;
}

long long do_p1(const Data& data)
{
  DistanceMap distMap = compute_distance_map(data);
  long long p1 = 0;
  for (DistanceMap::iterator itr = distMap.begin(); itr != distMap.end(); itr++)
  {
    p1 = std::max(p1, itr->second);
  }

  return p1;
}
