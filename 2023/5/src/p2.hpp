#ifndef AOC_2023_5_P2_HPP
#define AOC_2023_5_P2_HPP

#include "util.hpp"

std::ostream& operator<<(std::ostream& o, const Range& range);

std::ostream& operator<<(std::ostream& o, const std::vector<long long>& v);

std::ostream& operator<<(std::ostream& o, const std::set<long long>& v);

long long inverseRange(const std::vector<Range>& ranges,
                       const long long output);

std::set<long long> reverseRange(const std::set<long long>& endpoints,
                                 const std::vector<Range>& ranges);

std::set<long long> invertMapping(const std::vector<Range>& ranges,
                                  const std::set<long long> outputs);

long long doP2(const Data& data);

#endif  // AOC_2023_5_P2_HPP
