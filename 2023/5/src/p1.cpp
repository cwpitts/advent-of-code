#include "p1.hpp"

#include <climits>

#include "fmt/format.h"

long long doP1(const Data& data)
{
  long long result = UINT_MAX;

  for (const long long seed : data.seeds)
  {
    // Translate from seed to location
    result = std::min(result, translate(data, seed));
  }

  return result;
}
