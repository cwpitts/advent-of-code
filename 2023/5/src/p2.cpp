#include "p2.hpp"

#include <algorithm>
#include <climits>
#include <iostream>
#include <ostream>
#include <utility>

#include "fmt/format.h"
#include "util.hpp"

std::ostream& operator<<(std::ostream& o, const Range& range)
{
  o << "Range [" << range.destStart << ", " << range.sourceStart << ", "
    << range.length << "]";

  return o;
}

std::ostream& operator<<(std::ostream& o, const std::vector<long long>& v)
{
  for (unsigned int i = 0; i < v.size(); i += 1)
  {
    o << v[i];

    if (i < v.size() - 1)
    {
      o << " ";
    }
  }
  return o;
}

std::ostream& operator<<(std::ostream& o, const std::set<long long>& v)
{
  o << "{";
  unsigned int idx = 0;
  for (long long i : v)
  {
    o << i;

    if (idx < v.size() - 1)
    {
      o << " ";
    }

    idx += 1;
  }
  o << "}";
  return o;
}

long long inverseRange(const std::vector<Range>& ranges, const long long output)
{
  for (const Range& range : ranges)
  {
    if (range.destStart <= output and output < range.destStart + range.length)
    {
      return range.sourceStart + (output - range.destStart);
    }
  }
  return output;
}

std::set<long long> reverseRange(const std::set<long long>& endpoints,
                                 const std::vector<Range>& ranges)
{
  std::set<long long> candidates;
  for (const Range& range : ranges)
  {
    for (const long long endpoint : endpoints)
    {
      if (range.destStart <= endpoint and
          endpoint <= range.destStart + range.length)
      {
        // Undo mapping
        candidates.insert(endpoint - (range.destStart - range.sourceStart));
      }
    }

    candidates.insert(range.sourceStart);
    candidates.insert(range.sourceStart - 1);
    candidates.insert(range.sourceStart + range.length);
    candidates.insert(range.sourceStart + range.length - 1);
  }

  return candidates;
}

std::set<long long> invertMapping(const std::vector<Range>& ranges,
                                  const std::set<long long> outputs)
{
  std::set<long long> mapEndpoints;
  for (const Range& range : ranges)
  {
    mapEndpoints.insert(range.sourceStart);
    mapEndpoints.insert(range.sourceStart + range.length - 1);
  }

  // Reverse map outputs
  std::set<long long> inverseOutputs;
  for (long long output : outputs)
  {
    inverseOutputs.insert(inverseRange(ranges, output));
  }

  // Reverse map mapping endpoints
  std::set<long long> inverseEndpoints;
  for (long long endpoint : mapEndpoints)
  {
    inverseEndpoints.insert(endpoint);
  }
  if (*inverseEndpoints.begin() > 0)
  {
    inverseEndpoints.insert(*inverseEndpoints.begin() - 1);
    inverseEndpoints.insert(0);
  }
  if (*inverseEndpoints.rbegin() < UINT_MAX)
  {
    inverseEndpoints.insert(*inverseEndpoints.rbegin() + 1);
    inverseEndpoints.insert(UINT_MAX);
  }

  // Set intersection
  // Return
  std::set<long long> inputEndpoints;
  for (long long i : inverseEndpoints)
  {
    inputEndpoints.insert(i);
  }
  for (long long i : inverseOutputs)
  {
    inputEndpoints.insert(i);
  }

  return inputEndpoints;
}

long long doP2(const Data& data)
{
  // Compute all map inverses
  std::set<long long> humidityEndpoints =
      invertMapping(data.humidityToLocation, std::set<long long>{0, UINT_MAX});
  std::set<long long> temperatureEndpoints =
      invertMapping(data.tempToHumidity, humidityEndpoints);
  std::set<long long> lightEndpoints =
      invertMapping(data.lightToTemp, temperatureEndpoints);
  std::set<long long> waterEndpoints =
      invertMapping(data.waterToLight, lightEndpoints);
  std::set<long long> fertilizerEndpoints =
      invertMapping(data.fertilizerToWater, waterEndpoints);
  std::set<long long> soilEndpoints =
      invertMapping(data.soilToFertilizer, fertilizerEndpoints);
  std::set<long long> seeds = invertMapping(data.seedToSoil, soilEndpoints);

  long long result = UINT_MAX;

  for (unsigned int i = 0; i < data.seeds.size(); i += 2)
  {
    long long start = data.seeds[i];
    long long range = data.seeds[i + 1];

    result = std::min(result, translate(data, start));
    result = std::min(result, translate(data, start + range - 1));

    for (long long seed : seeds)
    {
      if (start <= seed and seed < start + range)
      {
        result = std::min(result, translate(data, seed));
      }
    }
  }

  return result;
}
