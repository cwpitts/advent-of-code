#ifndef AOC_2023_5_UTIL_HPP
#define AOC_2023_5_UTIL_HPP

#include <filesystem>
#include <map>
#include <set>
#include <vector>

namespace fs = std::filesystem;

struct Range
{
  long long destStart;
  long long sourceStart;
  long long length;
};

struct Data
{
  std::vector<long long> seeds;
  std::vector<Range> seedToSoil;
  std::vector<Range> soilToFertilizer;
  std::vector<Range> fertilizerToWater;
  std::vector<Range> waterToLight;
  std::vector<Range> lightToTemp;
  std::vector<Range> tempToHumidity;
  std::vector<Range> humidityToLocation;
};

Data readInput(fs::path inputPath);

void printDataSection(const Data& data, const std::string section);

long long rangeFunction(const std::vector<Range>& ranges,
                        const long long value);

long long translate(const Data& data, const long long value);

#endif  // AOC_2023_5_UTIL_HPP
