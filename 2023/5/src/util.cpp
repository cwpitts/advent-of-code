#include "util.hpp"

#include <fstream>
#include <iostream>
#include <ostream>
#include <sstream>

// clang-format off
#include "spdlog/spdlog.h"
// clang-format on
#include "fmt/format.h"

void readRanges(std::vector<Range>& section, std::ifstream& inFile)
{
  std::string line;

  while (std::getline(inFile, line))
  {
    if (line == "")
    {
      return;
    }

    std::istringstream iss(line);

    long long destStart;
    long long sourceStart;
    long long rangeLength;

    iss >> destStart >> sourceStart >> rangeLength;

    section.push_back(Range(destStart, sourceStart, rangeLength));
  }
}

/**
 * seeds: 79 14 55 13
 *
 * seed-to-soil map:
 * 50 98 2
 * 52 50 48
 *
 * soil-to-fertilizer map:
 * 0 15 37
 * 37 52 2
 * 39 0 15
 *
 * fertilizer-to-water map:
 * 49 53 8
 * 0 11 42
 * 42 0 7
 * 57 7 4
 *
 * water-to-light map:
 * 88 18 7
 * 18 25 70
 *
 * light-to-temperature map:
 * 45 77 23
 * 81 45 19
 * 68 64 13
 *
 * temperature-to-humidity map:
 * 0 69 1
 * 1 0 69
 *
 * humidity-to-location map:
 * 60 56 37
 * 56 93 4
 */
Data readInput(fs::path inputPath)
{
  Data data;

  std::ifstream inFile(inputPath);

  std::string line;

  // First is always seeds
  std::getline(inFile, line);

  line = line.substr(line.find(": ") + 2);
  std::istringstream iss(line);
  long long seed;
  while (iss >> seed)
  {
    data.seeds.push_back(seed);
  }

  while (std::getline(inFile, line))
  {
    if (line == "")
    {
      continue;
    }

    if (line.find("seed-to-soil") != std::string::npos)
    {
      readRanges(data.seedToSoil, inFile);
    }
    else if (line.find("soil-to-fertilizer") != std::string::npos)
    {
      readRanges(data.soilToFertilizer, inFile);
    }
    else if (line.find("fertilizer-to-water") != std::string::npos)
    {
      readRanges(data.fertilizerToWater, inFile);
    }
    else if (line.find("water-to-light") != std::string::npos)
    {
      readRanges(data.waterToLight, inFile);
    }
    else if (line.find("light-to-temperature") != std::string::npos)
    {
      readRanges(data.lightToTemp, inFile);
    }
    else if (line.find("temperature-to-humidity") != std::string::npos)
    {
      readRanges(data.tempToHumidity, inFile);
    }
    else if (line.find("humidity-to-location") != std::string::npos)
    {
      readRanges(data.humidityToLocation, inFile);
    }
  }

  inFile.close();

  return data;
}

void printDataSection(const Data& data, const std::string section)
{
  if (section == "seed-to-soil")
  {
    fmt::print("seed\tsoil\n");

    for (const Range& range : data.seedToSoil)
    {
      for (long long i = 0; i < range.length; i += 1)
      {
        fmt::print("{}\t{}\n", range.sourceStart + i, range.destStart + i);
      }
    }
  }
}

long long rangeFunction(const std::vector<Range>& ranges, const long long value)
{
  for (const Range& range : ranges)
  {
    if (range.sourceStart <= value and value < range.sourceStart + range.length)
    {
      return range.destStart + (value - range.sourceStart);
    }
  }

  return value;
}

long long translate(const Data& data, const long long value)
{
  long long soil = rangeFunction(data.seedToSoil, value);
  long long fertilizer = rangeFunction(data.soilToFertilizer, soil);
  long long water = rangeFunction(data.fertilizerToWater, fertilizer);
  long long light = rangeFunction(data.waterToLight, water);
  long long temperature = rangeFunction(data.lightToTemp, light);
  long long humidity = rangeFunction(data.tempToHumidity, temperature);
  long long location = rangeFunction(data.humidityToLocation, humidity);

  return location;
}
