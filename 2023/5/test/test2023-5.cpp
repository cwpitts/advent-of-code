#include <gtest/gtest.h>

#include <cmath>
#include <cstdint>
#include <cstdio>
#include <filesystem>
#include <iterator>
#include <limits>
#include <memory>
#include <optional>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <typeinfo>
#include <vector>

#include "p1.hpp"
#include "p2.hpp"
#include "util.hpp"

namespace fs = std::filesystem;

const fs::path TEST_DIR = fs::current_path().parent_path() / "test";

class AOC2023Day5 : public ::testing::TestWithParam<
                        std::tuple<std::string, unsigned int, unsigned int>>
{
 protected:
};

TEST_P(AOC2023Day5, Part1)
{
  const std::string input = std::get<0>(GetParam());
  const unsigned int expectedOutput = std::get<1>(GetParam());

  const Data data = readInput(TEST_DIR / "data" / input);

  unsigned int p1 = doP1(data);

  ASSERT_EQ(expectedOutput, p1);
}

TEST_P(AOC2023Day5, Part2)
{
  const std::string input = std::get<0>(GetParam());
  const unsigned int expectedOutput = std::get<2>(GetParam());

  const Data data = readInput(TEST_DIR / "data" / input);

  unsigned int p2 = doP2(data);

  ASSERT_EQ(expectedOutput, p2);
}

INSTANTIATE_TEST_CASE_P(RunProgramTests, AOC2023Day5,
                        ::testing::Values(std::make_tuple("test1", 35, 46),
                                          std::make_tuple("input", 551761867,
                                                          57451709)));
