#include <gtest/gtest.h>

#include <cmath>
#include <cstdint>
#include <cstdio>
#include <filesystem>
#include <iterator>
#include <limits>
#include <memory>
#include <optional>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <typeinfo>
#include <vector>

#include "p1.hpp"
#include "p2.hpp"
#include "util.hpp"

namespace fs = std::filesystem;

const fs::path TEST_DIR = fs::current_path().parent_path() / "test";

class AOC2023Day6 : public ::testing::TestWithParam<
                        std::tuple<std::string, long long, long long>>
{
 protected:
};

TEST_P(AOC2023Day6, Part1)
{
  const std::string input = std::get<0>(GetParam());
  const long long expectedOutput = std::get<1>(GetParam());

  const Data data = readInput(TEST_DIR / "data" / fs::path(input));

  const long long p1 = doP1(data);

  ASSERT_EQ(expectedOutput, p1);
}

TEST_P(AOC2023Day6, Part2)
{
  const std::string input = std::get<0>(GetParam());
  const long long expectedOutput = std::get<2>(GetParam());

  const Data data = readInput(TEST_DIR / "data" / fs::path(input));

  const long long p2 = doP2(data);

  ASSERT_EQ(expectedOutput, p2);
}

INSTANTIATE_TEST_CASE_P(
    RunProgramTests, AOC2023Day6,
    ::testing::Values(std::make_tuple("input", 114400, 21039729),
                      std::make_tuple("test1", 288, 71503)));
