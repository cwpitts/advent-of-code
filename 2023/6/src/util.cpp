#include "util.hpp"

#include <cmath>
#include <fstream>
#include <ostream>
#include <sstream>
#include <string>

#include "fmt/format.h"

Data readInput(fs::path inputPath)
{
  std::ifstream inFile(inputPath);

  Data data;

  std::string line;

  std::vector<double> times;
  std::vector<double> distances;

  while (std::getline(inFile, line))
  {
    std::istringstream iss(line.substr(line.find(":") + 1));

    double v;

    while (iss >> v)
    {
      if (line.find("Time") != std::string::npos)
      {
        times.push_back(v);
      }
      else
      {
        distances.push_back(v);
      }
    }
  }

  for (size_t i = 0; i < times.size(); i += 1)
  {
    data.races.push_back(Race{times.at(i), distances.at(i)});
  }

  inFile.close();

  return data;
}

std::ostream& operator<<(std::ostream& o, const Data& data)
{
  std::string times = "Times: ";
  std::string distances = "Distances: ";

  for (size_t i = 0; i < data.races.size(); i += 1)
  {
    times += std::to_string(data.races.at(i).time);
    distances += std::to_string(data.races.at(i).distance);

    if (i < data.races.size() - 1)
    {
      times += " ";
      distances += " ";
    }
  }

  o << times << std::endl;
  o << distances;

  return o;
}

std::pair<double, double> findRoots(double a, double b, double c)
{
  double discrinimant = pow(b, 2.0) - (4 * a * c);
  double upper = (-b - sqrt(discrinimant)) / (2 * a);
  double lower = (-b + sqrt(discrinimant)) / (2 * a);

  return std::make_pair(upper, lower);
}

long long numSolutions(double duration, double minDistance)
{
  /* clang-format off
		 speed(t) = t
		 d'(t) = duration - t
     distance = speed(t) * d't(t)
              = t * (duration - t)
              = t * duration - t^2

		Distance has to be > minDistance to win, so number of satisfying
		solutions is the distance between the zeros of:

		f(t) = t * duration - t^2 - m
   */
  std::pair<double, double> bounds = findRoots(-1, duration, -minDistance);

  long long result(ceil(bounds.first) - (floor(bounds.second) + 1));
  return result;
}
