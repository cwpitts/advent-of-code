#include "p1.hpp"

#include <climits>
#include <cmath>
#include <iostream>
#include <ostream>
#include <utility>

#include "fmt/format.h"

long long doP1(const Data& data)
{
  long long result = 1;

  for (const Race& race : data.races)
  {
    result *= numSolutions(race.time, race.distance);
  }

  return result;
}
