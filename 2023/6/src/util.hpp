#ifndef AOC_2023_6_UTIL_HPP
#define AOC_2023_6_UTIL_HPP

#include <filesystem>
#include <ostream>
#include <string>
#include <utility>
#include <vector>

namespace fs = std::filesystem;

struct Race
{
  double time;
  double distance;
};

struct Data
{
  std::vector<Race> races;
};

std::ostream& operator<<(std::ostream& o, const Data& data);

Data readInput(fs::path inputPath);

std::pair<double, double> findRoots(double a, double b, double c);

long long numSolutions(double duration, double minDistance);

#endif  // AOC_2023_6_UTIL_HPP
