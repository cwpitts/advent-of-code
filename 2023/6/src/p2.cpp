#include "p2.hpp"

#include <string>

#include "fmt/core.h"
#include "util.hpp"

long long doP2(const Data& data)
{
  std::string d;
  std::string t;

  for (const Race& race : data.races)
  {
    d += std::to_string(int(race.distance));
    t += std::to_string(int(race.time));
  }

  return numSolutions(std::atof(t.c_str()), std::atof(d.c_str()));
}
