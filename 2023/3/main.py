#!/usr/bin/env python3
""" Advent of Code Day 3
"""
import math
import operator
import timeit
from argparse import ArgumentParser, Namespace
from datetime import datetime, timedelta
from functools import reduce
from pathlib import Path
from typing import List, Set, Tuple

import matplotlib.pyplot as plt
import numpy as np
from rich.progress import Progress


def parse_args(arglist: List[str] = None) -> Namespace:
    """Parse and validate program arguments.

    Args:
      arglist (List[str], optional):
        Argument list, defaults to None (read from stdin)

    Returns:
      Namespace: Parsed and validated program arguments
    """
    argp: ArgumentParser = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def p1_get_neighbors(
    row: int, col: int, shape: Tuple[int, int]
) -> Set[Tuple[int, int]]:
    max_row = shape[0]
    max_col = shape[1]

    neighbors: Set[Tuple[int, int]] = set()

    for d_row in [-1, 0, 1]:
        for d_col in [-1, 0, 1]:
            if d_row == 0 and d_col == 0:
                continue

            new_row: int = row + d_row
            new_col: int = col + d_col

            if new_row < 0 or new_col < 0:
                continue

            if new_row >= max_row or new_col >= max_col:
                continue

            neighbors.add((new_row, new_col))

    return neighbors


def do_p1(data: np.ndarray) -> int:
    part_numbers: List[int] = []

    R, C = np.where(~np.char.isnumeric(data) & (data != "."))
    for (row, col) in zip(R, C):
        neighbors: Set[Tuple[int, int]] = p1_get_neighbors(row, col, data.shape)
        processed: Set[Tuple[int, int]] = set()

        for row, col in neighbors:
            if (row, col) in processed:
                continue

            if not data[row, col].isnumeric():
                continue

            processed.add((row, col))

            digits: List[str] = [data[row, col]]

            # Grab digits from left
            l_bound: int = col - 1
            while l_bound >= 0 and data[row, l_bound].isnumeric():
                processed.add((row, l_bound))
                digits.insert(0, data[row, l_bound])
                l_bound -= 1

            # Gran digits from right
            r_bound: int = col + 1
            while r_bound < data.shape[-1] and data[row, r_bound].isnumeric():
                processed.add((row, r_bound))
                digits.append(data[row, r_bound])
                r_bound += 1

            part_numbers.append(int("".join(digits)))

    return sum(part_numbers)


def do_p2(data: np.ndarray) -> int:
    gear_ratios: List[int] = []

    R, C = np.where(~np.char.isnumeric(data) & (data == "*"))
    for (row, col) in zip(R, C):
        neighbors: Set[Tuple[int, int]] = p1_get_neighbors(row, col, data.shape)
        processed: Set[Tuple[int, int]] = set()

        adjacent_numbers: List[int] = []

        for n_row, n_col in neighbors:
            if (n_row, n_col) in processed:
                continue

            if not data[n_row, n_col].isnumeric():
                continue

            processed.add((n_row, n_col))

            digits: List[str] = [data[n_row, n_col]]

            # Grab digits from left
            l_bound: int = n_col - 1
            while l_bound >= 0 and data[n_row, l_bound].isnumeric():
                processed.add((n_row, l_bound))
                digits.insert(0, data[n_row, l_bound])
                l_bound -= 1

            # Gran digits from right
            r_bound: int = n_col + 1
            while r_bound < data.shape[-1] and data[n_row, r_bound].isnumeric():
                processed.add((n_row, r_bound))
                digits.append(data[n_row, r_bound])
                r_bound += 1

            adjacent_numbers.append(int("".join(digits)))

        if len(adjacent_numbers) == 2:
            gear_ratios.append(reduce(operator.mul, adjacent_numbers))

    return sum(gear_ratios)


def parse_input(input_path: Path) -> np.ndarray:
    grid: List[List[str]] = []

    with open(input_path, "r") as in_file:
        # Read input
        for line in in_file:
            grid.append([c for c in line.strip()])

    return np.array(grid)


def main(args: Namespace):
    data: np.ndarray = parse_input(args.input)

    # Part 1
    p1_start: datetime = datetime.now()
    p1: int = do_p1(data)
    p1_end: datetime = datetime.now()
    p1_duration: timedelta = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start: datetime = datetime.now()
    p2: int = do_p2(data)
    p2_end: datetime = datetime.now()
    p2_duration: timedelta = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args: Namespace = parse_args()
    main(args)
