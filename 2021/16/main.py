#!/usr/bin/env python3
""" Advent of Code Day 16
"""
from argparse import ArgumentParser
from datetime import datetime
from pathlib import Path
from uuid import uuid4

import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
from tqdm import tqdm


class Packet:
    def __init__(self, version, type_id, length_type_id, value, sub_packets):
        self.version = version
        self.type_id = type_id
        self.length_type_id = length_type_id
        self.value = value
        self.sub_packets = sub_packets if sub_packets else []
        self.id = uuid4().hex

    def sum_version(self):
        ret = self.version

        if not self.sub_packets:
            return ret

        return ret + sum([sp.sum_version() for sp in self.sub_packets])

    def execute(self):
        if self.type_id == 0:
            return sum([sp.execute() for sp in self.sub_packets])
        if self.type_id == 1:
            return np.prod([sp.execute() for sp in self.sub_packets])
        if self.type_id == 2:
            return min([sp.execute() for sp in self.sub_packets])
        if self.type_id == 3:
            return max([sp.execute() for sp in self.sub_packets])
        if self.type_id == 4:
            return self.value
        if self.type_id == 5:
            left, right = self.sub_packets
            return int(left.execute() > right.execute())
        if self.type_id == 6:
            left, right = self.sub_packets
            return int(left.execute() < right.execute())
        if self.type_id == 7:
            left, right = self.sub_packets
            return int(left.execute() == right.execute())


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def packet_to_tree(packet, G=None):
    if not G:
        G = nx.DiGraph()

    if packet.type_id == 0:
        label = "sum"
    elif packet.type_id == 1:
        label = "prod"
    elif packet.type_id == 2:
        label = "min"
    elif packet.type_id == 3:
        label = "max"
    elif packet.type_id == 4:
        label = f"{packet.value}"
    elif packet.type_id == 5:
        label = ">"
    elif packet.type_id == 6:
        label = "<"
    elif packet.type_id == 7:
        label = "=="

    G.add_node(packet.id, label=label)

    for sub_packet in packet.sub_packets:
        G.add_edge(packet.id, sub_packet.id)
        G = packet_to_tree(sub_packet, G)

    return G


def hex_to_bin(h):
    return "".join([f"{int(c, base=16):04b}" for c in h])


def print_stream(data, ptr):
    print("-" * len(data))
    print(data)
    print("".join(["^" if i == ptr else " " for i in range(len(data))]))
    print("-" * len(data))


def read_packet(data, ptr):
    version = int(data[ptr : ptr + 3], base=2)
    ptr += 3

    type_id = int(data[ptr : ptr + 3], base=2)
    ptr += 3

    if type_id == 4:
        # Parse literal value packet
        v_str = ""
        while data[ptr] != "0":
            v_str += data[ptr + 1 : ptr + 5]
            ptr += 5
        v_str += data[ptr + 1 : ptr + 5]
        ptr += 5
        value = int(v_str, base=2)
        return Packet(version, type_id, None, value, []), ptr

    # Parse operator packet
    length_type_id = int(data[ptr], base=2)
    ptr += 1

    if length_type_id == 0:
        # Parse total length
        total_length = int(data[ptr : ptr + 15], base=2)
        ptr += 15

        # Read sub packets until we have read the number of bits read
        sub_packets = []
        ptr_s = ptr
        while ptr - ptr_s < total_length:
            new_packet, ptr = read_packet(data, ptr)
            sub_packets.append(new_packet)
        return Packet(version, type_id, length_type_id, None, sub_packets), ptr
    else:
        # Parse number of sub packets
        n_sub_packets = int(data[ptr : ptr + 11], base=2)
        ptr += 11

        # Read sub packets until the right number have been read
        sub_packets = []
        while len(sub_packets) < n_sub_packets:
            new_packet, ptr = read_packet(data, ptr)
            sub_packets.append(new_packet)
        return Packet(version, type_id, length_type_id, None, sub_packets), ptr


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        data = hex_to_bin(in_file.read().strip())

    # Parse packets
    # Part 1
    p1_start = datetime.now()

    p1_packets, p1_ptr = read_packet(data, 0)
    p1 = p1_packets.sum_version()

    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()
    p2 = p1_packets.execute()
    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args = parse_args()
    main(args)
