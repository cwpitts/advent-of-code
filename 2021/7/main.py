#!/usr/bin/env python3
""" Advent of Code Day 7
"""
from argparse import ArgumentParser
from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        data = np.array(list(map(int, in_file.read().split(","))))

    # Part 1
    p1_start = datetime.now()

    p1 = None
    for p in np.arange(data.max() + 1):
        cost = np.sum(np.abs(data - p))
        if p1 is None or cost < p1:
            p1 = cost

    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()

    p2 = None
    for p in np.arange(data.max() + 1):
        cost = np.sum(list(map(lambda x: np.sum(np.arange(x) + 1), np.abs((data - p)))))
        if p2 is None or cost < p2:
            p2 = cost

    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args = parse_args()
    main(args)
