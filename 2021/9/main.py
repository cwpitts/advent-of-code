#!/usr/bin/env python3
""" Advent of Code Day 9
"""
from argparse import ArgumentParser
from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def find_region(data, root):
    explored = set()
    q = []
    explored.add(root)
    q.append(root)
    while q:
        v = q.pop(0)
        y, x = v
        for dy, dx in ((0, 1), (0, -1), (1, 0), (-1, 0)):
            ny = y + dy
            nx = x + dx
            if ny < 0 or ny >= data.shape[0] or nx < 0 or nx >= data.shape[1]:
                continue
            if (
                (ny, nx) not in explored
                and data[y, x] < data[ny, nx]
                and data[ny, nx] < 9
            ):
                explored.add((ny, nx))
                q.append((ny, nx))

    return explored


def get_neighbor_indices(y, x, shape):
    DX, DY = np.meshgrid(np.arange(-1, 2), np.arange(-1, 2))
    dx = np.delete(DX.copy().flatten(), 4) + x
    dy = np.delete(DY.copy().flatten(), 4) + y

    dx_keep = np.logical_and(0 <= dx, dx < shape[1])
    dy_keep = np.logical_and(0 <= dy, dy < shape[0])
    both_keep = np.logical_and(dx_keep, dy_keep)

    dx = dx[both_keep]
    dy = dy[both_keep]

    return dy, dx


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        data = np.array([[int(c) for c in line.strip()] for line in in_file])

    # Part 1
    p1_start = datetime.now()

    low_points = []

    cell_x, cell_y = np.meshgrid(np.arange(data.shape[1]), np.arange(data.shape[0]))
    cells = list(zip(cell_y.flatten(), cell_x.flatten()))
    for y, x in cells:
        dy, dx = get_neighbor_indices(y, x, data.shape)
        if np.all(data[y, x] < data[dy, dx]):
            low_points.append((y, x))

    p1 = 0
    for y, x in low_points:
        p1 += data[y, x] + 1

    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()

    region_sizes = sorted([len(find_region(data, lp)) for lp in low_points])
    p2 = np.prod(region_sizes[-3:])

    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)

    output_dir = Path("output")
    if not output_dir.exists():
        output_dir.mkdir()

    viz = np.zeros(data.shape)
    colors = np.arange(len(low_points)) + 100
    np.random.shuffle(colors)
    for idx, region in enumerate([find_region(data, lp) for lp in low_points]):
        for cell in region:
            viz[cell] = colors[idx]

    fig, ax = plt.subplots()
    ax.imshow(viz, cmap="gray")
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)
    fig.savefig(output_dir / "p2_viz.png", bbox_inches="tight", dpi=300)

    plt.close("all")


if __name__ == "__main__":
    args = parse_args()
    main(args)
