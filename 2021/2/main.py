#!/usr/bin/env python3
""" Advent of Code Day 2
"""
from argparse import ArgumentParser
from datetime import datetime
from pathlib import Path
from typing import NamedTuple

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm


class Direction(NamedTuple):
    dx: int
    dy: int

    @staticmethod
    def from_line(line):
        direction, magnitude = line.split(" ")

        dx = int(direction in ("forward", "backward")) * int(magnitude)
        dy = int(direction in ("up", "down")) * int(magnitude)

        if direction == "backward":
            dx *= -1
        elif direction == "up":
            dy *= -1

        return Direction(dx, dy)


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        data = [x.strip() for x in in_file]

    # Part 1
    p1_start = datetime.now()
    steps1 = [Direction.from_line(x) for x in data]
    p1 = sum([d.dx for d in steps1]) * sum([d.dy for d in steps1])
    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()
    x = 0
    y = 0
    aim = 0
    for d in data:
        direction, magnitude = d.split(" ")
        magnitude = int(magnitude)

        if direction == "up":
            aim -= magnitude
        elif direction == "down":
            aim += magnitude
        elif direction == "forward":
            x += magnitude
            y += aim * magnitude
    p2 = x * y
    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args = parse_args()
    main(args)
