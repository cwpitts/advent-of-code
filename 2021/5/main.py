#!/usr/bin/env python3
""" Advent of Code Day 5
"""
from argparse import ArgumentParser
from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def get_points(p1, p2):
    x1, y1 = p1
    x2, y2 = p2

    if x2 < x1:
        x1, x2 = x2, x1
        y1, y2 = y2, y1

    ret = []

    dx = x2 - x1
    dy = y2 - y1

    if dx == 0:
        if y2 < y1:
            itr = np.arange(y2, y1 + 1)[::-1]
        else:
            itr = np.arange(y1, y2 + 1)
        for y in itr:
            ret.append((x1, y))
    else:
        for x in np.arange(x1, x2 + 1):
            y = y1 + dy * (x - x1) / dx
            ret.append((int(x), int(y)))

    return ret


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        data = []
        for line in in_file:
            start, end = line.strip().split(" -> ")
            start = np.array(tuple(map(int, start.split(","))), dtype=np.int64)
            end = np.array(tuple(map(int, end.split(","))), dtype=np.int64)
            data.append((start, end))

    # Part 1
    p1_start = datetime.now()

    p1_grid_size = np.array(data).max() + 1
    p1_grid = np.zeros((p1_grid_size, p1_grid_size), dtype=np.int64)
    for line in data:
        (x1, y1), (x2, y2) = line
        if x1 != x2 and y1 != y2:
            continue
        points = get_points((x1, y1), (x2, y2))
        for x, y in points:
            p1_grid[y, x] += 1
    p1 = np.sum(p1_grid > 1)

    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()

    p2_grid_size = np.array(data).max() + 1
    p2_grid = np.zeros((p2_grid_size, p2_grid_size), dtype=np.int64)
    for line in data:
        (x1, y1), (x2, y2) = line
        points = get_points((x1, y1), (x2, y2))
        for x, y in points:
            p2_grid[y, x] += 1
    p2 = np.sum(p2_grid > 1)

    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args = parse_args()
    main(args)
