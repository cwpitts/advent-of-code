#!/usr/bin/env python3
""" Advent of Code Day 21
"""
from argparse import ArgumentParser
from collections import Counter
from datetime import datetime
from itertools import product
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm

roll_counts = Counter(sum(rolls) for rolls in product([1, 2, 3], repeat=3))


class DeterministicDie:
    def __init__(self):
        self.idx = 1

    def roll(self):
        ret = self.idx
        self.idx += 1
        self.idx %= 101
        self.idx += 1 if self.idx == 0 else 0

        return ret


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def p2_game(pos_1, pos_2, score_1, score_2, cache={}):
    if (pos_1, pos_2, score_1, score_2) in cache:
        win_1, win_2 = cache[(pos_1, pos_2, score_1, score_2)]
        return win_1, win_2, cache

    if score_1 >= 21:
        return 1, 0, cache
    if score_2 >= 21:
        return 0, 1, cache

    p1_wins_from_here = 0
    p2_wins_from_here = 0

    for roll, count in roll_counts.items():
        new_pos_1 = (pos_1 + roll) % 10
        new_score_1 = score_1 + new_pos_1 + 1

        p2_wins_on_this_roll, p1_wins_on_this_roll, cache = p2_game(
            pos_2, new_pos_1, score_2, new_score_1, cache=cache
        )

        p1_wins_from_here += p1_wins_on_this_roll * count
        p2_wins_from_here += p2_wins_on_this_roll * count

        cache[(pos_1, pos_2, score_1, score_2)] = (p1_wins_from_here, p2_wins_from_here)

    return p1_wins_from_here, p2_wins_from_here, cache


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        pos_1 = int(in_file.readline().strip().split(" ")[-1]) - 1
        pos_2 = int(in_file.readline().strip().split(" ")[-1]) - 1
        positions = np.array([pos_1, pos_2], dtype=np.int64)

    # Part 1
    p1_start = datetime.now()

    d_p1 = DeterministicDie()
    scores_p1 = np.array([0, 0], dtype=np.int64)
    current_player_p1 = 0
    turns_taken_p1 = 0

    while np.all(scores_p1 < 1000):
        roll_sum = 0
        for i in np.arange(3):
            roll_sum += d_p1.roll()
        positions[current_player_p1] = (positions[current_player_p1] + roll_sum) % 10
        scores_p1[current_player_p1] += positions[current_player_p1] + 1
        current_player_p1 = (current_player_p1 + 1) % 2
        turns_taken_p1 += 1

    p1 = turns_taken_p1 * 3 * scores_p1[current_player_p1]

    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()

    p1_wins, p2_wins, cache = p2_game(pos_1, pos_2, 0, 0)

    p2 = max([p1_wins, p2_wins])

    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args = parse_args()
    main(args)
