#!/usr/bin/env python3
""" Advent of Code Day 15
"""
from argparse import ArgumentParser
from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
from tqdm import tqdm


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def get_neighbors(y, x, my, mx):
    ret = []

    for dy, dx in ((-1, 0), (1, 0), (0, 1), (0, -1)):
        ny = y + dy
        nx = x + dx

        if 0 <= ny < my and 0 <= nx < mx:
            ret.append((ny, nx))

    return ret


def make_graph(data):
    G = nx.DiGraph()

    my, mx = data.shape
    for y in np.arange(data.shape[0]):
        for x in np.arange(data.shape[1]):
            neighbors = get_neighbors(y, x, my, mx)
            for n_y, n_x in neighbors:
                G.add_edge((y, x), (n_y, n_x), weight=data[n_y, n_x])

    return G


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        data = np.array([list(map(int, line.strip())) for line in in_file])

    # Part 1
    p1_start = datetime.now()

    G1 = make_graph(data)
    p1 = nx.algorithms.dijkstra_path_length(
        G1, (0, 0), (data.shape[1] - 1, data.shape[0] - 1)
    )

    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()

    p2_data = data.copy()
    tile = p2_data.copy()
    for _ in range(4):
        tile += 1
        tile[tile == 10] = 1
        p2_data = np.concatenate([p2_data, tile], axis=1)

    tile = p2_data.copy()
    for _ in range(4):
        tile += 1
        tile[tile == 10] = 1
        p2_data = np.concatenate([p2_data, tile], axis=0)

    G2 = make_graph(p2_data)
    p2 = nx.algorithms.dijkstra_path_length(
        G2, (0, 0), (p2_data.shape[1] - 1, p2_data.shape[0] - 1)
    )

    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args = parse_args()
    main(args)
