#!/usr/bin/env python3
""" Advent of Code Day 17
"""
import re
from argparse import ArgumentParser
from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def triangle(n):
    return int((n * (n + 1)) / 2)


def hits_target(idx, idy, target_bounds):
    target_min_x, target_max_x, target_min_y, target_max_y = target_bounds

    x = 0
    y = 0
    dx = idx
    dy = idy
    while x < target_max_x and y > target_min_y:
        if target_min_x <= x <= target_max_x and target_min_y <= y <= target_max_y:
            return True
        x += dx
        y += dy
        dx += 1 if dx < 0 else -1 if dx > 0 else 0
        dy -= 1

    return target_min_x <= x <= target_max_x and target_min_y <= y <= target_max_y


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        data = re.findall(
            "x=([0-9]+)..([0-9]+), y=(-{0,1}[0-9]+)..(-{0,1}[0-9]+)",
            in_file.read().strip(),
        )[0]
        target_min_x, target_max_x, target_min_y, target_max_y = map(int, data)

    # Part 1
    p1_start = datetime.now()
    p1 = triangle(target_min_y)
    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()

    # Bound the search space
    min_x_vel = 0
    for n in np.arange(target_min_x):
        if triangle(n) >= target_min_x:
            min_x_vel = n
            break

    max_y_vel = 0
    for n in np.arange(np.abs(target_min_y)):
        if triangle(n) >= triangle(target_min_y):
            max_y_vel = n
            break

    valid = []
    for idx in np.arange(min_x_vel, target_max_x + 1):
        for idy in np.arange(target_min_y, max_y_vel + 1, 1):
            if hits_target(
                idx, idy, (target_min_x, target_max_x, target_min_y, target_max_y)
            ):
                valid.append((idx, idy))

    p2 = len(valid)

    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args = parse_args()
    main(args)
