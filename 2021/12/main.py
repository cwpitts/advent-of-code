#!/usr/bin/env python3
""" Advent of Code Day 12
"""
from argparse import ArgumentParser
from copy import deepcopy
from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
from tqdm import tqdm


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def find_paths(graph, src, dest, path=None, visit_counts=None, have_extra_visit=False):
    if path is None:
        path = []

    if visit_counts is None:
        visit_counts = {node: 0 for node in graph}

    path.append(src)

    visit_counts[src] += 1

    # Base case 1 is that we've found the destination
    if src == dest:
        return [path]

    ret = []

    for node in graph[src]:
        # Base case 3 is that we're heading back to the start
        if node == "start":
            continue

        # Base case 2 is that we've already been to this node
        already_visited = node in path
        if node.islower() and already_visited:
            # If we've already visited the node, do we have an extra visit?
            if not have_extra_visit:
                continue

            # If we have an extra visit, have we already used it?
            node_visits = [
                v
                for k, v in visit_counts.items()
                if k.islower() and k not in ("start", "end")
            ]
            if max(node_visits) == 2:
                continue

        # Find path from dest
        r = find_paths(
            graph,
            node,
            dest,
            path=deepcopy(path),
            visit_counts=deepcopy(visit_counts),
            have_extra_visit=have_extra_visit,
        )
        if r is not None:
            for p in r:
                ret.append(p)

    return ret if ret else None


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        data = [line.strip().split("-") for line in in_file]

    # Part 1
    p1_start = datetime.now()

    graph = {}
    for src, dest in data:
        if src not in graph:
            graph[src] = set()
        if dest not in graph:
            graph[dest] = set()
        graph[src].add(dest)
        graph[dest].add(src)

    p1_paths = find_paths(graph, "start", "end")

    p1 = len(p1_paths)

    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()

    p2_paths = find_paths(graph, "start", "end", have_extra_visit=True)

    p2 = len(p2_paths)

    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args = parse_args()
    main(args)
