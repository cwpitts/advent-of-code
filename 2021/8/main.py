#!/usr/bin/env python3
""" Advent of Code Day 8
"""
from argparse import ArgumentParser
from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from ordered_set import OrderedSet
from tqdm import tqdm

segment_count_to_candidates = {
    2: (1,),
    3: (7,),
    4: (4,),
    5: (2, 3, 5),
    6: (0, 6, 9),
    7: (8,),
}

digit_segment_counts = {0: 6, 1: 2, 2: 5, 3: 5, 4: 4, 5: 5, 6: 6, 7: 3, 8: 7, 9: 6}


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        data = [
            [[frozenset(x) for x in p.strip().split(" ")] for p in line.split("|")]
            for line in in_file
        ]

    # Part 1
    p1_start = datetime.now()

    digit_counts = {k: 0 for k in range(10)}

    for inputs, outputs in data:
        for output in outputs:
            l = len(output)
            if len(segment_count_to_candidates[l]) == 1:
                digit = next(
                    x for x in digit_segment_counts if digit_segment_counts[x] == l
                )
                digit_counts[digit] += 1

    p1 = sum(list(digit_counts.values()))

    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()

    p2 = 0

    for inputs, outputs in data:
        # Determine mappings using the parts of the different numbers that can't overlap
        pattern_map = {k: [] for k in range(10)}
        for inp in inputs:
            for o in segment_count_to_candidates[len(inp)]:
                pattern_map[o].append(frozenset(inp))

        # 1 can't overlap with 2, 5, 6
        for i in (2, 5, 6):
            pattern_map[i] = [
                p for p in pattern_map[i] if not pattern_map[1][0].issubset(p)
            ]

        # 3 and 4 should have an overlap of 3
        pattern_map[3] = [p for p in pattern_map[3] if len(p & pattern_map[4][0]) == 3]

        # 2 and 4 should have an overlap of 2
        pattern_map[2] = [p for p in pattern_map[2] if len(p & pattern_map[4][0]) == 2]

        # 5 and 4 should have an overlap of 3
        pattern_map[5] = [p for p in pattern_map[5] if len(p & pattern_map[4][0]) == 3]

        # 4 should be a subset of 9, and the overlap should be 4
        pattern_map[9] = [p for p in pattern_map[9] if pattern_map[4][0].issubset(p)]
        pattern_map[9] = [p for p in pattern_map[9] if len(p & pattern_map[4][0]) == 4]

        # 1 should be a subset of 3, and the overlap should be 2
        pattern_map[3] = [p for p in pattern_map[3] if pattern_map[1][0].issubset(p)]
        pattern_map[3] = [p for p in pattern_map[3] if len(p & pattern_map[1][0]) == 2]

        # 0 and 4 should have an overlap of 3
        pattern_map[0] = [p for p in pattern_map[0] if len(p & pattern_map[4][0]) == 3]

        # 1 should be a subset of 0, should have an overlap of 2
        pattern_map[0] = [p for p in pattern_map[0] if pattern_map[1][0].issubset(p)]
        pattern_map[0] = [p for p in pattern_map[0] if len(p & pattern_map[1][0]) == 2]

        inputs_to_outputs = {v[0]: k for k, v in pattern_map.items()}

        num = int("".join([str(inputs_to_outputs[o]) for o in outputs]))

        p2 += num

    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args = parse_args()
    main(args)
