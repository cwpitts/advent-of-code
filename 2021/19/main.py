#!/usr/bin/env python3
""" Advent of Code Day 19
"""
import re
from argparse import ArgumentParser
from collections import Counter
from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from scipy.spatial.distance import cdist
from tqdm import tqdm

label_re = re.compile(r"--- scanner ([0-9]+) ---")

# https://www.euclideanspace.com/maths/algebra/matrix/transforms/examples/index.htm
rotation_matrices = [
    np.array([[1, 0, 0],
              [0, 1, 0],
              [0, 0, 1]]),
    np.array([[1, 0, 0],
              [0, 0, -1],
              [0, 1, 0]]),
    np.array([[1, 0, 0],
              [0, -1, 0],
              [0, 0, -1]]),
    np.array([[1, 0, 0],
              [0, 0, 1],
              [0, -1, 0]]),
    np.array([[0, -1, 0],
              [1, 0, 0],
              [0, 0, 1]]),
    np.array([[0, 0, 1],
              [1, 0, 0],
              [0, 1, 0]]),
    np.array([[0, 1, 0],
              [1, 0, 0],
              [0, 0, -1]]),
    np.array([[0, 0, -1],
              [1, 0, 0],
              [0, -1, 0]]),
    np.array([[-1, 0, 0],
              [0, -1, 0],
              [0, 0, 1]]),
    np.array([[-1, 0, 0],
              [0, 0, -1],
              [0, -1, 0]]),
    np.array([[-1, 0, 0],
              [0, 1, 0],
              [0, 0, -1]]),
    np.array([[-1, 0, 0],
              [0, 0, 1],
              [0, 1, 0]]),
    np.array([[0, 1, 0],
              [-1, 0, 0],
              [0, 0, 1]]),
    np.array([[0, 0, 1],
              [-1, 0, 0],
              [0, -1, 0]]),
    np.array([[0, -1, 0],
              [-1, 0, 0],
              [0, 0, -1]]),
    np.array([[0, 0, -1],
              [-1, 0, 0],
              [0, 1, 0]]),
    np.array([[0, 0, -1],
              [0, 1, 0],
              [1, 0, 0]]),
    np.array([[0, 1, 0],
              [0, 0, 1],
              [1, 0, 0]]),
    np.array([[0, 0, 1],
              [0, -1, 0],
              [1, 0, 0]]),
    np.array([[0, -1, 0],
              [0, 0, -1],
              [1, 0, 0]]),
    np.array([[0, 0, -1],
              [0, -1, 0],
              [-1, 0, 0]]),
    np.array([[0, -1, 0],
              [0, 0, 1],
              [-1, 0, 0]]),
    np.array([[0, 0, 1],
              [0, 1, 0],
              [-1, 0, 0]]),
    np.array([[0, 1, 0],
              [0, 0, -1],
              [-1, 0, 0]]),
]


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def compute_distances(points):
    ret = set()

    for p0 in points:
        for p1 in points:
            if p0 is p1:
                continue

            ret.add(cdist(p0.reshape((1, 3)), p1.reshape((1, 3)), metric="cityblock")[0][0])

    return ret


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        data = []
        for scanner in in_file.read().strip().split("\n\n"):
            parts = scanner.split("\n")
            int(re.search(label_re, parts[0]).groups()[0])
            beacons = np.array([np.array(list(map(int, p.split(",")))) for p in parts[1:]])
            data.append(beacons)

    # Part 1
    p1_start = datetime.now()

    all_beacons = data[0]
    unaligned_scanners = data[1:]
    aligned_scanners = {tuple([0, 0, 0])}

    while unaligned_scanners:
        for idx, scanner in enumerate(unaligned_scanners):
            for rm_idx, rm in enumerate(rotation_matrices):
                rot_scanner = scanner @ rm
                cnt = Counter()
                for point2 in rot_scanner:
                    for point1 in all_beacons:
                        cnt[tuple(point1 - point2)] += 1

                v, count = cnt.most_common()[0]
                v = np.array(v)
                if count >= 12:
                    shifted_beacons = rot_scanner + v
                    aligned_scanners.add(tuple(v))
                    all_beacons = np.concatenate([all_beacons, shifted_beacons])
                    unaligned_scanners.pop(idx)
                    break

    p1 = len(set([tuple(v) for v in all_beacons]))

    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()

    p2 = 0
    for s1 in aligned_scanners:
        for s2 in aligned_scanners:
            if s1 is s2:
                continue
            d = cdist(np.array(s1).reshape(1, 3), np.array(s2).reshape(1, 3), metric="cityblock")[0][0]
            if d > p2:
                p2 = int(d)
    
    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args = parse_args()
    main(args)
