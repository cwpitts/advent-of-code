#!/usr/bin/env python3
""" Advent of Code Day 6
"""
from argparse import ArgumentParser
from collections import Counter
from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        data = Counter(map(int, in_file.read().strip().split(",")))

    # Part 1
    p1_start = datetime.now()

    for _ in range(80):
        new_data = {k: 0 for k in range(9)}
        for k, v in data.items():
            if k == 0:
                new_data[8] += v
                new_data[6] += v
            else:
                new_data[k - 1] += v
                data = new_data
    p1 = sum([v for v in data.values()])

    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()

    for _ in range(256 - 80):
        new_data = {k: 0 for k in range(9)}
        for k, v in data.items():
            if k == 0:
                new_data[8] += v
                new_data[6] += v
            else:
                new_data[k - 1] += v
                data = new_data
    p2 = sum([v for v in data.values()])

    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args = parse_args()
    main(args)
