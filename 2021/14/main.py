#!/usr/bin/env python3
""" Advent of Code Day 14
"""
from argparse import ArgumentParser
from collections import Counter, defaultdict
from copy import deepcopy
from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def run_steps(start_state, reactions, steps):
    state = deepcopy(start_state)
    for _ in range(steps):
        new_state = deepcopy(state)
        for pair, count in state.items():
            if count == 0:
                continue
            for s, ds in reactions[pair]:
                new_state[s] += ds * count
        state = deepcopy(new_state)

    return state


def score_state(state):
    c = Counter()
    for k, v in state.items():
        for char in k:
            c[char] += v

    ranking = c.most_common()
    return (np.ceil(ranking[0][1] / 2) - np.ceil(ranking[-1][1] / 2)).astype(np.int64)


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        initial_state, reactions = in_file.read().strip().split("\n\n")

        state = defaultdict(lambda: 0)
        for i in range(len(initial_state) - 1):
            state[initial_state[i : i + 2]] += 1
        reactions = [tuple(r.strip().split(" -> ")) for r in reactions.split("\n")]
        reactions = {
            r[0]: [(r[0], -1), (f"{r[0][0]}{r[1]}", 1), (f"{r[1]}{r[0][1]}", 1)]
            for r in reactions
        }

    # Part 1
    p1_start = datetime.now()

    p1_state = run_steps(deepcopy(state), reactions, 10)
    p1 = score_state(p1_state)

    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()

    p2_state = run_steps(p1_state, reactions, 30)
    p2 = score_state(p2_state)

    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args = parse_args()
    main(args)
