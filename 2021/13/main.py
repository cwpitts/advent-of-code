#!/usr/bin/env python3
""" Advent of Code Day 13
"""
import re
from argparse import ArgumentParser
from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        dot_data, fold_data = in_file.read().strip().split("\n\n")

        dot_data = [
            tuple(map(int, dot.strip().split(","))) for dot in dot_data.split("\n")
        ]
        paper = np.zeros(
            (max([d[1] for d in dot_data]) + 1, max([d[0] for d in dot_data]) + 1),
            dtype=np.int64,
        )
        for x, y in dot_data:
            paper[y, x] += 1

        fold_data = fold_data.split("\n")

        rgx = re.compile("[xy]=[0-9]+")
        fold_data = [re.findall(rgx, fd)[0].split("=") for fd in fold_data]
        fold_data = [(direction, int(magnitude)) for direction, magnitude in fold_data]

    # Part 1
    p1_start = datetime.now()

    p1_paper = paper.copy()

    for direction, magnitude in fold_data[:1]:
        if direction == "y":
            f1 = p1_paper[:magnitude, :]
            f2 = p1_paper[magnitude + 1 :, :][::-1, :]

            p1_paper = f1 + f2
        else:
            f1 = p1_paper[:, :magnitude]
            f2 = p1_paper[:, magnitude + 1 :][:, ::-1]

            p1_paper = f1 + f2

    p1 = np.sum(p1_paper > 0)

    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()

    p2_paper = paper.copy()

    for direction, magnitude in fold_data:
        if direction == "y":
            f1 = p2_paper[:magnitude, :]
            f2 = p2_paper[magnitude + 1 :, :][::-1, :]

            p2_paper = f1 + f2
        else:
            f1 = p2_paper[:, :magnitude]
            f2 = p2_paper[:, magnitude + 1 :][:, ::-1]

            p2_paper = f1 + f2

    output_dir = Path("output")
    if not output_dir.exists():
        output_dir.mkdir()

    fig, ax = plt.subplots()
    ax.imshow(p2_paper > 0, cmap="gray")
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)
    fig.savefig(output_dir / "p2_viz.png", bbox_inches="tight", dpi=300)

    p2 = "N/A"

    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args = parse_args()
    main(args)
