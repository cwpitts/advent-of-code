#!/usr/bin/env python3
""" Advent of Code Day 25
"""
from argparse import ArgumentParser
from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from aoc.life.life import Life
from tqdm import tqdm


class Cucumbers(Life):
    """Cucumber sim class.

    """

    def __init__(self, initial_state: np.ndarray):
        """Cucumber sim class.

        Args:
          initial_state (np.ndarray): Initial state of cucumbers
        """
        super().__init__(initial_state)

    def step(self):
        """Iterate the game of life one time.

        """
        new_state: np.ndarray = np.full(self.state.shape, ".")

        X, Y = np.meshgrid(np.arange(new_state.shape[1]), np.arange(new_state.shape[0]))

        # Move east herd
        east_y: np.ndarray = Y[self.state == ">"]
        east_x: np.ndarray = X[self.state == ">"]

        for row, col in zip(east_y, east_x):
            next_col: int = (col + 1) % self.state.shape[1]
            if self.state[row, next_col] != ".":
                new_state[row, col] = ">"
            else:
                new_state[row, next_col] = ">"

        # Move south herd
        south_y: np.ndarray = Y[self.state == "v"]
        south_x: np.ndarray = X[self.state == "v"]

        for row, col in zip(south_y, south_x):
            next_row: int = (row + 1) % self.state.shape[0]
            # When can a south fish move?
            # If the *previous state* has a south fish, we don't move
            # If the *new state* has an east fish, we don't move
            if self.state[next_row, col] == "v" or new_state[next_row, col] == ">":
                new_state[row, col] = "v"
            else:
                new_state[next_row, col] = "v"

        self.state = new_state.copy()

    def __repr__(self) -> str:
        """Build and return representation of cucumbers

        Returns:
          str: Formatted representation of the cucumber positions
        """
        return "\n".join(["".join(row) for row in self.state])


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        initial_state: np.ndarray = np.array(
            [[c for c in row.strip()] for row in in_file]
        )

    # Part 1
    p1_start = datetime.now()

    sim: Cucumbers = Cucumbers(initial_state)
    p1 = sim.run_to_convergence()

    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args = parse_args()
    main(args)
