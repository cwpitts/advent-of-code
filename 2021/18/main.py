#!/usr/bin/env python3
""" Advent of Code Day 18
"""
import ast
import math
from argparse import ArgumentParser
from copy import deepcopy
from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm


class Node:
    def __init__(self, value=None, parent=None):
        self.left = None
        self.right = None
        self.value = value
        self.parent = parent

    def add_left(self, child):
        self.left = child
        self.left.parent = self

    def add_right(self, child):
        self.right = child
        self.right.parent = self

    def __str__(self):
        if self.value is not None:
            return f"{self.value}"

        return f"[{self.left}, {self.right}]"

    def __repr__(self):
        return self.__str__()

    def reduce(self):
        while True:
            if self.reduce_by_explode():
                continue

            if self.reduce_by_split():
                continue

            return

    def reduce_by_explode(self, depth=0):
        if self.value is not None:
            return False

        if self.left and self.right and depth == 4:
            self.explode()
            return True

        return self.left.reduce_by_explode(depth + 1) or self.right.reduce_by_explode(
            depth + 1
        )

    def explode(self):
        # To explode, get the first left regular number
        first_left = self.find_left_number()

        if first_left is not None and first_left is not self:
            first_left.value += self.left.value

        # Get the first right regular number
        first_right = self.find_right_number()

        if first_right is not None and first_right is not self:
            first_right.value += self.right.value

        self.left = None
        self.right = None
        self.value = 0

    def find_left_number(self):
        current = self
        parent = self.parent

        first_left = None
        while True:
            if parent is None:
                # Base case: hit root node
                return None

            # Check left node
            first_left = parent.left
            if first_left.value is not None:
                return first_left

            if first_left is not current:
                # We found a left node that's not us, break out of loop
                break

            # Advance up the tree
            current = parent
            parent = parent.parent

        if first_left is None:
            # Base case: this node is the left-most node
            return None

        # You can't just find the first left node, it has to be the
        # "closest" to the node you're searching for, so you have to
        # go as far *right* as you can.
        while first_left.right.value is None:
            first_left = first_left.right

        return first_left.right

    def find_right_number(self):
        current = self
        parent = self.parent

        first_right = None
        while True:
            if parent is None:
                # Base case: hit root node
                return None

            # Check right node
            first_right = parent.right
            if first_right.value is not None:
                return first_right

            if first_right is not current:
                # We found a right node that's not us, break out of loop
                break

            # Advance up the tree
            current = parent
            parent = parent.parent

        if first_right is None:
            # Base case: this node is the right-most node
            return None

        # See comment at bottom of find_left_number for why this
        # happens
        while first_right.left.value is None:
            first_right = first_right.left

        return first_right.left

    def reduce_by_split(self):
        if self.value is None:
            return self.left.reduce_by_split() or self.right.reduce_by_split()

        if self.value < 10:
            return False

        new_left_val = math.floor(self.value / 2)
        new_right_val = math.ceil(self.value / 2)

        self.value = None
        self.left = Node(value=new_left_val, parent=self)
        self.right = Node(value=new_right_val, parent=self)

        return True

    @staticmethod
    def add(first, second):
        n = Node()

        n.left = first
        n.right = second

        first.parent = n
        second.parent = n

        return n

    def magnitude(self):
        if self.value is not None:
            return self.value

        return 3 * self.left.magnitude() + 2 * self.right.magnitude()


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def tree_from_list(data, parent=None):
    if isinstance(data, int):
        return Node(value=data, parent=parent)

    root = Node(parent=parent)

    root.left = tree_from_list(data[0], parent=root)
    root.right = tree_from_list(data[1], parent=root)

    return root


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        data = [tree_from_list(ast.literal_eval(line.strip())) for line in in_file]

    # Part 1
    p1_start = datetime.now()

    first = deepcopy(data[0])

    for tree in data[1:]:
        first = Node.add(deepcopy(first), deepcopy(tree))
        first.reduce()

    p1 = first.magnitude()

    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()

    p2 = None
    for t1 in data:
        for t2 in data:
            if t1 is not t2:
                t3 = Node.add(deepcopy(t1), deepcopy(t2))
                t3.reduce()

                if p2 is None or t3.magnitude() > p2:
                    p2 = t3.magnitude()

                t4 = Node.add(deepcopy(t2), deepcopy(t1))
                t4.reduce()

                if p2 is None or t3.magnitude() > p2:
                    p2 = t4.magnitude()

    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args = parse_args()
    main(args)
