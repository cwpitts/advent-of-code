#!/usr/bin/env python3
""" Advent of Code Day 4
"""
from argparse import ArgumentParser
from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def check_for_winning_board(markers):
    for i in np.arange(markers.shape[0]):
        if np.all(markers[i, :] == 1):
            return True
        if np.all(markers[:, i] == 1):
            return True

    return False


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        data = in_file.read().split("\n\n")
        numbers = list(map(int, data[0].split(",")))
        boards = []
        for b in data[1:]:
            new_board = []
            for row in b.split("\n"):
                if not row:
                    continue
                new_board.append(
                    list(map(int, [x for x in row.strip().split(" ") if x != ""]))
                )
            boards.append(np.array(new_board, dtype=np.int64))

    # Part 1
    p1_start = datetime.now()

    board_marks = [np.zeros(x.shape, dtype=x.dtype) for x in boards]
    p1_board = None
    p1_marks = None
    p1_last_num = None

    for n in numbers:
        for board, marks in zip(boards, board_marks):
            marks[board == n] = 1
            if check_for_winning_board(marks):
                p1_board = board
                p1_marks = marks
                p1_last_num = n
                break
        if p1_last_num:
            break

    p1 = np.sum(p1_board[~(p1_marks.astype(bool))]) * p1_last_num

    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()

    board_marks = [np.zeros(x.shape, dtype=x.dtype) for x in boards]
    completed_boards = [False for _ in boards]
    last_board = None
    p2_last_num = None

    for n in numbers:
        for idx, (board, marks, has_won) in enumerate(
            zip(boards, board_marks, completed_boards)
        ):
            if has_won:
                continue

            marks[board == n] = 1
            if check_for_winning_board(marks):
                completed_boards[idx] = True

            if np.sum(completed_boards) == len(completed_boards):
                last_board = board
                last_marks = marks
                p2_last_num = n
                break

        if p2_last_num:
            break

    p2 = np.sum(last_board[~(last_marks.astype(bool))]) * p2_last_num

    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args = parse_args()
    main(args)
