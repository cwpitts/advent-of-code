#!/usr/bin/env python3
""" Advent of Code Day 10
"""
from argparse import ArgumentParser
from datetime import datetime
from enum import Enum
from pathlib import Path
from queue import LifoQueue
from typing import NamedTuple

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm


class StackState(Enum):
    BALANCED = 0
    INCOMPLETE = 1
    CORRUPTED = 2


class StackCheckResult(NamedTuple):
    state: StackState
    char: str
    stack: LifoQueue


open_brace_pairs = {
    "{": "}",
    "(": ")",
    "[": "]",
    "<": ">",
}

close_brace_pairs = {
    ">": "<",
    "}": "{",
    "]": "[",
    ")": "(",
}

invalid_symbol_scores = {
    ")": 3,
    "]": 57,
    "}": 1197,
    ">": 25137,
}

completion_symbol_scores = {
    ")": 1,
    "]": 2,
    "}": 3,
    ">": 4,
}


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def check_line(line):
    s = LifoQueue()
    for c in line:
        if c in open_brace_pairs:
            s.put(c)
        elif c in close_brace_pairs:
            if s.queue[-1] != close_brace_pairs[c]:
                return StackCheckResult(StackState.CORRUPTED, c, s)
            s.get()

    if s.empty():
        StackCheckResult(StackState.BALANCED, None, s)

    return StackCheckResult(StackState.INCOMPLETE, None, s)


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        data = [line.strip() for line in in_file]

    # Part 1
    p1_start = datetime.now()

    valid_lines = []
    incomplete_lines = []
    corrupted_lines = []

    p1 = 0

    for line in data:
        result = check_line(line)

        if result.state == StackState.BALANCED:
            valid_lines.append((line, result))
        elif result.state == StackState.INCOMPLETE:
            incomplete_lines.append((line, result))
        else:
            corrupted_lines.append((line, result))
            p1 += invalid_symbol_scores[result.char]

    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()

    p2_scores = []
    for line, result in incomplete_lines:
        score = 0
        while not result.stack.empty():
            c = result.stack.get()
            score *= 5
            score += completion_symbol_scores[open_brace_pairs[c]]
        p2_scores.append(score)

    p2 = np.median(p2_scores).astype(int)

    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args = parse_args()
    main(args)
