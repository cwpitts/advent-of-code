#!/usr/bin/env python3
""" Advent of Code Day 22
"""
from argparse import ArgumentParser
from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm


class Cube:
    def __init__(self, x_low, x_high, y_low, y_high, z_low, z_high):
        self.x_low = x_low
        self.x_high = x_high
        self.y_low = y_low
        self.y_high = y_high
        self.z_low = z_low
        self.z_high = z_high

    def encloses(self, o):
        if o.x_low < self.x_low:
            return False

        if o.x_high > self.x_high:
            return False

        if o.y_low < self.y_low:
            return False

        if o.y_high > self.y_high:
            return False

        if o.z_low < self.z_low:
            return False

        if o.z_high > self.z_high:
            return False

        return True

    def intersects(self, o):
        x_overlap = (self.x_low <= o.x_low <= self.x_high) or (
            o.x_low <= self.x_low <= o.x_high
        )
        y_overlap = (self.y_low <= o.y_low <= self.y_high) or (
            o.y_low <= self.y_low <= o.y_high
        )
        z_overlap = (self.z_low <= o.z_low <= self.z_high) or (
            o.z_low <= self.z_low <= o.z_high
        )

        return x_overlap and y_overlap and z_overlap

    def volume(self):
        return (
            (self.x_high + 1 - self.x_low)
            * (self.y_high + 1 - self.y_low)
            * (self.z_high + 1 - self.z_low)
        )

    def get_active_points(self):
        active_points = []
        for x in np.arange(self.x_low, self.x_high + 1):
            for y in np.arange(self.y_low, self.y_high + 1):
                for z in np.arange(self.z_low, self.z_high + 1):
                    active_points.append((x, y, z))

        return active_points


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def run(data):
    core = []

    for toggle, limits in data:
        (x_low, x_high), (y_low, y_high), (z_low, z_high) = limits
        nc = Cube(x_low, x_high, y_low, y_high, z_low, z_high)

        intersections = True
        while intersections:
            intersections = False
            for idx in reversed(range(len(core))):
                cc = core[idx]

                # Case 1: no intersections
                if not nc.intersects(cc):
                    continue

                # Case 2: total enclosure
                if nc.encloses(cc):
                    core.pop(idx)
                    intersections = True
                    break

                # Case 3: split
                if cc.x_low < nc.x_low <= cc.x_high:
                    core.append(
                        Cube(
                            nc.x_low,
                            cc.x_high,
                            cc.y_low,
                            cc.y_high,
                            cc.z_low,
                            cc.z_high,
                        )
                    )
                    core[idx].x_high = nc.x_low - 1
                    intersections = True
                    break
                if cc.x_low <= nc.x_high < cc.x_high:
                    core.append(
                        Cube(
                            cc.x_low,
                            nc.x_high,
                            cc.y_low,
                            cc.y_high,
                            cc.z_low,
                            cc.z_high,
                        )
                    )
                    core[idx].x_low = nc.x_high + 1
                    intersections = True
                    break

                if cc.y_low < nc.y_low <= cc.y_high:
                    core.append(
                        Cube(
                            cc.x_low,
                            cc.x_high,
                            nc.y_low,
                            cc.y_high,
                            cc.z_low,
                            cc.z_high,
                        )
                    )
                    core[idx].y_high = nc.y_low - 1
                    intersections = True
                    break
                if cc.y_low <= nc.y_high < cc.y_high:
                    core.append(
                        Cube(
                            cc.x_low,
                            cc.x_high,
                            cc.y_low,
                            nc.y_high,
                            cc.z_low,
                            cc.z_high,
                        )
                    )
                    core[idx].y_low = nc.y_high + 1
                    intersections = True
                    break

                if cc.z_low < nc.z_low <= cc.z_high:
                    core.append(
                        Cube(
                            cc.x_low,
                            cc.x_high,
                            cc.y_low,
                            cc.y_high,
                            nc.z_low,
                            cc.z_high,
                        )
                    )
                    core[idx].z_high = nc.z_low - 1
                    intersections = True
                    break
                if cc.z_low <= nc.z_high < cc.z_high:
                    core.append(
                        Cube(
                            cc.x_low,
                            cc.x_high,
                            cc.y_low,
                            cc.y_high,
                            cc.z_low,
                            nc.z_high,
                        )
                    )
                    core[idx].z_low = nc.z_high + 1
                    intersections = True
                    break

        # Now new cuboid is distinct. If it is ON, add it to the core list
        if toggle == "on":
            core.append(nc)
    return core


def parse_start_stop(c):
    x, y, z = c.split(",")

    ret = []

    for p in (x, y, z):
        start, stop = p.split("..")
        start = start.split("=")[1]

        ret.append((int(start), int(stop)))

    return ret


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        data = []
        for line in in_file:
            toggle, cubes = line.strip().split(" ")
            data.append((toggle, parse_start_stop(cubes)))

    # Part 1
    p1_start = datetime.now()

    active_p1 = set()
    for toggle, cubes in data:
        (x_start, x_stop), (y_start, y_stop), (z_start, z_stop) = cubes
        for x in np.arange(max(x_start, -50), min(x_stop, 50) + 1):
            for y in np.arange(max(y_start, -50), min(y_stop, 50) + 1):
                for z in np.arange(max(z_start, -50), min(z_stop, 50) + 1):
                    if toggle == "on":
                        active_p1.add((x, y, z))
                    else:
                        active_p1.discard((x, y, z))

    p1 = len(active_p1)

    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()

    core_p2 = run(data)
    p2 = sum(c.volume() for c in core_p2)

    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args = parse_args()
    main(args)
