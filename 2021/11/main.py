#!/usr/bin/env python3
""" Advent of Code Day 11
"""
from argparse import ArgumentParser
from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def step_sim(data):
    X, Y = np.meshgrid(np.arange(data.shape[0]), np.arange(data.shape[1]))
    flashed = set()
    data += 1

    run = True
    while run:
        run = False
        x = X[data > 9]
        y = Y[data > 9]

        for (y, x) in zip(y, x):
            if (y, x) in flashed:
                continue

            run = True
            flashed.add((y, x))

            for dy in (-1, 0, 1):
                for dx in (-1, 0, 1):
                    if dy == 0 and dx == 0:
                        continue

                    ny = y + dy
                    nx = x + dx

                    if not 0 <= ny < data.shape[0] or not 0 <= nx < data.shape[1]:
                        continue

                    data[ny, nx] += 1
    for f in flashed:
        data[f] = 0

    return data


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        data = np.array(
            [[int(x) for x in line.strip()] for line in in_file], dtype=np.int64
        )

    # Part 1
    p1_start = datetime.now()

    p1 = 0
    p1_data = data.copy()
    for _ in np.arange(100):
        p1_data = step_sim(p1_data)
        p1 += np.sum(p1_data == 0)

    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()

    p2_data = data.copy()
    p2 = 0
    while not np.all(p2_data == 0):
        p2_data = step_sim(p2_data)
        p2 += 1

    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args = parse_args()
    main(args)
