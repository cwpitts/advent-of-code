#!/usr/bin/env python3
"""Advent of Code Day 23

Mostly taken from
https://raw.githubusercontent.com/taddeus/advent-of-code/master/2021/23_amphipod.py.
Spent a few days working on getting a few solutions fast enough, then
called it and looked at other solutions. Optimized a few things to
reduce computation, added some computation of values that can be known
beforehand, etc.

"""
from argparse import ArgumentParser
from datetime import datetime
from heapq import heappop, heappush
from itertools import zip_longest
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm

COSTS = {
    "A": 1,
    "B": 10,
    "C": 100,
    "D": 1000,
}

AMPHIPODS = "ABCD"


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def parse(f):
    f.readline()
    hallway = f.readline().strip().replace("#", "")
    top = f.readline().strip().replace("#", "")
    bottom = f.readline().strip().replace("#", "")
    return "".join(map("".join, zip(top, bottom))) + hallway


def swap(s, i, j):
    if i > j:
        i, j = j, i
    return s[:i] + s[j] + s[i + 1:j] + s[i] + s[j + 1 :]


def walk(src, dst):
    step = -1 if src > dst else 1
    return range(src + step, dst + step, step)


def into_hallway(state, door, r_size, n_rooms):
    first_door = n_rooms * r_size + 2
    doors = tuple(range(first_door, first_door + 8, 2))

    for end in (n_rooms * r_size, len(state) - 1):
        for dist, i in enumerate(walk(door, end)):
            if state[i] != ".":
                break
            if i not in doors:
                yield dist + 1, i


def get_neighbors(state, r_size, n_rooms):
    for room, room_type in zip(range(0, n_rooms * r_size, r_size), AMPHIPODS):
        door = n_rooms * r_size + 2 * (room // r_size + 1)
        for depth, cell in enumerate(state[room:room + r_size]):
            if cell != ".":
                if cell != room_type or any(
                    state[room + d] != room_type for d in range(depth + 1, r_size)
                ):
                    for door_dist, hall in into_hallway(state, door, r_size, n_rooms):
                        yield door_dist + depth + 1, room + depth, hall
                break

    for hall in range(n_rooms * r_size, len(state)):
        amphipod = state[hall]
        if amphipod != ".":
            room = AMPHIPODS.index(amphipod) * r_size
            door = n_rooms * r_size + 2 * (room // r_size + 1)
            if all(state[i] == "." for i in walk(hall, door)):
                door_dist = abs(hall - door)
                for depth in range(r_size - 1, -1, -1):
                    cell = state[room + depth]
                    if cell == ".":
                        yield door_dist + depth + 1, hall, room + depth
                    elif cell != amphipod:
                        break


def shortest_path(state, r_size):
    Q = [(0, state)]
    seen = {state: 0}
    previous = {state: None}
    n_rooms = len(set(state)) - 1
    final = "".join(sorted(state[:r_size * n_rooms])) + state[r_size * n_rooms:]

    while Q:
        cost, state = heappop(Q)
        if state == final:
            return cost, path_from_previous(previous, state)

        for dist, src, dst in get_neighbors(state, r_size, n_rooms):
            moved = swap(state, src, dst)
            moved_cost = cost + dist * COSTS[state[src]]
            if seen.get(moved, 1 << 32) > moved_cost:
                heappush(Q, (moved_cost, moved))
                seen[moved] = moved_cost
                previous[moved] = state


def path_from_previous(previous, u):
    path = []
    while u is not None:
        path.append(u)
        u = previous[u]

    return path[::-1]


def main(args):
    with open(args.input, "r") as in_file:
        data = parse(in_file)

    # Part 1
    p1_start = datetime.now()

    p1, p1_path = shortest_path(data, 2)

    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()

    n_rooms = len(set(data)) - 1
    data_2 = ""
    for idx, insert in zip(range(n_rooms), ("DD", "CB", "BA", "AC")):
        room = data[idx * 2:idx * 2 + 2]
        data_2 += f"{room[0]}{insert}{room[1]}"
    data_2 += data[n_rooms * 2:]
    p2, p2_path = shortest_path(data_2, 4)

    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args = parse_args()
    main(args)
