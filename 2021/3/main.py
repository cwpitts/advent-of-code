#!/usr/bin/env python3
""" Advent of Code Day 3
"""
from argparse import ArgumentParser
from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def get_oxygen_rating(data):
    arr = data.copy()
    col = 0

    while arr.shape[0] > 1:
        count_0, count_1 = np.bincount(arr[:, col])

        if count_0 > count_1:
            arr = arr[arr[:, col] == 0, :]
        else:
            arr = arr[arr[:, col] == 1, :]

        col += 1

    return int("".join(map(str, arr[0, :])), base=2)


def get_co2_rating(data):
    arr = data.copy()
    col = 0

    while arr.shape[0] > 1:
        count_0, count_1 = np.bincount(arr[:, col])

        if count_0 <= count_1:
            arr = arr[arr[:, col] == 0, :]
        else:
            arr = arr[arr[:, col] == 1, :]

        col += 1

    return int("".join(map(str, arr[0, :])), base=2)


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        data = []
        for line in in_file:
            data.append([int(c) for c in line.strip()])

    # Part 1
    p1_start = datetime.now()
    arr = np.array(data)
    epsilon_str = ""
    gamma_str = ""
    for col in np.arange(arr.shape[1]):
        gamma_str += str(np.argmax(np.bincount(arr[:, col])))
        epsilon_str += str(np.argmin(np.bincount(arr[:, col])))
    p1 = int(gamma_str, base=2) * int(epsilon_str, base=2)
    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()
    arr2 = np.array(data)
    p2 = get_oxygen_rating(arr2) * get_co2_rating(arr2)
    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args = parse_args()
    main(args)
