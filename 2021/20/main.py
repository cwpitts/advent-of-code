#!/usr/bin/env python3
""" Advent of Code Day 20
"""
from argparse import ArgumentParser
from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def get_neighbors(y, x, l):
    neighbors = []

    for dy in np.arange(-l, l + 1):
        for dx in np.arange(-l, l + 1):
            neighbors.append((y + dy, x + dx))

    return neighbors


def state_as_array(active):
    min_y = None
    max_y = None
    min_x = None
    max_x = None

    for y, x in active:
        if min_y is None or y < min_y:
            min_y = y
        if max_y is None or y > max_y:
            max_y = y
        if min_x is None or x < min_x:
            min_x = x
        if max_x is None or x > max_x:
            max_x = x

    arr = np.zeros((max_y - min_y + 1, max_x - min_x + 1))

    for y, x in active:
        arr[y - min_y, x - min_x] = 1

    return arr


# def iterate(active, algorithm, parity):
#     new_active = set()

#     for y, x in active:
#         # Which cells need to be checked? Obviously the active cells,
#         # but any cell that is next to an active cell should also be
#         # checked, since it will rely on the active cell for an update
#         # check.
#         for cy, cx in get_neighbors(y, x, 1):
#             val = ""
#             for ny, nx in get_neighbors(cy, cx, 1):
#                 val += "1" if (ny, nx) in active else "0"
#             int_val = int(val, base=2)
#             if algorithm[int_val] == 1:
#                 new_active.add((cy, cx))

#     return new_active, (parity + 1) % 2


def iterate(state, algorithm, i):
    pad_shape = (state.shape[0] + 4, state.shape[1] + 4)
    if algorithm[0] == 1 and i % 2 == 1:
        padded_image = np.ones(pad_shape, dtype=np.int64)
    else:
        padded_image = np.zeros(pad_shape, dtype=np.int64)

    padded_image[2:-2, 2:-2] = state

    new_state = np.zeros((state.shape[0] + 2, state.shape[1] + 2), dtype=np.int64)

    for (y, x), val in np.ndenumerate(padded_image):
        if (
            y == 0
            or y == padded_image.shape[0] - 1
            or x == 0
            or x == padded_image.shape[1] - 1
        ):
            # Skip the parts that are essentially toggling
            continue
        val_str = ""
        neighbors = padded_image[y - 1 : y + 2, x - 1 : x + 2]
        for row in neighbors:
            for v in row:
                val_str += str(v)
        val = int(val_str, base=2)

        new_state[y - 1, x - 1] = algorithm[val]

    return new_state


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        algorithm, initial_state = in_file.read().strip().split("\n\n")
        algorithm = [0 if c == "." else 1 for c in algorithm]
        initial_state = np.array(
            [
                [0 if c == "." else 1 for c in line]
                for line in initial_state.split("\n")
            ],
            dtype=np.int64,
        )

    # Part 1
    p1_start = datetime.now()

    p1_state = initial_state.copy()
    for i in range(2):
        p1_state = iterate(p1_state, algorithm, i)

    p1 = p1_state.sum()

    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()

    p2_state = initial_state.copy()
    for i in range(50):
        p2_state = iterate(p2_state, algorithm, i)

    p2 = p2_state.sum()

    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args = parse_args()
    main(args)
