#!/usr/bin/env python3
""" Advent of Code Day 24
"""
from argparse import ArgumentParser
from datetime import datetime
from pathlib import Path
from typing import List, Tuple

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm


def monad(prog: List[Tuple[str]], part: int):
    values: List[Tuple[str]] = []

    for cycle in np.arange(14):
        mini_prog: List[Tuple[str]] = prog[cycle * 18:cycle * 18 + 18]
        values.append((int(mini_prog[4][2]), int(mini_prog[5][2]), int(mini_prog[15][2])))

    stack: List[int] = [0]
    digits: List[int] = [None for _ in range(14)]

    for idx, (a, b, c) in enumerate(values):
        if int(a) == 1:
            stack.append([idx, int(c)])
        else:
            p_idx, p_c = stack.pop()
            cmp: int = p_c + b
            if part == 1:
                digits[p_idx] = min(9, 9 - cmp)
            else:
                digits[p_idx] = max(1, 1 - cmp)
            digits[idx] = digits[p_idx] + cmp

    return int("".join(map(str, digits)))

def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        prog: List[Tuple[str]] = [tuple(line.strip().split(" ")) for line in in_file]

    # Part 1
    p1_start = datetime.now()

    p1 = monad(prog, 1)

    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()

    p2 = monad(prog, 2)

    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args = parse_args()
    main(args)
