from setuptools import setup

setup(
    name="aoc",
    version="0.1.0",
    description="Advent of Code solutions and utilities",
    author="Christopher Pitts",
    author_email="cwpitts@protonmail.com",
    packages=["aoc"],
    install_requires=[
        "pytest",
        "pygame",
        "rich",
        "calamari-ocr",
        "numpy",
        "matplotlib",
        "pillow",
        "scikit-image",
        "pytesseract",
        "keras_ocr",
        "raylib",
    ],
)
