#!/usr/bin/env python3

import sys
from typing import Union, Tuple

def process_list(freq: int, deltas: list, seen: set) -> Union[Tuple[int, None, None],
                                                              Tuple[None, int, set]]:
    """
    Process list, tracking already seen frequencies.
    """

    for delta in deltas:
        freq += delta

        if freq in seen:
            return freq, None, None

        seen.add(freq)

    return None, freq, seen

if __name__ == '__main__':
    (f, d, s) = (0, [int(x) for x in open(sys.argv[1])], set())

    print('Part 1:', sum(d))

    (ans_p2, f, s) = process_list(f, d, s)

    while ans_p2 is None:
        (ans_p2, f, s) = process_list(f, d, s)

    print('Part 2:', ans_p2)
