# Langauges Used
* Python
* C
* C++
* Rust

# Frameworks Used
* Qt

# Toolchains Used
* CMake

# Notes
* Day 10's solution is an interactive UI! You load the input file, and can iterate forward and backward in time to find the solutions. There are buttons, menus, etc, but the key bindings are:
  * `Left Arrow` -> Move backward in time
  * `Right Arrow` -> Move forward in time
  * `Up Arrow` -> Zoom in
  * `Down Arrow` -> Zoom out
  * `Alt-f Alt-q` -> Quit
  * `Alt-f Alt-l` -> Load file
  * `Ctrl-t` -> Set time
  * `Ctrl-h` -> Set point size
