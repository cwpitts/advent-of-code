#!/usr/bin/env python3

import operator
import pprint
import sys
from datetime import datetime

def build_table(events: list) -> dict:
    table = {}
    index = 0
    while index < len(events) - 1:
        cur = events[index]
        (cur_time, cur_guard_id, cur_event) = (cur[0], cur[1], cur[2])

        if cur_guard_id not in table:
            table[cur_guard_id] = [0]*60
        
        nxt = events[index + 1]
        (nxt_time, nxt_guard_id, nxt_event) = (nxt[0], nxt[1], nxt[2])

        if nxt_guard_id == cur_guard_id:
            cur_min = int(cur_time.split(' ')[1].split(':')[1])
            nxt_min = int(nxt_time.split(' ')[1].split(':')[1])

            time_diff = ((nxt_min - cur_min) + 60) % 60

            # print(cur_guard_id, cur_time, '->', nxt_time, cur_event)
            # print(cur_min, nxt_min, time_diff)

            if cur_event == 'asleep':
                for i in range(cur_min, nxt_min):
                    table[cur_guard_id][i] += 1

        index += 1

    return table

def format_lines(lines: list) -> list:
    fmt_lines = []
    cur_id = 0
    for line in lines:
        time_str = ' '.join([line[0][1:], line[1][:-1]])
        if len(line) == 6:
            cur_id = line[3][1:]
            event = 'up'
        else:
            event = line[3]
        fmt_lines.append((time_str, cur_id, event))
    return fmt_lines


if __name__ == '__main__':
    lines = sorted([x.split() for x in open(sys.argv[1])],
                   key=operator.itemgetter(0, 1))

    formatted_lines = format_lines(lines)

    sleep_table = build_table(formatted_lines)

    for guard, minutes in sleep_table.items():
        m = ','.join(list(map(str, minutes)))
        print('Guard {g}: [{m}], sum={s}'.format(g=guard,
                                                 m=m,
                                                 s=sum(minutes)))

    # Part one is the sleepiest guard's sleepiest minute
    p1_guard = sorted(sleep_table.items(),
                             key=lambda x: sum(x[1]),
                             reverse=True)[0]

    p1_minute = max(enumerate(p1_guard[1]), key=operator.itemgetter(1))[0]

    print('Part 1: {}'.format(int(p1_guard[0]) * p1_minute))

    # Part two is the most regularly asleep guard's most-slept minute
    p2_guard = sorted(sleep_table.items(),
                      key=lambda x: max(x[1]),
                      reverse=True)[0]

    p2_minute = max(enumerate(p2_guard[1]), key=operator.itemgetter(1))[0]
    print('Part 2: {}'.format(int(p2_guard[0]) * p2_minute))
