#!/usr/bin/env python3

import sys
from typing import List

from claim import Claim

def parse_claim(line: str) -> (int, int, int, int, int):
    parts = line.split()
    coordinates = parts[2].split(',')
    size = parts[3].split('x')
    _id = int(parts[0][1:])
    x = int(coordinates[0])
    y = int(coordinates[1][:-1])
    width = int(size[0])
    height = int(size[1])

    return _id, x, y, width, height

def find_overlap(size: tuple, claims: list) -> int:
    grid = [[0 for x in range(size[0])]
            for x in range(size[0])]

    for claim in claims:
        for x_coord in range(claim.x, claim.x + claim.width):
            for y_coord in range(claim.y, claim.y + claim.height):
                if grid[x_coord][y_coord] == 0:
                    grid[x_coord][y_coord] = 1
                elif grid[x_coord][y_coord] == 1:
                    grid[x_coord][y_coord] = 2

    return sum([1 if col == 2 else 0
                for row in grid
                for col in row])

def find_clear_claim(grid: List[List], claims: list) -> int:
    for claim in claims:
        clear = True
        print(claim)
        for x_coord in range(claim.x, claim.x + claim.width):
            for y_coord in range(claim.y, claim.y + claim.height):
                clear = (grid[x_coord][y_coord] == 1)
                if not clear:
                    print('False at', (y_coord, x_coord))
                    break
            if not clear:
                break
        print(clear)
        for x_coord in range(claim.x, claim.x + claim.width):
            for y_coord in range(claim.y, claim.y + claim.height):
                sys.stdout.write(str(grid[x_coord][y_coord]))
            sys.stdout.write('\n')

        if clear:
            return claim.id_

def build_grid(size: tuple, claims: list) -> List[List]:
    grid = [[0 for x in range(size[0])]
            for x in range(size[0])]

    for claim in claims:
        for x_coord in range(claim.x, claim.x + claim.width):
            for y_coord in range(claim.y, claim.y + claim.height):
                if grid[x_coord][y_coord] == 0:
                    grid[x_coord][y_coord] = 1
                elif grid[x_coord][y_coord] == 1:
                    grid[x_coord][y_coord] = 2

    return grid

if __name__ == '__main__':
    claims = []
    max_x = 0
    max_y = 0

    for claim_line in open(sys.argv[1]):
        _id, x, y, width, height = parse_claim(claim_line)
        max_x = max(max_x, x + width)
        max_y = max(max_y, y + height)
        claims.append(Claim(claim_line))

    size = (max(max_x, max_y), max(max_x, max_y))

    ans_1 = find_overlap(size, claims)
    ans_2 = find_clear_claim(build_grid(size, claims), claims)

    print('Part 1:', ans_1)
    print('Part 2:', ans_2)
