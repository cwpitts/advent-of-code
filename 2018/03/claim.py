class Claim:
    def __init__(self, line):
        parts = line.split()
        coordinates = parts[2].split(',')
        size = parts[3].split('x')
        self.id_ = int(parts[0][1:])
        self.x = int(coordinates[0])
        self.y = int(coordinates[1][:-1])
        self.width = int(size[0])
        self.height = int(size[1])

    def __str__(self):
        s = "#{id_} ({x}, {y}), ({width}, {height})"
        return s.format(id_=self.id_,
                        x=self.x,
                        y=self.y,
                        width=self.width,
                        height=self.height)
