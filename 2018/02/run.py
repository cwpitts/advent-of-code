#!usr/bin/env python3

from collections import Counter
import sys
from typing import Tuple

def score(str1: str, str2: str) -> int:
    return sum(not (x[0][1] == x[1][1])
               for x in zip(enumerate(str1), enumerate(str2)))

def find_close_words(words: list) -> Tuple[str, str]:
    for word in words:
        for other_word in words:
            if score(word, other_word) == 1:
                return word, other_word
    return ('', '')

def find_common_letters(w1: str, w2: str) -> str:
    return ''.join([x[0][1] for x in zip(enumerate(w1), enumerate(w2))
                    if x[0][1] == x[1][1]])

if __name__ == '__main__':
    inputs = [x.strip() for x in open(sys.argv[1])]

    counts = [Counter(x) for x in inputs]
    two = sum([1 for x in counts
               if any(y[1] == 2 for y in x.items())])
    three = sum([1 for x in counts
                 if any(y[1] == 3 for y in x.items())])

    print('Part 1:', two * three)

    ans_p2 = find_common_letters(*find_close_words(inputs))
    print('Part 2:', ans_p2)
