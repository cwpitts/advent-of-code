#include "nanobot.hpp"

#include <sstream>
#include <iostream>
#include <cstdlib>

Nanobot::Nanobot(int x, int y, int z, int r)
{
  this->_pos = std::make_tuple(x, y, z);
  this->_radius = r;
}

Nanobot::~Nanobot()
{}

std::tuple<int, int, int> Nanobot::pos()
{
  return this->_pos;
}

int Nanobot::radius()
{
  return this->_radius;
}

std::string Nanobot::toString()
{
  std::ostringstream oss;

  oss << "Nanobot @ (" << std::get<0>(this->_pos) << ",";
  oss << std::get<1>(this->_pos) << ",";
  oss << std::get<2>(this->_pos) << "), radius " << this->_radius;

  return oss.str();
}

std::vector<Nanobot*> Nanobot::inRange(std::vector<Nanobot*> bots)
{
  std::vector<Nanobot*> ret;

  for (Nanobot* bot : bots)
  {
    //std::cout << "bot at (" << std::get<0>(bot->pos()) << "," << std::get<1>(bot->pos()) << "," << std::get<2>(bot->pos()) << ") ";

    int dist = abs(std::get<0>(this->_pos) - std::get<0>(bot->pos()))
      + abs(std::get<1>(this->_pos) - std::get<1>(bot->pos()))
      + abs(std::get<2>(this->_pos) - std::get<2>(bot->pos()));

    //std::cout << "dist=" << dist;
    
    if (dist <= this->_radius)
    {
      //std::cout << ", in range!" << std::endl;
      ret.push_back(bot);
    }
    else
    {
      //std::cout << ", not in range!" << std::endl;
    }
  }
  
  return ret;
}

void Nanobot::dist(std::vector<std::vector<std::vector<int>>>& dist)
{
  for (int x = 0; x < dist.size(); x++)
  {
    for (int y = 0; y < dist.size(); y++)
    {
      for (int z = 0; z < dist.size(); z++)
      {
	int _dist = abs(std::get<0>(this->_pos) - x)
	  + abs(std::get<1>(this->_pos) - y)
	  + abs(std::get<2>(this->_pos) - z);

	if (_dist <= this->_radius)
	{
	  dist[x][y][z] += 1;
	}
      }
    }
  }
}
