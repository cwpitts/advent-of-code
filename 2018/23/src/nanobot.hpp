#ifndef NANOBOT_HPP
#define NANOBOT_HPP

#include <tuple>
#include <string>
#include <vector>

class Nanobot
{
public:
  Nanobot(int x, int y, int z, int r);
  ~Nanobot();

  std::tuple<int, int, int> pos();
  int radius();
  std::string toString();
  std::vector<Nanobot*> inRange(const std::vector<Nanobot*> bots);
  void dist(std::vector<std::vector<std::vector<int>>>& dist);

private:
  std::tuple<int, int, int> _pos;
  int _radius;
};
#endif // NANOBOT_HPP
