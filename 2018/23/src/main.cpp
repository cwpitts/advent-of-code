#include <tuple>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <cctype>

#include "nanobot.hpp"

int parseInt(std::string line, int start, int& end)
{
  while (! isdigit(line[start]) && line[start] != '-')
  {
    start += 1;
  }
  end = start + 1;
  while (end < line.size() && isdigit(line[end]))
  {
    end += 1;
  }

  return std::atoi(line.substr(start, end - start).c_str());
}

Nanobot* parseNanobot(const std::string line)
{
  int end;
  int x = parseInt(line, 0, end);
  int y = parseInt(line, end, end);
  int z = parseInt(line, end, end);
  int r = parseInt(line, end, end);

  Nanobot* ret = new Nanobot(x, y, z, r);

  // std::cout << ret->toString() << std::endl;
  
  return ret;
}

void printDistances(const std::vector<std::vector<std::vector<int>>>& dist)
{
  for (int x = 0; x < dist.size(); x++)
  {
    for (int y = 0; y < dist.size(); y++)
    {
      for (int z = 0; z < dist.size(); z++)
      {
	std::cout << "dist[" << x << "][" << y << "][" << z << "]=" << dist[x][y][z] << std::endl;
      }
    }
  }
}

int main(int argc, char* argv[])
{
  if (argc == 1)
  {
    std::cerr << "No input file provided!" << std::endl;
    return 1;
  }

  std::ifstream inputFile(argv[1]);
  if (! inputFile.is_open())
  {
    std::cerr << "Could not open input file!" << std::endl;
    return 1;
  }

  Nanobot* strongest = NULL;
  std::vector<Nanobot*> bots;

  std::string line;
  while (std::getline(inputFile, line))
  {
    Nanobot* bot = parseNanobot(line);
    if (strongest == NULL || bot->radius() > strongest->radius())
    {
      strongest = bot;
    }
    bots.push_back(bot);
  }

  std::cout << bots.size() << " bots" << std::endl;
  std::cout << "Strongest: " << strongest->toString() << std::endl;
  std::cout << "Part 1: " << strongest->inRange(bots).size() << std::endl;

  std::vector<std::vector<std::vector<int>>> dist(bots.size(), std::vector<std::vector<int>>(bots.size(), std::vector<int>(bots.size(), 0)));

  for (Nanobot* bot : bots)
  {
    bot->dist(dist);
  }
  
  // printDistances(dist);
  
  for (Nanobot* bot : bots)
  {
    delete bot;
  }
  
  return 0;
}
