const SIZE: usize = 300;
const RADIX: u32 = 10;
const BLOCK_SIZE: usize = 3;

fn calc_power(x: i64, y: i64, serial_number: i64) -> i64 {
    /*
    1. Find the fuel cell's rack ID, which is its X coordinate plus 10.
    2. Begin with a power level of the rack ID times the Y coordinate.
    3. Increase the power level by the value of the grid serial number (your
       puzzle input).
    4. Set the power level to itself multiplied by the rack ID.
    5. Keep only the hundreds digit of the power level (so 12345 becomes 3;
       numbers with no hundreds digit become 0).
    6. Subtract 5 from the power level.
     */

    // Find the fuel cell's rack ID, which is its X coordinate plus 10.
    let rack_id: i64 = x + 10;
    // Begin with a power level of the rack ID times the Y coordinate.
    let mut power_level: i64 = rack_id * y;
    /*
    Increase the power level by the value of the grid serial number (your
    puzzle input).
     */
    power_level += serial_number;
    // Set the power level to itself multiplied by the rack ID.
    power_level *= rack_id;
    /*
    Keep only the hundreds digit of the power level (so 12345 becomes 3;
    numbers with no hundreds digit become 0).
     */
    let power_level_str: Vec<char> = power_level.to_string().chars().collect();
    if power_level_str.len() < 3 {
        power_level = 0;
    } else {
        let digit = power_level_str[power_level_str.len() - 3];
        power_level = digit.to_digit(RADIX).unwrap() as i64;
    }
    
    // Subtract 5 from the power level.
    return power_level - 5;
}

fn find_max_power_by_block(grid: [[i64; SIZE]; SIZE], block_size: usize) -> (i64, i64) {
    let mut max_power: i64 = 0;
    let mut block_x: i64 = 0;
    let mut block_y: i64 = 0;

    for y in 0..(SIZE - block_size) {
        for x in 0..(SIZE - block_size) {
            // Calculate the block power
            let mut block_power = 0;
            for dy in 0..3 {
                for dx in 0..3 {
                    block_power += grid[y + dy][x + dx];
                }
            }
            if block_power > max_power {
                max_power = block_power;
                block_x = x as i64;
                block_y = y as i64;
            }
        }
    }
    return (block_x + 1, block_y + 1);
}

fn find_max_power(grid: [[i64; SIZE]; SIZE]) -> (i64, i64, i64) {
    let mut max_size: i64 = 0;
    let mut max_x: i64 = 0;
    let mut max_y: i64 = 0;
    let mut max_power: i64 = 0;

    // Iterate over sizes
    for d in 1..SIZE {
        // Only check what we need
        for y in 0..grid.len() - d {
            for x in 0..grid[y].len() - d {
                let mut power = 0;
                // Add up cells
                for dy in 0..d {
                    for dx in 0..d {
                        power += grid[y + dy][x + dx];
                    }

                    // Keep max
                    if power > max_power {
                        max_power = power;
                        max_size = d as i64;
                        max_x = x as i64;
                        max_y = y as i64;
                    }
                }
            }
        }
    }
    return (max_x + 1, max_y + 1, max_size);
}

fn main() {
    // Get arguments
    let args: Vec<String> = std::env::args().collect();
    if args.len() == 1 {
        println!("Missing input!");
        std::process::exit(1);
    }

    // Get filename
    let contents = std::fs::read_to_string(&args[1]);
    let serial_number: i64 = contents.unwrap().parse().unwrap();
    println!("{}", serial_number);

    // Build grid
    let mut grid: [[i64; SIZE]; SIZE] = [[0; SIZE]; SIZE];
    for row in 0..grid.len() {
        for col in 0..grid[0].len() {
            grid[row][col] = calc_power((col + 1) as i64,
                                        (row + 1) as i64,
                                        serial_number);
        }
    }
    
    // Calculate max block power for part 1
    let (p1_x, p1_y) = find_max_power_by_block(grid, BLOCK_SIZE);
    println!("Part 1: ({}, {})", p1_x, p1_y);
    
    // Calculate max block power for part 2 (unrestricted size)
    let (p2_x, p2_y, p2_size) = find_max_power(grid);
    println!("Part 2: ({}, {}, {})", p2_x, p2_y, p2_size);
}
