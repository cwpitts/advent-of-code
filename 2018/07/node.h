#ifndef NODE_H
#define NODE_H

#define MAX_NODES 26
#define ASCII_OFFSET 65

struct Node
{
  char name;
  struct Node* parents[MAX_NODES];
  struct Node* children[MAX_NODES];
  int temporary;
  int permanent;
  int complete;
  int inProgress;
};

void addNode(struct Node* parent, struct Node* child);
void printNode(struct Node* node);
void freeNode(struct Node* node);
void initNode(struct Node* node, char name);
int indexOf(char c);
void printNodeList(struct Node** list, char* str);
void addNodeToList(struct Node* node, struct Node** list);
void removeNodeFromList(struct Node* node, struct Node** list);
int nodeInList(struct Node* node, struct Node** list);
struct Node* findHeadNode(struct Node** list);
int hasParents(struct Node* node);
#endif // NODE_H
