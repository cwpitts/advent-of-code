#include "node.h"
#include <stdlib.h>
#include <stdio.h>

void addNode(struct Node* parent, struct Node* child)
{
  parent->children[indexOf(child->name)] = child;
  child->parents[indexOf(parent->name)] = parent;
}

void freeNode(struct Node* parent)
{
  for (int i = 0; i < MAX_NODES; i++)
  {
    if (parent->children[i] != 0)
    {
      freeNode(parent->children[i]); 
    }
  }
  free(parent);
}

void printNode(struct Node* node)
{
  if (node == NULL)
  {
    return;
  }

  printf("Node %c\n", node->name);
  printf("  Children: ");
  for (int i = 0; i < MAX_NODES; i++)
  {
    if (node->children[i] != NULL)
    {
      printf("%c", node->children[i]->name);
    }
  }

  printf("\n  Parents: ");
  for (int i = 0; i < MAX_NODES; i++)
  {
    if (node->parents[i] != NULL)
    {
      printf("%c", node->parents[i]->name);
    }
  }

  printf("\n  In progress: %d\n  Complete: %d\n",
	 node->inProgress, node->complete);
}

void initNode(struct Node* node, char name)
{
  node->name = name;
  node->temporary = 0;
  node->permanent = 0;
  node->complete = 0;
  node->inProgress = 0;
}

int indexOf(char c)
{
  // Abusing chars
  return c - ASCII_OFFSET;
}

void printNodeList(struct Node** list, char* str)
{
  printf("%s:\n", str);
  for (int i = 0; i < MAX_NODES; i++)
  {
    if (list[i] != NULL)
    {
      printf("%d: %c\n", i, list[i]->name); 
    }
  }
}

void addNodeToList(struct Node* node, struct Node** list)
{
  int index = 0;
  while (index < MAX_NODES && list[index] != NULL)
  {
    if (list[index] == node)
    {
      return;
    }
    index++;
  }
  list[index] = node;
}

void removeNodeFromList(struct Node* node, struct Node** list)
{
  for (int i = 0; i < MAX_NODES; i++)
  {
    if (list[i] != NULL && list[i] == node)
    {
      list[i] = NULL;
      for (int j = i; j < MAX_NODES - 1; j++)
      {
	list[j] = list[j + 1];
      }
      list[MAX_NODES] = NULL;
    }
  }
}

int nodeInList(struct Node* node, struct Node** list)
{
  for (int i = 0; i < MAX_NODES; i++)
  {
    if (list[i] != NULL && list[i]->name == node->name)
    {
      return i;
    }
  }
  return -1;
}

struct Node* findHeadNode(struct Node** list)
{
  for (int i = 0; i < MAX_NODES; i++)
  {
    if (list[i] != NULL && hasParents(list[i]) == 0)
    {
      return list[i];
    }
  }

  return NULL;
}

int hasParents(struct Node* node)
{
  for (int i = 0; i < MAX_NODES; i++)
  {
    if (node->parents[i] != NULL)
    {
      return 1;
    }
  }

  return 0;
}
