#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "node.h"

const int NUM_WORKERS = 5;
const int BASE_TIME = 60;

int parentsComplete(struct Node* node)
{
  for (int i = 0; i < MAX_NODES; i++)
  {
    if (node->parents[i] != NULL && node->parents[i]->complete == 0)
    {
      return 0; 
    }
  }
  
  return 1;
}

int workLeft(struct Node** list)
{
  for (int i = 0; i < MAX_NODES; i++)
  {
    if (list[i] != NULL && list[i]->complete == 0)
    {
      return 1;
    }
  }

  return 0;
}

struct Node* getNextAvailableTask(struct Node** tasks)
{
  for (int i = 0; i < MAX_NODES; i++)
  {
    /*
      Task exists, parents are complete, and it's not already done or in
      progress
    */
    if (tasks[i] != NULL
	&& parentsComplete(tasks[i])
	&& tasks[i]->inProgress == 0
	&& tasks[i]->complete == 0)
    {
      return tasks[i];
    }
  }

  return NULL;
}

void printStatus(struct Node** availableTasks, int* clocks, int n, int seconds, struct Node** allTasks)
{
  printf("%d\t", seconds);
  for (int i = 0; i < n; i++)
  {
    if (availableTasks[i] != NULL)
    {
      printf("%c\t", availableTasks[i]->name);
      // printNode(tasks[i]);
    }
    else
    {
      printf(".\t");
    }
  }

  for (int i = 0; i < MAX_NODES; i++)
  {
    if (allTasks[i] != NULL && allTasks[i]->complete == 1)
    {
      printf("%c", allTasks[i]->name);
    }
  }
  printf("\n");
}

int timeTasks(struct Node* head, struct Node** tasks, int numWorkers)
{
  int seconds = 0;
  int workerClocks[numWorkers];
  struct Node* workerTasks[numWorkers];
  struct Node* availableTasks[MAX_NODES] = {NULL};

  // Initialize worker clocks
  for (int i = 0; i < numWorkers; i++)
  {
    workerClocks[i] = 0;
  }

  // Initialize worker tasks
  for (int i = 0; i < numWorkers; i++)
  {
    workerTasks[i] = NULL;
  }
  
  // Assign first task
  addNodeToList(head, availableTasks);
  workerTasks[0] = getNextAvailableTask(availableTasks);
  workerTasks[0]->inProgress = 1;
  workerClocks[0] = BASE_TIME + indexOf(workerTasks[0]->name) + 1;

  /* printf("Seconds\t"); */
  /* for (int i = 0; i < numWorkers; i++) */
  /* { */
  /*   printf("%d\t", i + 1); */
  /* } */
  /* printf("\n"); */
  /* printStatus(workerTasks, workerClocks, numWorkers, seconds, tasks); */

  while (workLeft(tasks) == 1)
  {
    // Decrement clocks
    for (int i = 0; i < numWorkers; i++)
    {
      if (workerClocks[i] > 0)
      {
	workerClocks[i] -= 1;

	// Did you just finish? Hooray! Mark it.
	if (workerClocks[i] == 0)
	{
	  workerTasks[i]->complete = 1;
	  workerTasks[i]->inProgress = 0;

	  // And add any children that can now be done
	  for (int j = 0; j < MAX_NODES; j++)
	  {
	    // Null check and parents have to be complete
	    if (workerTasks[i]->children[j] != NULL
	  	&& parentsComplete(workerTasks[i]) == 1)
	    {
	      addNodeToList(workerTasks[i]->children[j], availableTasks);
	    }
	  }

	  // Take it off the queue for the worker
	  workerTasks[i] = NULL;
	}
      }      
    }

    // Iterate over the workers, find idle ones
    for (int i = 0; i < numWorkers; i++)
    {
      // If there's work, assign them the time (BASE_TIME + index)
      if (workerTasks[i] == NULL && getNextAvailableTask(availableTasks) != NULL)
      {
	workerTasks[i] = getNextAvailableTask(availableTasks);
	workerTasks[i]->inProgress = 1;
	workerClocks[i] = BASE_TIME + indexOf(workerTasks[i]->name) + 1;
      }
    }
    
    // Increment time
    seconds += 1;
    
    //printStatus(workerTasks, workerClocks, numWorkers, seconds, tasks);
  }

  return seconds;
}

int hasDependencies(struct Node* node, struct Node** list)
{
  //printf("Does %c have a dependency (parent)?\n", node->name);
  //printNode(node);
  for (int i = 0; i < MAX_NODES; i++)
  {
    /* if (node->parents[i] != NULL) */
    /* { */
    /*   printf("%c\n", node->parents[i]->name); */
    /* } */

    if (node->parents[i] != NULL && nodeInList(node->parents[i], list) != -1)
    {
      //printf("It does: %c\n", node->parents[i]->name);
      return 1;
    }
  }
  //printf("It does not\n");
  return 0;
}

void doTasks(struct Node* curNode, struct Node** availableNodes)
{
  curNode->complete = 1;
  printf("%c", curNode->name);
  /* printf("%c\n", curNode->name); */
  /* printNode(curNode); */
  for (int i = 0; i < MAX_NODES; i++)
  {
    if (curNode->children[i] != NULL && parentsComplete(curNode->children[i]) == 1)
    {
      addNodeToList(curNode->children[i], availableNodes);
    }
  }

  // printNodeList(availableNodes, "availableNodes");
  
  struct Node* nextNode = NULL;
  for (int i = 0; i < MAX_NODES; i++)
  {
    if (nextNode == NULL
	|| (availableNodes[i] != NULL
	    && indexOf(availableNodes[i]->name) < indexOf(nextNode->name)
	    && hasDependencies(availableNodes[i], availableNodes) == 0))
    {
      nextNode = availableNodes[i];
    }
  }

  if (nextNode == NULL)
  {
    return;
  }

  // printf("next node is %c\n", nextNode->name);
  removeNodeFromList(nextNode, availableNodes);
  doTasks(nextNode, availableNodes);
}

void prepend(struct Node* node, struct Node** sortedNodes)
{
  for (int i = MAX_NODES - 1; i > 0; i--)
  {
    // printf("nodes[%d]=nodes[%d]\n", i, i - 1);
    sortedNodes[i] = sortedNodes[i - 1];
  }
  // printf("nodes[0]=%p (%c)\n", node, node->name);
  sortedNodes[0] = node;
}

int unmarked(struct Node** nodes)
{
  //printf("Node marks:\n");
  for (int i = 0; i < MAX_NODES; i++)
  {
    /* if (nodes[i] != NULL) */
    /* { */
    /*   printf("%c: permanent=%d, temporary=%d\n", */
    /* 	     nodes[i]->name, */
    /* 	     nodes[i]->permanent, */
    /* 	     nodes[i]->temporary); */
    /* } */
    if (nodes[i] != NULL && nodes[i]->permanent == 0)
    {
      return 1;
    }
  }
  
  return 0;
}

int visit(struct Node* node, struct Node** sortedNodes)
{
  if (node->permanent == 1)
  {
    return 1;
  }

  if (node->temporary == 1)
  {
    return -1;
  }

  //printf("Marking node %c\n", node->name);
  node->temporary = 1;

  for (int i = 0; i < MAX_NODES; i++)
  {
    if (node->children[i] != NULL)
    {
      int ret = visit(node->children[i], sortedNodes);
      if (ret == -1)
      {
	//printf("Unmarking %c\n", node->name);
	node->temporary = 0;
	return -1;
      }
    }
  }

  node->permanent = 1;
  prepend(node, sortedNodes);
  
  return 0;
}

void topoSort(struct Node** allNodes, struct Node** sortedNodes)
{
  while (unmarked(allNodes) != 0)
  {
    //printf("Found unmarked nodes, continuing\n");
    for (int i = 0; i < MAX_NODES; i++)
    {
      if (allNodes[i] != NULL && allNodes[i]->permanent == 0)
      {
	//printf("Visiting node %c\n", allNodes[i]->name);
	visit(allNodes[i], sortedNodes);
      }
    }
  }
}

int main(int argc, char* argv[])
{
  // Read inut
  FILE* fp = NULL;
  char* line = NULL;
  long unsigned int len = 0;
  int read = 0;

  fp = fopen(argv[1], "r");
  if (fp == NULL)
  {
    fprintf(stderr, "Failed to open file\n");
    exit(EXIT_FAILURE);
  }

  struct Node* allNodes[MAX_NODES];
  for (int i = 0; i < MAX_NODES; i++)
  {
    allNodes[i] = NULL;
  }

  /* for (int i = 0; i < MAX_NODES; i++) */
  /* { */
  /*   printf("allNodes[%d]=%p\n", i, allNodes[i]); */
  /* } */

  while ((read = getline(&line, &len, fp)) != -1)
  {
    // printf("%s", line);
    char parent = line[5];
    char child = line[36];
    free(line);

    if (allNodes[indexOf(parent)] == NULL)
    {
      allNodes[indexOf(parent)] = malloc(sizeof(struct Node));
      initNode(allNodes[indexOf(parent)], parent);
    }

    if (allNodes[indexOf(child)] == NULL)
    {
      allNodes[indexOf(child)] = malloc(sizeof(struct Node));
      initNode(allNodes[indexOf(child)], child);
    }

    addNode(allNodes[indexOf(parent)], allNodes[indexOf(child)]);
  }

  for (int i = 0; i < MAX_NODES; i++)
  {
    printNode(allNodes[i]);
  }

  struct Node* availableNodes[MAX_NODES] = {NULL};
  struct Node* head = findHeadNode(allNodes);

  printf("Part 1: ");
  doTasks(head, availableNodes);
  printf("\n");

  for (int i = 0; i < MAX_NODES; i++)
  {
    if (allNodes[i] != NULL)
    {
      allNodes[i]->complete = 0; 
    }
  }

  int totalTime = timeTasks(head, allNodes, NUM_WORKERS);
  printf("Part 2: %d\n", totalTime);

  for (int i = 0; i < MAX_NODES; i++)
  {
    if (allNodes[i] != NULL)
    {
      free(allNodes[i]);
    }

    allNodes[i] = NULL;
    availableNodes[i] = NULL;
  }

  fclose(fp);
  fp = NULL;
  head = NULL;

  return 0;
}
