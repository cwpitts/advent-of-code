#!/usr/bin/env

import copy
import pprint
import sys


def printState(state):
    for pot in state:
        sys.stdout.write('{}'.format(pot.index).ljust(4))
    sys.stdout.write('\n')
    for pot in state:
        sys.stdout.write('{}'.format(pot.state).ljust(4))
    sys.stdout.write('\n\n')

def update(state, transitions):
    # Check from the lowest number to the highest
    start = min(state)
    end = max(state)

    return {i for i in range(start - 3, end + 4)
            if transitions.get(''.join(['#' if c + i in state else '.'
                                        for c in [-2, -1, 0, 1, 2]])) == '#'}

if __name__ == '__main__':
    inputs = [x .strip()for x in open(sys.argv[1])
              if x.strip()]

    plants = set(x for x, c in enumerate(inputs[0][len('initial state: '):])
                 if c == '#')
    print(plants)

    transitions = {
        x.split(' => ')[0]: x.split(' => ')[1]
        for x in inputs[1:]
    }

    pprint.pprint(transitions)

    prev_sum = 0
    diffs = []
    iterations = 2000
    for i in range(iterations):
        plants = update(plants, transitions)
        cur_sum = sum(plants)
        diffs.append(cur_sum - prev_sum)
        prev_sum = cur_sum
        if i == 19:
            print('Part 1:', sum(plants))

    avg_diff = sum(diffs)//len(diffs)
    print('Part 2:', (50000000000 - iterations)*avg_diff + prev_sum)
