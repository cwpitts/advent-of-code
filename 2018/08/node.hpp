#include <string>
#include <vector>

class Node
{
public:
  Node(std::vector<Node*> children, std::vector<int> metadata);
  ~Node();
  const std::vector<Node*> getChildren();
  const std::vector<int> getMetadata();
  int sumMetadata();
  void addChild(Node* child);
  int getId();

private:
  int id;
  std::vector<Node*> children;
  std::vector<int> metadata;
};
