#include "node.hpp"

Node::Node(std::vector<Node*> children, std::vector<int> metadata)
{
  static int idCounter = 0;
  this->children = children;
  this->metadata = metadata;
  this->id = idCounter++;
}

Node::~Node()
{
  for (Node* c : this->children)
  {
    delete c;
  }
}

const std::vector<Node*> Node::getChildren()
{
  return this->children;
}

const std::vector<int> Node::getMetadata()
{
  return this->metadata;
}

int Node::sumMetadata()
{
  int res = 0;
  for(int i : this->metadata)
  {
    res += i;
  }
  return res;
}

void Node::addChild(Node* child)
{
  this->children.push_back(child);
}

int Node::getId()
{
  return this->id;
}
