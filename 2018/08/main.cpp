#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

#include "node.hpp"

Node* parseTree(std::istringstream& input)
{
  int numChildren;
  int numMetadata;

  input >> numChildren;
  input >> numMetadata;

  // Parse out child subtrees
  std::vector<Node*> children;
  for (int i = 0; i < numChildren; i++)
  {
    Node* subtree = parseTree(input);
    children.push_back(subtree);
  }

  std::vector<int> metadata;
  for (int i = 0; i < numMetadata; i++)
  {
    int tmp;
    input >> tmp;
    metadata.push_back(tmp);
  }
  
  return new Node(children, metadata);
}

void nodeToDot(Node* node, std::ofstream& dotFile)
{
  dotFile << node->getId() << " [label=\"";
  std::vector<int> metadata = node->getMetadata();
  for (int i = 0; i < metadata.size(); i++)
  {
    dotFile << metadata[i];
    if (i < metadata.size() - 1)
    {
      dotFile << " ";
    }
  }
  dotFile << "\"];" << std::endl;

  for (Node* child : node->getChildren())
  {
    dotFile << node->getId() << " -- " << child->getId() << ";" << std::endl;
    nodeToDot(child, dotFile);
  }
}

void generateDotFile(Node* head)
{
  std::ofstream dotFile("graph.dot");
  if (! dotFile.is_open())
  {
    std::cerr << "Failed to open dotfile for writing" << std::endl;
    return;
  }

  dotFile << "graph solution {" << std::endl;
  nodeToDot(head, dotFile);
  dotFile << "}";
  
  dotFile.close();
}

int sumTree(Node* node)
{
  int subtreeSum = 0;

  for (Node* child : node->getChildren())
  {
    subtreeSum += sumTree(child);
  }
  
  return node->sumMetadata() + subtreeSum;
}

int valueOf(Node* node)
{
  std::vector<int> metadata = node->getMetadata();
  std::vector<Node*> children = node->getChildren();

  int value = 0;
  
  if (children.size() == 0)
  {
    value = node->sumMetadata();
  }
  else
  {
    for (int i = 0; i < metadata.size(); i++)
    {
      if (metadata[i] > 0 && metadata[i] - 1 < children.size())
      {
	value += valueOf(children[metadata[i] - 1]);
      }
    }
  }

  return value;
}

int main(int argc, char* argv[])
{
  if (argc == 1)
  {
    std::cerr << "No input file found!" << std::endl;
    exit(1);
  }

  // Read input
  std::ifstream inputFile(argv[1]);
  std::string input;
  std::getline(inputFile, input);
  inputFile.close();
  std::istringstream iss(input);

  // Parse tree
  Node* head = parseTree(iss);

  // Produce part 1 answer
  std::cout << "Part 1: " << sumTree(head) << std::endl;

  // Produce part 2 answer
  std::cout << "Part 2: " << valueOf(head) << std::endl;
  
  // Generate graph
  generateDotFile(head);

  delete head;
  
  return 0;
}
