#!/usr/bin/env python3

import operator
import string
import sys
from typing import Union, List

def can_react(a: str, b: str) -> bool:
    # Case check
    if a.istitle() and b.istitle():
        return False
    # Case check
    if not a.istitle() and not b.istitle():
        return False
    # Letter check (could also be .lower())
    if a.upper() != b.upper():
        return False
    return True

def react(polymer: str) -> Union[str, bool]:
    for i in range(len(polymer) - 1):
        if can_react(polymer[i], polymer[i + 1]):
            return polymer[:i] + polymer[i + 2:], True
    return polymer, False

def fully_react(polymer: str) -> int:
    (reduced, has_reacted) = react(polymer)
    while has_reacted:
        (reduced, has_reacted) = react(reduced)
    return len(reduced)

if __name__ == '__main__':
    _input = open(sys.argv[1]).read().strip()

    ans_p1 = fully_react(_input)
    print('Part 1:', ans_p1)

    ans_p2 = (2 ** 63) - 1
    for unit in string.ascii_lowercase:
        print('Removing', unit)
        candidate = _input.replace(unit, '').replace(unit.upper(), '')
        candidate_length = fully_react(candidate)
        print(candidate_length)
        ans_p2 = min(ans_p2, candidate_length)

    print('Part 2:', ans_p2)
