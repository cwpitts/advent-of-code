#include <string>
#include <iostream>
#include <cstdlib>

void printScoreboard(std::string& scoreboard, int& elf0, int& elf1)
{
  for (int i = 0; i < scoreboard.size(); i++)
  {
    if (i == elf0)
    {
      std::cout << '(' << scoreboard[i] << ')';
    }
    else if (i == elf1)
    {
      std::cout << '[' << scoreboard[i] << ']';
    }
    else
    {
      std::cout << scoreboard[i];
    }
  }
  std::cout << std::endl;
}

void iterateScoreboard(std::string& scoreboard, int& elf0, int& elf1)
{
  scoreboard += std::to_string((scoreboard[elf0] - '0') + (scoreboard[elf1] - '0'));
  elf0 = (elf0 + (scoreboard[elf0] - '0') + 1) % scoreboard.size();
  elf1 = (elf1 + (scoreboard[elf1] - '0') + 1) % scoreboard.size();
}

int main(int argc, char* argv[])
{
  std::string target = argv[1];
  std::string scores = "37";

  int elf0 = 0;
  int elf1 = 1;

  printScoreboard(scores, elf0, elf1);
  while (scores.size() < std::atoi(target.c_str()))
  {
    iterateScoreboard(scores, elf0, elf1);
  }

  for (int i = 0; i < 10; i++)
  {
    iterateScoreboard(scores, elf0, elf1);
  }

  std::cout << "Part 1: ";
  for (int i = std::atoi(target.c_str()); i < std::atoi(target.c_str()) + 10; i++)
  {
    std::cout << scores[i];
  }
  std::cout << std::endl;

  while (scores.find(target, scores.size() - 10) == std::string::npos)
  {
    iterateScoreboard(scores, elf0, elf1);
  }

  std::cout << "Part 2: " << scores.find(target, scores.size() - 10) << std::endl;
  
  return 0;
}
