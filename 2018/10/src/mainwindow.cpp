#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QGraphicsItem>
#include <QGraphicsEllipseItem>
#include <QDebug>
#include <QKeyEvent>
#include <QGraphicsScene>
#include <QString>
#include <QFileDialog>
#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QStringList>
#include <QStringRef>
#include <QInputDialog>
#include <QWheelEvent>

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  qApp->installEventFilter(this);
  this->time = 0;
  this->pointSize = 2;
  this->pen.setColor(Qt::black);
  this->brush.setColor(Qt::black);
  this->brush.setStyle(Qt::SolidPattern);

  QGraphicsScene* scene = new QGraphicsScene;
  this->ui->graphicsView->setScene(scene);
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::on_backwardButton_clicked()
{
  this->backward();
}

void MainWindow::on_forwardButton_clicked()
{
  this->forward();
}

bool MainWindow::eventFilter(QObject* obj, QEvent* event)
{
  if (event->type() == QEvent::KeyPress)
  {
    QKeyEvent* kevent = (QKeyEvent*)event;

    if (kevent->key() == Qt::Key_Left)
    {
      this->backward();
      return true;
    }
    else if (kevent->key() == Qt::Key_Right)
    {
      this->forward();
      return true;
    }
    else if (kevent->modifiers() == Qt::ControlModifier
	     && kevent->key() == Qt::Key_T)
    {
      bool ok;
      int newTime = QInputDialog::getInt(this, "Set New Time Value", "New time:", this->time,
					 -2147483647, 2147483647, 1, &ok);
      if (ok)
      {
	this->time = newTime;
	this->updateDisplay();
      }
      return true;
    }
    else if (kevent->modifiers() == Qt::ControlModifier
	     && kevent->key() == Qt::Key_H)
    {
      bool ok;
      int newSize = QInputDialog::getInt(this, "Set New Point Size", "Size:", this->pointSize,
					 -2147483647, 2147483647, 1, &ok);
      if (ok)
      {
	this->pointSize = newSize;
	this->updateDisplay();
      }
      return true;
    }
    else if (kevent->key() == Qt::Key_Up)
    {
      this->ui->graphicsView->scale(1 / 0.1, 1 / 0.1);
      return true;
    }
    else if (kevent->key() == Qt::Key_Down)
    {
      this->ui->graphicsView->scale(0.1, 0.1);
      return true;
    }
  }

  return false;
}

void MainWindow::forward()
{
  this->time += 1;
  this->updateDisplay();
}

void MainWindow::backward()
{
  this->time -= 1;
  this->updateDisplay();
}

void MainWindow::updateDisplay()
{
  this->ui->graphicsView->scene()->clear();

  int minX = 0;
  int minY = 0;
  int maxX = 0;
  int maxY = 0;
  for (int i = 0; i < this->stars.size(); i++)
  {
    struct Star star = this->stars[i];
    minX = qMin(minX, star.pos.x() + star.vel.x() * this->time);
    minY = qMin(minY, star.pos.y() + star.vel.y() * this->time);
    maxX = qMax(maxX, star.pos.x() + star.vel.x() * this->time);
    maxY = qMax(maxY, star.pos.y() + star.vel.y() * this->time);

    QPointF newPos = this->ui->graphicsView->mapToScene(star.pos.x() + star.vel.x() * this->time,
							star.pos.y() + star.vel.y() * this->time);

    this->ui->graphicsView->scene()->addEllipse(star.pos.x() + star.vel.x() * this->time,
						star.pos.y() + star.vel.y() * this->time,
						this->pointSize, this->pointSize,
						this->pen, this->brush);
  }

  QRectF rect(minX - (minX * 0.5), minY - (minY * 0.5), (maxX - minX) * 1.5, (maxY - minY) * 1.5);
  this->ui->graphicsView->fitInView(rect, Qt::KeepAspectRatio);

  this->ui->numPointsLabel->setText(QString::number(this->stars.size()) + " points");
  this->ui->sizeLabel->setText("size=" + QString::number(this->pointSize));
  this->ui->timeLabel->setText("t=" + QString::number(this->time));
  this->ui->dxdyLabel->setText("dX=" + QString::number(maxX - minX)
			       + ", dY=" + QString::number(maxY - minY)
			       + ", area=" + QString::number(rect.height() * rect.width()));
}

void MainWindow::resizeEvent(QResizeEvent* event)
{
  this->updateDisplay();
}

void MainWindow::on_action_Quit_triggered()
{
  QApplication::quit();
}

void MainWindow::on_action_Load_triggered()
{
  this->stars.clear();
  this->ui->graphicsView->scene()->clear();
  this->time = 0;
  QString path = QFileDialog::getOpenFileName(this, "Open Input", QDir::currentPath());
  QFile file(path);
  if (file.open(QIODevice::ReadOnly))
  {
    QTextStream ts(&file);
    while (! ts.atEnd())
    {
      QString line = ts.readLine();

      this->parseStar(line);
    }
    file.close(); 
  }
  this->updateDisplay();
}

void MainWindow::updateStars()
{
  for (int i = 0; i < this->stars.size(); i++)
  {
    this->stars[i].pos.setX(this->stars[i].vel.x());
    this->stars[i].pos.setY(this->stars[i].vel.y());
  }
}

void MainWindow::parseStar(QString line)
{
  // Position X
  int start = 0;
  while (! line[start].isDigit() && line[start] != '-')
  {
    start += 1;
  }
  int end = start + 1;
  while (line[end].isDigit())
  {
    end += 1;
  }
  int startX = QStringRef(&line, start, end - start).toInt();

  // Position Y
  start = end + 1;
  while (! line[start].isDigit() && line[start] != '-')
  {
    start += 1;
  }
  end = start + 1;
  while (line[end].isDigit())
  {
    end += 1;
  }
  int startY = QStringRef(&line, start, end - start).toInt();
  
  // Velocity X
  start = end + 1;
  while (! line[start].isDigit() && line[start] != '-')
  {
    start += 1;
  }
  end = start + 1;
  while (line[end].isDigit())
  {
    end += 1;
  }
  int velX = QStringRef(&line, start, end - start).toInt();

  // Velocity Y
  start = end + 1;
  while (! line[start].isDigit() && line[start] != '-')
  {
    start += 1;
  }
  end = start + 1;
  while (line[end].isDigit())
  {
    end += 1;
  }
  int velY = QStringRef(&line, start, end - start).toInt();
  
  struct Star star;
  star.pos.setX(startX);
  star.pos.setY(startY);
  star.vel.setX(velX);
  star.vel.setY(velY);

  this->stars.push_back(star);
}
