#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QEvent>
#include <QGraphicsTextItem>
#include <QPoint>
#include <QVector>
#include <QResizeEvent>
#include <QPen>
#include <QBrush>

#include "star.hpp"

namespace Ui {
  class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

private:
  Ui::MainWindow *ui;
  bool eventFilter(QObject* obj, QEvent* event);
  void forward();
  void backward();
  void updateDisplay();
  void resizeEvent(QResizeEvent* event);
  void updateStars();
  void parseStar(QString line);
  
  int time;
  int pointSize;
  QVector<struct Star> stars;
  QPen pen;
  QBrush brush;

private slots:
  void on_backwardButton_clicked();
  void on_forwardButton_clicked();
  void on_action_Quit_triggered();
  void on_action_Load_triggered();
};

#endif // MAINWINDOW_H
