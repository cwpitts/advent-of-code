#include <vector>
#include <iostream>
#include <fstream>
#include <utility>
#include <string>
#include <map>
#include <deque>

#define FOREST std::vector<std::vector<char>>*
const char OPEN = '.';
const char LUMBERYARD = '#';
const char TREE = '|';

void printForest(const FOREST areaMap)
{
  if (areaMap->size() == 0)
  {
    return;
  }

  for (unsigned int i = 0; i < areaMap[0].size(); i++)
  {
    std::cout << '-';
  }
  std::cout << std::endl;

  for (std::vector<char> row : *areaMap)
  {
    for (char c : row)
    {
      std::cout << c;
    }
    std::cout << std::endl;
  }

  for (unsigned int i = 0; i < areaMap[0].size(); i++)
  {
    std::cout << '-';
  }
  std::cout << std::endl;
}

int sumAround(const FOREST areaMap, const int x, const int y, const char sumType)
{
  int sum = 0;
  
  for (int dy = -1; dy < 2; dy++)
  {
    for (int dx = -1; dx < 2; dx++)
    {
      // Check boundaries
      if (y + dy < 0 || y + dy >= areaMap->size())
      {
	sum += 0;
      }
      else if (x + dx < 0 || x + dx >= areaMap[0].size())
      {
	sum += 0;
      }
      else if (dx == 0 && dy == 0)
      {
	sum += 0;
      }
      else
      {
	sum += (int)((*areaMap)[y + dy][x + dx] == sumType);
      }
    }
  }

  return sum;
}

std::pair<FOREST, FOREST> iterate(FOREST first, FOREST second)
{
  std::pair<FOREST, FOREST> ret;

  for (int y = 0; y < first->size(); y++)
  {
    for (int x = 0; x < (*first)[y].size(); x++)
    {
      if ((*first)[y][x] == OPEN)
      {
	/*
	  An open acre will become filled with trees if three or more adjacent
	  acres contained trees. Otherwise, nothing happens.
	*/
	//std::cout << "Checking open at (" << y << "," << x << "): trees=" << sumAround(first, x, y, TREE) << std::endl;
	if (sumAround(first, x, y, TREE) >= 3)
	{
	  //std::cout << "Going to tree!" << std::endl;
	  (*second)[y][x] = TREE;
	}
	else
	{
	  (*second)[y][x] = OPEN;
	}
      }
      else if ((*first)[y][x] == LUMBERYARD)
      {
	/*
	  An acre containing a lumberyard will remain a lumberyard if it was
	  adjacent to at least one other lumberyard and at least one acre
	  containing trees. Otherwise, it becomes open.
	*/
	//std::cout << "Checking lumberyard at (" << y << "," << x << "): lumberyards=" << sumAround(first, x, y, LUMBERYARD) << ", trees=" << sumAround(first, x, y, TREE) << std::endl;
	if (sumAround(first, x, y, LUMBERYARD) == 0
	    || sumAround(first, x, y, TREE) == 0)
	{
	  //std::cout << "Going to open!" << std::endl;
	  (*second)[y][x] = OPEN;
	}
	else
	{
	  (*second)[y][x] = LUMBERYARD;
	}
      }
      else if ((*first)[y][x] == TREE)
      {
	/*
	  An acre filled with trees will become a lumberyard if three or more
	  adjacent acres were lumberyards. Otherwise, nothing happens.
	 */
	//std::cout << "Checking open at (" << y << "," << x << "): lumberyards=" << sumAround(first, x, y, LUMBERYARD) << std::endl;
	if (sumAround(first, x, y, LUMBERYARD) >= 3)
	{
	  //std::cout << "Going to lumberyard!" << std::endl;
	  (*second)[y][x] = LUMBERYARD;
	}
	else
	{
	  (*second)[y][x] = TREE;
	}
      }
    }
  }
  
  ret.first = second;
  ret.second = first;
  return ret;
}

void sumSquares(const FOREST areaMap, int& lumberyards, int& open, int& trees)
{
  lumberyards = 0;
  open = 0;
  trees = 0;
  
  for (std::vector<char> row : *areaMap)
  {
    for (char c : row)
    {
      lumberyards += (int)(c == LUMBERYARD);
      open += (int)(c == OPEN);
      trees += (int)(c == TREE);
    }
  }
}

std::string makeString(const FOREST areaMap)
{
  std::string ret = "";

  for (std::vector<char> row : *areaMap)
  {
    for (char c : row)
    {
      ret += c;
    }
  }
  
  return ret;
}

int main(int argc, char* argv[])
{
  if (argc == 1)
  {
    std::cerr << "No input file provided" << std::endl;
    return 1;
  }

  std::ifstream inputFile(argv[1]);
  if (! inputFile.is_open())
  {
    std::cerr << "Could not open input file" << std::endl;
    return 1;
  }

  FOREST first = new std::vector<std::vector<char>>;
  FOREST second = new std::vector<std::vector<char>>;

  std::string line;
  while (std::getline(inputFile, line))
  {
    std::vector<char> row;
    for (char c : line)
    {
      row.push_back(c);
    }
    first->push_back(row);
    second->push_back(row);
  }

  printForest(first);

  std::pair<FOREST, FOREST> tmp;
  for (int i = 0; i < 10; i++)
  {
    tmp = iterate(first, second);
    first = tmp.first;
    second = tmp.second;
  }

  printForest(first);

  int lumberyards = 0;
  int open = 0;
  int trees = 0;

  sumSquares(first, lumberyards, open, trees);
  
  std::cout << "Part 1: " << lumberyards * trees << std::endl;

  std::map<std::string, int> forestToID;
  std::map<int, int> resourceValue;
  //std::vector<int> cycle;
  std::deque<int> cycle;
    
  // The data repeats, so track what we've seen
  // 1000000000 - 10 full loop
  tmp = iterate(first, second);
  first = tmp.first;
  second = tmp.second;

  int index = 11;
  while (! forestToID.count(makeString(first)))
  {
    // Track what we've seen with the string
    forestToID[makeString(first)] = index;

    // Also track the ID
    sumSquares(first, lumberyards, open, trees);
    resourceValue[index] = lumberyards * trees;

    // Keep the ID cycle
    cycle.push_back(index);

    tmp = iterate(first, second);
    first = tmp.first;
    second = tmp.second;
    index += 1;
  }

  while (cycle[0] != forestToID[makeString(first)])
  {
    cycle.erase(cycle.begin());
  }

  /*
    To get a cycle value, rebase to beginning of cycle, then mod by size to
    get index in cycle
   */
  std::cout << "Part 2: " << resourceValue[cycle[(1000000000 - cycle[0]) % cycle.size()]] << std::endl;

  delete first;
  delete second;
  
  return 0;
}
