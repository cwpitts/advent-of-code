#include <vector>
#include <iostream>
#include <string>
#include <fstream>

#include "cart.hpp"

int uncrashedCarts(const std::vector<Cart>& carts)
{
  int uncrashed = 0;
  for (Cart c : carts)
  {
    uncrashed += c.hasCrashed();
  }
  return uncrashed;
}

const void printTrack(const std::vector<std::string>& track, std::string label)
{
  std::cout << label << std::endl;
  for (std::string row : track)
  {
    std::cout << row << std::endl;
  }
}

const void printCarts(const std::vector<Cart>& carts)
{
  std::cout << "Carts:" << std::endl;
  for (Cart c : carts)
  {
    std::cout << "----------" << std::endl;
    std::cout << c.toString() << std::endl;
    std::cout << "----------" << std::endl;
  }
}

void initPuzzle(std::ifstream& inputFile, std::vector<std::string>& track,
		std::vector<std::string>& trackMap, std::vector<Cart>& carts)
{
  int y = 0;
  std::string row;
  while (std::getline(inputFile, row))
  {
    track.push_back(row);
    for (int x = 0; x < row.size(); x++)
    {
      if (row[x] == '>')
      {
	carts.push_back(Cart(x, y, row[x], 1, 0));
	row[x] = '-';
      }
      else if (row[x] == '<')
      {
	carts.push_back(Cart(x, y, row[x], -1, 0));
	row[x] = '-';
      }
      else if (row[x] == 'v')
      {
	carts.push_back(Cart(x, y, row[x], 0, 1));
	row[x] = '|';
      }
      else if (row[x] == '^')
      {
	carts.push_back(Cart(x, y, row[x], 0, -1));
	row[x] = '|';
      }
    }
    trackMap.push_back(row);
    y += 1;
  }
}

bool cleanTracks(std::vector<Cart>& carts,
		 std::vector<std::string>& trackMap,
		 std::vector<std::string>& track,
		 int& collisionX, int& collisionY)
{
  // std::cout << "Cleaning tracks" << std::endl;
  bool ret = false;
  // Look for carts with the same coordinates
  for (int f = 0; f < carts.size(); f++)
  {
    for (int s = 0; s < carts.size(); s++)
    {
      // std::cout << carts[f].getID() << ' ' << carts[s].getID() << std::endl;
      if (carts[f].getID() != carts[s].getID()
	  && carts[f].getX() == carts[s].getX()
	  && carts[f].getY() == carts[s].getY())
      {
	// std::cout << "Crashed!" << std::endl;
	carts[f].crash();
	carts[s].crash();

	// Set collision coordinates
	collisionX = carts[f].getX();
	collisionY = carts[f].getY();
	
	// Reset tracks
	track[carts[f].getY()][carts[f].getX()] = trackMap[carts[f].getY()][carts[f].getX()];
	
	ret = true;
      }
    }
  }

  // Clear tracks
  for (int i = carts.size() - 1; i >= 0; i--)
  {
    // std::cout << "Cart " << i << " has crashed: " << carts[i].hasCrashed() << std::endl;
    if (carts[i].hasCrashed())
    {
      carts.erase(carts.begin() + i);
    }
  }

  return ret;
}

void updateCarts(std::vector<Cart>& carts,
		 std::vector<std::string>& trackMap,
		 std::vector<std::string>& track)
{
  // std::cout << "Updating " << carts.size() << " carts" << std::endl;
  for (int i = 0; i < carts.size(); i++)
  {
    // std::cout << "Moving cart " << i << std::endl;
    carts[i].move(trackMap, track);
  }
}

int main(int argc, char* argv[])
{
  if (argc == 1)
  {
    std::cerr << "No input file provided!" << std::endl;
    exit(1);
  }

  std::ifstream inputFile(argv[1]);
  if (! inputFile.is_open())
  {
    std::cerr << "Could not open input file " << argv[1] << std::endl;
    exit(1);
  }

  std::vector<std::string> track;
  std::vector<std::string> trackMap;
  std::vector<Cart> carts;
  initPuzzle(inputFile, track, trackMap, carts);
  inputFile.close();

  int collisionX = -1;
  int collisionY = -1;

  // Part 1
  while (! cleanTracks(carts, trackMap, track, collisionX, collisionY))
  {
    updateCarts(carts, trackMap, track);
    // printTrack(track, "Tracks");
  }

  std::cout << "Part 1: (" << collisionX << "," << collisionY << ")" << std::endl;

  std::cout << "Carts left: " << carts.size() << std::endl;
  std::cout << "Iterating until one cart is left" << std::endl;
  cleanTracks(carts, trackMap, track, collisionX, collisionY);
  // printTrack(track, "Tracks");
  while (carts.size() > 1)
  {
    updateCarts(carts, trackMap, track);
    cleanTracks(carts, trackMap, track, collisionX, collisionY);
  }
  std::cout << "One cart left:" << std::endl;
  std::cout << carts[0].toString() << std::endl;
  std::cout << "One final tick" << std::endl;
  updateCarts(carts, trackMap, track);
  cleanTracks(carts, trackMap, track, collisionX, collisionY);

  std::cout << "Part 2: (" << carts[0].getX() << "," << carts[0].getY() << ")" << std::endl;
  std::cout << carts[0].toString() << std::endl;
  //printTrack(track, "Final Track");
  
  return 0;
}
