#ifndef CART_HPP
#define CART_HPP

#include <string>
#include <vector>

class Cart
{
public:
  Cart(int x, int y, char symbol, int dx, int dy);
  ~Cart();
  int getX();
  int getY();
  int getDx();
  int getDy();
  void move(const std::vector<std::string>& trackMap, std::vector<std::string>& track);
  std::string toString();
  bool hasCrashed();
  void crash();
  int getID();

private:
  int x;
  int y;
  char symbol;
  int dx;
  int dy;
  int lastTurn;
  bool crashed;
  int id;
  int prevX;
  int prevY;

  void turnRight();
  void turnLeft();
};
#endif // CART_HPP
