#include <iostream>

#include "cart.hpp"

Cart::Cart(int x, int y, char symbol, int dx, int dy)
{
  static int idCounter = 0;
  this->x = x;
  this->y = y;
  this->symbol = symbol;
  this->dx = dx;
  this->dy = dy;
  this->lastTurn = 0;
  this->crashed = false;
  
  this->id = idCounter;
  idCounter += 1;
}

Cart::~Cart() {}

int Cart::getX()
{
  return this->x;
}

int Cart::getY()
{
  return this->y;
}

int Cart::getDx()
{
  return this->dx;
}

int Cart::getDy()
{
  return this->dy;
}

void Cart::move(const std::vector<std::string>& trackMap, std::vector<std::string>& track)
{
  // Look at the map where we are and where we're going
  // std::cout << "At (" << this->x << ',' << this->y << "): "<< trackMap[this->y][this->x] << std::endl;
  // std::cout << "Next (" << this->x + this->dx << ',' << this->y + this->dy << "): "<< trackMap[this->y + this->dy][this->x + this->dx] << std::endl;

  // Update last position
  this->prevX = this->x;
  this->prevY = this->y;
  
  // Move off the current track piece
  track[this->y][this->x] = trackMap[this->y][this->x];

  // Move onto the next track piece
  this->y += this->dy;
  this->x += this->dx;

  // Handle new track piece
  if (trackMap[this->y][this->x] == '\\')
  {
    if (this->dy == -1 || this->dy == 1)
    {
      // If the cart is moving up into the turn
      // If the cart is moving down into the turn
      this->turnLeft();
    }
    else if (this->dx == -1 || this->dx == 1)
    {
      // If the cart is moving left into the turn
      // If the cart if moving right into the turn
      this->turnRight();
    }
  }
  else if (trackMap[this->y][this->x] == '/')
  {
    if (this->dy == -1 || this->dy == 1)
    {
      // If the cart is moving up into the turn
      // If the cart is moving down into the turn
      this->turnRight();
    }
    else if (this->dx == -1 || this-> dx == 1)
    {
      // If the cart is moving left into the turn
      // If the cart if moving right into the turn
      this->turnLeft();
    }
  }
  else if (trackMap[this->y][this->x] == '+')
  {
    // Intersections
    /*
      Ordering of turns:
      0: left
      1: straight
      2: right
    */
    if (this->lastTurn == 0)
    {
      // Turn left
      this->turnLeft();
    }
    else if (this->lastTurn == 2)
    {
      // Turn right
      this->turnRight();
    }
    else if (this->lastTurn == 1)
    {
      // Go straight (i.e. go straight)
    }
    this->lastTurn = (this->lastTurn + 1) % 3;
  }

  track[this->y][this->x] = this->symbol;
}

std::string Cart::toString()
{
  std::string ret = "Cart " + std::to_string(this->id)
    + " (" + std::to_string(this->x) + ","
    + std::to_string(this->y) + ")\n"
    + "symbol: " + this->symbol + "\n"
    + "velocity = (" + std::to_string(this->dx) + ","
    + std::to_string(this->dy) + ")\n"
    + "last turn: " + std::to_string(this->lastTurn) + "\n"
    + "last position: ("
    + std::to_string(this->prevX) + "," +  std::to_string(this->prevY) + ")";
  return ret;
}

void Cart::turnLeft()
{
  if (this->symbol == '>')
  {
    this->symbol = '^';
  }
  else if (this->symbol == '^')
  {
    this->symbol = '<';
  }
  else if (this->symbol == '<')
  {
    this->symbol = 'v';
  }
  else if (this->symbol == 'v')
  {
    this->symbol = '>';
  }

  if (this->dx == 1)
  {
    this->dx = 0;
    this->dy = -1;
  }
  else if (this->dx == -1)
  {
    this->dx = 0;
    this->dy = 1;
  }
  else if (this->dy == 1)
  {
    this->dy = 0;
    this->dx = 1;
  }
  else if (this->dy == -1)
  {
    this->dy = 0;
    this->dx = -1;
  } 
}

void Cart::turnRight()
{
    if (this->symbol == '>')
  {
    this->symbol = 'v';
  }
  else if (this->symbol == 'v')
  {
    this->symbol = '<';
  }
  else if (this->symbol == '<')
  {
    this->symbol = '^';
  }
  else if (this->symbol == '^')
  {
    this->symbol = '>';
  }

  if (this->dx == 1)
  {
    this->dx = 0;
    this->dy = 1;
  }
  else if (this->dx == -1)
  {
    this->dx = 0;
    this->dy = -1;
  }
  else if (this->dy == 1)
  {
    this->dy = 0;
    this->dx = -1;
  }
  else if (this->dy == -1)
  {
    this->dy = 0;
    this->dx = 1;
  }
}

bool Cart::hasCrashed()
{
  return this->crashed;
}

void Cart::crash()
{
  this->crashed = true;
}

int Cart::getID()
{
  return this->id;
}
