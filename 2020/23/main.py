#!/usr/bin/env python3
""" Advent of Code Day 23
"""
import random
import timeit
from argparse import ArgumentParser
from dataclasses import dataclass
from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import numba
import numpy as np
from tqdm import tqdm

plt.style.use("fivethirtyeight")


@dataclass
class Node:
    val: int
    next: object

    def __str__(self):
        return f"Node {self.val}, next={self.next.val}"

    def __repr__(self):
        return self.__str__()


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def make_problem(p_size):
    problem = np.arange(1, p_size + 1, dtype=np.int64)
    random.shuffle(problem[:9])

    return problem


def performance():
    # Array size performance
    times_roll_size = []
    times_ll_size = []
    for p_size in tqdm(np.arange(100, 10100, 100)):
        problem = make_problem(p_size)
        times_roll_size.append(timeit.timeit(lambda: sim_roll(problem), number=100))
        times_ll_size.append(timeit.timeit(lambda: sim(problem), number=100))

    fig, ax = plt.subplots()
    ax.plot(times_roll_size, label="np.roll")
    ax.plot(times_ll_size, label="Linked list")
    ax.set_xlabel("Problem size (array length)")
    ax.set_ylabel("t (sec)")
    ax.legend()
    ax.set_title("Performance vs. array length")
    fig.savefig("aoc-23-perf-size.png", dpi=300, bbox_inches="tight")
    plt.close("all")

    # Number of moves performance
    times_roll_moves = []
    times_ll_moves = []
    problem = make_problem(1000)
    for p_size in tqdm(np.arange(100, 1100, 100)):
        times_roll_moves.append(
            timeit.timeit(lambda: sim_roll(problem, n_rounds=p_size), number=100)
        )
        times_ll_moves.append(
            timeit.timeit(lambda: sim(problem, n_rounds=p_size), number=100)
        )

    fig, ax = plt.subplots()
    ax.plot(times_roll_moves, label="np.roll")
    ax.plot(times_ll_moves, label="Linked list")
    ax.set_xlabel("Problem size (# of moves)")
    ax.set_ylabel("t (sec)")
    ax.legend()
    ax.set_title("Performance vs. number of moves")
    fig.savefig("aoc-23-perf-moves.png", dpi=300, bbox_inches="tight")
    plt.close("all")


def format_cups(cups, start_cup):
    fmt_arr = [start_cup.val]
    c = start_cup.next
    while c.val != start_cup.val:
        fmt_arr.append(c.val)
        c = c.next
    return " ".join([f"({c})" if c == start_cup.val else str(c) for c in fmt_arr])


def sim_roll(arr, n_rounds=100):
    arr = arr.copy()

    cup = arr[0]
    for move in np.arange(n_rounds):
        arr = np.roll(arr, -np.argwhere(arr == cup)[0][0])
        # Roll until cup is at the beginning
        taken = arr[1:4].copy()
        arr = arr[~np.isin(arr, taken)]

        dest = cup - 1
        while np.any(taken == dest) or dest < 1:
            dest -= 1
            if dest < min(arr.min(), taken.min()):
                dest = max(arr.max(), taken.max())

        # Roll until destination cup is at the front and insert
        arr = np.roll(arr, -np.argwhere(arr == dest)[0][0])
        arr = np.insert(arr, 1, taken)

        # Choose new cup
        cup_idx = np.argwhere(arr == cup)[0][0]
        new_cup_idx = (cup_idx + 1) % arr.shape[0]
        cup = arr[new_cup_idx]

    return arr


def sim(arr, n_rounds=100):
    cups = {}
    prev = None
    for cup in arr[::-1]:
        cups[cup] = Node(cup, prev)
        prev = cups[cup]
    cups[arr[-1]].next = cups[arr[0]]

    current_cup = cups[arr[0]]
    for move in np.arange(n_rounds):
        # Select cups to take
        take = [current_cup.next, current_cup.next.next, current_cup.next.next.next]

        # Calculate destination
        dest = cups[current_cup.val - 1 if current_cup.val > 1 else arr.max()]
        while dest in take:
            dest = cups[dest.val - 1 if dest.val > 1 else arr.max()]

        # Adjust indices
        current_cup.next = take[-1].next
        take[-1].next = dest.next
        dest.next = take[0]
        current_cup = current_cup.next

    return cups, current_cup


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        arr = np.array([int(c) for c in in_file.read().strip()])

    # Part 1
    p1_start = datetime.now()
    final_arr_p1, _ = sim(arr)
    p1_ans = []
    p1_nxt = final_arr_p1[1].next
    while p1_nxt != final_arr_p1[1]:
        p1_ans.append(p1_nxt.val)
        p1_nxt = p1_nxt.next
    p1 = "".join(map(str, p1_ans))
    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()
    arr2 = np.arange(1, 1_000_001, dtype=np.int64)
    arr2[:9] = arr
    final_arr_p2, _ = sim(arr2, n_rounds=10_000_000)
    p2 = final_arr_p2[1].next.val * final_arr_p2[1].next.next.val
    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        print("Running performance metrics")
        performance()


if __name__ == "__main__":
    args = parse_args()
    main(args)
