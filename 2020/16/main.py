#!/usr/bin/env python3
""" Advent of Code Day 16
"""
from argparse import ArgumentParser
from datetime import datetime
from pathlib import Path

import numpy as np


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")

    return argp.parse_args(arglist)


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        rules = {}
        line = in_file.readline().strip()
        while line != "your ticket:":
            if line != "":
                rule, values = line.split(": ")
                rules[rule] = [
                    np.array(list(map(int, x.split("-"))))
                    for x in values.split(" ")
                    if x != "or"
                ]
            line = in_file.readline().strip()

        ticket = np.array(in_file.readline().strip().split(","), dtype=np.int64)

        while line != "nearby tickets:":
            line = in_file.readline().strip()
        line = in_file.readline().strip()

        max_value = 0
        nearby_tickets = []
        while line != "":
            arr = list(map(int, line.split(",")))
            max_value = max(max_value, max(arr))
            nearby_tickets.append(arr)
            line = in_file.readline().strip()
        nearby_tickets = np.array(nearby_tickets)

    # Part 1
    p1_start = datetime.now()
    valid_values = np.zeros(max_value + 1, dtype=np.int64)
    for value_set in rules.values():
        for low, high in value_set:
            valid_values[low : high + 1] = 1
    p1 = 0
    for idx, row in enumerate(nearby_tickets):
        p1 += row[valid_values[row] == 0].sum()
    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()
    keep_idx = []
    for idx, row in enumerate(nearby_tickets):
        if row[valid_values[row] == 0].shape[0] == 0:
            keep_idx.append(idx)
    nearby_tickets = nearby_tickets[keep_idx, :]
    columns = {col: set(rules.keys()) for col in np.arange(nearby_tickets.shape[1])}
    for col, values in enumerate(nearby_tickets.T):
        for rule, bounds in rules.items():
            if not np.all(
                np.logical_or(
                    *list(
                        np.logical_and(values >= low, values <= high)
                        for low, high in bounds
                    )
                )
            ):
                columns[col].remove(rule)
    while any(len(rules) > 1 for rules in columns.values()):
        for col, rules in columns.items():
            if len(rules) == 1:
                for col_del, rules_del in columns.items():
                    if col_del != col:
                        rules_del -= rules
    departure_columns = [
        col for col, value in columns.items() if list(value)[0].startswith("departure")
    ]
    p2 = np.prod(ticket[departure_columns])
    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")


if __name__ == "__main__":
    args = parse_args()
    main(args)
