#!/usr/bin/env python3
""" Advent of Code Day 10
"""
import time
from argparse import ArgumentParser
from collections import Counter
from copy import deepcopy
from dataclasses import dataclass
from datetime import datetime
from pathlib import Path

import numpy as np
from rich.console import Console
from rich.table import Table


@dataclass
class GridState:
    grid: dict


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "--visualize", help="visualize solution", action="store_true", default=False
    )

    return argp.parse_args(arglist)


def show_visualization(states):
    console = Console()
    for idx, state in enumerate(states):
        console.clear()
        table = Table(show_header=True, style="bold")
        table.add_column("Adapter", width=8, justify="center")
        table.add_column("Arrangements", width=14, justify="center")
        entries = sorted(state.grid.keys())
        implicit_fmt = "[bold white on blue]{}[/bold white on blue]"
        table.add_row(
            implicit_fmt.format(entries[0]), implicit_fmt.format(state.grid[entries[0]])
        )
        for entry in entries[1:-1]:
            value = state.grid[entry]
            entry_str = f"[green]{entry}[/green]"
            value_str = f"[magenta]{value}[/magenta]" if value > 0 else str(value)
            table.add_row(entry_str, value_str)
        table.add_row(
            implicit_fmt.format(entries[-1]),
            implicit_fmt.format(state.grid[entries[-1]]),
        )
        console.print(table)
        time.sleep(1)

    console.print(f"[yellow]{states[-1].grid[entries[-2]]}[/yellow]")


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        nums = sorted(list(map(int, in_file)))

    # Part 1
    nums.insert(0, 0)
    nums.append(nums[-1] + 3)
    p1_start = datetime.now()
    diffs = []
    for i in range(len(nums) - 1):
        diffs.append(nums[i + 1] - nums[i])
    c = Counter(diffs)
    p1 = c[1] * c[3]
    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()
    states = []
    grid = {n: 0 for n in nums}
    grid[0] = 1
    if args.visualize:
        states.append(GridState(deepcopy(grid)))
    for num in nums[1:-1]:
        grid[num] += grid[num - 1] if num - 1 in grid else 0
        grid[num] += grid[num - 2] if num - 2 in grid else 0
        grid[num] += grid[num - 3] if num - 3 in grid else 0
        if args.visualize:
            states.append(GridState(deepcopy(grid)))
    p2 = grid[nums[-2]]
    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.visualize:
        show_visualization(states)


if __name__ == "__main__":
    args = parse_args()
    main(args)
