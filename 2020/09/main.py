#!/usr/bin/env python3
""" Advent of Code Day 09
"""
from argparse import ArgumentParser
from datetime import datetime
from pathlib import Path

import numpy as np


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")

    return argp.parse_args(arglist)


def sum_in_window(nums, s):
    for num1 in nums:
        for num2 in nums:
            if num1 + num2 == s:
                return True
    return False


def print_prog(prog, start, end):
    for i, n in enumerate(prog):
        if i == start:
            print("[", end="")
        print(n, end="")
        if i == end:
            print("]", end="")
        if i < len(prog) - 1:
            print(",", end="")
    print(f" sum={sum(prog[start:end])}")

def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        prog = list(map(int, in_file))

    # Part 1
    p1_start = datetime.now()
    preamble_length = 25
    for i in range(preamble_length, len(prog)):
        window = prog[i - preamble_length:i]
        if not sum_in_window(window, prog[i]):
            p1 = prog[i]
            break
    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()
    # Iterate through list and keep rolling sum
    for i in range(len(prog)):
        s = 0
        j = i
        while s < p1:
            s += prog[j]
            j += 1
        if s == p1:
            start = i
            end = j
            break
    p2 = min(prog[start:end]) + max(prog[start:end])
    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")


if __name__ == "__main__":
    args = parse_args()
    main(args)
