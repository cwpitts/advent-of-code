#!/usr/bin/env python3
""" Advent of Code Day 24
"""
from argparse import ArgumentParser
from collections import defaultdict
from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def get_neighbors(cq, cr):
    ret = set()
    for dq, dr in [(0, -1), (1, -1), (1, 0), (0, 1), (-1, 1), (-1, 0)]:
        ret.add((cq + dq, cr + dr))
    return ret


def parse_coords(line):
    q = 0
    r = 0

    idx = 0
    while idx < len(line):
        if line[idx] == "e":
            q += 1
            idx += 1
        elif line[idx] == "w":
            q -= 1
            idx += 1
        elif line[idx:idx + 2] == "se":
            r += 1
            idx += 2
        elif line[idx:idx + 2] == "sw":
            q -= 1
            r += 1
            idx += 2
        elif line[idx:idx + 2] == "ne":
            q += 1
            r -= 1
            idx += 2
        elif line[idx:idx + 2] == "nw":
            r -= 1
            idx += 2

    return q, r


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        coords = [parse_coords(line.strip()) for line in in_file]

    # Part 1
    p1_start = datetime.now()
    tiles = defaultdict(lambda: "white")
    for c in coords:
        tiles[c] = "black" if tiles[c] == "white" else "white"
    p1 = sum([1 if v == "black" else 0 for v in tiles.values()])
    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()
    black_tiles = {tile for tile in tiles if tiles[tile] == "black"}
    for day in range(100):
        new_black_tiles = set()
        interesting_tiles = set()
        # For each tile, get neighbors
        for tile in black_tiles:
            interesting_tiles |= get_neighbors(*tile)
            interesting_tiles.add(tile)
        for tile in interesting_tiles:
            neighbors = get_neighbors(*tile)
            active_neighbors = sum(n in black_tiles for n in neighbors)
            if tile in black_tiles:
                # Black only stays black if nonzero and < 2 neighbors are black
                if active_neighbors != 0 and active_neighbors <= 2:
                    new_black_tiles.add(tile)
            else:
                # White goes to black if exactly two neighbors are black
                if active_neighbors == 2:
                    new_black_tiles.add(tile)
        black_tiles = new_black_tiles
    p2 = len(black_tiles)
    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args = parse_args()
    main(args)
