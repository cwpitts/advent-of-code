#!/usr/bin/env python3
""" Advent of Code Day 11
"""
import time
from argparse import ArgumentParser
from dataclasses import dataclass
from datetime import datetime
from pathlib import Path

import numpy as np
from rich.console import Console
from rich.table import Table


@dataclass
class GameState:
    grid: np.ndarray
    iterations: int


grid_colors = {
    "#": "[green]{}[/green]",
    ".": "[white]{}[/white]",
    "L": "[blue]{}[/blue]",
}


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "--visualize", help="visualize solution", action="store_true", default=False
    )
    argp.add_argument(
        "--vis-part", help="part to visualize", type=int, choices=[1, 2], default=1
    )

    return argp.parse_args(arglist)


def show_visualization(states):
    console = Console()
    for state in states:
        console.clear()
        for row in state.grid:
            row_fmt = "".join([grid_colors[x].format(x) for x in row])
            console.print(row_fmt)
        console.print(
            f"[bold white]Iterations:[/bold white] [magenta]{state.iterations}[/magenta]"
        )
        time.sleep(1)
    console.print(
        f"[bold white]Occupied seats:[/bold white] [yellow]{np.sum(states[-1].grid == '#')}[/yellow]"
    )


def make_grid(grid, iterations):
    ret = grid.copy()

    for index, state in np.ndenumerate(grid):
        row, col = index
        adjacent = grid[
            max(row - 1, 0) : min(row + 2, grid.shape[0]),
            max(col - 1, 0) : min(col + 2, grid.shape[1]),
        ]
        occupied = np.sum(adjacent == "#") - (grid[index] == "#")
        if state == "L" and occupied == 0:
            # Empty seats become occupied if there are no adjacent occupied seats
            ret[index] = "#"
        elif state == "#" and occupied >= 4:
            ret[index] = "L"

    return ret, iterations + 1


def in_bounds(row, col, shape):
    if row < 0 or col < 0:
        return False
    if row >= shape[0] or col >= shape[1]:
        return False
    return True


def count_los(row, col, dr, dc, grid):
    row_iter = row + dr
    col_iter = col + dc

    while in_bounds(row_iter, col_iter, grid.shape):
        if grid[row_iter, col_iter] == "#":
            return 1
        if grid[row_iter, col_iter] == "L":
            return 0
        row_iter += dr
        col_iter += dc

    return 0


def make_los_grid(grid, iterations):
    ret = grid.copy()
    occ_grid = np.zeros(grid.shape)

    for index, state in np.ndenumerate(grid):
        row, col = index
        occupied = 0
        # Up
        occupied += count_los(row, col, -1, 0, grid)
        # Down
        occupied += count_los(row, col, 1, 0, grid)
        # Left
        occupied += count_los(row, col, 0, -1, grid)
        # Right
        occupied += count_los(row, col, 0, 1, grid)
        # UpRight
        occupied += count_los(row, col, -1, 1, grid)
        # DownRight
        occupied += count_los(row, col, 1, 1, grid)
        # UpLeft
        occupied += count_los(row, col, -1, -1, grid)
        # DownLeft
        occupied += count_los(row, col, 1, -1, grid)
        if grid[row, col] == "L" and occupied == 0:
            ret[row, col] = "#"
        elif grid[row, col] == "#" and occupied >= 5:
            ret[row, col] = "L"
        if grid[row, col] == "L" or grid[row, col] == "#":
            occ_grid[index] = occupied

    return ret, iterations + 1, occ_grid


def main(args):
    with open(args.input, "r") as in_file:
        grid = []
        for line in in_file:
            row = []
            for c in line.strip():
                row.append(c)
            grid.append(row)
    grid = np.array(grid)
    grid_orig = grid.copy()

    # Part 1
    p1_start = datetime.now()
    states_p1 = []
    if args.visualize:
        states_p1.append(GameState(grid.copy(), 0))
    new_grid, iterations = make_grid(grid, 0)
    if args.visualize:
        states_p1.append(GameState(new_grid.copy(), iterations))
    while np.any(new_grid != grid):
        grid = new_grid.copy()
        new_grid, iterations = make_grid(grid, iterations)
        if args.visualize:
            states_p1.append(GameState(new_grid.copy(), iterations))
    p1 = np.sum(new_grid == "#")
    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()
    states_p2 = []
    grid = grid_orig.copy()
    if args.visualize:
        states_p2.append(GameState(grid.copy(), 0))
    new_grid, iterations, occ = make_los_grid(grid, 0)
    if args.visualize:
        states_p2.append(GameState(new_grid.copy(), iterations))
    while np.any(new_grid != grid):
        grid = new_grid.copy()
        new_grid, iterations, occ = make_los_grid(grid, iterations)
        if args.visualize:
            states_p2.append(GameState(new_grid.copy(), iterations))
    p2 = np.sum(new_grid == "#")
    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.visualize:
        if args.vis_part == 1:
            show_visualization(states_p1)
        else:
            show_visualization(states_p2)


if __name__ == "__main__":
    args = parse_args()
    main(args)
