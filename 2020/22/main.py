#!/usr/bin/env python3
""" Advent of Code Day 22
"""
from argparse import ArgumentParser
from collections import deque
from copy import deepcopy
from datetime import datetime
from itertools import islice
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def play(deck1, deck2):
    while deck1 and deck2:
        card1 = deck1.popleft()
        card2 = deck2.popleft()

        if card1 > card2:
            deck1.extend([card1, card2])
        else:
            deck2.extend([card2, card1])
    winning_deck = deck1 if deck1 else deck2
    return winning_deck


def play_recursive(deck1, deck2):
    history = set()
    history.add((tuple(deck1), tuple(deck2)))

    while deck1 and deck2:
        card1 = deck1.popleft()
        card2 = deck2.popleft()

        if len(deck1) >= card1 and len(deck2) >= card2:
            winner, _ = play_recursive(
                deque(list(deck1)[:card1]), deque(list(deck2)[:card2])
            )
        else:
            winner = 1 if card1 > card2 else 2

        if winner == 1:
            deck1.extend([card1, card2])
        else:
            deck2.extend([card2, card1])

        game_state = (tuple(deck1), tuple(deck2))
        if game_state in history:
            return 1, deck1
        history.add(game_state)

    winner, winning_deck = (1, deck1) if deck1 else (2, deck2)
    return winner, winning_deck


def score_game(winning_deck):
    multipliers = np.arange(len(winning_deck))[::-1] + 1
    return multipliers.dot(np.array(winning_deck))


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        deck1, deck2 = tuple(
            map(lambda x: deque(map(int, x.split()[2:])), in_file.read().split("\n\n"))
        )

    # Part 1
    p1_start = datetime.now()
    winning_deck_1 = play(deepcopy(deck1), deepcopy(deck2))
    p1 = score_game(winning_deck_1)
    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()
    winner, winning_deck_2 = play_recursive(deck1, deck2)
    p2 = score_game(winning_deck_2)
    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args = parse_args()
    main(args)
