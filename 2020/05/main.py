#!/usr/bin/env python3
""" Advent of Code Day 05
"""
import math
import time
from argparse import ArgumentParser
from dataclasses import dataclass
from datetime import datetime
from pathlib import Path
from timeit import timeit

import matplotlib.pyplot as plt
import numpy as np
import rich
from rich.console import Console
from rich.table import Table
from tqdm import tqdm

plt.style.use("fivethirtyeight")


@dataclass
class NonPartitionState:
    low: int
    high: int
    index: int
    cpass: str


@dataclass
class PartitionState:
    arr: list
    index: int
    cpass: str


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "--visualize", help="generate visualization", action="store_true", default=False
    )
    argp.add_argument(
        "--vis-strategy",
        help="select strategy to visualize",
        default="nonpartition",
        choices=["nonpartition", "partition"],
    )
    argp.add_argument(
        "--performance",
        help="run performance analysis",
        action="store_true",
        default=False,
    )
    return argp.parse_args(arglist)


def performance(max_size):
    partition_times = []
    nonpartition_times = []

    for size in tqdm(np.arange(1, max_size + 1, 1)):
        row_problem = "".join(
            [np.random.choice(["F", "B"]) for _ in range(int(0.7 * size))]
        )
        col_problem = "".join(
            [np.random.choice(["R", "L"]) for _ in range(int(0.3 * size))]
        )

        partition_times.append(
            timeit(
                lambda: calculate_id_partition(row_problem, col_problem, False),
                number=1000,
            )
        )
        nonpartition_times.append(
            timeit(lambda: calculate_id(row_problem, col_problem, False), number=1000)
        )

    fig, ax = plt.subplots()
    ax.plot(partition_times, label="Partition")
    ax.plot(nonpartition_times, label="No partition")
    ax.set_title("Execution time in seconds")
    ax.set_xlabel("Puzzle size")
    ax.set_ylabel("t (sec)")
    ax.legend()
    fig.savefig(f"aoc-05-perf.png", bbox_inches="tight", dpi=300)


def calculate_id_partition(p_row, p_col, visualize):
    row_arr = list(range(2 ** len(p_row)))
    state = [[], []]

    for idx, c in enumerate(p_row):
        if visualize:
            state[0].append(PartitionState(row_arr, idx, p_row))
        if c == "F":
            row_arr = row_arr[: math.ceil((row_arr[0] - row_arr[-1]) / 2) - 1]
        elif c == "B":
            row_arr = row_arr[math.ceil((row_arr[0] - row_arr[-1]) / 2) - 1 :]

    if visualize:
        state[0].append(PartitionState(row_arr, None, p_row))

    col_arr = list(range(2 ** len(p_col)))
    for idx, c in enumerate(p_col):
        if visualize:
            state[1].append(PartitionState(col_arr, idx, p_col))
        if c == "R":
            col_arr = col_arr[math.ceil((col_arr[0] - col_arr[-1]) / 2) - 1 :]
        elif c == "L":
            col_arr = col_arr[: math.ceil((col_arr[0] - col_arr[-1]) / 2) - 1]

    if visualize:
        state[1].append(PartitionState(col_arr, None, p_col))

    return row_arr[0] * 8 + col_arr[0], state


def format_pass(idx, cpass):
    pass_fmt = "".join(
        [
            f"[magenta]{val}[/magenta]" if i == idx else val
            for i, val in enumerate(cpass)
        ]
    )
    return pass_fmt


def show_visualization(states, strategy_state):
    console = Console()
    for state in states[0]:
        console.clear()
        row_table = Table(show_header=True, header_style="bold")
        if strategy_state is NonPartitionState:
            row_table.add_column("r_low", width=7, justify="center")
            row_table.add_column("r_high", width=7, justify="center")
            row_table.add_column("pass", width=10, justify="center")
            row_table.add_row(
                f"[blue]{state.low}[/blue]",
                f"[green]{state.high}[/green]",
                format_pass(state.index, state.cpass),
            )
        else:
            row_table.add_column("pass", width=10, justify="center")
            row_table.add_column("row array", width=50, justify="center")
            row_table.add_row(
                format_pass(state.index, state.cpass),
                f"[{','.join(map(str, state.arr))}]",
            )
        console.print(row_table)
        time.sleep(1)

    for state in states[1]:
        console.clear()
        col_table = Table(show_header=True, header_style="bold")
        if strategy_state is NonPartitionState:
            col_table.add_column("c_low", width=7, justify="center")
            col_table.add_column("c_high", width=7, justify="center")
            col_table.add_column("pass", width=10, justify="center")
            col_table.add_row(
                f"[blue]{state.low}[/blue]",
                f"[green]{state.high}[/green]",
                format_pass(state.index, state.cpass),
            )
        else:
            col_table.add_column("pass", width=10, justify="center")
            col_table.add_column("column array", width=50, justify="center")
            col_table.add_row(
                format_pass(state.index, state.cpass),
                f"[{','.join(map(str, state.arr))}]",
            )
        console.print(row_table)
        console.print(col_table)
        time.sleep(1)

    if strategy_state == NonPartitionState:
        r_high = states[0][-1].high
        c_high = states[1][-1].high
    else:
        r_high = states[0][-1].arr[0]
        c_high = states[1][-1].arr[0]
    console.print(
        f"[red]{r_high}[/red] * [bold white]8[/bold white] + [yellow]{c_high}[/yellow] = [bold white]{r_high * 8 + c_high}[/bold white]"
    )


def calculate_id(p_row, p_col, visualize):
    r_low = 0
    r_high = 2 ** len(p_row) - 1
    state = [[], []]
    for idx, c in enumerate(p_row):
        if visualize:
            state[0].append(NonPartitionState(r_low, r_high, idx, p_row))
        if c == "F":
            r_high -= math.ceil((r_high - r_low) / 2)
        elif c == "B":
            r_low += math.ceil((r_high - r_low) / 2)

    if visualize:
        state[0].append(NonPartitionState(r_low, r_high, None, p_row))

    c_low = 0
    c_high = 7
    for idx, c in enumerate(p_col):
        if visualize:
            state[1].append(NonPartitionState(c_low, c_high, idx, p_col))
        if c == "R":
            c_low += math.ceil((c_high - c_low) / 2)
        elif c == "L":
            c_high -= math.ceil((c_high - c_low) / 2)

    if visualize:
        state[1].append(NonPartitionState(c_low, c_high, None, p_col))

    return r_high * 8 + c_high, state


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        passes = [line.strip() for line in in_file]

    if args.visualize:
        if args.vis_strategy == "nonpartition":
            _, states = calculate_id(passes[0][:7], passes[0][7:], True)
            show_visualization(states, NonPartitionState)
        else:
            _, states = calculate_id_partition(passes[0][:7], passes[0][7:], True)
            show_visualization(states, PartitionState)
        exit()

    # Part 1
    p1_start = datetime.now()
    p1_ids = set(calculate_id(p[:7], p[7:], False)[0] for p in passes)
    p1 = max(p1_ids)
    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()
    for idd in np.arange(0, 127 * 8 + 7):
        if idd not in p1_ids and idd - 1 in p1_ids and idd + 1 in p1_ids:
            p2 = idd
            break
    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.performance:
        performance(18)


if __name__ == "__main__":
    args = parse_args()
    main(args)
