#!/usr/bin/env python3
""" Advent of Code Day 13
"""
from argparse import ArgumentParser
from datetime import datetime
from pathlib import Path

import numpy as np


def euclidx(a, b):
    if a == 0:
        return 0
    if b % a == 0:
        return 1
    return b - euclidx(b % a, a) * b // a


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")

    return argp.parse_args(arglist)


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        arrival = int(in_file.readline())
        times = np.array(
            [int(x) if x != "x" else np.nan for x in in_file.readline().split(",")]
        )

    # Part 1
    p1_start = datetime.now()
    min_wait = None
    min_bus = None
    for bus in times:
        if not np.isnan(bus):
            departure = bus
            while departure < arrival:
                departure += bus
            wait = departure - arrival
            if min_wait is None or min_wait > wait:
                min_wait = wait
                min_bus = bus
    p1 = int(min_wait * min_bus)
    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()
    a = np.where(~np.isnan(times))[0]
    n = times[a].astype(np.ulonglong)
    N = n.prod()
    x = np.sum(
        [a[i] * (N // n[i]) * euclidx(N // n[i], n[i]) for i in np.arange(n.shape[0])]
    )
    p2 = int(N - x % N)
    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")


if __name__ == "__main__":
    args = parse_args()
    main(args)
