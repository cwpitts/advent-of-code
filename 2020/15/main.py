#!/usr/bin/env python3
""" Advent of Code Day 15
"""
from argparse import ArgumentParser
from datetime import datetime
from pathlib import Path
import tqdm

import numpy as np


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")

    return argp.parse_args(arglist)

def solve(arr, target):
    ages = {}

    for turn, num in enumerate(arr):
        ages[num] = turn

    last_num = arr[-1]
    for turn in tqdm.tqdm(range(arr.shape[0] - 1, target - 1)):
        tmp = turn - ages[last_num] if last_num in ages else 0
        ages[last_num] = turn
        last_num = tmp

    return last_num


def solve_array(arr, target):
    # Note: using np.nan breaks the loop for some reason
    ages = np.full(target, -1, dtype=np.int64)

    for turn, num in enumerate(arr):
        ages[num] = turn

    last_num = arr[-1]
    for turn in range(arr.shape[0] - 1, target - 1):
        tmp = 0 if ages[last_num] == -1 else turn - ages[last_num]
        ages[last_num] = turn
        last_num = tmp

    return last_num


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        nums = np.array(list(map(int, in_file.read().strip().split(","))), dtype=np.int64)

    # Part 1
    p1_start = datetime.now()
    p1 = solve_array(nums, 2020)
    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()
    p2 = solve_array(nums, 30000000)
    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")


if __name__ == "__main__":
    args = parse_args()
    main(args)
