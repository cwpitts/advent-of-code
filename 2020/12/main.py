#!/usr/bin/env python3
""" Advent of Code Day 12
"""
from argparse import ArgumentParser
from copy import deepcopy
from dataclasses import dataclass
from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.animation import FuncAnimation
from matplotlib.patches import RegularPolygon
from matplotlib.text import Text
from matplotlib.transforms import Affine2D
from tqdm import tqdm


class Position:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return f"({self.x}, {self.y})"

    def __repr__(self):
        return self.__str__()


@dataclass
class ShipState:
    pos: Position
    heading: int
    waypoint: Position

    def __init__(self, pos, heading, waypoint):
        self.pos = deepcopy(pos)
        self.heading = heading
        self.waypoint = deepcopy(waypoint)


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "--visualize", help="visualize solution", action="store_true", default=False
    )

    return argp.parse_args(arglist)


def make_info_str(step, state):
    return f"""
Step: ${step}$
Position: $({state.pos.x}, {state.pos.y})$
Heading: ${state.heading}^\\circ$
Distance: ${np.abs(state.pos.x) + np.abs(state.pos.y)}$
"""


def show_visualization(states, padding=10):
    x_min, x_max = min(s.pos.x for s in states), max(s.pos.x for s in states)
    y_min, y_max = min(s.pos.y for s in states), max(s.pos.y for s in states)

    fig, ax = plt.subplots()

    ax.set_title("Advent of Code 2020 Day 12")

    ship_patch = RegularPolygon(
        (states[0].pos.x, states[0].pos.y), 3, radius=2, color="black"
    )
    ax.add_patch(ship_patch)

    waypoint_patch = RegularPolygon(
        (states[0].pos.x, states[0].pos.y + 3), 10, radius=0.5, color="red"
    )
    ax.add_patch(waypoint_patch)

    info = Text(
        x=x_min - padding * 0.5,
        y=y_min - padding * 0.5,
        text=make_info_str(0, states[0]),
        color="black",
        usetex=True,
    )
    ax.add_artist(info)

    ax.set_xlim(x_min - padding, x_max + padding)
    ax.set_ylim(y_min - padding, y_max + padding)

    def update_pos(state_):
        idx, state = state_
        ship_patch.xy = (state.pos.x, state.pos.y)
        info.set_text(make_info_str(idx, state))
        rot_coords = ax.transData.transform([state.pos.x, state.pos.y])
        rot = Affine2D().rotate_deg_around(*rot_coords, -90 + state.heading)
        ship_patch.set_transform(ax.transData + rot)

        waypoint_patch.xy = (state.pos.x, state.pos.y + 1)
        waypoint_patch.set_transform(ax.transData + rot)

        return ship_patch, waypoint_patch, info

    ani = FuncAnimation(
        fig, update_pos, frames=enumerate(states), repeat=False, interval=1000, fargs=()
    )
    ani.save("anim.gif")


def navigate(steps, visualize):
    states = []

    ship = Position(0, 0)
    heading = 0

    if visualize:
        states.append(ShipState(ship, heading, None))

    for action, value in steps:
        if action == "N":
            ship.y += value
        elif action == "S":
            ship.y -= value
        elif action == "E":
            ship.x += value
        elif action == "W":
            ship.x -= value
        elif action == "L":
            heading += value
            heading = heading % 360
        elif action == "R":
            heading -= value
            heading = heading % 360
        elif action == "F":
            if heading == 0:
                ship.x += value
            elif heading == 90:
                ship.y += value
            elif heading == 180:
                ship.x -= value
            elif heading == 270:
                ship.y -= value

        if visualize:
            states.append(ShipState(ship, heading, None))

    return ship, states


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        steps = [(line[0], int(line[1:].strip())) for line in in_file]

    # Part 1
    p1_start = datetime.now()
    pos_p1, states_p1 = navigate(steps, args.visualize)
    p1 = abs(pos_p1.x) + abs(pos_p1.y)
    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()
    waypoint = Position(10, 1)
    ship = Position(0, 0)
    for action, value in steps:
        if action == "N":
            waypoint.y += value
        elif action == "S":
            waypoint.y -= value
        elif action == "E":
            waypoint.x += value
        elif action == "W":
            waypoint.x -= value
        elif action == "L":
            for _ in range(value // 90):
                waypoint.x, waypoint.y = -waypoint.y, waypoint.x
        elif action == "R":
            for _ in range(value // 90):
                waypoint.x, waypoint.y = waypoint.y, -waypoint.x
        elif action == "F":
            ship.x += waypoint.x * value
            ship.y += waypoint.y * value
    p2 = abs(ship.x) + abs(ship.y)
    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.visualize:
        show_visualization(states_p1)


if __name__ == "__main__":
    _args = parse_args()
    main(_args)
