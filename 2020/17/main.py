#!/usr/bin/env python3
""" Advent of Code Day 17
"""
import timeit
from argparse import ArgumentParser
from copy import deepcopy
from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm

plt.style.use("fivethirtyeight")


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )

    return argp.parse_args(arglist)


def performance(args, size):
    times_3d = []
    times_4d = []

    def run_3d(active_nodes, iterations):
        pass

    active_nodes_3d = get_initial_active_nodes(args, 3)
    active_nodes_4d = get_initial_active_nodes(args, 4)

    for iterations in tqdm(np.arange(1, size + 1)):
        an3d = deepcopy(active_nodes_3d)
        times_3d.append(timeit.timeit(lambda: sim(an3d, iterations), number=100))

        an4d = deepcopy(active_nodes_4d)
        times_4d.append(timeit.timeit(lambda: sim(an4d, iterations), number=100))

    fig, ax = plt.subplots()
    ax.plot(times_3d, label="3D")
    ax.plot(times_4d, label="4D")
    ax.set_title("Execution time in seconds")
    ax.set_xlabel("Puzzle size (iterations)")
    ax.set_ylabel("t (sec)")
    ax.legend()
    fig.savefig(f"aoc-17-perf-{size}.png", bbox_inches="tight", dpi=300)


def get_neighbors(x, y, z, w=None):
    ret = set()
    for dx in [-1, 0, 1]:
        for dy in [-1, 0, 1]:
            for dz in [-1, 0, 1]:
                if w is None:
                    if not dx == dy == dz == 0:
                        ret.add((x + dx, y + dy, z + dz))
                else:
                    for dw in [-1, 0, 1]:
                        if not dx == dy == dz == dw == 0:
                            ret.add((x + dx, y + dy, z + dz, w + dw))
    return ret


def get_initial_active_nodes(args, dim):
    with open(args.input, "r") as in_file:
        # Read input
        init_active_nodes = set()
        for y, row in enumerate(in_file.read().splitlines()):
            for x, val in enumerate(row):
                if val == "#":
                    if dim == 3:
                        # Indexed x, y, z
                        init_active_nodes.add((x, y, 0))
                    elif dim == 4:
                        # Indexed x, y, z, w
                        init_active_nodes.add((x, y, 0, 0))
    return init_active_nodes


def sim(active_nodes, iterations):
    for iteration in range(iterations):
        interesting_nodes = set()
        new_active_nodes = set()

        for node in active_nodes:
            interesting_nodes |= get_neighbors(*node)

        for node in interesting_nodes:
            active_neighbors = sum(
                neighbor in active_nodes for neighbor in get_neighbors(*node)
            )
            if node in active_nodes:
                if active_neighbors in (2, 3):
                    new_active_nodes.add(node)
            else:
                if active_neighbors == 3:
                    new_active_nodes.add(node)

        active_nodes = new_active_nodes
    return len(active_nodes)


def main(args):
    # Part 1
    p1_start = datetime.now()
    active_nodes = get_initial_active_nodes(args, 3)
    p1 = sim(active_nodes, 6)
    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()
    active_nodes = get_initial_active_nodes(args, 4)
    p2 = sim(active_nodes, 6)
    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        print("Running performance")
        performance(args, 10)


if __name__ == "__main__":
    args = parse_args()
    main(args)
