#!/usr/bin/env python3
""" Advent of Code Day 01
"""
from argparse import ArgumentParser
from datetime import datetime
from pathlib import Path

import numpy as np


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")

    return argp.parse_args(arglist)


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        numbers = set(map(int, in_file))

    # Part 1
    p1_start = datetime.now()
    for n in numbers:
        m = 2020 - n 
        if m in numbers:
            p1 = n * m
    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()
    for n in numbers:
        for m in numbers:
            for p in numbers:
                if n + m + p == 2020:
                    p2 = n * m * p
    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")


if __name__ == "__main__":
    args = parse_args()
    main(args)
