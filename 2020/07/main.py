#!/usr/bin/env python3
""" Advent of Code Day 07
"""
from argparse import ArgumentParser
from datetime import datetime
from itertools import tee
from pathlib import Path

import matplotlib.pyplot as plt
import networkx as nx
import numpy as np

plt.style.use("fivethirtyeight")
plt.rcParams.update({
    "text.usetex": True
})

def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")

    return argp.parse_args(arglist)


def expand(g, node):
    ret = 1
    for u, v in g.edges(node):
        data = g.get_edge_data(u, v)
        count = data["weight"]
        ret += count * expand(g, v)
    return ret


def main(args):
    rules = {}
    with open(args.input, "r") as in_file:
        # Read input
        for line in in_file:
            source, sinks = line.split(" contain ")
            adjective, color, _ = source.split(" ")
            if "no other bags" not in sinks:
                produces = [part.split(" ")[:-1] for part in sinks[:-1].split(", ")]
                rules[(adjective, color)] = produces

    # Part 1
    p1_start = datetime.now()
    g = nx.DiGraph()
    for src, dests in rules.items():
        for dest in dests:
            g.add_edge("-".join(src), "-".join(dest[1:]), weight=int(dest[0]))
    p1 = sum(
        [
            nx.has_path(g, "-".join(src), "shiny-gold")
            for src in rules.keys()
            if src != ("shiny", "gold")
        ]
    )
    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()
    p2 = expand(g, "shiny-gold") - 1
    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    # Visualizations!
    pos = nx.drawing.nx_agraph.graphviz_layout(g, root="shiny-gold")
    fig, ax = plt.subplots()
    fig.set_size_inches(8, 6)
    nx.draw(g, pos, with_labels=True)
    edge_labels = {(u, v): g.get_edge_data(u, v)["weight"] for u, v in g.edges()}
    nx.draw_networkx_edge_labels(g, pos, edge_labels=edge_labels)
    ax.annotate("\copyright 2020 C. W. Pitts", (35, 2), color="black", fontsize="x-small")
    fig.savefig("aoc-07.png", bbox_inches="tight", dpi=300)

if __name__ == "__main__":
    args = parse_args()
    main(args)
