from pathlib import Path
import pytest
import tempfile

from main import main, parse_args

data_dir = Path("test/data")

def test_main():
    aoc_input = data_dir / "input"
    with open(data_dir / "out.txt") as in_file:
        expected = [line.strip() for line in in_file]

    with tempfile.NamedTemporaryFile(prefix="aoc-") as out_file:
        args = parse_args([str(aoc_input), out_file.name])
        main(args)

        out_file.seek(0)

        actual = [line.strip().decode("utf-8") for line in out_file]

    assert expected == actual
