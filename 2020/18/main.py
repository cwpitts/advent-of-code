#!/usr/bin/env python3
""" Advent of Code Day 18
"""
import random
import timeit
from argparse import ArgumentParser
from collections import deque
from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm

ops = {
    "*": lambda stack: stack.append(stack.pop() * stack.pop()),
    "+": lambda stack: stack.append(stack.pop() + stack.pop()),
}


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def make_expr(expr_size):
    ret = ""

    operator_indices = []

    for idx in np.arange(expr_size):
        if idx % 2 == 0:
            ret += str(np.random.randint(10))
        else:
            ret += "*" if np.random.binomial(1, 0.5) else "+"
            operator_indices.append(idx)
        idx += 1

    return ret


def make_problem(size):
    exprs = [make_expr(random.randrange(3, 20, 2)) for _ in np.arange(size)]
    return exprs

def performance(size):
    precedence = {
        "+": 1,
        "*": 1,
    }

    times = []
    for s in tqdm(np.arange(1, size + 1, 2)):
        problem = make_problem(s)
        times.append(
            timeit.timeit(
                lambda: eval_postfix(infix_to_postfix(problem, precedence)), number=10000
            )
        )

    return times

def infix_to_postfix(expr, precedence):
    output = deque()
    operators = deque()

    for token in expr:
        if token.isnumeric():
            output.append(token)
        elif token == ")":
            while operators[-1] != "(":
                output.append(operators.pop())
            operators.pop()
        elif token == "(":
            operators.append(token)
        else:
            while (
                operators
                and operators[-1] != "("
                and precedence[operators[-1]] >= precedence[token]
            ):
                output.append(operators.pop())
            operators.append(token)

    while operators:
        output.append(operators.pop())

    return output


def eval_postfix(expr):
    stack = deque()

    for token in expr:
        if token.isnumeric():
            stack.append(int(token))
        else:
            ops[token](stack)

    return stack


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        lines = [expr.replace(" ", "") for expr in in_file.read().splitlines()]

    # Part 1
    p1_start = datetime.now()
    precedence_p1 = {
        "+": 1,
        "*": 1,
    }
    p1 = sum([eval_postfix(infix_to_postfix(expr, precedence_p1))[0] for expr in lines])
    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()
    precedence_p2 = {
        "+": 2,
        "*": 1,
    }
    p2 = sum([eval_postfix(infix_to_postfix(expr, precedence_p2))[0] for expr in lines])
    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args = parse_args()
    main(args)
