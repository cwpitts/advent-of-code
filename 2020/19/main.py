#!/usr/bin/env python3
""" Advent of Code Day 19
"""
from argparse import ArgumentParser
from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def can_create(grammar, S, rule_string):
    # Reaching the end of either the input string or the
    # parsing sequence means we should be done
    if not S or not rule_string:
        # We're only done if they both end at the same time, however
        return not S and not rule_string

    # Get current rule to match against
    rule = grammar[rule_string[0]]
    if not isinstance(rule, list):
        # Matching terminal rule
        if S[0] == rule:
            # If we match, strip the first item from the input and
            # the rule string (which was a literal character which
            # can no longer be expanded), and recurse
            return can_create(grammar, S[1:], rule_string[1:])
        # Didn't match terminal rule, can't progress
        return False
    for sub_rule in rule:
        # For each sub-rule, expand and continue
        new_rule_string = sub_rule + rule_string[1:]
        if can_create(grammar, S, new_rule_string):
            return True
    return False


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        lines = in_file.read().splitlines()
        rules = {}
        progs = []

        for rule in lines[: lines.index("")]:
            left, right = rule.split(": ")
            left = int(left)

            if right[0] == '"':
                rules[left] = right[1]
            else:
                rules[left] = [[int(x) for x in y.split()] for y in right.split("|")]

        progs = lines[lines.index("") + 1 :]

    # Part 1
    p1_start = datetime.now()
    p1 = sum(can_create(rules, prog, [0]) for prog in progs)
    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()
    """
    8: 42 | 42 8
    11: 42 31 | 42 11 31
    """
    rules[8] = [[42], [42, 8]]
    rules[11] = [[42, 31], [42, 11, 31]]
    p2 = sum(can_create(rules, prog, [0]) for prog in progs)
    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args = parse_args()
    main(args)
