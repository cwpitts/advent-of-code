#!/usr/bin/env python3
""" Advent of Code Day 08
"""
import time
from argparse import ArgumentParser
from collections import deque
from copy import deepcopy
from dataclasses import dataclass
from datetime import datetime
from pathlib import Path

import numpy as np
from rich.console import Console
from rich.table import Table


@dataclass
class MachineState:
    acc: dict
    ip: int
    prog: list
    eval_counts: list


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "--visualize", help="visualize solution", action="store_true", default=False
    )
    argp.add_argument(
        "--vis-part", help="part to visualize", type=int, choices=[1, 2], default=1
    )

    return argp.parse_args(arglist)


def show_visualization(states):
    console = Console()
    max_inst_length = max(max(len(inst) for inst in state.prog) for state in states)

    for state in states:
        console.clear()
        table = Table(show_header=True, style="bold")
        table.add_column("Eval count", width=10, justify="center")
        table.add_column(
            "Program", width=max(len("Program"), max_inst_length), justify="left"
        )
        table.add_column("IP", width=5, justify="center")
        table.add_column("Acc", width=5, justify="center")

        for idx, (cnt, inst) in enumerate(zip(state.eval_counts, state.prog)):
            acc_str = f"[cyan]{state.acc}[/cyan]" if idx == 0 else ""
            ip_str = "[yellow]<-[/yellow]" if state.ip == idx else ""
            cnt_str = f"[magenta]{cnt}[/magenta]" if cnt > 0 else str(cnt)
            table.add_row(cnt_str, inst, ip_str, acc_str)

        console.print(table)
        time.sleep(1)

    if states[-1].ip != len(states[-1].prog):
        console.print("[red]LOOP![/red]")
    else:
        console.print("[green]TERMINATED![/green]")


def swap_inst(prog, x):
    new_prog = []
    for i, inst in enumerate(prog):
        if i != x:
            new_prog.append(inst)
        else:
            if inst.startswith("nop"):
                new_prog.append(inst.replace("nop", "jmp"))
            elif inst.startswith("jmp"):
                new_prog.append(inst.replace("jmp", "nop"))
    return new_prog


def step(prog, ip, acc):
    inst = prog[ip].split(" ")
    if inst[0] == "nop":
        ip += 1
    elif inst[0] == "acc":
        acc += int(inst[1])
        ip += 1
    elif inst[0] == "jmp":
        ip += int(inst[1])

    return ip, acc


def run_prog(prog, visualize=False):
    eval_counts = [0 for _ in prog]

    states = deque()

    ptr = 0
    acc = 0
    while eval_counts[ptr] < 1 and ptr < len(prog) - 1:
        if visualize:
            states.append(MachineState(acc, ptr, deepcopy(prog), deepcopy(eval_counts)))
        eval_counts[ptr] += 1
        ptr, acc = step(prog, ptr, acc)

    if visualize:
        states.append(MachineState(acc, ptr, deepcopy(prog), deepcopy(eval_counts)))
    terminated = False

    if ptr == len(prog) - 1:
        terminated = True
        ptr, acc = step(prog, ptr, acc)
        if visualize:
            states.append(MachineState(acc, ptr, deepcopy(prog), deepcopy(eval_counts)))

    return acc, terminated, states


def iter_swaps(prog):
    for idx, inst in enumerate(prog):
        if inst.startswith("nop") or inst.startswith("jmp"):
            yield idx


def main(args):
    with open(args.input, "r") as in_file:
        prog = [line.strip() for line in in_file]

    # Part 1
    p1_start = datetime.now()
    p1, _, _ = run_prog(prog)
    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()
    p2 = None
    for idx in iter_swaps(prog):
        new_prog = swap_inst(prog, idx)
        p2, terminated, _ = run_prog(new_prog)
        if terminated:
            break
    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.visualize:
        if args.vis_part == 1:
            _, _, p1_states = run_prog(prog, True)
            show_visualization(p1_states)
        else:
            _, _, p2_states = run_prog(new_prog, True)
            show_visualization(p2_states)


if __name__ == "__main__":
    args = parse_args()
    main(args)
