#!/usr/bin/env python3
""" Advent of Code Day 06
"""
from argparse import ArgumentParser
from datetime import datetime
from pathlib import Path
import string

import numpy as np


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")

    return argp.parse_args(arglist)

def main(args):
    with open(args.input, "r") as in_file:
        input_lines = in_file.read()
        groups = [x.strip().split("\n") for x in input_lines.split("\n\n")]

    # Part 1
    p1_start = datetime.now()
    p1 = 0
    for group in groups:
        c = set()
        for person in group:
            c |= set(person)
        p1 += len(c)
    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()
    p2 = 0
    for group in groups:
        c = set(string.ascii_lowercase)
        for person in group:
            c &= set(person)
        p2 += len(c)
    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")


if __name__ == "__main__":
    args = parse_args()
    main(args)
