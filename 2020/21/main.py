#!/usr/bin/env python3
""" Advent of Code Day 21
"""
from argparse import ArgumentParser
from collections import Counter, defaultdict
from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        meals = defaultdict(lambda: [])
        ingredients = set()
        ingredient_count = Counter()
        for line in in_file:
            ing, allergens = line.strip().split(" (contains ")

            ing = set(ing.split())
            allergens = set(allergens[:-1].split(", "))

            for ingredient in ing:
                ingredient_count[ingredient] += 1

            for allergen in allergens:
                meals[allergen].append(ing)
                ingredients |= ing

    # Part 1
    p1_start = datetime.now()
    allergens = set()
    # The allergen has to appear in every meal where it's mentioned
    for allergen, meal_list in meals.items():
        allergens |= set.intersection(*meal_list)
    non_allergens = ingredients - allergens
    p1 = sum([ingredient_count[ing] for ing in non_allergens])
    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()
    translations = {allergen: set.intersection(*meal_list) - non_allergens for allergen, meal_list in meals.items()}
    translations = sorted(translations.items(), key=lambda x: len(x[1]))
    for allergen, names in translations:
        if len(names) == 1:
            for allergen_remove, names_remove in translations:
                if allergen != allergen_remove:
                    names_remove -= names
    p2 = ",".join([list(x[1])[0] for x in sorted(translations, key=lambda y: y[0])])
    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args = parse_args()
    main(args)
