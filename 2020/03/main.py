#!/usr/bin/env python3
import math
import timeit
from argparse import ArgumentParser
from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import tqdm

plt.style.use("fivethirtyeight")


def nomod_count_trees(grid, dy, dx):
    needed_length = dx * (len(grid) // dy)
    m = math.ceil(needed_length / len(grid[0]))
    new_grid = [line * m for line in grid]

    p = [0, 0]  # y, x
    count = 0
    while p[0] < len(new_grid) - 1:
        p[0] += dy
        p[1] += dx
        if new_grid[p[0]][p[1]] == "#":
            count += 1

    return count


def count_trees(grid, dy, dx):
    p = [0, 0]
    count = 0
    while p[0] < len(grid) - 1:
        p[0] += dy
        p[1] += dx
        p[1] = p[1] % len(grid[0])
        if grid[p[0]][p[1]] == "#":
            count += 1

    return count


def performance(size):
    mod_times = []
    nomod_times = []

    for grid_size in tqdm.tqdm(np.arange(1, size + 1)):
        grid = np.random.randint(0, 2, size=(grid_size, 10)).astype(object)
        grid[grid == 0] = "."
        grid[grid == 1] = "#"
        grid = grid.tolist()

        mod_times.append(timeit.timeit(lambda: count_trees(grid, 1, 10), number=100))
        nomod_times.append(
            timeit.timeit(lambda: nomod_count_trees(grid, 1, 10), number=100)
        )

    fig, ax = plt.subplots()
    ax.plot(mod_times, label="Mod")
    ax.plot(nomod_times, label="No mod")
    ax.set_title("Execution time in seconds")
    ax.set_xlabel("Puzzle size")
    ax.set_ylabel("t (sec)")
    ax.legend()
    fig.savefig(f"aoc-03-perf-{size}.png", bbox_inches="tight", dpi=300)


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--performance", help="run performance", action="store_true"
    )

    return argp.parse_args(arglist)


def main(args):
    with open(args.input, "r") as in_file:
        grid = [line.strip() for line in in_file]

    # Part 1
    p1_start = datetime.now()
    p1 = count_trees(grid, 1, 3)
    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()
    slopes = [(1, 1), (1, 3), (1, 5), (1, 7), (2, 1)]
    hits = [
        count_trees(grid, y, x) for y, x in slopes
    ]
    p2 = np.prod(hits)
    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.performance:
        performance(100)
        performance(500)


if __name__ == "__main__":
    args = parse_args()
    main(args)
