#!/usr/bin/env python3
""" Advent of Code Day 04
"""
import re
from argparse import ArgumentParser
from datetime import datetime
from pathlib import Path

import numpy as np

fields = set(["eyr", "byr", "hcl", "pid", "ecl", "hgt", "iyr"])
byr_rgx = re.compile("(?<=byr:)([0-9]{4})")
iyr_rgx = re.compile("(?<=iyr:)([0-9]{4})")
eyr_rgx = re.compile("(?<=eyr:)([0-9]{4})")
hgt_rgx = re.compile("(?<=hgt:)([0-9]{2,3})(cm|in)")
hcl_rgx = re.compile("(?<=hcl:#)([a-f0-9]{6})")
ecl_rgx = re.compile("(?<=ecl:)(amb|blu|brn|gry|grn|hzl|oth)")
pid_rgx = re.compile("(?<=pid:)([0-9]{9})(?![0-9])")
cid_rgx = re.compile("(?<=cid:)([0-9]+)")


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")

    return argp.parse_args(arglist)

def has_all_fields(passport):
    if all([x in passport for x in fields]):
        return True
    return False


def validate_fields(passport):
    byr = re.search(byr_rgx, passport)
    if byr is None:
        return False
    if not 1920 <= int(byr.group()) <= 2002:
        return False

    iyr = re.search(iyr_rgx, passport)
    if iyr is None:
        return None
    if not 2010 <= int(iyr.group()) <= 2020:
        return False

    eyr = re.search(eyr_rgx, passport)
    if eyr is None:
        return False
    if not 2020 <= int(eyr.group()) <= 2030:
        return False

    hgt = re.search(hgt_rgx, passport)
    if hgt is None:
        return None
    hgt, unit = int(hgt.group(1)), hgt.group(2)

    if unit == "cm" and not (150 <= hgt <= 193):
        return False
    if unit == "in" and not (59 <= hgt <= 76):
        return False

    hcl = re.search(hcl_rgx, passport)
    if hcl is None:
        return False

    ecl = re.search(ecl_rgx, passport)
    if ecl is None:
        return False

    pid = re.search(pid_rgx, passport)
    if pid is None:
        return False

    return True

def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        passports = in_file.read().split("\n\n")

    # Part 1
    p1_start = datetime.now()
    p1 = 0
    valid_passports = set()
    for passport in passports:
        if has_all_fields(passport):
            p1 += 1
            valid_passports.add(passport)
    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()
    p2 = 0
    for passport in valid_passports:
        if validate_fields(passport):
            p2 += 1
    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

if __name__ == "__main__":
    args = parse_args()
    main(args)
