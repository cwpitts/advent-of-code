#!/usr/bin/env python3
""" Advent of Code Day 25
"""
from argparse import ArgumentParser
from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def transform_step(value, subject_number=7):
    value *= subject_number
    value = value % 20201227
    return value


def find_loop_size(public_key):
    ret = 0
    value = 1

    while value != public_key:
        value = transform_step(value)
        ret += 1

    return ret


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        card_key, door_key = map(int, in_file.read().strip().split("\n"))

    # Part 1
    p1_start = datetime.now()
    find_loop_size(card_key)
    door_loop_size = find_loop_size(door_key)
    p1 = 1
    for _ in range(door_loop_size):
        p1 = transform_step(p1, card_key)
    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()
    p2 = None
    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args = parse_args()
    main(args)
