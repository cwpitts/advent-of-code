#!/usr/bin/env python3
""" Advent of Code Day 20
"""
from argparse import ArgumentParser
from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm


class Tile:
    def __init__(self, tile_id, tile_arr):
        self.tile_id = tile_id
        self.arr = tile_arr

    @property
    def left(self):
        return self.arr[:, 0]

    @property
    def left_str(self):
        return "".join(self.left)

    @property
    def right(self):
        return self.arr[:, -1]

    @property
    def right_str(self):
        return "".join(self.right)

    @property
    def top(self):
        return self.arr[0, :]

    @property
    def top_str(self):
        return "".join(self.top)

    @property
    def bottom(self):
        return self.arr[-1, :]

    @property
    def bottom_str(self):
        return "".join(self.bottom)

    def flip(self):
        return Tile(self.tile_id, np.flip(self.arr, axis=0))

    def rotate(self, n=0):
        a = self.arr
        for _ in np.arange(n % 4):
            a = np.rot90(a)
        return Tile(self.tile_id, a)

    def __repr__(self):
        return np.array_repr(self.arr)


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "-p", "--perf", help="run performance", action="store_true", default=False
    )
    return argp.parse_args(arglist)


def performance(size):
    pass


def find_bottom_link(tile, all_tiles):
    for match_tile in all_tiles:
        # Obviously, don't match against the same tile
        if match_tile.tile_id != tile.tile_id:
            # Compare tile bottom string to match_tile top string
            if tile.bottom_str == match_tile.top_str:
                return match_tile

    return None

def find_right_link(tile, all_tiles):
    for match_tile in all_tiles:
        # Skip matching the tile against itself
        if match_tile.tile_id != tile.tile_id:
            # Compate tile right string to match_tile left string
            if tile.right_str == match_tile.left_str:
                return match_tile

    return None

def make_image(corner_tile, all_tiles, dim):
    image = [[None for _ in np.arange(dim)] for _ in np.arange(dim)]

    # Starting from the corner tile
    image[0][0] = corner_tile

    for row in np.arange(dim):
        for col in np.arange(dim):
            if row == col == 0:
                continue
            # Determine if we're starting a row
            if col == 0:
                # If starting a row, find an upper connection
                tile = find_bottom_link(image[row - 1][col], all_tiles)
                if not tile:
                    return None
                image[row][col] = tile
            else:
                # Match against the previous entry's right edge
                tile = find_right_link(image[row][col - 1], all_tiles)
                if not tile:
                    return None
                image[row][col] = tile

    return image

def stitch_image(tiles):
    edges = set()
    for tile in tiles.values():
        left = tile.left_str
        right = tile.right_str
        top = tile.top_str
        bottom = tile.bottom_str

        # Insert edges if they are not present (also counting rotated form)
        if left not in edges and left[::-1] not in edges:
            edges.add(left)
        if right not in edges and right[::-1] not in edges:
            edges.add(right)
        if top not in edges and top[::-1] not in edges:
            edges.add(top)
        if bottom not in edges and bottom[::-1] not in edges:
            edges.add(bottom)

    counts = {edge: 0 for edge in edges}

    for tile in tiles.values():
        if tile.left_str in edges:
            counts[tile.left_str] += 1
        if tile.left_str[::-1] in edges:
            counts[tile.left_str[::-1]] += 1
        if tile.right_str in edges:
            counts[tile.right_str] += 1
        if tile.right_str[::-1] in edges:
            counts[tile.right_str[::-1]] += 1
        if tile.top_str in edges:
            counts[tile.top_str] += 1
        if tile.top_str[::-1] in edges:
            counts[tile.top_str[::-1]] += 1
        if tile.bottom_str in edges:
            counts[tile.bottom_str] += 1
        if tile.bottom_str[::-1] in edges:
            counts[tile.bottom_str[::-1]] += 1

    unique_edges = {tile_id: 0 for tile_id in tiles.keys()}

    for tile_id, tile in tiles.items():
        if counts.get(tile.left_str, 0) == 1 or counts.get(tile.left_str[::-1], 0) == 1:
            unique_edges[tile_id] += 1
        if (
            counts.get(tile.right_str, 0) == 1
            or counts.get(tile.right_str[::-1], 0) == 1
        ):
            unique_edges[tile_id] += 1
        if counts.get(tile.top_str, 0) == 1 or counts.get(tile.top_str[::-1], 0) == 1:
            unique_edges[tile_id] += 1
        if (
            counts.get(tile.bottom_str, 0) == 1
            or counts.get(tile.bottom_str[::-1], 0) == 1
        ):
            unique_edges[tile_id] += 1

    corners = [
        tile_id
        for tile_id, unique_edge_count in unique_edges.items()
        if unique_edge_count == 2
    ]

    # Generate all flips and rotations of tiles
    all_tiles = []
    for tile in tiles.values():
        # Flips
        for flip in [True, False]:
            new_tile = tile
            if flip:
                new_tile = new_tile.flip()
            # Rotations
            for n_rot in np.arange(4):
                all_tiles.append(new_tile.rotate(n_rot))

    # Try to assemble a grid treating each corner tile as the top left tile
    dim = np.sqrt(len(tiles)).astype(np.int64)
    img = None
    for tile_id in corners:
        tile = tiles[tile_id]
        # Try each flip and each rotation
        for flip in [True, False]:
            if flip:
                tile = tile.flip()
            for n_rot in np.arange(4):
                tile = tile.rotate(n_rot)
                img = make_image(tile, all_tiles, dim)
                if img:
                    break

    return corners, img


def count_non_monster_pixels(img, sea_monster):
    monster_coords = np.nonzero(sea_monster)
    monster_pixels = 0

    row_slides = img.shape[0] - sea_monster.shape[0]
    col_slides = img.shape[1] - sea_monster.shape[1]

    for rs in range(row_slides):
        for cs in range(col_slides):
            window = img[rs:rs + sea_monster.shape[0], cs:cs + sea_monster.shape[1]]
            if np.all(window[monster_coords]):
                monster_pixels += sea_monster.sum()

    return img.sum() - monster_pixels

def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        tiles = {}
        for tile in in_file.read().strip().split("\n\n"):
            tile_id, img = tile.split(":")
            tile_id = int(tile_id.split()[1])
            img = img[1:]
            tiles[tile_id] = Tile(
                tile_id, np.array([[c for c in row] for row in img.split("\n")])
            )

    # Part 1
    p1_start = datetime.now()
    corners, img = stitch_image(tiles)
    p1 = np.prod(corners)
    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()
    # Make full image
    img = np.vstack([np.hstack([tile.arr[1:-1, 1:-1] for tile in row]) for row in img])
    sea_monster = np.array([[c for c in "                  # "],
                            [c for c in "#    ##    ##    ###"],
                            [c for c in " #  #  #  #  #  #   "]])
    img = (img == "#").astype(np.int64)
    sea_monster = (sea_monster == "#").astype(np.int64)

    p2 = 1e7
    for flip in [True, False]:
        img_rot = img.copy()
        if flip:
            img_rot = img_rot[:, ::-1]
        for n_rot in np.arange(4):
            for _ in np.arange(n_rot):
                img_rot = np.rot90(img_rot)
            p2 = min(p2, count_non_monster_pixels(img_rot, sea_monster))
    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.perf:
        performance(None)


if __name__ == "__main__":
    args = parse_args()
    main(args)
