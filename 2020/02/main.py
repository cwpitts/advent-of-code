#!/usr/bin/env python3
""" Advent of Code Day 02
"""
from argparse import ArgumentParser
from collections import Counter
from datetime import datetime
from pathlib import Path

import numpy as np


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")

    return argp.parse_args(arglist)


def main(args):
    rules = []
    letters = []
    passwords = []
    with open(args.input, "r") as in_file:
        # Read input
        for line in in_file:
            rule_part, letter, password = line.strip().split(' ')
            letter = letter[:-1]
            rules.append(tuple(map(int, rule_part.split("-"))))
            letters.append(letter)
            passwords.append(password)

    # Part 1
    p1_start = datetime.now()
    p1 = 0
    for rule, letter, password in zip(rules, letters, passwords):
        password = Counter(password)
        if rule[0] <= password[letter] <= rule[1]:
            p1 += 1
    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()
    p2 = 0
    for rule, letter, password in zip(rules, letters, passwords):
        if (password[rule[0] - 1] == letter) != (password[rule[1] - 1] == letter):
            p2 += 1
    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")


if __name__ == "__main__":
    args = parse_args()
    main(args)
