#!/usr/bin/env python3
""" Advent of Code Day 14
"""
import itertools
import re
from argparse import ArgumentParser
from copy import deepcopy
from dataclasses import dataclass
from datetime import datetime
from pathlib import Path
from time import sleep

import numpy as np
import rich
from rich.console import Console
from rich.table import Table
from tqdm import tqdm


@dataclass
class MachineState:
    memory: list
    inputs: list
    cur_inst: str
    cur_mask: str


loc_rgx = re.compile("mem\[([0-9]+)\]")


def parse_args(arglist=None):
    argp = ArgumentParser()
    argp.add_argument("input", help="path to input file", type=Path)
    argp.add_argument("output", help="path to output file", type=Path, default="output")
    argp.add_argument(
        "--visualize", help="visualize solution", action="store_true", default=False
    )
    argp.add_argument(
        "--vis-part", help="part to visualize", type=int, choices=[1, 2], default=1
    )

    return argp.parse_args(arglist)


def show_visualization(states):
    console = Console()

    for state in states:
        console.clear()

        table = Table(show_header=True)
        table.add_column("Memory", justify="center", width=10)
        table.add_column("Value", justify="center", width=40)
        mem_items = sorted(state.memory.items(), key=lambda x: x[0])
        for mem_loc, mem_val in mem_items:
            row_color = "red" if f"mem[{mem_loc}]" in state.cur_inst else "white"
            table.add_row(f"[{row_color}]{mem_loc}[/{row_color}]", f"[{row_color}]{mem_val}[/{row_color}]")

        if state.cur_inst.startswith("mask"):
            inst_color = "yellow"
        else:
            inst_color = "blue"

        console.print(f"[white]Current instruction[/white]: [{inst_color}]{state.cur_inst}[/{inst_color}]")
        console.print(f"[white]Current mask[/white]: [purple]{''.join(state.cur_mask)}[/purple]")
        console.print(table)

        sleep(0.5)

        console.print(f"Memory sum: [yellow]{sum(state.memory.values())}[/yellow]")


def parse_prog_line(line):
    loc_str, val_str = line.split(" = ")
    loc = int(re.findall(loc_rgx, loc_str)[0])
    val = f"{int(val_str):036b}"

    return loc, np.array([c for c in val])


def run_prog(prog, visualize):
    states = []
    mem = {}
    cur_mask = prog[0]
    for inst in prog:
        if inst.startswith("mask"):
            cur_mask = np.array([c for c in inst.split(" = ")[1]])
        else:
            loc, val = parse_prog_line(inst)
            masked_val = val.copy()
            masked_val[cur_mask == "1"] = "1"
            masked_val[cur_mask == "0"] = "0"
            mem[loc] = int("".join(masked_val.tolist()), 2)
        if visualize:
            states.append(
                MachineState(
                    memory=deepcopy(mem),
                    inputs=deepcopy(prog),
                    cur_inst=inst,
                    cur_mask=cur_mask,
                )
            )
    return mem, states


def run_prog_floating(prog, visualize):
    states = []
    cur_mask = prog[0]
    mem = {}
    for inst in prog:
        if inst.startswith("mask"):
            cur_mask = np.array([c for c in inst.split(" = ")[1]])
        else:
            loc, val = parse_prog_line(inst)
            val = int("".join(val.tolist()), 2)
            loc = np.array([c for c in f"{loc:036b}"])
            loc[cur_mask == "X"] = "X"
            loc[cur_mask == "1"] = "1"
            indices = np.where(loc == "X")[0]
            for m in itertools.product(*(range(2) for _ in range(len(indices)))):
                new_loc = loc.copy()
                idx = np.array(m) == 1
                new_loc[indices[idx]] = "1"
                new_loc[indices[~idx]] = "0"
                new_loc = int("".join(new_loc.tolist()), 2)
                mem[new_loc] = val

        if visualize:
            states.append(
                MachineState(
                    memory=deepcopy(mem),
                    inputs=deepcopy(prog),
                    cur_inst=inst,
                    cur_mask=cur_mask,
                )
            )

    return mem, states


def main(args):
    with open(args.input, "r") as in_file:
        # Read input
        prog = [line.strip() for line in in_file]

    # Part 1
    p1_start = datetime.now()
    mem_p1, states_p1 = run_prog(prog, args.visualize and args.vis_part == 1)
    p1 = sum(mem_p1.values())
    p1_end = datetime.now()
    p1_duration = p1_end - p1_start

    print(f"{p1} in {p1_duration}")

    # Part 2
    p2_start = datetime.now()
    mem_p2, states_p2 = run_prog_floating(prog, args.visualize and args.vis_part == 2)
    p2 = sum(mem_p2.values())
    p2_end = datetime.now()
    p2_duration = p2_end - p2_start

    print(f"{p2} in {p2_duration}")

    with open(args.output, "w") as out_file:
        out_file.write(f"{p1}\n")
        out_file.write(f"{p2}\n")

    if args.visualize:
        show_visualization(states_p1 if args.vis_part == 1 else states_p2)


if __name__ == "__main__":
    args = parse_args()
    main(args)
